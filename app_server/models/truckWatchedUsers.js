var mongoose = require( 'mongoose' );

var truckWatchedUsersSchema = new mongoose.Schema({
    truck:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Truck'
    },
    watched_users: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    }],
},{
    timestamps: true
});

mongoose.model('TruckWatchedUsers', truckWatchedUsersSchema);



