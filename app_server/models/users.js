var mongoose = require( 'mongoose' );
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var config = require('../config/config');

var userSchema = new mongoose.Schema({
  first_name: {
    type: String,
    trim: true
    
  },
  username: {
    type: String
  },
  last_name: {
    type: String,
    trim: true
    
  },
  email: {
    type: String,
    lowercase: true,
    trim: true,
    required: true
  },
  company: {
    type: String,
    trim: true,
    required: false
  },
  address: {
    type: String,
    trim: true,
    required: false
  },
  phone: {
    type: String,
    default: null
  },
  phoneVerificationCode: {
    type: String,
    default: null
  },
  phoneVerified: {
    type: Boolean,
    default: false
  },
  emailVerified: {
    type: Boolean,
    default: false
  },
  avatar: {
    type: String,
    default: null
  },
  subscription_cancel_reason:{
    type: String,
  },
  subscription_type: {
    type: String,
    // default: 'free'
  },
  subscription_expire_on: {
    type: Date, 
    // default: +new Date() + config.freeDaysTrail*24*60*60*1000
  },
  mc_Number: {
     type: String,
     default: null
  },  
  state_business_license: {
     type: String,
     default: null
  },
  last_login_at:{
    type: Number,
    default: null
  },
  created_at: {
    type: Date, 
    default: Date.now
  },
  role: {
        type: String,
        default: 'broker'
  },
  equipment_type:[{
    type:String
  }],
  status: {
        type: String,
        enum: ['active', 'inactive'],
        default: 'active'
  },
  businessType: {
    type: String,
    default: null
 }, 
 mcVerified: {
  type: String,
  default: false
}, 
  agreement_doc:[{'filename':{type: String, required: true, trim: true}}],
  electronic_signature: {
    type: String,
    default: null
  },
  subscription:{
         type:Boolean,
         default:true
  },
  account_info:{
         type:Boolean,
         default:false
  },
  device_id:{
    type: String,
    default: null
  },
  stripe_id: String,
  hash: String,
  salt: String,
  notes:[{
    type:mongoose.Schema.Types.ObjectId,
    ref:'Note'  
  }],
  upload_documents:[{
    type:mongoose.Schema.Types.ObjectId,
    ref:'UserDocument'
  }],
  subscription_package:{
    type:mongoose.Schema.Types.ObjectId,
    ref:'SubscriptionPackages'
  },
  schedule:{
    type:mongoose.Schema.Types.ObjectId,
    ref:'UserStripeSubscriptionSchedule'
  },
  company_id:{
    type:mongoose.Schema.Types.ObjectId,
    ref:"Company"
  },
  parent_id:{
    type:mongoose.Schema.Types.ObjectId,
    ref:'User'
  },
  created_by:{
    type:mongoose.Schema.Types.ObjectId,
    ref:'User'
  },
  updated_by:{
    type:mongoose.Schema.Types.ObjectId,
    ref:'User'
  },
  is_company_admin:{
    type:Boolean,
    default:false
  },
  found_us:{
    type:String,
    default: 'Other'
  },
  is_internal:{
    type:Boolean,
    default: false
  },
  payment_status: {
    type:String,
    // enum: ['active','past_due','unpaid','canceled','incomplete','incomplete_expired','trialing']
  },
  subscription_canceled_on:{
    type: Date
  },
  source_data:{
    type:String,
    default:'ForkFreight'
  },
  free_subscription_package_after_cancel_paid: {
    type:mongoose.Schema.Types.ObjectId,
    ref:'SubscriptionPackages'
  },
  is_Mobile:{
    type:Boolean,
    default:false

  },
  registration_started:{
    type: Date
  },
  canceled_by:{
    type:mongoose.Schema.Types.ObjectId,
    ref:'User' 
  },
  ref_Id:{
    type:mongoose.Schema.Types.ObjectId,
    ref:'referal' 
  },
  sign_up_promotion:{
    type:String,
   default: null
  },
  coupons:[String]
},{
    timestamps: true
});

userSchema.pre("save", true, function(next, done) {
    var self = this;
    
    mongoose.models["User"].findOne({email: self.email}, function(err, user) {
        if(err) {
            done(err);
        } else if(user) {
            self.invalidate("email", "Email must be unique");
            done(new Error("Email must be unique"));
        } else {
            done();
        }
    });
    next();
});

userSchema.methods.setPassword = function(password){
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
};

userSchema.methods.validPassword = function(password) {
  if(!this.salt) return false;
  var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
  return this.hash === hash;
};

userSchema.methods.generateJwt = function() {
  var expiry = new Date();
  expiry.setDate(expiry.getDate() + 7);

  return jwt.sign({
    _id: this._id,
    email: this.email,
    name: this.name,
    role: this.role,
    iat: parseInt((new Date()).getTime() / 1000),
    exp: parseInt(expiry.getTime() / 1000),
  }, config.secretKey);
};

mongoose.model('User', userSchema);
