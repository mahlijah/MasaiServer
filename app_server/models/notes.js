var mongoose = require( 'mongoose' );

var noteSchema = new mongoose.Schema({
  comment: {
    type: String,
    trim: true,
    required: true
  },
  user: { type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  created_by: { type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  created_at: {
    type: Date, 
    default: Date.now
  },
 
});

mongoose.model('Note', noteSchema);