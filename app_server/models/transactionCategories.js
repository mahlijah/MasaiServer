var mongoose = require('mongoose');

var transactionCategoriesSchema = new mongoose.Schema({
    name: {
        type:String,
        default:'Other'
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    updated_at: {
        type: Date
    },
    transactions:[{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Transaction'
      }]
});

module.exports = mongoose.model('TransactionCategories', transactionCategoriesSchema);
