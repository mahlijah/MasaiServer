var mongoose = require( 'mongoose' );

var loadWatchedUsersSchema = new mongoose.Schema({
    userId:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    loads: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Load'
    }],
},{
    timestamps: true
});

mongoose.model('LoadWatchedUsers', loadWatchedUsersSchema);



