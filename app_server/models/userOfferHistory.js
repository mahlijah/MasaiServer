const mongoose = require('mongoose');

const userDocumentSchema = mongoose.Schema({
    user:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'User'
    },
    offer:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'Offer'
    },
    created_by:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'User'
    },
    updated_by:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'User'
    },
    created_at: {
        type: Date,
        default: Date.now
 }
},{
    timestamp:true
})

mongoose.model('UserOfferHistory',userDocumentSchema);