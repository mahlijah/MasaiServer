var mongoose = require('mongoose');
mongoose.plugin(schema => { schema.options.usePushEach = true });
var citySchema = new mongoose.Schema({

StreetAddress: {
    type: String,
    required : false
  },
  City: {
    type: String,
    required : false
  },
  State: {
    type: String,
    required: true
  },
  Zip: {
    type: String,
    required: false
  },
  ExternalId: {
    type: String,
    required: false
  },
  Source: {
    type: Number,
    required : false
  },
  TimeTaken: { 
  type : String, 
  required : false
},
UpdatedGeocoding:{
    type: String,
    required: false

  },
  Version: {
    type: String,
    required : false
  },
  ErrorMessage: {
    type: String,
    required: false
  },
  TransactionId: {
    type: String,
    required : false
  },
  naaccrQualCode: { 
      type: String, 
      required : false 
    },
    naaccrQualType: {
    type: String,
    required: false
  },
  FeatureMatchingResultType: {
    type: String,
    required: false
  },
  MatchedLocationType: {
    type: String,
    required : false
  }, 
  RegionSizeUnits: {
    type: Number,
    required: false
  },
  InterpolationType: {
    type: Number,
    required: false
  },
  RegionSize:{
    type: Number,
    required: false
  },
  InterpolationSubType:{
    type: Number,
    required : false
  },
 ready_date: {
    type: String,
    required: false
  },
  FeatureMatchingGeographyType: {
    type: String,
    required: false
  },
  MatchScore: {
    type: String,
    required: false
  },
  FeatureMatchingHierarchy: {
    type: String,
    trim: true,
    required: false
  },

  TieHandlingStrategyType: {
    type: String,
    required: false
  }, 
  FeatureMatchingResultTypeTieBreakingNotes: {
    type: String,
    required: false
  },
  GeocodeQualityType : {
    type: String,
    required: false
  },
  FeatureMatchingHierarchyNotes: {
    type: String
    ,required : false
  },
  FeatureMatchingResultCount: {
    type: String,
    required : false
  },
  Latitude: {
    type: String,
    required : false
  },
  Longitude: {
    type : String,
    required : false
  },
  created_at: {
    type: Date,
    default: Date.now 
  },
  updated_at: {
    type: Date
  },
  MatchType: {
    type: String
  }
});
mongoose.model('City', citySchema);
