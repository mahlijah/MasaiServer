var mongoose = require('mongoose');
mongoose.plugin(schema => { schema.options.usePushEach = true });
var loadHistorySchema = new mongoose.Schema({
    
  loadId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Load'
      },
  user_type: {
    type: String,
  },
  load_status: {
    type: String,
    default: null
  },

  pickup_addresses: [{
    type: String,
    required: true
  }] ,
  pickup_cities: [{
    type: String,
    required: true
  }],
  pickup_states: [{
    type: String,
    required: true
  }],
  pickup_zips: [{
    type: Number
  }],
  pickup_lat_long: { 'type': {type: String, enum: "MultiPoint", default: "MultiPoint"}, coordinates: { type: [], default: [[0,0]]} },
  delivery_addresses: [{
    type: String,
    required: true

  }],
  delivery_cities: [{
    type: String
  }],
  delivery_states: [{
    type: String,
    required: true
  }],
  delivery_zips: [{
    type: Number
  }],
  delivery_lat_long: { 'type': {type: String, enum: "MultiPoint", default: "MultiPoint"}, coordinates: { type: [],  default: [[0,0]] } },
  equipment_type: {
    type: String,
    required: true
  },
  no_of_count: {
    type: Number,
    required: true
  },
  target_rate: {
    type: String
  }, 

  
  load_weight: {
    type: Number,
    required: true
  },
  load_length: {
    type: Number,
    required: true
  },
  tracking_number:{
    type: Number,
    required: false
  },
   distance:{
    type: Number,
    default: 0
  },
  // unit_of_measurement: {
  //   type: String,
  //   required: true
  // },

  ready_date: {
    type: String,
    required: false
  },
  pickup_date: {
    type: String,
    required: false
  },
  delivery_date: {
    type: String,
    required: false
  },
  description: {
    type: String,
    trim: true,
    required: false
  },

  post_to: {
    type: String,
    required: false
  }, 
  datAssetId: {
    type: String,
    required: false
  },
  postersReferenceId : {
    type: String,
    required: false
  },
  // phone: {
  //   type: String
  //   //required: true
  // },
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  accepted_bid_user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  watched_users: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }],
  bids: [{
    amount: Number,
    description: String,
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    },
    created_at: {
      type: Date,
      default: Date.now
    }
  }],
  // images: {
  //   type: [String]
  // },
  // safe_image: {
  //   type: String,
  //   default: null
  // },

  created_at: {
    type: Date,
    default: Date.now 
  },
  updated_at: {
    type: Date
  },
  is_opened: {
    type: Boolean,
    default: true
  },
  agreement_signed: {
    type: Boolean,
    default: false
  },
  shipper_signed_agreement: {
    type: String,
    default: null
  },
  carrier_signed_signature: {
    type: String,
    default: null
  },
  expire_on: {
    type: Date,
    default: +new Date() + 30 * 7 * 60 * 60 * 1000 // 7 days
  },
  factor_load: {
    type: Boolean,
    default: false
  },
  internal: {
    type: Boolean,
    default: true
  },
  source: {
    type: String,
    default: "ForkFreight"
  }
}, {
    timestamps: true
  });
  loadHistorySchema.index({delivery_lat_long:'2dsphere', pickup_lat_long:'2dsphere'});

mongoose.model('LoadHistory', loadHistorySchema);
