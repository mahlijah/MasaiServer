const mongoose = require('mongoose');

var loadsByCsvUpload = mongoose.Schema({
    s3_key:{
        type: String
    },
    file_location:{
        type: String
    },
    original_name:{
        type: String
    },
    mimetype:{
        type: String
    },
    status:{
        type: String,
        enum:['Scheduled','Processing','Failed','Success'],
        default:'Scheduled'
    },
    total_rows:{
        type: Number
    },
    success:{
        type:Number
    },
    failed:[
       {type: mongoose.Schema.Types.Mixed}
    ],
    user:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'User'
    }
},{
    timestamps:true
})

mongoose.model("LoadsByCsvUpload",loadsByCsvUpload);