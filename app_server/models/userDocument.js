const mongoose = require('mongoose');
const config=require('../config/config')

const userDocumentSchema = mongoose.Schema({
    user_id:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'User'
    },
    company:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'Company'
    },
    transaction_id:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'Transaction'
    },
    document_path:{
        type: String,
       
    },
    document_type:{
        type: String,
        enum:[...config.ReciptEnums,...config.CompanyEnums],
        default: 'Other Documents'
    },
    
    s3_key:{
        type: String
    },
    originalname:{
        type: String
    },
    mimetype:{
        type: String
    },
    is_delete:{
        type: Boolean,
        default: false
    },
    expire_at:{
        type: Date,
        default: null
    },
    created_at:{
        type:Date,
        default: Date.now
    }
})

mongoose.model('UserDocument',userDocumentSchema);