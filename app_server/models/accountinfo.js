var mongoose = require( 'mongoose' );
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var config = require('../config/config');
var accountinfoSchema = new mongoose.Schema({
    user_id: {
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'User'
  },
  account_holder_name: {
    type: String,
    trim: true,
    required: true
  },
  account_number: {
    type: String,
    trim: true,
    required: true
  },
  ifsc_code: {
    type: String,
    default: 'free'
  },
  created_at: {
    type: Date, 
    default: Date.now
  },
  hash: String,
  salt: String
},{
    timestamps: true
});

mongoose.model('AccountInfo', accountinfoSchema);