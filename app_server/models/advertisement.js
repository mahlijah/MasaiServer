var mongoose = require( 'mongoose' );
var config = require('../config/config');

var AdvertisementSchema = new mongoose.Schema({
  title: {
    type: String,
    trim: true,
    required: true
  },
  description:{
    type: String,
  },
  file_url:{
    type:String,
    require:true
  },
  is_active:{
    type:Boolean,
    default:true
  },
  is_delete:{
    type:Boolean,
    default:false
  },
  originalname:{
      type: String
  },
  s3_key:{
      type: String
  },
  mimetype:{
      type: String
  },
  expire_at:{
    type: Date, 
    default: +new Date() + config.advertisement_expire_after*24*60*60*1000
  },
  external_link:{
    type:String,
  },
  action:{
    type: String
  },
  type:{
    type: String
  },
  value:{
    type: String
  },
  start_date:{
    type: Date,
  }
},{
  timestamps:true
});

mongoose.model('Advertisement', AdvertisementSchema);