let mongoose = require( 'mongoose' );

let config = require('../config/config');
let influencerSchema = new mongoose.Schema({
  name: {
    type: String,
    trim: true,
    required: true
  },
  phone: {
    type: String,
  },
  email: { 
    type: String,
  },
  created_by:{
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'User'
},
is_deleted:{
  type: Boolean,
  default: false
},
  created_at: {
    type: Date, 
    default: Date.now
  },
updated_at:{
    type: Date,
    default: Date.now
}
},{
    timestamps: true
});

mongoose.model('Influencer', influencerSchema);