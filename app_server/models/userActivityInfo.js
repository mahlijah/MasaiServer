const mongoose = require("mongoose");

const activityinfoSchema = new mongoose.Schema(
  {
    user_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User"
    },
    log_date: {
      type: Date,
      default: Date.now
    },
    events: [
      {
        event_type: { type: String },
        event_method:{type: String},
        db_model: {type: String},
        event_message:{type: String},
        created_at: { type: Date, default: Date.now },
        request_details: { type: mongoose.Schema.Types.Mixed }
      },
    ],
    admin_events: [
      {
        event_type: { type: String },
        event_method:{type: String},
        db_model: {type: String},
        event_message:{type: String},
        created_at: { type: Date, default: Date.now },
        request_details: { type: mongoose.Schema.Types.Mixed }
      },
    ],
    created_by: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User"
    },
  },
  {
    timestamps: true,
  }
);

mongoose.model("UserActivityInfo", activityinfoSchema);
