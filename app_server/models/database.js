var mongoose = require('mongoose');
const common = require('../helpers/common');
var gracefulShutdown;
var dbURI =process.env.DBConnection || 'mongodb+srv://mikedev:ut7brrcMl5UaCAjq@development.sa6q4.mongodb.net/ForkFreightProd?authSource=admin&replicaSet=atlas-95rgp4-shard-0&w=majority&readPreference=primary&appname=MongoDB%20Compass&retryWrites=true&ssl=true';
// var dbURI =process.env.DBConnection || 'mongodb+srv://forkfreighttest:TotalRecall1998@truckmyload-8gnrn.mongodb.net/truckmyload?retryWrites=true&w=majority';
//var dbURI = process.env.DBConnection || 'mongodb+srv://mikeuser:TotalRecall1998@ForkFreightProd.dldsw.mongodb.net/ForkFreightProd?retryWrites=true&w=majority';
const dbOptions = {
  //useMongoClient: true,
  //autoIndex: false, // Don't build indexes
  poolSize: 5, // Maintain up to 10 socket connections
  // If not connected, return errors immediately rather than waiting for reconnect
  bufferMaxEntries: 0,
  useUnifiedTopology: true,
  useNewUrlParser: true 
};

mongoose.connect(dbURI, dbOptions);

// CONNECTION EVENTS
mongoose.connection.on('connected', function() {
  common.log('Mongoose connected to database ');//, dbURI );
 
});
mongoose.connection.on('error', function(err) {
  common.log('Mongoose connection error: ' + err);
});
mongoose.connection.on('disconnected', function() {
  common.log('Mongoose disconnected');
});

// CAPTURE APP TERMINATION / RESTART EVENTS
// To be called when process is restarted or terminated
gracefulShutdown = function(msg, callback) {
  mongoose.connection.close(function() {
    common.log('Mongoose disconnected through ' + msg);
    callback();
  });
};
// For nodemon restarts
process.once('SIGUSR2', function() {
  gracefulShutdown('nodemon restart', function() {
    process.kill(process.pid, 'SIGUSR2');
  });
});
// For app termination
process.on('SIGINT', function() {
  gracefulShutdown('app termination', function() {
    process.exit(0);
  });
});
// For Heroku app termination
process.on('SIGTERM', function() {
  gracefulShutdown('Heroku app termination', function() {
    process.exit(0);
  });
});

// BRING IN YOUR SCHEMAS & MODELS
require('./users');
require('./state');

require('./loads');
require('./trucks');
require('./price');
require('./datUser');
require('./accountinfo');
require('./transaction');
require('./postEverywhereLoad');
require('./factorinngRequest');
require('./notification');
require('./city');
require('./loadHistory');
require('./searchObject');
require('./messages');
require('./notes');
require('./resetpassword');
require('./userDocument');
require('./temporaryUsers');
require('./transactionCategories');
require('./datCompany');
require('./truckWatchedUsers');
require('./LoadWatchedUsers');
require('./smsAlert');
require('./siteModule');
require('./subscriptionPackages');
require('./company');
require('./advertisement');
require('./userStripeDetail');
require('./emailTemplate');
require('./emailType');
require('./smsType');
require('./smsTemplates');
require('./loadsByCsvUpload');
require('./trucksByCsvUpload');
require('./role');
require('./demo');
require('./userActivityInfo');
require('./tempNotes');
require('./userStripeScheduleSubscription');
require('./removeableloads');
require('./offer');
require('./userOfferHistory');
require('./advertisementClick');
require('./companyTransCreditDetails');
require('./referal');
require('./influencer');