var mongoose = require( 'mongoose' );
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var config = require('../config/config');

var datUserSchema = new mongoose.Schema({
  loginId: {
    type: String,
    trim: true,
    required: true
  },
  password: {
    type: String,
    trim: true,
    required: true
  },
  primaryToken: {
    type: String,
    trim: true,
    required: true
  },
  secondaryToken: {
    type: String,
    trim: true,
    required: true
  },
  tokenexpiration: {
    type: Date,
    trim: true,
    required: true
  },
  environment: {
    type: String,
    default: 'test'
  },
  schemaUrl: {
    type: String,
    default: 'http://cnx.test.dat.com:9280/wsdl/TfmiFreightMatching.wsdl'
  },
  created_at: {
    type: Date, 
    default: Date.now
  },
  role: {
        type: String,
        enum: ['search', 'post'],
        default: 'search'
  }
},{
    timestamps: true
});


mongoose.model('DatUser', datUserSchema);