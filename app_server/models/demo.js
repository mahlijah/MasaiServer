const mongoose = require('mongoose');

const DemoSchema = mongoose.Schema({
    page_name:{
        type: String,
        required:true
    },
    video_popup_title:{
        type: String
       
    },
    video_title :{
        type: String
    },
    video_link:{
        type: String
    },
    video_description :{
        type: String

    },
    video_instructions :[{
        type: String
        
    }],
    
    
},{
    timestamps: true
})

mongoose.model('Demo',DemoSchema);