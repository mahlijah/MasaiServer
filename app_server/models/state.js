var mongoose = require( 'mongoose' );
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var config = require('../config/config');

var stateSchema = new mongoose.Schema({
  name: {
    type: String,
    trim: true,
    required: true
  },
  abbreviation: {
    type: String,
    trim: true,
    required: true
  },
  truck: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Truck'}],
  load: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Load'}],
  created_at: {
    type: Date, 
    default: Date.now
  },
 
});

mongoose.model('State', stateSchema);