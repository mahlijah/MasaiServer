var mongoose = require( 'mongoose' );
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var config = require('../config/config');

var messageSchema = new mongoose.Schema({
  first_name: {
    type: String,
    trim: true,
    required: true
  },
  userid: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: false
  },
  last_name: {
    type: String,
    trim: true,
    required: true
  },
  email: {
    type: String,
    lowercase: true,
    trim: true,
    required: true
  },
  company: {
    type: String,
    trim: true,
    required: false
  },
  address: {
    type: String,
    trim: true,
    required: false
  },
  phone: {
    type: String,
    default: null
  },
  content : {
    type:String,
    required : true
  },
  created_at: {
    type: Date, 
    default: Date.now
  },
  status: {
        type: String,
        enum: ['Open', 'Closed', 'Resolved'],
        default: 'Open'
  }
});

mongoose.model('Message', messageSchema);