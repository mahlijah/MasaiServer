const mongoose = require('mongoose');
const config = require('../config/config');


var emailTemplateSchema = new mongoose.Schema({
    subject: { type: String },
    body: { type: String },
    type: { 
        type: String,
        required: true
    },
    
},{ timestamps:true });


mongoose.model('EmailTemplate', emailTemplateSchema);