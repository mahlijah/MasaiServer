var mongoose = require( 'mongoose' );

var priceSchema = new mongoose.Schema({
   subscription_price: {
    type: Number, 
    default: 2999
  }
});

module.exports=mongoose.model('Price', priceSchema);
