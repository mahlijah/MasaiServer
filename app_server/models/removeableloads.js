const mongoose = require( 'mongoose' );
const removeableloads = new mongoose.Schema({
  tracking_number: {
    type: Number,
    required: true
  },
  created_by: {
  type: mongoose.Schema.Types.ObjectId, ref: 'User'
  },
  created_at: {
    type: Date, 
    default: Date.now
  },
},{
    timestamps: true
});
mongoose.model('removeableloads', removeableloads);