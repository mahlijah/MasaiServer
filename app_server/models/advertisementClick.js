var mongoose = require( 'mongoose' );

var advertisementClickSchema = new mongoose.Schema({
    user:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'User'
    },    
    advertisement:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'Advertisement'
    },
    ip:{
        type:String
    },
    lookup:{
        type: mongoose.Schema.Types.Mixed
    }
},{
  timestamps:true
});

mongoose.model('AdvertisementClick', advertisementClickSchema);