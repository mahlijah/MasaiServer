var mongoose = require( 'mongoose' );
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var config = require('../config/config');
var postEverywhereLoadSchema = new mongoose.Schema({
    UserName: {
    type: String
  },
  Password: {
    type: String,
    trim: true,
    required: true
  },
  ContactName: {
    type: String,
    trim: true,
    required: true
  },
  ContactPhone: {
    type: String
  },
  ContactFax: {
    type: String
  },
  CompanyName: {
    type: String
  },
  UserID: {
    type: Number
  },
  TrackingNumber: {
    type: String
  },
  OriginCity: {
    type: String
  },
  OriginState: {
    type: String
  },
  PickupDate: {
    type: Date, 
    default: Date.now
  },
  DestinationCity: {
    type: String
  },
  DestinationState: {
    type: String
  },
  DeliveryDate: {
    type: Date, 
    default: Date.now
  },
  Equipment: {
    type: String
  },
  LoadSize: {
    type: Number, 
    default: 0.00
  },
  Lenght: {
    type: Number, 
    default: 0.00
  },
  Weight: {
    type: Number, 
    default: 0.00
  },
  LoadCount: {
    type: Number, 
    default: 0.00
  },
  Stops: {
    type: Number, 
    default: 0.00
  },
  Distance: {
    type: Number, 
    default: 0.00
  },
  Rate: {
    type: Number, 
    default: 0.00
  },
  Comment: {
    type: String
  },
  hash: String,
  salt: String
},{
    timestamps: true
});

mongoose.model('PostEverywhereLoad', postEverywhereLoadSchema);