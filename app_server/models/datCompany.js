var mongoose = require('mongoose');

var datCompanySchema = new mongoose.Schema({
    company: { type: String },
    companyUrl: { type: String },
    contactName: { type: String },
    contactPhone: { type: String },
    email: { type: String },
    officeId: { type: Number },
    officeLocation: { type: String },
    role: { type:String }
}, {
    timestamps: true
});


mongoose.model('DatCompany', datCompanySchema);