var mongoose = require('mongoose');

var CompanySchema = new mongoose.Schema({
    company_name: { type: String },
    address: { type: String },
    city: { type: String },
    state: { type: String },
    email: { type: String },
    phone: { type: String },
    mc_number: { type: String },
    upload_documents:[{
      type:mongoose.Schema.Types.ObjectId,
      ref:'UserDocument'
    }],
}, {
    timestamps: true
});


mongoose.model('Company', CompanySchema);