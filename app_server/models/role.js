const mongoose = require('mongoose');

let roleScheme = mongoose.Schema({
    name:{
        type:String,
        required:true
    },
    is_public:{
        type:Boolean,
        default:true
    },
    is_deleted:{
        type:Boolean,
        default:false
    }
},{
    timestamp:true
})

mongoose.model('Role',roleScheme);