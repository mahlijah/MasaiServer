const mongoose = require('mongoose');
const config = require('../config/config');

const subscriptionPackagesSchema = mongoose.Schema({
    price:{
        type: Number,
        required: false
    },
    subscription_name:{
        type:String
    },
    stripe_plan_id:{
        type:String,
    },
    stripe_price_id:{
        type:String,
    },
    role:{
        type:String
    },
    module_access:[{
        type:mongoose.Schema.Types.ObjectId,
        ref:'SiteModule'
    }],
    is_delete:{
        type:Boolean,
        default:false
    },
    active:{
        type:Boolean,
        default:true
    },
    with_carrier:{
        type:Boolean,
        default:false
    },
    order:{
        type:Number
    },
    is_free:{
        type:Boolean,
        default:false
    },
    is_private:{
        type:Boolean,
        default:false
    }
},{
    timestamps:true 
})

mongoose.model('SubscriptionPackages',subscriptionPackagesSchema);