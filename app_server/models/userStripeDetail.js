const mongoose = require('mongoose');

var userStripeSchema = new mongoose.Schema({
    subscription: {
      type: mongoose.Schema.Types.Mixed
    },
    Customer: {
        type: mongoose.Schema.Types.Mixed
    },
    paymentMethod: {
        type: mongoose.Schema.Types.Mixed
    },
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    created_by:{ type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    updated_by:{ type: mongoose.Schema.Types.ObjectId, ref: 'User'}
  },{
      timestamps:true
  });
  
  mongoose.model('UserStripeDetail', userStripeSchema);