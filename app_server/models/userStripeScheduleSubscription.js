const mongoose = require('mongoose');

const userStripeScheduleSubscriptionSchema = new mongoose.Schema({
    user:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'User'
    },
    subscription_package:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'SubscriptionPackages'
    },
    role:{
        type: String
    },
    execute_on:{
        type: Date
    },
    stripe_schedule_id:{
        type: String
    },
    is_delete:{
        type:Boolean,
        default:false
    }
},{
    timestamp:true
})

mongoose.model('UserStripeSubscriptionSchedule',userStripeScheduleSubscriptionSchema);