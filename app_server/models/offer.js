const mongoose = require('mongoose');

const offerSchema = new mongoose.Schema({
    start_date:{
        type: Date
    },
    end_date:{
        type:Date
    },
    title:{
        type:String
    },
    offer_on:[{
        type:mongoose.Schema.Types.ObjectId,
        ref:'SubscriptionPackages'
    }],
    offer_type:{
        type:String
    },
    offer_value:{
        type:String
    },
    offer_upto:{
        type:String
    },
    offer_on_pay:{
        type:String
    },
    is_delete:{
        type:Boolean,
        default:false
    }
},{
    timestamps:true
})

mongoose.model("Offer", offerSchema);