var mongoose = require( 'mongoose' );

var transactionSchema = new mongoose.Schema({
   amount: {
    type: Number, 
    default: 0.00, 
    required : true
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  transactionType : {
      type: String
  },
  transactionDate : {
    type: String,
    default : Date.now
  },
  description : {
      type : String, 
      required : true
  },
   created_at: {
    type: Date,
    default: Date.now 
  },
  updated_at: {
    type: Date
  },
  isDeleted: {
   type: Boolean,
   default: false 
 },
 dueDate : {
   type: String,
   default : Date.now
 },
 isPaid: {
    type: Boolean,
    default: true 
  },
 transaction_category_id:{
  type: mongoose.Schema.Types.ObjectId,
  ref:'TransactionCategories'
 },
 upload_documents:[{
  type:mongoose.Schema.Types.ObjectId,
  ref:'UserDocument'
 }]


});

module.exports=mongoose.model('Transaction', transactionSchema);
