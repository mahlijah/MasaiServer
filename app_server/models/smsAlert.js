var mongoose = require('mongoose');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var config = require('../config/config');

var smsAlertSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId, ref: 'User'
    },
    searchObjectId: {
        type: mongoose.Schema.Types.ObjectId, ref: 'SearchObject'
    },
    alert: {
        type: Boolean,
        default: false
    },
    emailAlert:{
        type: Boolean,
        default: false
    }
    
});

module.exports = mongoose.model('SmsAlert', smsAlertSchema);