var mongoose = require( 'mongoose' );

var truckSchema = new mongoose.Schema({
  
  truck_status: {
    type: String,
    default: null
  },
  origin_bycity_address: {
    type: String
    //required: true
  },
  origin_bycity_city: {
    type: String
    // required: true
  },
  origin_bycity_state: {
    type: String
    // required: true
  },  
  origin_bycity_zip: {
    type: Number
    // required: true
  },
  origin_bycity_lat_long: { 'type': {type: String, enum: "Point", default: "Point"}, coordinates: { type: [] } },

  origin_bycity_range: {
    type: String
    // required: true
  },

  // destination_type: {
  //   type: String,
  //   required: true
  // },
  destination_bycity_address: {
    type: String
    // required: false
  },
  destination_bycity_city: {
    type: String
    // required: false
  },
  destination_bycity_state: {
    type: String
    // required: false
  },  
  destination_bycity_zip: {
    type: Number
    // required: false
  },
  destination_bycity_lat_long: { 'type': {type: String, enum: "Point", default: "Point"}, coordinates: { type: [] } },
  destination_bycity_range: {
    type: String
    // required: true
  },


  origin_bystate_states: [{
    type: String
    // required: true
  }],
  destination_bystate_states: [{
    type: String
    // required: true
  }],

  
  equipment_type: [{
    type: String
    // required: true
  }],

  // custom added by me 
  width: {
    type: Number,
  //  required: true
  },  
  // custom added by me end

  weight: {
    type: Number
    //required: true
  },
  length: {
    type: Number,
    required: false
  },
  tracking_number:{
    type: Number,
    required: false
  },
  rate_per_mile: {
    type: Number
    //required: true
  },

  ready_date: {
    type: Date,
    required: false
  },
  ready_end:{
    type: Date
  },
  // ready_start: {
  //   type: String,
  //   required: true
  // },
  // ready_end: {
  //   type: String,
  //   required: false
  // },

  // dm_height: {
  //   type: Number,
  //   required: true
  // },
  
  // dm_volume: {
  //   type: Number,
  //   required: true
  // },
  // name: {
  //   type: String,
  //   trim: true,
  //   required: true
  // },
  description: {
    type: String,
    trim: true,
    required: false
  }, 
  datAssetId: {
    type: String,
    required: false
  },
  postersReferenceId : {
    type: String,
    required: false
  },

  updated_at: {
    type: Date
  },
  // is_ltl: {
  //   type: Boolean,
  //   required: true
  // },
  // sp_count: {
  //   type: Number,
  //   required: true
  // },
  // no_of_stops: {
  //   type: Number,
  //   required: true
  // },
  // availability: {
  //   type: String,
  //   required: true
  // },  
  
  // phone: {
  //    type: String,
  //    required: true
  // },   
  company: {
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'DatCompany'
  },
  created_by: {
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'User'
  },
  watched_users: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }],
  // images: {
  //    type: [String]   
  // },
  // safe_image: {
  //   type: String,
  //   default: null
  // }, 
  is_opened: {
    type: Boolean,
    default: true
  },
  created_at: {
    type: Date, 
    default: Date.now
  },
  internal: {
    type: Boolean,
    default: true
  },
  source: {
    type: String,
    default: "ForkFreight"
  },
  contact:{
    type:String
  },
  amount_of_load:{
    type:String,
    enum:{ 
      values:['P','F'],
      message: 'F/P value {VALUE} is not supported. Should be either F or P'
    }
  }
},{
    timestamps: true
});
truckSchema.index({destination_bycity_lat_long:'2dsphere',origin_bycity_lat_long:'2dsphere'});
mongoose.model('Truck', truckSchema);
