var mongoose = require( 'mongoose' );
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var config = require('../config/config');

var searchObjectSchema = new mongoose.Schema({
  searchParams: {
    type: Object,
    required: false
  },
  users:[{
      type: mongoose.Schema.Types.ObjectId,
        ref: 'User' }
    ],  
   alert:{
     type:Boolean,
     required:false,
  },   
  created_at: {
    type: Date, 
    default: Date.now
  },
});

module.exports=mongoose.model('SearchObject', searchObjectSchema);