let mongoose = require( 'mongoose' );
let config = require('../config/config');
let referalSchema = new mongoose.Schema({
  referralLink: {
    type: String,
    unique: true
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'User'
  },
  influencerId:{
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'Influencer'
  },
  is_promo:{
    type: Boolean,
    default: false
  },
  promo_code:{
    type: String
  },
 promo_details:[
   {
    code: { type: String },
    promo_id:{type: String},
    role:{type: String},
    created_at: { type: Date, default: Date.now }
 }
 ],
  is_delete:{
    type: Boolean,
    default: false
  },
  created_by:{
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'User'
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt:{
    type: Date,
    default: Date.now
  }
},{
    timestamps: true
});

mongoose.model('referal', referalSchema);