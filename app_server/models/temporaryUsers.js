const mongoose= require('mongoose');
const config = require('../config/config');

const temporaryUsersSchema = new mongoose.Schema({
    first_name:{
        type: String,
        required:false
    },
    last_name:{
        type:String,
        required:false
    },
    email:{
        type:String,
        required:true
    },
    address: {
      type: String,
      trim: true,
      required: false
    },
    phone:{
        type:String,
        required:false,
    },
    role: {
        type: String,
        enum: config.Roles,
        default: 'broker'
    },
    password:{
      type:String,
      default: null
    },
    company: {
      type: String,
      trim: true,
      default: null
    },
    company_id:{
      type:mongoose.Schema.Types.ObjectId,
      ref:"Company"
    },
    mc_Number: {
       type: String,
       default: null
    },
    is_company_admin:{
      type:Boolean,
      default:false
    },  
    created_at: {
      type: Date, 
      default: Date.now
    }, 
    stripe_id: String,
    payment_status: String,
    subscription_package:{
      type:mongoose.Schema.Types.ObjectId,
      ref:'SubscriptionPackages'
    },
    subscription_type: {
      type: String,
      default: 'free'
    },
    subscription_expire_on: {
      type: Date, 
      default: +new Date() + 30*24*60*60*1000
    },
    subscription:{
           type:Boolean,
           default:true
    },
    notes:[{
      type:mongoose.Schema.Types.ObjectId,
      ref:'Note'  
    }],
    equipment_type:[{
      type:String
    }],
    ref_Id:{
      type:mongoose.Schema.Types.ObjectId,
      ref:'referal'
    }
})

mongoose.model("TemporaryUsers",temporaryUsersSchema);