var mongoose = require( 'mongoose' );

var factoringRequestSchema = new mongoose.Schema({
  name: {
    type: String,
    trim: true,
    required: true
  },
  company: {
    type: String,
    trim: true,
    required: true
  },
  email: {
    type: String,
    trim: true,
    required: true
  },
  phone: {
    type: String,
    trim: true,
    required: true
  },
  mcNumber: {
    type: String,
    trim: true,
    required: true
  },
  message: {
    type: String,
    default: 'test'
  },
  created_at: {
    type: Date, 
    default: Date.now
  }
},{
    timestamps: true
});


mongoose.model('FactoringRequest', factoringRequestSchema);