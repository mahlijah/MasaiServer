var mongoose = require('mongoose');
mongoose.plugin(schema => { schema.options.usePushEach = true });
var notificationSchema = new mongoose.Schema({
  name: {
    type: String,
    trim: true,
    required: true
  },
   description: {
    type: String,
    trim: true,
    required: false
  },
  emails: {
    type: JSON
},
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  created_at: {
    type: Date,
    default: Date.now 
  },
  updated_at: {
    type: Date
  },
  is_active: {
    type: Boolean,
    default: true
  },
  jobOptions: {
        type: JSON,
    },
   
    // user timezone for notification to go out at right time.
    timezone: {
        type: String
    },
     repeatOn: {
        type: JSON,
        required: true
    },
    sendAt: {
        type: String,
        required: true

    },
    searchCriteria: {
        type : JSON,
        required : true
    },
    convertedSearchCriteria: {
      type : JSON,
      required : false
  }
});

mongoose.model('Notification', notificationSchema);
