const mongoose = require('mongoose');
const config = require('../config/config');


var smsTemplateSchema = new mongoose.Schema({
    
    body: { type: String },
    type: { 
        type: String,
        required: true
    },
    
},{ timestamps:true });


mongoose.model('SmsTemplate', smsTemplateSchema);