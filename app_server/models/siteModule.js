const mongoose = require('mongoose');

const siteModuleSchema = mongoose.Schema({
    name:{
        type:String
    },
    frontend_key:{
        type:String
    },
    is_delete:{
        type:Boolean,
        default: false
    },
    roles:[
        {
            type:String
        }
    ],
    company_admin:{
        type:Boolean,
        default: false
    },
    full_access:{
        type:Boolean,
        default: false
    },
    order:{
        type:Number
    }
},{timestamps:true});

mongoose.model('SiteModule',siteModuleSchema)