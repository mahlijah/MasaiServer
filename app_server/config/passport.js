var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
var User = mongoose.model('User');

passport.use(new LocalStrategy({
    usernameField: 'email'
  },
  function(username, password, done) {
    User.findOne({ email: username }, function (err, user) {
      if (err) { return done(err); }
      if (!user) {
        return done(null, false, {code: 424, message: 'Invalid email. Please try another email.'});
      }      
      if (!user.validPassword(password)) {
        return done(null, false, {code: 424, message: 'Password Incorrect. Try again or click Forgot to reset'});
      }
      return done(null, user);
    });
  }
));