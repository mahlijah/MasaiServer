// Define app configuration in a single location, but pull in values from
// system environment variables (so we don't check them in to source control!)
module.exports = {
    
    // SMTP Settings
    mailHost: "smtp.sendgrid.net", //"smtpout.secureserver.net",
    mailPort: process.env.SENDGRID_PORT ||587,
    mailSecure: process.env.SENDGRID_SECURE || false,
    mailUser: "apikey",//"freightdepot@786net.com",
    mailPass: process.env.SENDGRID_API || "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",
    
    // Support email
    supportEmail : "info@forkfreight.com",
    
    // Secret Key
    secretKey: 'eypZAZy0CY^g9%KreypZAZy0CY^g9%Kr',

    freeDaysTrail : process.env.FREEDAYSTRAIL || 14,
    advertisement_expire_after: process.env.ADVERTISEMENT_EXPIRE_AFTER || 30,
    
    // Twilio Account SID - found on your dashboard
    accountSid: process.env.TWILIO_ACCOUNT_SID || 'AC4080fbd5b3cb536c866d04c6c9178a3d',

    // Twilio Auth Token - found on your dashboard
    authToken: process.env.TWILIO_AUTH_TOKEN || '41162f3733c41da4b09386c8a46c0d87',

    // A Twilio number that you have purchased through the twilio.com web
    // interface or API
    twilioNumber: process.env.TWILIO_NUMBER || +18446154477,

    // The port your web application will run on
    port: process.env.PORT || 5000,

    AZURE_STORAGE_ACCOUNT : 'loadtrucker',



    AZURE_STORAGE_ACCESS_KEY: 'ExDJDKB+qtLkEoMExCPpXR8wGE2/eB5MjCLQfegw8cdlZXBYA5The8nt/sS9RZHiPwqUo3Shbh2C2+mqYrKyIw==',

    AZURE_STORAGE_CONNECTION_STRING : 'DefaultEndpointsProtocol=[http|https];AccountName=loadtrucker;AccountKey=ExDJDKB+qtLkEoMExCPpXR8wGE2/eB5MjCLQfegw8cdlZXBYA5The8nt/sS9RZHiPwqUo3Shbh2C2+mqYrKyIw==;EndpointSuffix=core.windows.net',

    AZURE_STORAGE_CONTAINER: 'loadtruckerimages',

    AZURE_STORAGE_URL: 'https://loadtrucker.blob.core.windows.net/',
    WEb_Server_Url: 'https://loadtrucker.herokuapp.com',
    // Stripe key
    stripe_key: process.env.STRIPE_KEY || "sk_test_YdjEqiYVjXzPrbEmyeGAHS0s00qjUZoOid",
    STRIPE_PLAN: "plan_GI2WUgviwAWgw0",
    TRANSCREDIT_KEY: process.env.TRANSCREDIT_KEY || "FO6d23Gm9rpQTest",
    //DAT tokens
    // DATtokenPrimary,
    // DATtokenSecondary,
    // DATtokenExpiration
    //Google Mapping API key 
    //googleGoecoder_key: "AIzaSyC6WZGzkjs5t3wi64t-1fIkDdmRjm0wdjs",
    googleGoecoder_key:"AIzaSyAua3kea52tD0uzKOyl8SzFbW0PTguO4fw",
    maprequest_key: process.env.MAPREQUEST_KEY || "1gJVc3StkUP8Qg07NnqgGsPdvjnGUe4T",//Forkfreght
    //maprequest_key: process.env.MAPREQUEST_KEY || "xuj5N1AVqEVU9XKWefK5Fy20tFOcfEYP",//kaushal

    truckCloseAfter: process.env.TRUCK_CLOSE_AFTER || 96,
    notification: {
        enabled: process.env.JOBS_NOTIFICATION_ENABLED !== undefined ? (process.env.JOBS_NOTIFICATION_ENABLED == 'true') : true,
        redisUri: process.env.REDIS_URL || "rediss://:pc1fb525baff0c82a2b1cb7b8d92ff2bef5c9522e8888c052c2ad8278a7cb7f1f@ec2-3-222-143-186.compute-1.amazonaws.com:8379",
        monitorIntervalMinutes: process.env.JOBS_NOTIFICATION_MONITOR_INTERVAL_MINUTES || 30,
        notificationQueue: {
            name:  'ForkFreightNotifications', // Queue for managing subscriptions to send emails
            concurrency: 1
        },
        queueOptions: {
            prefix: 'jobs', // Unique redis prefix for bull to work with redis
        },
        batchSize: 100 // Batch size of subscriptions to monitor on queue
    },
    loadJobs: {
        enabled: process.env.JOBS_NOTIFICATION_ENABLED !== undefined ? (process.env.JOBS_NOTIFICATION_ENABLED == 'true') : true,
        redisUri: process.env.REDIS_URL || "rediss://:pc1fb525baff0c82a2b1cb7b8d92ff2bef5c9522e8888c052c2ad8278a7cb7f1f@ec2-3-222-143-186.compute-1.amazonaws.com:8379",
        monitorIntervalMinutes: process.env.JOBS_NOTIFICATION_MONITOR_INTERVAL_MINUTES || 30,
        newloadQueue: {
            name:  'New Loads', // Queue for managing subscriptions to send emails
            concurrency: process.env.JOBS_CONCURRENCY  || 1           
        },
        deleteloadQueue: {
            name:  'Deleted Loads', // Queue for managing subscriptions to send emails
            concurrency: process.env.JOBS_CONCURRENCY  || 1
        },
        newCsvLoadsQueue: {
            name:  'New CSV Loads', 
            concurrency: process.env.JOBS_CONCURRENCY  || 1
        },
        queueOptions: {
            prefix: 'bull', // Unique redis prefix for bull to work with redis
            removeOnComplete: true
        },
        broadcastNewloads: {
            name:  'Socket Broadcast New Loads', // Queue for managing subscriptions to send emails
            concurrency: process.env.JOBS_CONCURRENCY  || 1           
        },
        batchSize: process.env.CLEANUP_BATCH_SIZE || 100, // Batch size of subscriptions to monitor on queue,
        cleanupInterval : process.env.CLEANUP_INTERVAL ||  1800000
    },
    truckJobs: {
        enabled: process.env.JOBS_NOTIFICATION_ENABLED !== undefined ? (process.env.JOBS_NOTIFICATION_ENABLED == 'true') : true,
        redisUri: process.env.REDIS_URL || "rediss://:pc1fb525baff0c82a2b1cb7b8d92ff2bef5c9522e8888c052c2ad8278a7cb7f1f@ec2-3-222-143-186.compute-1.amazonaws.com:8379",
        monitorIntervalMinutes: process.env.JOBS_NOTIFICATION_MONITOR_INTERVAL_MINUTES || 30,
        newCsvTrucksQueue: {
            name:  'New CSV Trucks', 
            concurrency: process.env.JOBS_CONCURRENCY  || 1
        },
        queueOptions: {
            prefix: 'bull', // Unique redis prefix for bull to work with redis
            removeOnComplete: true
        },
        batchSize: process.env.CLEANUP_BATCH_SIZE || 100, // Batch size of subscriptions to monitor on queue,
        cleanupInterval : process.env.CLEANUP_INTERVAL ||  1800000
    },
    AWSAccessKeyId: process.env.AWS_ACCESS_KEY_ID || "AKIA3KNCPM6URTASRJ3M",
    AWSSecretKey: process.env.AWS_SECRET_KEY || "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",
    AWSBucketName:'forkfreight',
    Roles:['broker', 'shipper', 'carrier', 'dispatcher', 'administrator', 'telemarketing'],
    ReciptEnums:['RateConfirmationDocuments','BillofLading','FactoringInvoices','OtherDocuments','Expenses'],
    CompanyEnums:['InsuranceDocuments','MotorCarrierAuthorityDocuments','E-SignedDocuments'],
    DEBUG: process.env.DEBUG || true ,
    portalUrl:process.env.PORTAL_URL|| 'https://app.forkfreight.com',
    SALES_CRM_ACCESS_KEY: process.env.CRM_ACCESS_KEY || 'Y-NNjNW7TSSuwtS6cVF3Fw',
    defaultLoadRadius : process.env.DEFAULT_SEARCH_RADIUS || 500
};
