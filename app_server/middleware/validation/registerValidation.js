module.exports.registerStep1Validation = function(req,res,next){
    var errorList=[];
    if(!req.body.first_name) errorList.push({field:'first_name',message:'First name is required'}); 
    if(!req.body.last_name) errorList.push({field:'last_name',message:'Last name is required'}); 
    if(!req.body.email) errorList.push({field:'email',message:'email is required'}); 
    if(!req.body.phone) errorList.push({field:'phone',message:'Phone number is required'}); 
    if(!req.body.role) errorList.push({field:'role',message:'Role is required'}); 
    if(errorList.length>0) return res.status(400).send({message:'Some data is required. check error list for more detail', err: errorList});
    next();
}



module.exports.registerStep2Validation = function(req,res,next){
    var errorList=[];
    if(!req.body.id) errorList.push({field:'id',message:'temporary user id is required'}); 
    if(!req.body.company_id) errorList.push({field:'company_id',message:'Company_id is required'}); 
    if(errorList.length>0) return res.status(400).send({message:'Some data is required. check error list for more detail', err: errorList});
    next();
}

module.exports.registerStep3Validation = function(req,res,next){
    var errorList=[];
    if(!req.body.id) errorList.push({field:'id',message:'temporary user id is required'}); 
    // if(!req.body.password) errorList.push({field:'password',message:'password is required'}); 
    if(errorList.length>0) return res.status(400).send({message:'Some data is required. check error list for more detail', err: errorList});
    next();
}


module.exports.registerStep1MobileValidation = function(req,res,next){
    var errorList=[];
    if(!req.body.email) errorList.push({field:'email',message:'email is required'}); 
    if(errorList.length>0) return res.status(400).send({message:'Some data is required. check error list for more detail', err: errorList});
    next();
}

