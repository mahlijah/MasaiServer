const mongoose = require('mongoose');
const Role = mongoose.model('Role');

module.exports.duplicateRoleValidation = async function (req, res, next) {
    if (!req.body.name) {
        return res.status(400).send({ filed: 'name', message: "name is required" });
    }
    try {
        var role = await Role.findOne({name:req.body.name}).lean();
        if(role && role._id != req.params.id){
            return res.status(400).send({ status: false, message: "role already exist" }); 
        }
        next();
    }
    catch (err) {
        common.log('Error while check duplicate role validation', err);
        return res.status(400).send(err)
    }
}