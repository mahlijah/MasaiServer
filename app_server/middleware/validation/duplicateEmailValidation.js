const mongoose = require('mongoose');
const User = mongoose.model('User');
const TemporaryUsers = mongoose.model('TemporaryUsers');
const common = require('../../helpers/common');

module.exports.duplicateEmailValidation = async function (req, res, next) {
    if (!req.body.email) {
        return res.status(400).send({ filed: 'email', message: "email is required" });
    }
    try {
        var user = await User.findOne({ email: req.body.email }).exec();
        var emailExist = false;
        if (user && req.params.id) {
            if (user._id != req.params.id) emailExist = true;
        } else if (user) {
            emailExist = true;
        }
        if (emailExist) {
            return res.status(400).send({ status: false, message: "Email already exist" });
        }
        next();
    }
    catch (err) {
        common.log('Error while check email validation', err);
        return res.status(400).send(err)
    }
}

module.exports.duplicateNewRegisterEmailValidation = async function (req, res, next) {
    if (!req.body.email) {
        return res.status(400).send({ filed: 'email', message: "email is required" });
    }
    try {
        var user = await User.findOne({ email: req.body.email }).exec();
        var errorMessage= "This email is already registered. Click 'Forgot' to reset your password";
        if (user) {
            return res.status(400).send({ status: false, message: errorMessage });
        } else {
            var tempUser = await TemporaryUsers.findOne({ email: req.body.email }).populate('company_id').exec();
            if (req.body.id && tempUser) {
                if (tempUser._id != req.body.id) return res.status(400).send({ status: false, message: errorMessage, userdata: tempUser });
            } else if (tempUser) {
                return res.status(400).send({ status: false, message: errorMessage, userdata: tempUser });
            }
        }
        next();
    }
    catch (err) {
        common.log('Error while check email validation', err);
        return res.status(400).send(err)
    }
}

module.exports.duplicateNewRegisterEmailForMarketingUserValidation = async function (req, res, next) {
    if (!req.body.email) {
        return res.status(400).send({ filed: 'email', message: "email is required" });
    }
    try {
        var user = await User.findOne({ email: req.body.email }).exec();
        var errorMessage= "User is already registered, please refresh page";
        if (user) {
            return res.status(400).send({ status: false, message: errorMessage });
        } else {
            var tempUser = await TemporaryUsers.findOne({ email: req.body.email }).populate('company_id').exec();
            if (req.body.temp_id && tempUser) {
                if (tempUser._id != req.body.temp_id) return res.status(400).send({ status: false, message: errorMessage, userdata: tempUser });
            } else if (tempUser) {
                return res.status(400).send({ status: false, message: errorMessage, userdata: tempUser });
            }
        }
        next();
    }
    catch (err) {
        common.log('Error while check email validation', err);
        return res.status(400).send(err)
    }
}

module.exports.duplicateNewRegisterPhoneValidation = async function (req, res, next) {
    if (!req.body.phone) {
        return res.status(400).send({ filed: 'phone', message: "phone is required" });
    }
    try {
        var user = await User.findOne({ phone: req.body.phone }).exec();
        var errorMessage= "This phone number is already associated with another account ";
        if (user) {
            return res.status(400).send({ status: false, message: errorMessage + encryptEmail(user.email) });
        } else {
            var tempUser = await TemporaryUsers.findOne({ phone: req.body.phone }).exec();
            if (req.body.id && tempUser) {
                if (tempUser._id != req.body.id) return res.status(400).send({ status: false, message: errorMessage + encryptEmail(user.email) });
            } else if (tempUser) {
                return res.status(400).send({ status: false, message: errorMessage + encryptEmail(tempUser.email) });
            }
        }
        next();
    }
    catch (err) {
        common.log('Error while check phone validation', err);
        return res.status(400).send(err)
    }
}
var encryptEmail = function(email){
    var return_email = '';
    if(email){
        var email_split = email.split("@");
        var domain = email_split[1];
        var username = email_split[0];
        return_email += username.substring(0,1);
        var encrypt = username.substring(1);
        for (let index = 0; index < encrypt.length; index++) {
            return_email +="*";
        }
        return_email = return_email+"@"+domain;
    }
    return return_email;
}