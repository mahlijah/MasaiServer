var mongoose = require('mongoose');
var User = mongoose.model('User');
var Role = mongoose.model('Role');
var jwt = require('jsonwebtoken');
var config = require('../config/config');
const common = require('../helpers/common');

module.exports.AuthMiddleWare = function (req, res, next) {
    verifyToken(req,res,next,false);
}
module.exports.AuthAdminMiddleWare = function (req, res, next) {
    verifyToken(req,res,next,true);
}
module.exports.AuthTelemarketingAndAdminMiddleWare = function (req, res, next) {
    verifyToken(req,res,next,false,false,true);
}
module.exports.AuthCompanyAdminMiddleWare = function (req, res, next) {
    verifyToken(req,res,next,false,true);
}

function verifyToken(req,res,next,isAdministationCheck=false,isCompanyAdminCheck=false, isPrivate=false){
    const authHeader = req.headers.authorization;

    if (authHeader) {
        const token = authHeader.split(' ')[1];

        jwt.verify(token, config.secretKey, (err, user) => {
            if (err) {
                return res.sendStatus(401);
            }
            User.findById(user._id).exec(async function (err, _user) {
                if (err) {
                    console.error("Token verify get user --->", err)
                    return res.status(404).send({ message: "User not found" });
                }
                if(_user){
                    if (_user.status != "active") {
                        return res.status(401).send({ message: "UnauthorizedError: User no longer active please contact administrator" });
                    }
                    // if (!_user.subscription) {
                    //     return res.status(400).send({ message: "UnauthorizedError: Subscription inactive" });
                    // }
                    if (_user.last_login_at > user.iat) {
                        return res.status(401).send({ message: "UnauthorizedError: Login from another device" });
                    }
                    if(isAdministationCheck && user.role != 'administrator'){
                        return res.status(401).send({message: "UnauthorizedError: Administrator access only"})
                    }
                    if(isCompanyAdminCheck && !user.is_company_admin && user.role != 'telemarketing' && user.role != 'administrator'){
                        return res.status(400).send({message: "UnauthorizedError: Company Administrator access only"})
                    }
                    if(isPrivate){
                        let role = await Role.findOne({name:user.role})
                        if(role && !role.is_public){
                            req.payload = user;
                            next();
                            return;
                        }else{
                            return res.status(401).send({message: "UnauthorizedError: Private user access only"})
                        }
                    }
                    req.payload = user;
                    next();
                }else{
                    common.log("User not found in database",user);
                    return res.status(401).send({message:"UnauthorizedError: User not found related to this token. Login again"})
                }
            });
        });
    } else {
        return res.sendStatus(401);
    }
}