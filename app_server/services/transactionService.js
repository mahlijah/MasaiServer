var mongoose = require('mongoose');
var Transaction = mongoose.model('Transaction');
const { ObjectId } = require('mongodb');
const moment = require('moment');
const common = require('../helpers/common');

module.exports = {
    getIncomePaidUnpaidGraphData: async function (start_date, end_date, user_id) {
        try {
            var search_options = {};
            if(start_date){
                start_date = new Date(start_date);
                start_date.setHours(0, 0, 0);
            }
            if(end_date){
                end_date = new Date(end_date);
                end_date.setHours(23, 59, 59);
            }
            search_options["isDeleted"] = false;
            search_options["userId"] = ObjectId(user_id);
            search_options["transactionType"] = 'Income';
            if (start_date != null
                && end_date != null
            ) {
                search_options["$expr"] =
                {
                    "$and": [
                        { $gte: [{ "$dateFromString": { "dateString": "$transactionDate" } }, start_date] },
                        { $lte: [{ "$dateFromString": { "dateString": "$transactionDate" } }, end_date] }
                    ]
                }
            }
            if (start_date != null
                && end_date == null) {
                search_options["$expr"] =
                {
                    $gte: [{ "$dateFromString": { "dateString": "$transactionDate" } }, start_date]
                }
            }
            if (end_date != null
                && start_date == null) {
                search_options["$expr"] =
                {
                    $lte: [{ "$dateFromString": { "dateString": "$transactionDate" } }, end_date]
                }
            }

            var default_due_date = new Date();
            default_due_date.setDate(default_due_date.getDate() + 1);
            default_due_date = moment(default_due_date).format("yyyy-MM-D")
            var data = await Transaction.aggregate([
                {
                    $match: search_options
                }, {
                    $project: {
                        paid: {
                            $cond: {
                                if: {
                                    $ifNull: [
                                        "$isPaid", true]
                                },
                                then: "$amount",
                                else: 0
                            }
                        },
                        unpaid: {
                            $cond: {
                                if: {
                                    $eq: [
                                        "$isPaid", false
                                    ]
                                },
                                then: "$amount",
                                else: 0
                            }
                        },
                        overdue: {
                            $cond: {
                                if: { $lte: [{ "$dateFromString": { "dateString": { $ifNull: ["$dueDate", default_due_date] } } }, new Date()] },
                                then: "$amount",
                                else: 0
                            }
                        },
                    }
                }, {
                    $group: {
                        _id: null,
                        paid: { $sum: '$paid' },
                        unpaid: { $sum: '$unpaid' },
                        overdue: { $sum: '$overdue' }
                    }
                }, {
                    $project: {
                        _id: 0,
                        paid: 1,
                        unpaid: 1,
                        overdue: 1
                    }
                }
            ])

            let result = {
                paid: 0,
                unpaid: 0,
                overdue: 0
            };
            if (data.length > 0) {
                result = Object.assign({}, ...data)
            }
            return result;

        }
        catch (err) {
            common.log("Error while get income graph data", err);
            throw err;
        }
    },
    getPieChartData: async function (start_date, end_date, user_id, transactionCategoryIds) {
        try {
            if(start_date){
                start_date = new Date(start_date);
                start_date.setHours(0, 0, 0);
            }
            if(end_date){
                end_date = new Date(end_date);
                end_date.setHours(23, 59, 59);
            }
            var search_options = {};
            search_options["isDeleted"] = false;
            search_options["userId"] = ObjectId(user_id);
            search_options["$expr"] = {
                "$and": [
                    { $gte: [{ "$dateFromString": { "dateString": "$transactionDate" } }, start_date] },
                    { $lte: [{ "$dateFromString": { "dateString": "$transactionDate" } }, end_date] }
                ]
            }

            if (transactionCategoryIds && transactionCategoryIds.length > 0) {
                search_options["transaction_category_id"] = {
                    "$in": transactionCategoryIds
                }
            }
            var data = await Transaction.aggregate([
                {
                    $match: search_options
                }, {
                    $project: {
                        transaction_category_id: "$transaction_category_id",
                        expenses: {
                            $cond: {
                                if: {
                                    $regexMatch: {
                                        input: "$transactionType",
                                        regex: "^Expense.*$",
                                    }
                                },
                                then: "$amount",
                                else: 0
                            }
                        },
                        income: {
                            $cond: {
                                if: {
                                    $regexMatch: {
                                        input: "$transactionType",
                                        regex: "^Income.*$",
                                    }
                                },
                                then: "$amount",
                                else: 0
                            }
                        },
                    }
                }, {
                    $group: {
                        _id: { $toObjectId: "$transaction_category_id" },
                        expenses: { $sum: '$expenses' },
                        income: { $sum: '$income' }
                    }
                }, {
                    $lookup: {
                        from: "transactioncategories",
                        localField: '_id',
                        foreignField: '_id',
                        as: "label"
                    }
                },
                {
                    $unwind: { path: "$label", preserveNullAndEmptyArrays: true }
                }, {
                    $project: {
                        _id: 0,
                        label: { $ifNull: ['$label.name', 'Other'] },
                        expenses: '$expenses',
                        income: '$income'
                    }
                }, {
                    $sort: { label: 1 }
                }
            ])
            return data;
        }
        catch (err) {
            common.log("Error while get pie chart data", err);
            throw err;
        }
    },
    getGraphData: async function (_data, user_id) {
        try {
            var getDataType = await getGraphDataType(_data.start_date, _data.end_date);
            var data = []
            var returnDataFormatlist = await getRetrunDataFormatListByType(_data.start_date, _data.end_date, getDataType)
            switch (getDataType) {
                case "yearly":
                    returnDataFormatlist = returnDataFormatlist.map(obj => parseInt(obj));
                    data = await graphDataYearWise(user_id, _data, returnDataFormatlist)
                    break;
                case "monthly":
                    data = await graphDataMonthsWise(user_id, _data, returnDataFormatlist)
                    break;
                case "month":
                    data = await graphDataMonthWise(user_id, _data, returnDataFormatlist)
                    break;
                case "weekly":
                    data = await graphDataWeeklyWise(user_id, _data, returnDataFormatlist)
                    break;
                case "hourly":
                    data = await graphDataMonthWise(user_id, _data, returnDataFormatlist)
                    break;
            }
            return data;
        }
        catch (err) {
            common.log("Error While get load data", err);
            throw err
        }
    },
    addDecimal: function (value) {
        var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
        });
        return formatter.format(value);
    }
}

async function graphDataMonthsWise(user_id, search_object, mList) {
    var months_name = [
        "",
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec"
    ]
    try {
        var search_options = {};
        search_options["isDeleted"] = false;
        search_options["userId"] = ObjectId(user_id);
        if (search_object !== undefined) {
            search_object.start_date = new Date(search_object.start_date);
            search_object.start_date.setHours(0, 0, 0);
            search_object.end_date = new Date(search_object.end_date)
            search_object.end_date.setHours(23, 59, 59);
            if (search_object.start_date != null
                && search_object.end_date != null
            ) {
                search_options["$expr"] =
                {
                    "$and": [
                        { $gte: [{ "$dateFromString": { "dateString": "$transactionDate" } }, search_object.start_date] },
                        { $lte: [{ "$dateFromString": { "dateString": "$transactionDate" } }, search_object.end_date] }
                    ]
                }
            }
            if (search_object.start_date != null
                && search_object.end_date == null) {
                    search_object.start_date = new Date(search_object.start_date);
                    search_object.start_date.setHours(0, 0, 0);
                search_options["$expr"] =
                {
                    $gte: [{ "$dateFromString": { "dateString": "$transactionDate" } }, search_object.start_date]
                }
            }
            if (search_object.end_date != null
                && search_object.start_date == null) {
                    search_object.end_date = new Date(search_object.end_date)
                    search_object.end_date.setHours(23, 59, 59);
                search_options["$expr"] =
                {
                    $lte: [{ "$dateFromString": { "dateString": "$transactionDate" } }, search_object.end_date]
                }
            }
            if (search_object.description) {
                search_options["description"] = { $regex: '*.' + search_object.description + '.*', $options: 'i' };
            }
            if (search_object.amount) {
                search_options["amount"] = { $eq: search_object.amount };
            }
            if (search_object.transactionCategoryId) {
                search_options["transaction_category_id"] = search_object.transactionCategoryId;
            }
            if (search_object.transactionType) {
                search_options["transactionType"] = search_object.transactionType;
            }
        }

        var data = await Transaction.aggregate([
            {
                $match: search_options
            }, {
                $addFields:
                {
                    date: { "$dateFromString": { "dateString": "$transactionDate" } }
                }
            }, {
                $project: {
                    _id: 0,
                    month: {
                        $month: "$date"
                    },
                    year: {
                        $year: '$date'
                    },
                    expenses: {
                        $cond: {
                            if: {
                                $regexMatch: {
                                    input: "$transactionType",
                                    regex: "^Expense.*$",
                                }
                            },
                            then: "$amount",
                            else: 0
                        }
                    },
                    income: {
                        $cond: {
                            if: {
                                $regexMatch: {
                                    input: "$transactionType",
                                    regex: "^Income.*$",
                                }
                            },
                            then: "$amount",
                            else: 0
                        }
                    },
                }
            }, {
                $group: {
                    _id: {
                        month: '$month',
                        year: '$year'
                    },
                    expenses: { $sum: '$expenses' },
                    income: { $sum: '$income' }
                }
            }, {
                $sort: { '_id.year': 1, '_id.month': 1 }
            }, {
                $addFields: {
                    months: months_name,
                }
            }, {
                $project: {
                    label: {
                        $concat: [{
                            $arrayElemAt: [
                                "$months",
                                "$_id.month"
                            ]
                        }, ' ', { $toString: '$_id.year' }]
                    },
                    expenses: 1,
                    income: 1
                },
            }, {
                $group: {
                    _id: "",
                    data: {
                        $push: '$$ROOT'
                    }
                }
            }, {
                $addFields: {
                    data: {
                        $reduce: {
                            input: {
                                $setDifference: [
                                    mList,
                                    "$data.label"
                                ]
                            },
                            initialValue: "$data",
                            in: {
                                $concatArrays: [
                                    "$$value",
                                    [{
                                        "_id": {
                                            "month": { $indexOfArray: [months_name, { $substr: ["$$this", 0, { $indexOfBytes: ["$$this", " "] }] }] },
                                            "year": { $toInt: { $trim: { input: { $substr: ["$$this", { $indexOfBytes: ["$$this", " "] }, { $strLenCP: "$$this" }] } } } }
                                        },
                                        "expenses": 0,
                                        "income": 0,
                                        label: "$$this",
                                    }]
                                ]
                            }
                        }
                    }
                }
            },
            { $unwind: "$data" },
            { $sort: { "data._id.year": 1, "data._id.month": 1 } },
            {
                $project: {
                    "_id": 0,
                    "label": "$data.label",
                    "expenses": "$data.expenses",
                    "income": "$data.income"
                }
            }
        ]);
        return data;
    }
    catch (err) {
        common.log("Error While get load data", err);
        throw err;
    }
}
async function graphDataYearWise(user_id, search_object, yList) {
    try {
        var search_options = {};
        search_options["isDeleted"] = false;
        search_options["userId"] = ObjectId(user_id);
        if (search_object !== undefined) {
            if (search_object.start_date != null
                && search_object.end_date != null
            ) {
                search_object.start_date = new Date(search_object.start_date);
                search_object.start_date.setHours(0, 0, 0);
                search_object.end_date = new Date(search_object.end_date)
                search_object.end_date.setHours(23, 59, 59);
                search_options["$expr"] =
                {
                    "$and": [
                        { $gte: [{ "$dateFromString": { "dateString": "$transactionDate" } }, search_object.start_date] },
                        { $lte: [{ "$dateFromString": { "dateString": "$transactionDate" } }, search_object.end_date] }
                    ]
                }
            }
            if (search_object.start_date != null
                && search_object.end_date == null) {
                search_object.start_date = new Date(search_object.start_date);
                search_object.start_date.setHours(0, 0, 0);
                search_options["$expr"] =
                {
                    $gte: [{ "$dateFromString": { "dateString": "$transactionDate" } }, search_object.start_date]
                }
            }
            if (search_object.end_date != null
                && search_object.start_date == null) {
                search_object.end_date = new Date(search_object.end_date)
                search_object.end_date.setHours(23, 59, 59);
                search_options["$expr"] =
                {
                    $lte: [{ "$dateFromString": { "dateString": "$transactionDate" } }, search_object.end_date]
                }
            }
            if (search_object.description) {
                search_options["description"] = { $regex: '*.' + search_object.description + '.*', $options: 'i' };
            }
            if (search_object.amount) {
                search_options["amount"] = { $eq: search_object.amount };
            }
            if (search_object.transactionCategoryId) {
                search_options["transaction_category_id"] = search_object.transactionCategoryId;
            }
            if (search_object.transactionType) {
                search_options["transactionType"] = search_object.transactionType;
            }
        }
        var data = await Transaction.aggregate([
            {
                $match: search_options
            }, {
                $addFields:
                {
                    date: { "$dateFromString": { "dateString": "$transactionDate" } }
                }
            }, {
                $project: {
                    _id: 0,
                    year: {
                        $year: '$date'
                    },
                    expenses: {
                        $cond: {
                            if: {
                                $regexMatch: {
                                    input: "$transactionType",
                                    regex: "^Expense.*$",
                                }
                            },
                            then: "$amount",
                            else: 0
                        }
                    },
                    income: {
                        $cond: {
                            if: {
                                $regexMatch: {
                                    input: "$transactionType",
                                    regex: "^Income.*$",
                                }
                            },
                            then: "$amount",
                            else: 0
                        }
                    },
                }
            }, {
                $group: {
                    _id: '$year',
                    expenses: { $sum: '$expenses' },
                    income: { $sum: '$income' }
                }
            }, {
                $sort: { '_id': 1 }
            }, {
                $group: {
                    _id: "",
                    data: {
                        $push: {
                            label: "$_id",
                            expenses: "$expenses",
                            income: "$income"
                        }
                    }
                }
            }, {
                $addFields: {
                    data: {
                        $reduce: {
                            input: {
                                $setDifference: [
                                    yList,
                                    "$data.label"
                                ]
                            },
                            initialValue: "$data",
                            in: {
                                $concatArrays: [
                                    "$$value",
                                    [{
                                        "expenses": 0,
                                        "income": 0,
                                        label: "$$this",
                                    }]
                                ]
                            }
                        }
                    }
                }
            },
            { $unwind: "$data" },
            { $sort: { "data.label": 1 } },
            {
                $project: {
                    "_id": 0,
                    "label": "$data.label",
                    "expenses": "$data.expenses",
                    "income": "$data.income"
                }
            }
        ]);
        return data;
    }
    catch (err) {
        common.log("Error While get load data", err);
        throw err;
    }
}
async function graphDataMonthWise(user_id, search_object, List) {
    var months_name = [
        "",
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec"
    ]
    try {
        var search_options = {};
        search_options["isDeleted"] = false;
        search_options["userId"] = ObjectId(user_id);
        if (search_object !== undefined) {
            if (search_object.start_date != null
                && search_object.end_date != null
            ) {
                search_object.start_date = new Date(search_object.start_date);
                search_object.start_date.setHours(0, 0, 0);
                search_object.end_date = new Date(search_object.end_date)
                search_object.end_date.setHours(23, 59, 59);
                search_options["$expr"] =
                {
                    "$and": [
                        { $gte: [{ "$dateFromString": { "dateString": "$transactionDate" } }, search_object.start_date] },
                        { $lte: [{ "$dateFromString": { "dateString": "$transactionDate" } }, search_object.end_date] }
                    ]
                }
            }
            if (search_object.start_date != null
                && search_object.end_date == null) {
                search_object.start_date = new Date(search_object.start_date);
                search_object.start_date.setHours(0, 0, 0);
                search_options["$expr"] =
                {
                    $gte: [{ "$dateFromString": { "dateString": "$transactionDate" } }, search_object.start_date]
                }
            }
            if (search_object.end_date != null
                && search_object.start_date == null) {
                search_object.end_date = new Date(search_object.end_date)
                search_object.end_date.setHours(23, 59, 59);
                search_options["$expr"] =
                {
                    $lte: [{ "$dateFromString": { "dateString": "$transactionDate" } }, search_object.end_date]
                }
            }
            if (search_object.description) {
                search_options["description"] = { $regex: '*.' + search_object.description + '.*', $options: 'i' };
            }
            if (search_object.amount) {
                search_options["amount"] = { $eq: search_object.amount };
            }
            if (search_object.transactionCategoryId) {
                search_options["transaction_category_id"] = search_object.transactionCategoryId;
            }
            if (search_object.transactionType) {
                search_options["transactionType"] = search_object.transactionType;
            }
        }
        var data = await Transaction.aggregate([
            {
                $match: search_options
            }, {
                $addFields:
                {
                    date: { "$dateFromString": { "dateString": "$transactionDate" } }
                }
            }, {
                $project: {
                    _id: 0,
                    month: {
                        $month: '$date'
                    },
                    date: {
                        $dayOfMonth: '$date'
                    },
                    expenses: {
                        $cond: {
                            if: {
                                $regexMatch: {
                                    input: "$transactionType",
                                    regex: "^Expense.*$",
                                }
                            },
                            then: "$amount",
                            else: 0
                        }
                    },
                    income: {
                        $cond: {
                            if: {
                                $regexMatch: {
                                    input: "$transactionType",
                                    regex: "^Income.*$",
                                }
                            },
                            then: "$amount",
                            else: 0
                        }
                    },
                }
            }, {
                $group: {
                    _id: {
                        month: '$month',
                        date: '$date'
                    },
                    expenses: { $sum: '$expenses' },
                    income: { $sum: '$income' }
                }
            }, {
                $sort: { '_id.month': 1, '_id.date': 1 }
            }, {
                $addFields: {
                    months: months_name,
                }
            }, {
                $project: {
                    label: {
                        $concat: [{
                            $arrayElemAt: [
                                "$months",
                                "$_id.month"
                            ]
                        }, ' ', { $toString: '$_id.date' }]
                    },
                    expenses: 1,
                    income: 1
                },
            }, {
                $group: {
                    _id: "",
                    data: {
                        $push: '$$ROOT'
                    }
                }
            }, {
                $addFields: {
                    data: {
                        $reduce: {
                            input: {
                                $setDifference: [
                                    List,
                                    "$data.label"
                                ]
                            },
                            initialValue: "$data",
                            in: {
                                $concatArrays: [
                                    "$$value",
                                    [{
                                        "_id": {
                                            "month": { $indexOfArray: [months_name, { $substr: ["$$this", 0, { $indexOfBytes: ["$$this", " "] }] }] },
                                            "date": { $toInt: { $trim: { input: { $substr: ["$$this", { $indexOfBytes: ["$$this", " "] }, { $strLenCP: "$$this" }] } } } }
                                        },
                                        "expenses": 0,
                                        "income": 0,
                                        label: "$$this",
                                    }]
                                ]
                            }
                        }
                    }
                }
            },
            { $unwind: "$data" },
            { $sort: { "data._id.month": 1, "data._id.date": 1 } },
            {
                $project: {
                    "_id": 0,
                    "label": "$data.label",
                    "expenses": "$data.expenses",
                    "income": "$data.income"
                }
            }
        ]);
        return data;
    }
    catch (err) {
        common.log("Error While get load data", err);
        throw err;
    }
}
async function graphDataWeeklyWise(user_id, search_object, List) {
    var weekdays_name = [
        "",
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday"
    ]
    try {
        var search_options = {};
        search_options["isDeleted"] = false;
        search_options["userId"] = ObjectId(user_id);
        if (search_object !== undefined) {
            if (search_object.start_date != null
                && search_object.end_date != null
            ) {
                search_object.start_date = new Date(search_object.start_date);
                search_object.start_date.setHours(0, 0, 0);
                search_object.end_date = new Date(search_object.end_date)
                search_object.end_date.setHours(23, 59, 59);
                search_options["$expr"] =
                {
                    "$and": [
                        { $gte: [{ "$dateFromString": { "dateString": "$transactionDate" } }, search_object.start_date] },
                        { $lte: [{ "$dateFromString": { "dateString": "$transactionDate" } }, search_object.end_date] }
                    ]
                }
            }
            if (search_object.start_date != null
                && search_object.end_date == null) {
                search_object.start_date = new Date(search_object.start_date);
                search_object.start_date.setHours(0, 0, 0);
                search_options["$expr"] =
                {
                    $gte: [{ "$dateFromString": { "dateString": "$transactionDate" } }, search_object.start_date]
                }
            }
            if (search_object.end_date != null
                && search_object.start_date == null) {
                search_object.end_date = new Date(search_object.end_date)
                search_object.end_date.setHours(23, 59, 59);
                search_options["$expr"] =
                {
                    $lte: [{ "$dateFromString": { "dateString": "$transactionDate" } }, search_object.end_date]
                }
            }
            if (search_object.description) {
                search_options["description"] = { $regex: '*.' + search_object.description + '.*', $options: 'i' };
            }
            if (search_object.amount) {
                search_options["amount"] = { $eq: search_object.amount };
            }
            if (search_object.transactionCategoryId) {
                search_options["transaction_category_id"] = search_object.transactionCategoryId;
            }
            if (search_object.transactionType) {
                search_options["transactionType"] = search_object.transactionType;
            }
        }
        var data = await Transaction.aggregate([
            {
                $match: search_options
            }, {
                $project: {
                    _id: 0,
                    date: { "$dateFromString": { "dateString": "$transactionDate" } },
                    transactionDate: "$transactionDate",
                    expenses: {
                        $cond: {
                            if: {
                                $regexMatch: {
                                    input: "$transactionType",
                                    regex: "^Expense.*$",
                                }
                            },
                            then: "$amount",
                            else: 0
                        }
                    },
                    income: {
                        $cond: {
                            if: {
                                $regexMatch: {
                                    input: "$transactionType",
                                    regex: "^Income.*$",
                                }
                            },
                            then: "$amount",
                            else: 0
                        }
                    },
                }
            }, {
                $group: {
                    _id: {
                        date: '$date',
                        transactionDate: '$transactionDate'
                    },
                    expenses: { $sum: '$expenses' },
                    income: { $sum: '$income' }
                }
            }, {
                $sort: { '_id.date': 1 }
            }, {
                $project: {
                    label: '$_id.transactionDate',
                    expenses: 1,
                    income: 1
                },
            }, {
                $group: {
                    _id: "",
                    data: {
                        $push: '$$ROOT'
                    }
                }
            }, {
                $addFields: {
                    data: {
                        $reduce: {
                            input: {
                                $setDifference: [
                                    List,
                                    "$data.label"
                                ]
                            },
                            initialValue: "$data",
                            in: {
                                $concatArrays: [
                                    "$$value",
                                    [{
                                        "_id": {
                                            date: { "$dateFromString": { "dateString": "$$this" } },
                                            transactionDate: "$$this"
                                        },
                                        "expenses": 0,
                                        "income": 0,
                                        label: "$$this",
                                    }]
                                ]
                            }
                        }
                    }
                }
            },
            { $unwind: "$data" },
            { $sort: { "data._id.date": 1 } },
            {
                $addFields: {
                    weeks: weekdays_name,
                }
            },
            {
                $project: {
                    "_id": 0,
                    "label": {
                        $arrayElemAt: [
                            "$weeks",
                            {
                                $dayOfWeek: '$data._id.date'
                            }
                        ]
                    },
                    "expenses": "$data.expenses",
                    "income": "$data.income"
                }
            }
        ]);
        return data;
    }
    catch (err) {
        common.log("Error While get load data", err);
        throw err;
    }
}
async function getRetrunDataFormatListByType(start_date, end_date, type) {
    var format = '';
    var dateIncrementBy = '';
    switch (type) {
        case "yearly":
            format = 'yyyy';
            dateIncrementBy = 'y';
            break;
        case "monthly":
            format = 'MMM yyyy';
            dateIncrementBy = 'M';
            break;
        case "month":
            format = 'MMM D';
            dateIncrementBy = 'd';
            break;
        case "weekly":
            format = 'YYYY-MM-DD';
            dateIncrementBy = 'd';
            break;
        case "hourly":
            format = 'MMM D';
            dateIncrementBy = 'h';
            break;
    }
    return await getList(start_date, end_date, format, dateIncrementBy);

}
async function getGraphDataType(start_date, end_date) {
    try {
        start_date = moment(start_date).startOf('day');
        end_date = moment(end_date).endOf('day');
        if (end_date.diff(start_date, 'months', true) > 12) {
            return "yearly";
        }
        else if (end_date.diff(start_date, 'months', true) > 1) {
            return "monthly";
        } else if (end_date.diff(start_date, 'days', true) >= 7) {
            return "month";
        } else if (end_date.diff(start_date, 'hours', true) > 24) {
            return "weekly";
        } else if (end_date.diff(start_date, 'hours', true) <= 24) {
            return "hourly";
        }
        return '';
    }
    catch (err) {
        common.log("error getGraphDataType", { err, start_date, end_date })
        return '';
    }
}
async function getList(start_date, end_date, format, increse) {
    try {
        start_date = moment(start_date).startOf('day');
        end_date = moment(end_date).endOf('day');
        var last_month = end_date.format(format);
        var processed = "";
        var list = [];
        while (processed != last_month) {
            processed = start_date.format(format);
            list.push(processed);
            start_date.add(1, increse)
        }
        return list;
    }
    catch (error) {
        common.log("Error while getList:-", error)
        throw error;
    }
}