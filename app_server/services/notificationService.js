/*
    Notification service handles all operations between report notifications and the queue which holds all active notifications
 */

var mongoose = require('mongoose');
var Notification = mongoose.model('Notification');
var Load = mongoose.model('Load');
var loadCtrl = require('../controllers/loads');
var User = mongoose.model('User');
const moment = require('moment');
var common = require('../helpers/common');
const rp = require('request-promise');
const Queue = require('bull');

var queueName;
var notificationQueue;

var initialized = false;
var reportDateStringFormat = 'YYYY-MM-DD';
var config = require('../config/config');


module.exports = {
    init: function init() {
        if (!initialized) {
            
            queueName = process.env.SUBCRIPTION_NAME || config.notification.notificationQueue.name; 
            notificationQueue = new Queue(queueName, config.notification.redisUri, config.notification.queueOptions);
 
            // Set up handler for processing notification jobs
            //notificationQueue.process(config.notification.notificationQueue.concurrency, processNotificationJob);
            
            initialized = true;
        }
    },
    
    createJob: function createJob(notification) {
        if (!notificationQueue) { return; } // Queue is not initialized
        
        let schedule = getCronString(notification.sendAt, notification.repeatOn);
        let timezone = notification.timezone ? notification.timezone : moment.tz.guess(); // this is handle old clients without timezone.
        return getNotificationJobDataFromNotification(notification).then(notificationJobData => {
            if(notificationJobData != null)
            {
                let jobOptions = {
                    jobId: notificationJobData.id,
                    repeat: {
                        cron: schedule,
                        tz:timezone
                    },
                    removeOnComplete: true,
                    removeOnFail: false
                };
                
                return notificationQueue.add(notificationJobData, jobOptions).then(job => {
                    // If job is null, it already exists
                    common.log('Added job ' + notificationJobData.id);
                    return job;
                });
            }

        });
    },
    
    updateJob: function updateJob(oldNotification, newNotification) {
        if (!notificationQueue) { return; } // Queue is not initialized

        return this.removeJob(oldNotification).then(() => {
            return this.createJob(newNotification);
        });
    },
    
    removeJob: function removeJob(notification) {
        if (!notificationQueue) { return Promise.reject('Queue is not initialized in removeJob'); }
        if (!notification) { return Promise.reject('No notification present in removeJob'); }

        // only remove if job options available
        if (notification.jobOptions && notification.jobOptions.repeat) {
            return notificationQueue.removeRepeatable(notification.jobOptions.repeat);
        } else {
            common.log('Notification does not contain job details notification ' + notification.id);
            return Promise.resolve();
        }
    },
    
    removeRepeatableJob: function(repeatableJob) {
        if (!notificationQueue) return Promise.reject('Queue is not initialized in removeRepeatableJob');
        if (!repeatableJob ||  !repeatableJob.id) return Promise.reject('No job present in removeRepeatableJob');
        repeatableJob.jobId = repeatableJob.id;
        return notificationQueue.removeRepeatable(repeatableJob);
    },
    
    getRepeatableJobs: function getRepeatableJobs() {
        return notificationQueue.getRepeatableJobs();
    },
    
    getRepeatableJobCount: function getRepeatableJobCount() {
        return notificationQueue.getRepeatableCount();
    },
    
    createReportJob(notification, token) {
        let jobId = notification.id + '_' + token.date; // Unique per notification and report date
        // Look for existing job to avoid extra DB calls to get report job data
        return reportQueue.getJob(jobId).then(job => {
            if (job) {
                
                // Job already exists, retry if failed
                return job.getState().then(state => {
                    common.log('Job', jobId, 'exists in state:', state);
                    if (state === 'failed') {
                        common.log('Retrying job', jobId);
                        return job.retry();
                    } else {
                        // TODO handle other states. Allow rerun of completed reports?
                        return job;
                    }
                });
            } else {
                return getReportJobDataFromNotification(notification).then(reportJobData => {
                    let jobOptions = {
                        jobId: jobId,
                        removeOnComplete: true,
                        removeOnFail: false
                    };
                    reportJobData.token = token;
                    reportJobData.businessDate = token.date;
                    reportJobData.subJobId = token.subJobId;
                    common.log(reportJobData);
                    return reportQueue.add(reportJobData, jobOptions);
                });
            }
        });
    }
};

/*
    Process next notification by sending emails to all recipients that report is available for processing
 */
async function processNotificationJob(job) {

    let subJobId;
    let notificationsSuspended = false;
    let recipients =[] 
    
    for( let recipient of job.data.recipients){
        recipients.push(recipient.email);
    }
    
    try {
        //common.log('Processing notification: ' + job);
        let searchCriteria = job.data.searchCriteria;
        if(searchCriteria != null && searchCriteria.start_date != null){
            searchCriteria["start_date"] = moment().format('YYYY-MM-DD');
        }
        if(searchCriteria != null && searchCriteria.end_date != null){
            searchCriteria["end_date"] = null;
        }
        let search_options = await common.generateSearchObject(searchCriteria);
        
        let limitResults = 10;
        
        let totalcount = await Load.find(search_options)
        .collation({ locale: 'en', strength: 2 } )
        .count()
        .exec();

        let loads = await Load.find(search_options)
        .collation({ locale: 'en', strength: 2 } )
        .populate('bids.user')
        .populate('created_by')
        .sort({updatedAt : -1})
        .limit(limitResults)
        .exec();

        if(loads != null && loads.length > 0){
            //send email because loads were found matching the search criteria.
            let userName = "";
            let user = await User.find({_id : job.data.user}).exec();
            if(user != null && user.length> 0){
                userName = user[0].first_name + ' ' + user[0].last_name;
            }
            var subject = 'Fork Freight Loads';    
              
            var message = '<p>Hi ' + userName + ',<br></p><p>the are ' + totalcount + '  loads that match your search criteria.</p>' + 
            '<br>Details<br> Type  Origin        Destination <br>'  
            for(let load of loads){

             message = message +    load.equipment_type + ' ' + load.pickup_addresses + ' ' + load.delivery_addresses + '<br>'
            

            }

           message = message +  '</p><br><p>If you have any questions or need support, please email us at '+config.supportEmail+'</p><br><p>Best wishes,<br>Fork Freight Team</p>';
  
            //common.sendEmail(subject, message, config.supportEmail,  recipients);
            await emailTemplateService.sendEmail('save-alerts-for-loads',{message},recipients)
             
        }


    } catch (e) {
        common.log(e);
       
    }
}


/*
    Return new object containing necessary data for notification job
    id
    company
    recipients
        - email
        - language
 */
function getNotificationJobDataFromNotification(notification) {
    let notificationJobData = {};
    notificationJobData.id = notification.id;
    notificationJobData.user = notification.created_by;
    notificationJobData.searchCriteria = notification.searchCriteria;
    
    //get user info
    return User.find({ _id: notificationJobData.user, status : "active" }, { _id: 1, email: 1 })
    .then(users => {
        // Set all email recipients language
        notificationJobData.recipients = users.map(user => {
            return {email: user.email}
        });
        
        // Set additional recipients language to company language
        if (notification.emails && notification.emails.length > 0) {
            let emails = notification.emails.split(",");
            notificationJobData.recipients = notificationJobData.recipients.concat(emails.map(email => {
                return {email: email};
            }))
        }
        return notificationJobData;
    }).catch(err => {
        common.log("Error getting job data for notification:", notification.id, "; user:", notification.created_by, ";", err + err.stack);
    });
}

/*
    Return new object containing necessary data for notification job
        id
        reportType
        parameterBag
        reportDateRange
        attachmentFormat
        company
            - id
            - cdnContainerName
 */
function getReportJobDataFromNotification(notification) {
    let reportJobData = sails.util._.pick(notification, [
        'id',
        'reportType',
        'parameterBag',
        'reportDateRange',
        'notificationFormat'
    ]);
    
    // Get Company info
    return Company.findOne({id: notification.company}, {fields: {_id: 1, cdnContainerName: 1}}).then(company => {
        reportJobData.company = sails.util._.pick(company, ['id', 'cdnContainerName']);

        return reportJobData;
    }).catch(err => {
        common.log("Error getting job data for notification", err + err.stack);
    });
}

function getCronString(sendAtTime, repeatOnDate) {
    let cronString = `${sendAtTime.split(':')[1]} ${sendAtTime.split(':')[0]}`;
    if (repeatOnDate.frequency === 'monthly') {
        cronString += ` ${repeatOnDate.monthDay} * *`;
    } else {
        if (repeatOnDate.weekDayNumbers && repeatOnDate.weekDayNumbers.length > 0) {
            cronString += ` * * ${repeatOnDate.weekDayNumbers.join(',')}`;
        } else {
            cronString += ' * * *'; // TODO fix this because no days of the week means no report
        }
    }
    return cronString;
}

