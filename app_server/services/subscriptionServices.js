const mongoose = require('mongoose');
const SiteModule = mongoose.model('SiteModule');
const Role = mongoose.model('Role');
const SubscriptionPackages = mongoose.model('SubscriptionPackages');
const common = require('../helpers/common');
const StripeHelper = require('../helpers/stripe');

module.exports = {
    getModuleAccessableList: async function (role) {
        try {
            // const accessableModules = await SiteModule.aggregate([{
            //     $lookup: {
            //         from: 'subscriptionpackages',
            //         localField: '_id',
            //         foreignField: 'module_access',
            //         as: 'subscriptionPackages',
            //     }
            // }, {
            //     $unwind: {
            //         path: '$subscriptionPackages'
            //     }
            // }, {
            //     $match: {
            //         'subscriptionPackages.role': role
            //     }
            // }, {
            //     $group: {
            //         _id: {
            //             id: '$_id',
            //             name: '$name',
            //             frontend_key: "$frontend_key",
            //             createdAt: '$createdAt'
            //         }
            //     }
            // }, {
            //     $project: {
            //         '_id': "$_id.id",
            //         "name": "$_id.name",
            //         frontend_key: "$_id.frontend_key",
            //         createdAt: '$_id.createdAt'
            //     }
            // }, {
            //     $sort: {
            //         createdAt: 1
            //     }
            // }])
            const accessibleModules = await SiteModule.find({
                is_delete:false,
                $or:[{
                    roles:role
                },{
                    company_admin:true
                },{
                    full_access:true
                }]
            },{
                _id:1,
                name:1,
                frontend_key: 1,
                company_admin:1,
                full_access:1
            }).lean()
            return accessibleModules;
        } catch (err) {
            common.log("Error while call getModule Accessible List:- ",err);
            throw err;
        }
    },
    getSubscriptionPackageById: async function(id){
        try{
            const sp = await SubscriptionPackages.findById(id).lean();
            if(sp && sp.stripe_plan_id){
                sp.stripe_plan = await StripeHelper.retrievePlan(sp.stripe_plan_id);
            }
            if(sp && sp.stripe_price_id){
                sp.stripe_price = await StripeHelper.retrievePrice(sp.stripe_price_id);
            }
            return sp;
        }
        catch(err){
            common.log("Error while ", err);
            throw err
        }
    },
    isRolePrivate:async function(role){
        try {
           let _role =  await Role.findOne({name:role}).lean();
           return _role ? !_role.is_public :false;
        } catch (error) {
            return false;
        }
    },
    getFreeSubscriptionsId: async function(){
        try{
            let rd=[],freeSub =  await SubscriptionPackages.find({is_free:true},{_id:1}).lean();
            freeSub.forEach(s=>{
                rd.push(s._id);
            })
            return rd;
        }
        catch(error){
            common.log("Error while getFreeSubscriptionsId--- ",error)
            throw error;
        }
    },
    getPaidSubscriptionsId: async function(){
        try{
            let rd=[],freeSub =  await SubscriptionPackages.find({$or:[{is_free:false},{is_free:null}]},{_id:1}).lean();
            freeSub.forEach(s=>{
                rd.push(s._id);
            })
            return rd;
        }
        catch(error){
            common.log("Error while getFreeSubscriptionsId--- ",error)
            throw error;
        }
    }
}