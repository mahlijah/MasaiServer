const mongoose = require('mongoose');
const EmailTemplate = mongoose.model('EmailTemplate');
const config = require('../config/config');
const common = require('../helpers/common');
var jwt = require('jsonwebtoken');
module.exports.getTemplateByType = async function(_type){
    try{
        const template = await EmailTemplate.findOne({type:_type}).lean();
        return template;
    } catch(err){
        common.log('Error while get email Template :--',err)
        throw err;
    };
}

module.exports.activationTokenGeneration=(user)=>{
    var expiry = new Date();
    expiry.setDate(expiry.getDate() + 1);
    const activationToken = jwt.sign({
      email: user.email,
      role: user.role,
      exp: parseInt(expiry.getTime() / 1000),
    }, config.secretKey);
    return activationToken
}

module.exports.companyTokenGeneration=(user,requestedBy)=>{
    var expiry = new Date();
    expiry.setDate(expiry.getDate() + 1);
    const company = jwt.sign({
        company_id: requestedBy.company_id._id,
        requested_user_id:requestedBy._id,
        user_id:user._id,
        exp: parseInt(expiry.getTime() / 1000),
    }, config.secretKey);
    return company
}

module.exports.sendEmail = async function(type,_data,to,link=null){
    let data = {..._data,config};
    if(link){
        data.link = link;
    }
    
    const template = await this.getTemplateByType(type);
    if(template){
        let subject = template.subject;
        let body = template.body;
        subject = updateData(subject,data);
        body = updateData(body,data);
        common.sendEmail(subject,body,config.supportEmail,to);
    }
}

function updateData(html,data){
    var rgx = /{{\w+\.\w+}}{1,}|{{\w+}}|{{\w+\.\w+\.\w+}}{1,}/g;
    let keys = [...html.matchAll(rgx)];
    //console.log(keys);
    for (let i = 0; i < keys.length; i++) {
        let key = keys[i];
        key = key[0];
        html = html.replace(key,getValue(data,key));        
    }
    return html;
}

function getValue(data,key){
    key = key.replace('{{','')
    key = key.replace('}}','')
    key = key.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
    key = key.replace(/^\./, '');           // strip a leading dot
    var a = key.split('.');
    for (var i = 0, n = a.length; i < n; ++i) {
        var k = a[i];
        if (k in data) {
            
            data = data[k];
        } else {
            return;
        }
    }
    return data;
}