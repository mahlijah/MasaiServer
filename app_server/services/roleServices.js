const mongoose = require('mongoose');
const Role = mongoose.model('Role');

module.exports = {
    getPublicRoles: async()=>{
        try {
            let rl=[], data = await Role.find({is_public:true}).lean();
            data.forEach(role=>{
                rl.push(role.name);
            })
            return rl;
        } catch (error) {
            throw error;
        }
    },
    getPrivateRoles: async()=>{
        try {
            let rl=[], data = await Role.find({is_public:false}).lean();
            data.forEach(role=>{
                rl.push(role.name);
            })
            return rl;
        } catch (error) {
            throw error;
        }
    }
}