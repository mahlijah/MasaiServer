const mongoose = require('mongoose');
const SmsTemplate = mongoose.model('SmsTemplate');
const config = require('../config/config');
const common = require('../helpers/common');
const twilioSMS = require('../helpers/twilioSMS');
var jwt = require('jsonwebtoken');
module.exports.getSmsTemplateByType = async function(_type){
    try{
        const template = await SmsTemplate.findOne({type:_type})
        return template;
    } catch(err){
        common.log('Error while get email Template :--',err)
        throw err;
    };
}


module.exports.sendTwilioSMS = async function(type,_data,user_phone_number){
    let data = {..._data,config};
    const template = await this.getSmsTemplateByType(type);
    let body = template.body;
    body = updateData(body,data);
    twilioSMS.sendSMS(user_phone_number,body);
}

function updateData(html,data){
    var rgx = /{{\w+\.\w+}}{1,}|{{\w+}}|{{\w+\.\w+\.\w+}}{1,}/g;
    let keys = [...html.matchAll(rgx)];
    console.log(keys);
    for (let i = 0; i < keys.length; i++) {
        let key = keys[i];
        key = key[0];
        html = html.replace(key,getValue(data,key));        
    }
    return html;
}

function getValue(data,key){
    key = key.replace('{{','')
    key = key.replace('}}','')
    key = key.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
    key = key.replace(/^\./, '');           // strip a leading dot
    var a = key.split('.');
    for (var i = 0, n = a.length; i < n; ++i) {
        var k = a[i];
        if (k in data) {
            data = data[k];
        } else {
            return;
        }
    }
    return data;
}