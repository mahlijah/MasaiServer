/*
    Loads service handles all additional processing for loads.
 */

var mongoose = require('mongoose');
var LoadHistory = mongoose.model('LoadHistory');
var searchObject = mongoose.model('SearchObject');
var Load = mongoose.model('Load');
const Queue = require('bull');
const {ObjectId} = require('mongodb'); // or ObjectID 
var initialized = false;
var config = require('../config/config');
const io = require('socket.io-client');
const eventEmitter = require('events');
var jwt = require('jsonwebtoken');
const common = require('../helpers/common');

class emitLoadEvents extends eventEmitter{
    async send (load){
        this.emit('broadcastNewLoad', load);
    }
}

module.exports = {
    init: function init() {
        if (!initialized) {                        
            newLoadsQueue = new Queue(config.loadJobs.newloadQueue.name, config.loadJobs.redisUri, config.loadJobs.queueOptions);
            deletedLoadsQueue = new Queue(config.loadJobs.deleteloadQueue.name, config.loadJobs.redisUri, config.loadJobs.queueOptions);
            // Set up handler for processing jobs
            newLoadsQueue.process(config.loadJobs.newloadQueue.concurrency, this.processNewLoads);
            deletedLoadsQueue.process(config.loadJobs.deleteloadQueue.concurrency, this.processLoadDeletions);
            initialized = true;
            
           //clean the queueevery 5 seconds
            setInterval(() => {
                common.log("cleaning up queues")
                newLoadsQueue.clean(5000);
                deletedLoadsQueue.clean(5000);
            }, config.loadJobs.cleanupInterval);           
        }
    }
    ,

/*
    Process next notification by sending emails to all recipients that report is available for processing
 */
processNewLoads : async function processNewLoads(job, done){
       
    try {
            
            let data = job.data;
            var doc = await Load.findOne({_id : ObjectId(data.id)});
            
            if(doc != null){
            //common.log('Processing new load: ' + job.data.id);
            let historyRecord = new LoadHistory();
            historyRecord.pickup_addresses 	= 	doc.pickup_addresses;	

            historyRecord.pickup_cities 		= 	doc.pickup_cities;
            historyRecord.pickup_states		=	doc.pickup_states;
            historyRecord.delivery_addresses = 	doc.delivery_addresses;	
            historyRecord.delivery_cities	= 	doc.delivery_cities;
            historyRecord.delivery_states	= 	doc.delivery_states;
            historyRecord.delivery_lat_long  =  doc.delivery_lat_long;
            historyRecord.pickup_lat_long	= doc.pickup_lat_long;
            historyRecord.equipment_type 	= 	doc.equipment_type;
            historyRecord.no_of_count 		= 	doc.no_of_count;
            historyRecord.target_rate 		= 	doc.target_rate;
            historyRecord.load_weight 		= 	doc.load_weight;
            historyRecord.load_length 		= 	doc.load_length;
            historyRecord.tracking_number	=	doc.tracking_number;
            historyRecord.ready_date 		= 	doc.ready_date;
            historyRecord.pickup_date 		= 	doc.pickup_date;
            historyRecord.delivery_date 		= 	doc.delivery_date;							
            historyRecord.description 		=doc.description;							
            historyRecord.created_by 		= 	 doc.created_by;
            historyRecord.internal = doc.internal;
            historyRecord.source = doc.source;
            historyRecord.loadId = doc._id;
            await historyRecord.save();

           // send to socket for broadcast to rooms. 
            let ele = new emitLoadEvents();
            await ele.send(doc);//

            let search = {};
             //TODO:include search radius           
            //search["pickup_city_radius"] = "50", 
            ///search["delivery_city_radius"] = "50", 
           
           
           search["$and"] = [];

           if(doc.load_length != null ){
                let filter = {};
                filter["$or"] = [{"searchParams.load_length" : null}, {"searchParams.load_length" : doc.load_length}];
                search["$and"].push(filter);
            }

           if(doc.pickup_addresses != null && doc.pickup_addresses.length > 0 ){
               let filter = {};
               filter["$or"] = [{"searchParams.pickup_address" : null}, {"searchParams.pickup_address" : doc.pickup_addresses[0]}];
               search["$and"].push(filter );
            } 

            if(doc.pickup_cities != null && doc.pickup_cities.length > 0 ){
                let filter = {};
                filter["$or"] = [{"searchParams.pickup_city" : null}, {"searchParams.pickup_city" : doc.pickup_cities[0]}];
                search["$and"].push(filter);
             } 

             if(doc.delivery_address != null && doc.delivery_address.length > 0 ){
                let filter = {};
                filter["$or"] = [{"searchParams.delivery_address" : null}, {"searchParams.delivery_address" : doc.delivery_addresses[0]}];
                search["$and"].push(filter);
             } 

             if(doc.delivery_states != null && doc.delivery_states.length > 0 ){
                let filter = {};
                filter["$or"] = [{"searchParams.delivery_states" : null}, {"searchParams.delivery_states" : doc.delivery_states[0]}];
                search["$and"].push(filter);
             }
             
             if(doc.equipment_type != null ){
                let filter = {};
                filter["$or"] = [{"searchParams.equipment_type" : null}, {"searchParams.equipment_type" : doc.equipment_type}];
                search["$and"].push(filter);
             }

             //TODO : fix date range
            //  if(doc.ready_date != null ){
            //     let filter = {};
            //     filter["$or"] = [{"searchParams.equipment_type" : null}, {"searchParams.equipment_type" : doc.equipment_type}];
            //     search["$and"].push(filter);
            //  }
            //  search["$and"].push(dateRange);         
           

            let searchlist = await searchObject.find(search);
            if(searchlist != null && searchlist.length > 0)
            {
                var expiry = new Date();
                expiry.setDate(expiry.getDate() + 7);
              
                const newtoken= jwt.sign({
                  _id: data.id,
                  exp: parseInt(expiry.getTime() / 1000),
                }, config.secretKey);

                //common.log(newtoken);
                let serviceEndpoint = process.env.SERVICE_ENDPOINT || "http://localhost:" + config.port;
                let socket = io(serviceEndpoint,{query: { token: newtoken}});// io("http://localhost:5000",{query: { token: data.token}});
                socket.emit('broadcastNewLoad', { room : searchlist[0]._id, load:doc});
            }

          
        }
        else
        {
            common.log("data not found" + data);
        }
        
        done();
    } catch (e) {
        common.log(e);        
    }
},

/*
    Process next load deletion.
 */
 processLoadDeletions : async function processLoadDeletions(job, done) {
    try {
            //common.log('Processing load deletion: ' + job.data);
            let doc = job.data;
            let historyResult = await	LoadHistory.updateMany({tracking_number:Number(doc.tracking_number), created_by : ObjectId(doc.created_by)}, {$set:{is_opened : false}}) ; 						
            let result = await	Load.deleteOne({tracking_number:Number(doc.tracking_number), created_by : ObjectId(doc.created_by)}) ; 
            done();  
    } catch (e) {
        common.log(e);
    }
}
}