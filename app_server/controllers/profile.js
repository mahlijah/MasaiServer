
var mongoose = require('mongoose');
var User = mongoose.model('User');
var Role = mongoose.model('Role');
var UserStripeSubscriptionSchedule = mongoose.model('UserStripeSubscriptionSchedule');
var TemporaryUsers = mongoose.model('TemporaryUsers');
var UserDocument = mongoose.model('UserDocument');
var common = require('../helpers/common');
var crypto = require('crypto');
var config = require('../config/config');
var fs = require('fs');
var stripe = require("stripe")(config.stripe_key);
var multer = require('multer')
var upload = multer();
var path = require('path');
var ISODate = require('isodate');
var Company = mongoose.model('Company');
const UserStripeDetail = mongoose.model('UserStripeDetail');
const { s3GetFileStream } = require('../helpers/s3FileDownload');
const {adminStoreLogs}= require('../controllers/userActivityLog');
const emailTemplateService = require('../services/emailTemplateService');

//crm helper
const crmHelper = require('../helpers/crm');

var Transaction = mongoose.model('Transaction');

var mammoth = require("mammoth");
const _ = require('lodash');
var async = require("async");
var s3FileDownload = require('../helpers/s3FileDownload').s3FileDownload;
var createZipFile = require('../helpers/s3FileDownload').createZipFile;

const stripeHelper = require('../helpers/stripe');
const SubscriptionPackagesController = require('./subscriptionPackages');
const { ObjectId } = require('mongodb');
const subscriptionServices = require('../services/subscriptionServices');
const { Roles } = require('../config/config');

const SubscriptionPackages = mongoose.model('SubscriptionPackages');

module.exports.StripePaymentComplete = async function (req, res) {

  try {
    const endpointSecret = process.env.STRIPE_WHSECRET || "whsec_EmMy01bIk1HDiweZx5rtrOY2OhumgLnF";
    const sig = req.headers['stripe-signature'];
    const url = req.headers.origin || process.env.PORTAL_URL;
    let event;
    common.log("Webhook:-", event)
    try {
      
      event = req.body; //stripe.webhooks.constructEvent(req.rawBody, sig, endpointSecret);
    } catch (err) {
      return res.status(400).send(`Webhook Error: ${err.message}`);
    }

    // Handle the checkout.session.completed event
    if (event.type === 'checkout.session.completed') {
      const session = event.data.object;
      //update user account, set stripe id
      if (session.customer_email && session.subscription) {
        const subscription = await stripeHelper.getSubscription(session.subscription);

        let user = {}
        let [_user, tpUser] = await Promise.all([
          User.findOne({ email: session.customer_email }),
          TemporaryUsers.findOne({ email: session.customer_email })
        ]);
        if (_user) {
          let role = _user.role;
          const sp = await SubscriptionPackagesController.getSubscriptionPackagesFromStripePlanId(subscription.plan.id, role);
          let sp_id = null, sp_name = "pro";
          if (sp) {
            sp_id = sp._id;
            sp_name = sp.subscription_name;
          }
          user = await User.findOneAndUpdate({ email: session.customer_email }, {
            $set: {
              role: role,
              stripe_id: session.customer,
              subscription_package: sp_id,
              subscription: true,
              subscription_type: sp_name,
              subscription_expire_on: (+new Date() + 30 * 24 * 60 * 60 * 1000),
              payment_status: subscription.status
            }
          }, { new: true })


          // common.sendActivationEmail(user, url);
          let activationToken = emailTemplateService.activationTokenGeneration(user);
          let activationLink = url + '/public/activateUser?token=' + activationToken;
          await emailTemplateService.sendEmail('welcome-email', { user }, user.email, activationLink);

          /**update info in crm too */
          crmHelper.add_or_modify_crm_contact(user);
        } else if (tpUser) {
          const sp = await SubscriptionPackagesController.getSubscriptionPackagesFromStripePlanId(subscription.plan.id, tpUser.role);
          let sp_id = null, sp_name = "pro";
          if (sp) {
            sp_id = sp._id;
            sp_name = sp.subscription_name;
          }
          let temp_user = await TemporaryUsers.findOneAndUpdate({ email: session.customer_email }, {
            $set: {
              stripe_id: session.customer,
              subscription_package: sp_id,
              subscription: true,
              subscription_type: sp_name,
              subscription_expire_on: (+new Date() + 30 * 24 * 60 * 60 * 1000),
              payment_status: subscription.status
            },
          }, { new: true });

          /**update info in crm too 
          crmHelper.add_or_modify_crm_contact(temp_user);*/

        }
      }
    }
    if (event.type == 'invoice.payment_failed') {
      const invoice = event.data.object;
      let user = await User.findOne({ stripe_id: invoice.customer }).lean();
      if (user) {
        let sp = await stripeHelper.getSubscription(invoice.subscription)
        let sub= invoice.status=='uncollectible'? false: user.subscription;
        await User.updateOne({ stripe_id: invoice.customer }, {
          $set: {
            subscription: sub,
            payment_status: sp.status,
            subscription_expire_on: invoice.status=='uncollectible'? new Date().toISOString() :new Date(sp.current_period_end * 1000).toISOString()
          }
        });
        let profile_link = url + "/private/user-profile"
        await emailTemplateService.sendEmail('cc-declined-email', { user }, user.email, profile_link)
      }
    }
    if (event.type == 'customer.subscription.updated') {
      const subscription = event.data.object;
      let user = await User.findOne({ stripe_id: subscription.customer }).lean();
      if (user) {
        // let subscription = await stripeHelper.getSubscription(subscription.id)
        const sp = await SubscriptionPackagesController.getSubscriptionPackagesFromPlanId(subscription.plan.id);
      
        let expire_date = (subscription.status != 'canceled' || subscription.status != 'past_due') ? new Date(subscription.current_period_end * 1000) : user.subscription_expire_on;
        await User.updateOne({ stripe_id: subscription.customer }, {
          $set: {
            role: sp.role,
            payment_status: subscription.status,
            subscription: subscription.status != 'canceled' || subscription.status != 'past_due',
            subscription_expire_on: expire_date,
            subscription_package: sp._id,
            subscription_type: sp.subscription_name,
          }
        });
        // let profile_link = url + "/private/user-profile"
        // await emailTemplateService.sendEmail('cc-decline-email', { user }, user.email, profile_link)
      }
    }
    if (event.type == 'customer.subscription.deleted') {
      const subscription = event.data.object;
      let user = await User.findOne({ stripe_id: subscription.customer }).lean();
      if (user) {
        let sp = await stripeHelper.getSubscription(subscription.id)
        updateObject = {
          payment_status: sp.status,
          subscription: false
        }
        if (user.free_subscription_package_after_cancel_paid) {
          updateObject.subscription_package = user.free_subscription_package_after_cancel_paid;
          updateObject.free_subscription_package_after_cancel_paid = null;
        }

        await User.updateOne({ stripe_id: subscription.customer }, {
          $set: updateObject
        });
        // let profile_link = url + "/private/user-profile"
        // await emailTemplateService.sendEmail('cc-decline-email', { user }, user.email, profile_link)
      }
    }
    if (event.type == 'charge.succeeded') {
      const charge = event.data.object;
      let user = await User.findOne({ stripe_id: charge.customer }).lean();
      if (user) {
        const customer = await stripeHelper.getCustomer(charge.customer);
        if (customer && customer.subscriptions && customer.subscriptions.data) {
          const sub = customer.subscriptions.data[0];
          await User.updateOne({ stripe_id: charge.customer }, {
            $set: {
              payment_status: sub.status,
              subscription: true,
              subscription_expire_on: new Date(sub.current_period_end * 1000).toISOString()
            }
          })
        }
      }
    }
    if (event.type == 'subscription_schedule.released') {
      const subscription_schedule = event.data.object;
      let schedule = await UserStripeSubscriptionSchedule.find({ stripe_schedule_id: subscription_schedule.id }).lean();
      if (schedule) {
        await UserStripeSubscriptionSchedule.updateOne({ _id: schedule._id }, { $set: { is_delete: true } });
        await User.updateOne({ _id: schedule.user }, {
          $set: {
            subscription_package: schedule.subscription_package,
            role: schedule.role,
            schedule: null
          }
        })
      }
    }
    res.json({ received: true });

  }
  catch (err) {

    common.log(err);
    res.status(400).send(`Webhook Error: ${err.message}`);

  }
}

module.exports.createStripeToken = async function (req, res) {
  try {
    const user = await User.findOne({ email: req.body.email, stripe_id: { $ne: null } }).lean();
    if (user != null && user.subscription) {
      return res.status(400).send("user has active subscription.");
    }
    const url = req.headers.origin || process.env.PORTAL_URL;
    const plan_id = req.body.plan_id || process.env.STRIPE_PLAN || 'plan_JVDnY9uXN2hAdm'; //'plan_GI2WUgviwAWgw0';
    const success_url = url  + (req.body.success_url || 'heatmap');
    const cancel_url = url  + (req.body.cancel_url || req.body.success_url || 'subscription-check');
    const session = await stripeHelper.createCheckoutSession(plan_id, req.body.price_id, req.body.email, success_url, cancel_url, true,undefined,req.body.promo_id);
    return res.status(200).send({ sessionid: session.id });
  }
  catch (error) {
    common.log(error);
    return res.status(400).send("error occured");
  }
}

module.exports.profileRead = async function (req, res) {
  try {
    var projection = {
      _id: 1,
      first_name: 1,
      last_name: 1,
      email: 1,
      emailVerified: 1,
      phone: 1,
      phoneVerified: 1,
      avatar: 1,
      subscription_cancel_reason: 1,
      subscription_type: 1,
      subscription_expire_on: 1,
      company_id: 1,
      role: 1,
      status: 1,
      subscription: 1,
      subscription_package: 1,
      is_company_admin: 1,
      stripe_id: 1,
      schedule: 1,
      free_subscription_package_after_cancel_paid: 1,
      payment_status: 1
    }
    let user = await User.findById(req.payload._id, projection).populate('company_id').populate({
      path: 'schedule',
      populate: {
        path: 'subscription_package'
      }
    }).populate('subscription_package').populate('free_subscription_package_after_cancel_paid').lean();
    if (!user) {
      return res.status(400).send({ success: false, message: "User not found" })
    }
    let role = await Role.findOne({ name: user.role }).lean();
    user.is_public = role.is_public;
    if (user.stripe_id && role.is_public) {
      try {
        user.payment_methods = await stripeHelper.getPaymentMethods(user.stripe_id);
        const customer = await stripeHelper.getCustomer(user.stripe_id);
        if (customer && customer.subscriptions && customer.subscriptions.data) {
          user.subscriptions = customer.subscriptions.data[0]
        }
      }
      catch (err) {
        // await User.updateOne({ _id: req.payload._id }, { $set: { stripe_id: null } });
      }
    }
    if (role.is_public && (!user.subscription_package || !user.subscription_package.is_free)) {
      var date = new Date();
      var date2 = new Date(user.subscription_expire_on);
      if (date2 < date) {
        if (user.subscription && !user.stripe_id) {
          await User.updateOne({ _id: req.payload._id }, { $set: { subscription: false } });
          user.subscription = false;
        }
        else if (user.subscriptions && user.stripe_id) {
          user.subscription_expire_on = new Date(user.subscriptions.current_period_end * 1000).toISOString();
          await User.updateOne({ _id: req.payload._id }, { $set: { subscription_expire_on: user.subscription_expire_on, payment_status: user.subscriptions.status } });
        }
      }
    }
    return res.status(200).json(user);
  }
  catch (err) {
    common.log("Error while profile read", err);
    return res.status(400).send({ success: false, message: err.message });
  }
};

module.exports.authuser = async function (req, res) {
  try {
    let user = await User.findById(req.payload._id).populate({
      path: 'subscription_package',
      populate: {
        path: 'module_access',
        select: 'frontend_key -_id'
      }
    }).lean();
    return res.status(200).json(user);
  }
  catch (err) {
    common.log('Error while get auth user: ', err);
    return res.status(400).send(err.message);
  }
};

module.exports.userProfileRead = async function (req, res) {
  if (!req.payload._id) {
    res.status(401).send({ message: "UnauthorizedError: Private profile" });
  } else {
    var uid = req.params.userID
    common.log('userid is ', uid)
    if (!uid || uid == '') {
      res.status(400).send({ message: "User ID is missing" });
    } else {
      try {
        let user = await User.findById(uid);
        if (user) {
          var date = new Date();
          var date2 = new Date(user?.subscription_expire_on);
          if (date2 < date) {
            if (user.subscription && user.stripe_id) {
              const confirmation = await stripeHelper.getCustomer(user.stripe_id);
              if(confirmation?.subscriptions?.data[0]?.current_period_end){
              user.subscription_expire_on = new Date(confirmation.subscriptions.data[0]?.current_period_end * 1000).toISOString();
              User.updateOne({ _id: uid }, { $set: { subscription_expire_on: user.subscription_expire_on } });
              }
              return res.status(200).json(user);
            } else {
              return res.status(200).json(user);
            }
          }
          else {
            return res.status(200).json(user);
          }
        } else {
          return res.status(400).send({ sucess: false, message: "User not found" });
        }
      }
      catch (err) {
        return res.status(400).send({ success: false, message: err.message })
      }
    }
  }
}

module.exports.updateProfile = async function (req, res) {
  try {
    let stripeupdateflag= false;
    let user = await User.findById(req.payload._id);
    if (user) {
      if (req.body.first_name) user.first_name = req.body.first_name;
      if (req.body.last_name) user.last_name = req.body.last_name;
      if (req.body.role) user.role = req.body.role;

      if (req.body.phone && user.phone != req.body.phone) {
        user.phone = req.body.phone;
        user.phoneVerified = false;
      }
      if (req.body.password) {
        user.setPassword(req.body.password);
      }
      //update if email is not existed
      if(req.body.email){
        let Email_validation= await validate_update_email(req.body.email, req.payload._id);
        if(Email_validation.success){
          if(req.body.email!==user.email){
            user.email= req.body.email;
            stripeupdateflag= true;
          }

        }else{
          common.log("ERROR in email validation:__", Email_validation);
           return res.status(400).send(Email_validation);
        }
    }

      let upsert = user.toObject();
      delete upsert._id;
      await User.updateOne({ _id: ObjectId(req.payload._id) }, { $set: upsert });

      if(stripeupdateflag)
       await updateEmailOnStripe(req.body.email,user);

      return res.send({ success: true, message: "User Info update successfully" })
    } else {
      return res.status(404).send({ success: false, message: "User not found" });
    }
  } catch (err) {
    common.log('Error while updateProfile :--', err)
    return res.status(400).send({ success: false, message: err.message });
  }
}

function updateProfile(req, files, res) {

  User
    .findById(req.payload._id)
    .exec(function (err, user) {

      if (!err) {
        if (user) {
          if (!req.body.first_name || !req.body.last_name || !req.body.company || !req.body.address) {
            res.status(400).send({ message: "first and last name, company , and address are required" });
            return;
          }
          if (req.body.password) {
            user.setPassword(req.body.password);
          }
          if (files) {
            // user.agreement_doc = [];
            _.forEach(files, function (file) {
              var ext = path.extname(file.originalname)
              if (ext == '.docx' || ext == '.doc' || ext == '.pdf') {
                user.agreement_doc.push({ "filename": file.filename });
              }
            })
          }
          user.first_name = req.body.first_name;
          user.last_name = req.body.last_name;
          user.company = req.body.company;
          user.address = req.body.address;
          user.mc_Number = req.body.mc_Number;
          if (req.body.sign_file) {
            user.electronic_signature = req.body.sign_file
          }
          if (user.phone != req.body.phone) {
            user.phone = req.body.phone;
            user.phoneVerified = false;
          }
          var upsertData = user.toObject();
          delete upsertData._id;

          if (req.body.avatar != "" && req.body.avatar != null) {
            common.SaveImageToDisk(req.body.avatar, function (image_url) {

              if (image_url != "" && image_url != null) {
                upsertData.avatar = image_url;
              }

              User.update({ _id: user.id }, upsertData, { upsert: true }, function (err) {
                if (!err) {
                  var token = user.generateJwt();
                  res.status(200).json({ "token": token });
                } else {
                  res.status(400).send({ message: err.message });
                  return;
                }
              });

            });

          } else if (req.body.profile_image != "" && req.body.profile_image != null) {
            upsertData.avatar = req.body.profile_image;


            User.update({ _id: user.id }, upsertData, { upsert: true }, function (err) {
              if (!err) {
                var token = user.generateJwt();
                res.status(200).json({ "token": token });
              } else {
                res.status(400).send({ message: err.message });
                return;
              }
            });
          } else {
            //  User.update({ _id: user.id}, upsertData, {upsert: true}, function (err) {
            common.log(upsertData)
            User.update({ _id: user.id }, upsertData, function (err) {
              if (!err) {

                var token = user.generateJwt();
                res.status(200).json({ "token": token });
              } else {

                res.status(400).send({ message: err.message });
                return;
              }
            });

          }

        } else {
          res.status(401).send({ message: 'UnauthorizedError: User not found' });
        }
      } else {
        res.status(404).json(err);
      }

    });
}

module.exports.updateProfile2 = function (req, res) {
  var accountRequest = req.body.accountRequest;
  if (accountRequest) {
    var data = common.splitRequestXMLData(accountRequest);

    var docData
    if (!data.userName) {
      return res.status(401).send({ message: "UnauthorizedError: Private profile" });
    } else {
      let fileName;
      var storage = multer.diskStorage({
        destination: function (req, file, callback) {
          callback(null, './public/uploads');
        },
        filename: function (req, file, callback) {
          var fileName = path.parse(file.originalname).name;
          callback(null, fileName.split(' ').join('') + Date.now() + path.extname(file.originalname))
        }
      });
      var upload = multer({
        storage: storage
      }).any();
      upload(req, res, function (err) {
        if (err) {
          res.status(400).send(err);
        } else {
          if (req.files) {
            var fileList = [];
            // if (req.file.originalname.indexOf('.docx') > -1 || req.file.originalname.indexOf('.doc') > -1 || req.file.originalname.indexOf('.pdf') > -1) {
            // var filePath = path.join(__dirname, '../../uploads/') + req.file.filename;
            //  common.log("filepath",req.file.filename);
            // mammoth.convertToHtml({ path: filePath })
            //  .then(function (result) {
            //  var text = result.value; // The raw text
            // fs.unlink(filePath);
            // docData = text;
            // })
            // .done();    

            updateProfile2(data, req.files, res)
          }
          //   else {
          //    // common.log("here")
          //     res.status(400).send({ message: "Agreement files should be doc/docx./pdf" });
          //     return;
          //   }
          // } 
          else {
            updateProfile2(req, docData, res)
          }
        }
      });
    }
  }

};

function updateProfile2(req, files, res) {
  var accountRequest = req.body.accountRequest;
  if (accountRequest) {
    var data = common.splitRequestXMLData(accountRequest);
    common.log("data")
    common.log(data.userName)
    common.log("data")
    User.findOne({ username: data.userName }, function (err, user) {
      common.log(user)
      if (user !== null) {
        var hash = crypto.pbkdf2Sync(data.password, user.salt, 1000, 64, 'sha512').toString('hex');
        if (hash != user.hash) {
          res.status(400).send({ message: "Incorrect password" });
          return;
        }
      }
      if (!err) {
        if (user) {
          var address = data.addr1 + ',' + data.addr2 + ',' + data.city + ',' + data.state + ',' + data.postalCode + ',' + data.country;
          // common.log(data.fName+'123'+data.lName+'456'+'')
          if (!data.fName || !data.lName || !data.compName || !address || !data.MCNumber) {
            res.status(400).send({ message: "All fields are required!!" });
            return;
          }
          if (data.password) {
            user.setPassword(data.password);
          }
          if (files) {
            // user.agreement_doc = [];
            _.forEach(files, function (file) {
              var ext = path.extname(file.originalname)
              if (ext == '.docx' || ext == '.doc' || ext == '.pdf') {
                user.agreement_doc.push({ "filename": file.filename });
              }
            })
          }
          user.first_name = data.fName;
          user.last_name = data.lName;
          user.company = data.compName;
          user.address = address;
          user.mc_Number = data.MCNumber;
          if (data.sign_file) {
            user.electronic_signature = data.sign_file
          }

          var upsertData = user.toObject();
          delete upsertData._id;
          // common.log(upsertData)
          if (req.body.avatar != "" && req.body.avatar != null) {
            common.SaveImageToDisk(req.body.avatar, function (image_url) {

              if (image_url != "" && image_url != null) {
                upsertData.avatar = image_url;
              }
              User.update({ _id: user.id }, upsertData, { upsert: true }, function (err) {
                if (!err) {
                  var token = user.generateJwt();
                  res.status(200).json({ "token": token });
                } else {
                  res.status(400).send({ message: err.message });
                  return;
                }
              });

            });

          } else if (req.body.profile_image != "" && req.body.profile_image != null) {
            upsertData.avatar = req.body.profile_image;
            User.update({ _id: user.id }, upsertData, { upsert: true }, function (err) {
              if (!err) {
                var token = user.generateJwt();
                res.status(200).json({ "token": token });
              } else {
                res.status(400).send({ message: err.message });
                return;
              }
            });
          } else {
            User.update({ _id: user.id }, upsertData, { upsert: true }, function (err) {
              //User.update({ _id: user.id, $set:upsertData}, function(err) { 
              common.log(upsertData.__v)
              if (!err) {
                res.status(200).send({ message: "Account updated!" });
                return;
                //res.send(user);
                common.log("user.id")
                common.log(user.id)
                common.log("user.id")
                var token = user.generateJwt();
                //res.status(200).json({ "token": token });
              } else {
                res.status(400).send({ message: "Account not found" });
                return;
              }
            });

          }

        } else {
          res.status(401).send({ message: 'UnauthorizedError: User not found' });
        }
      } else {
        res.status(404).json(err);
      }

    });
  }
}

module.exports.updateStripeSubscription = async function (req, res) {
  try {
    const url = req.headers.origin || process.env.PORTAL_URL;
    const success_url = url + '/' + (req.body.success_url || 'heatmap');
    const cancel_url = url + '/' + (req.body.cancel_url || req.body.success_url || 'subscription-check');
    const plan_id = req.body.plan_id
    const user = await User.findById(req.payload._id).populate('subscription_package');
    if (req.body.is_free == true) {
      const sp = await SubscriptionPackagesController.getSubscriptionPackagesFromStripePlanId(plan_id, (req.body.role || user.role), req.body.is_free);
      if (user.stripe_id) {
        const customer = await stripeHelper.getCustomer(user.stripe_id);
        if (customer && customer.subscriptions && customer.subscriptions.data && customer.subscriptions.data.length > 0) {
          const subscription = await stripeHelper.cancelSubscription(customer.subscriptions.data[0].id);
          var subscription_expire = new Date(subscription.current_period_end * 1000).toISOString();
          await User.updateOne({ _id: user._id }, {
            $set: {
              subscription_expire_on: subscription_expire,
              subscription: false,
              subscription_cancel_reason: "Downgrade to free",
              payment_status: subscription.status,
              free_subscription_package_after_cancel_paid: sp._id
            }
          });
          return res.status(200).send({ success: true, message: 'Plan Will Update after end of the current billing cycle' });
        } else {
          await User.updateOne({ _id: user._id }, {
            $set: {
              stripe_id: customer ? user.stripe_id : null,
              role: sp.role,
              subscription_package: sp._id,
              subscription_type: sp.subscription_name,
            }
          });
          return res.status(200).send({ success: true, message: 'Plan Upgrade successfully' });
        }
      } else {
        await User.updateOne({ _id: user._id }, {
          $set: {
            role: sp.role,
            subscription_package: sp._id,
            subscription_type: sp.subscription_name,
            subscription: true,
            subscription_expire_on: null
          }
        });
        return res.status(200).send({ success: true, message: 'Plan Upgrade successfully' });
      }
    }
    else {
      if (user.stripe_id) {
        const customer = await stripeHelper.getCustomer(user.stripe_id);
        if (customer.subscriptions && customer.subscriptions.data && customer.subscriptions.data.length > 0) {
          const plan = await stripeHelper.retrievePlan(plan_id);
          const currentPlan = await stripeHelper.retrievePlan(user.subscription_package.stripe_plan_id);
          let sp;
          if (currentPlan.amount <= plan.amount || customer.subscriptions.data[0].status == 'trialing') {
            const subscription = await stripeHelper.changeSubscription(customer.subscriptions.data[0].id, plan_id);
            sp = await SubscriptionPackagesController.getSubscriptionPackagesFromStripePlanId(subscription.plan.id, (req.body.role || user.role), req.body.is_free);
            await User.updateOne({ _id: ObjectId(req.payload._id) }, {
              $set: {
                role: sp.role,
                subscription_package: sp._id,
                subscription_type: sp.subscription_name,
                subscription: true,
                payment_status: subscription.status,
                free_subscription_package_after_cancel_paid: null
              }
            });

          } else {
            const { schedule, execute_on } = await stripeHelper.scheduleChangeSubscription(customer.subscriptions.data[0].id, plan_id);
            sp = await SubscriptionPackagesController.getSubscriptionPackagesFromStripePlanId(plan_id, (req.body.role || user.role), req.body.is_free);
            let userStripeScheduleSubscription = new UserStripeSubscriptionSchedule();
            userStripeScheduleSubscription.user = user._id;
            userStripeScheduleSubscription.subscription_package = sp._id;
            userStripeScheduleSubscription.role = (req.body.role || user.role);
            userStripeScheduleSubscription.stripe_schedule_id = schedule.id;
            userStripeScheduleSubscription.execute_on = execute_on;
            userStripeScheduleSubscription.save();
            await User.updateOne({ _id: user._id }, { $set: { schedule: userStripeScheduleSubscription._id } });
          }

          let activationToken = emailTemplateService.activationTokenGeneration(user);
          let activationLink = url + '/public/activateUser?token=' + activationToken;
          await emailTemplateService.sendEmail('upgrade-subscription', { user, subscription_package: sp }, user.email, activationLink);


          return res.status(200).send({ success: true, message: 'Plan Upgrade successfully' });

        } else {
          const session = await stripeHelper.createCheckoutSessionWithCustomerId(plan_id, null, user.stripe_id, success_url, cancel_url);
          return res.status(200).send({ success: false, sessionid: session.id });
        }
      } else {
        let trial_for_plan = true;
        let trial_period_days;
        let price_id = req.body.price_id;
        if (user.subscription_expire_on) {
          var currentDate = new Date();
          if (user.subscription_expire_on <= currentDate) {
            trial_for_plan = false;
            price_id = null;
          } else {
            const diffTime = Math.abs(user.subscription_expire_on - currentDate);
            trial_period_days = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
          }
        }
        const session = await stripeHelper.createCheckoutSession(plan_id, price_id, req.body.email, success_url, cancel_url, trial_for_plan, trial_period_days);
        return res.status(200).send({ success: false, sessionid: session.id });
      }
    }

  } catch (err) {
    common.log('Error while updateStripeSubscription :--', err)
    console.log(err.message)
    return res.status(400).send({ success: false, message: err.message });
  }
}

module.exports.restartSubscription = async function (req, res) {
  try {
    var user = await User.findById(req.params.id).lean();
    if (!user) {
      return res.status(404).send({ success: false, message: "User not found" })
    }
    if (!user.stripe_id) {
      const customers = await stripeHelper.findCustomerByEmail(user.email);
      if(customers.data.length==0){
        return res.status(404).send({ success: false, message: 'User Stripe account not found' });
      }
      user.stripe_id = customers.data[0].id;
      await User.updateOne({_id:ObjectId(req.params.id)},{
        $set:{
          stripe_id:customers.data[0].id
        }
      })
    }

    //get customer details 
    const customer = await stripeHelper.getCustomer(user.stripe_id);
     let sp = await subscriptionServices.getSubscriptionPackageById(req.body.subscription_package_id);
    //check payment method of user 
    if(!customer.invoice_settings || !customer.invoice_settings.default_payment_method){
      const pml = await stripeHelper.getPaymentMethods(user.stripe_id);
      if(!pml.data || pml.data.length==0) 
        throw new Error('Payment method not found.');
      // return res.status(400).send({success:false, message:'Payment method not found.'})

      await stripeHelper.updateCustomerDefaultPM(user.stripe_id,pml.data[0].id);
    }
 
    //get price or create one requested
    let price_id = req.body.price_id;
    if(!price_id && req.body.price){
      let stripePrice = (await stripeHelper.getPriceList()).find(price => price.unit_amount == (req.body.price * 100));
      
      if (stripePrice) {
        price_id = stripePrice.id;
      } else {
        let name = "Offer Price USD" + req.body.price;
        let newPrice = await stripeHelper.createPrice(name, req.body.price);
        price_id = newPrice.id
      }
    }
 
    const subscription = await stripeHelper.createSubscription_restart(user.stripe_id, sp.stripe_plan_id, true, null, price_id, req.body.coupon_id);
    // console.log("new subscriptions:___",subscription,"\n________________________");
      //customer void invoice if open or uncollectable and delete previous subscription
    if (customer && customer.subscriptions && customer.subscriptions.data && customer.subscriptions.data.length > 0) {
      // console.log("***********************************")
      const invoice = customer.subscriptions.data[0].latest_invoice;
      if(invoice && (invoice.status =="open" || invoice.status =="uncollectible")){
          await stripeHelper.voidInvoice(invoice.id);
      }
      await stripeHelper.deleteSubscription(customer.subscriptions.data[0].id);
    }

    await User.updateOne({
      _id: ObjectId(req.params.id)
    }, {
      $set: {
        role: sp.role,
        subscription_package: sp._id,
        subscription_type: sp.subscription_name,
        subscription: true,
        payment_status: subscription.status,
        free_subscription_package_after_cancel_paid: null,
        status:'active'
      }
    });
    /**create record in user stripe details for audit log */
    let usd = new UserStripeDetail();
    usd.created_by= req.payload._id;
    usd.updated_by = req.payload._id;
    usd.user = user._id;
    usd.customer = customer;
    // usd.paymentMethod = req.body.paymentMethod
    usd.subscription = subscription;
    usd.save();

    return res.send({ success: true, message: 'Restart Subscription success' })
  } catch (err) {
    common.log('Error while restartSubscription :--', err)
    return res.status(400).send({ success: false, message: err.message });
  }finally{
    await adminStoreLogs(req,user,"restart subscription","User")
  }
}

module.exports.cancelSubscription = async function (req, res) {
  if (!req.body.reason) {
    return res.status(400).send({ field: 'reason', message: "cancel reason is required." });
  }
  try {
    let user = await User.findOne({ email: req.payload.email }).populate('subscription_package').lean();
    if (!user) {
      return res.status(400).send({ success: false, message: "User not found" });
    }
    if (user.subscription_package && user.subscription_package.is_free) {
      await User.updateOne({ _id: user._id }, {
        $set: {
          subscription_expire_on: new Date(),
          subscription: false,
          subscription_cancel_reason: req.body.reason,
          subscription_canceled_on: new Date()
        }
      });

      /**
      * update the user in crm too 
      */
      user.subscription = false;
      crmHelper.add_or_modify_crm_contact(user);


      //common.sendSelfCancelSubscriptionEmail(user);
      let url = req.headers.origin || config.portalUrl;
      let link = url + "private/user-profile";
      await emailTemplateService.sendEmail('self-cancellation-subscription', { user }, user.email, link);
      return res.status(200).json({ success: true, status: 200 });
    }
    if (!user.stripe_id) {
      return res.status(400).send({ success: false, message: "No subscription detail found" });
    }
    const customer = await stripeHelper.getCustomer(user.stripe_id);
    if (!customer) {
      return res.status(400).send({ success: false, message: "Failed to retrieve customer info" });
    }
    if (customer.subscriptions && customer.subscriptions.data && customer.subscriptions.data.length > 0) {
      const subscription = await stripeHelper.cancelSubscription(customer.subscriptions.data[0].id);
      var subscription_expire = new Date(subscription.current_period_end * 1000).toISOString();
      await User.updateOne({ _id: user._id }, {
        $set: {
          subscription_expire_on: subscription_expire,
          subscription: false,
          subscription_cancel_reason: req.body.reason,
          payment_status: subscription.status,
          subscription_canceled_on: new Date(subscription.canceled_at * 1000).toISOString(),
          canceled_by: req.payload._id
        }
      });

      /**
       * update the user in crm too 
       */
      user.subscription = false;
      crmHelper.add_or_modify_crm_contact(user);
      //common.sendSelfCancelSubscriptionEmail(user);
      let url = req.headers.origin || config.portalUrl;
      let link = url + "private/user-profile";
      await emailTemplateService.sendEmail('self-cancellation-subscription', { user }, user.email, link)
      return res.status(200).json({ success: true, status: 200 });
    } else {
      return res.status(400).send({ success: false, message: "Your Subscription is already Cancelled" });
    }
  }
  catch (err) {
    common.log("Error while cancelling your subscription:-  ", err);
    return res.status(400).send({ success: false, message: err.message });
  }
};

module.exports.updateDcuments = function (req, res) {
  if (req.payload._id && req.body.dosc) {
    var dosc = req.body.dosc;
    User
      .findById(req.payload._id)
      .exec(function (err, user) {
        if (!err) {
          if (user) {
            if (user.agreement_doc) {
              var agreement_doc = user.agreement_doc;
              var array = [];
              agreement_doc.forEach(element => {
                if (element.filename != dosc) {
                  array.push({ 'filename': element.filename })
                }
              });
              user.agreement_doc = array;
              var upsertData = user.toObject();
              common.log(upsertData);
              delete upsertData;
              common.log('../public/uploads/' + dosc)
              User.update({ _id: user._id }, upsertData, function (err) {
                if (!err) {
                  fs.exists('./public/uploads/' + dosc, function (exists) {
                    if (exists) {
                      //Show in green
                      common.log('File exists. Deleting now ...');
                      fs.unlinkSync('./public/uploads/' + dosc);
                    } else {
                      //Show in red
                      common.log('File not found, so not deleting.');
                    }
                  });

                  var token = user.generateJwt();
                  res.status(200).json({ "token": token });
                } else {
                  res.status(400).send({ message: err.message });
                  return;
                }
              });
            }
          }
        }
      });
    res.status(400).send({ sucess: false, message: err.message });
  };
};

module.exports.updateAvatar = function (req, res) {
  try {
    User.findById(req.payload._id).exec(function (err, user) {
      if (err) {
        common.log("Error while find user ==>", err)
        return res.status(400).send(err);
      }
      if (!user) {
        return res.status(400).send({ message: 'User not found' });
      }
      User.update({ _id: req.payload._id }, { $set: { avatar: req.file.location } }, function (err) {
        if (err) {
          common.log("Error while update avatar ==>", err)
          return res.status(400).send(err);
        }
        return res.send({
          success: true,
          message: 'Profile Image change successfully'
        })
      })
    })
  }
  catch (err) {
    common.log("Error catchr ==>", err)
    return res.status(400).send(err);
  }
}

module.exports.uploadDocument = function (req, res) {
  if (!req.body.document_type) {
    common.log("Error  document type--- ");
    return res.status(400).send({
      field: 'document_type',
      message: 'document type is required'
    })
  }
  if (!req.file) {
    common.log("Error  file is required--- ");
    return res.status(400).send({
      field: 'file',
      message: 'file is required'
    })
  }
  documentSave(req.payload._id, req, res)

}

module.exports.uploadFilterDocument = async function (req, res) {
  if (!req.body.startDate || !req.body.endDate) {
    common.log("Error  document type--- ");
    return res.status(400).send({
      field: 'date',
      message: 'date is required'
    })
  }

  if (!req.body.document_type) {
    common.log("Document type is missing ");
    return res.status(400).json({ message: 'Document Type is invalid' })
  }

  if (!req.payload._id) {
    common.log("Error while  user not found");
    return res.status(400).send({ message: "User not found" })

  } else {

    let search_filter = await UserDocument.find({ user_id: ObjectId(req.payload._id), document_type: req.body.document_type, created_at: { $gte: new Date(req.body.startDate).setHours(00, 00, 00), $lte: new Date(req.body.endDate).setHours(23, 59, 59) } });
    if (!search_filter) {
      res.status(400).json({ message: 'searching data is not avaliable' })
    } else {
      res.status(200).json({ success: true, data: search_filter })
    }
  }

}

module.exports.getDocumentWithTransactionId = async function (req, res) {
  let { document_type, transaction_id } = req.query;

  if (document_type != 'Expenses') {
    common.log("Document type Expenses is missing ");
    return res.status(400).json({ message: 'Document Type is invalid' })
  }

  if (!transaction_id) {
    common.log("transaction Id is missing ");
    return res.status(400).json({ message: 'Document Type is invalid' })
  }

  let search_filter = await UserDocument.find({ user_id: ObjectId(req.payload._id), document_type: document_type, transaction_id: ObjectId(transaction_id), is_delete: false });
  if (!search_filter) {
    res.status(400).json({ message: 'searching data is not avaliable' })
  } else {
    res.status(200).json({ success: true, data: search_filter })
  }


}


const documentSave = (userId, doc, res) => {
  var date;

  if (doc.body.document_type == 'Expenses' && doc.body.transcations_id == "") {
    common.log("Transcation Id is missing ");
    return res.status(400).json({ message: 'Transcation Id is missing' })
  }
  console.log(doc.body.document_type)
  console.log("RateConfirmationDocuments")
  if (!config.CompanyEnums.includes(doc.body.document_type) && !config.ReciptEnums.includes(doc.body.document_type)) {
    common.log("Document type is missing ");
    return res.status(400).json({ message: 'Document Type is invalid' })
  }

  User.findById(userId).exec(async function (err, user) {
    if (err) {

      common.log("Error ", err);
      return res.status(400).send(err)
    }
    if (!user) {
      common.log("Error while  user not found");
      return res.status(400).send({ message: "User not found" })
    }

    var ud = new UserDocument();

    if (doc.body.document_type == 'Expenses' && doc.body.transactions_id) {
      ud.transaction_id = doc.body.transactions_id;
    }
    if (config.CompanyEnums.includes(doc.body.document_type)) {
      ud.company = user.company_id;
      ud.expire_at = doc.body.expirydate
      date = new Date(ud.expire_at);
      date.setDate(date.getDate() - 30);
      let ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(date);
      let mo = new Intl.DateTimeFormat('en', { month: 'short' }).format(date);
      let da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(date);
      date = `${mo}-${da - 1}-${ye}`


    }
    // if (config.ReciptEnums.includes(doc.body.document_type)) {
    //    ud.expire_at= doc.body.expiry_at 
    // } 
    ud.user_id = user._id;
    ud.document_path = doc.file.location;
    ud.s3_key = doc.file.key;
    ud.originalname = doc.file.originalname;
    ud.mimetype = doc.file.mimetype;
    ud.document_type = doc.body.document_type;
    ud.save();
    if (ud.company) {
      emailTemplateService.sendEmail('document-expiration-date-reminder', { user: user, document: { document_type: ud.document_type, originalname: ud.originalname, expire_at: date } }, user.email);
    }

    User.updateOne({ _id: user._id }, { $push: { upload_documents: ud._id } }, function (err) {
      if (err) {
        common.log("Error while update user document", err);
        return res.status(400).send(err.message);
      }
      return res.status(200).send(ud);
    });
  })
}


module.exports.deleteUploadDocument = function (req, res) {
  if (!req.params.id) {
    return res.status(400).send({
      field: 'id',
      message: 'User document id is required'
    })
  }
  try {
    UserDocument.findById(req.params.id).exec(function (err, userDocument) {
      if (err) {
        common.log("Error while delete user document", err);
        return res.status(400).send(err)
      }
      if (!userDocument) {
        common.log("User document not found");
        return res.status(400).send({ message: "User document not found" })
      }

      UserDocument.updateOne({ _id: req.params.id }, { $set: { is_delete: true } }, function (err) {
        if (err) {
          common.log("Error while delete user document", err);
          return res.status(400).send(err.message);
        }
        return res.status(200).send({ message: "User document deleted successfull" });
      });
    })
  }
  catch (err) {
    common.log("Error while delet user document", err);
    res.status(400).send(err)
  }
}

module.exports.downloadFile = function (req, res) {
  if (!req.params.id) {
    return res.status(400).send({ param: 'id', message: 'User Document id is required' });
  }
  try {
    UserDocument.findById(req.params.id).exec(function (err, userDocument) {
      if (err) {
        common.log("Error while get user document detail", err);
        return res.status(400).send(err);
      }
      if (userDocument) {
        s3FileDownload(userDocument, req, res);
      } else {
        common.log("User Document not found");
        return res.status(400).send({ message: "User Document not found" });
      }

    })
  }
  catch (err) {
    common.log("Error while get user document detail", err);
    return res.status(400).send(err);
  }
}



module.exports.downloadZipFile = async function (req, res) {
  let { startDate, endDate, documentIds } = req.body
  let user_documents;
  let query = {}
  let user = ObjectId(req.payload._id)
  try {
    let ids = (documentIds || []).map(function (id) {
      return ObjectId(id)
    })

    // if (ids && ids.length>0 && startDate && endDate) {
    //   query["$and"] = []
    //   query["$and"].push({ _id: { $in: ids }, created_at: { $gte: ISODate(startDate), $lt: ISODate(endDate) }, user_id: user })

    // } 
    // if (ids && ids.length>0) {
    //   query = { _id: { $in: ids }, user_id: user }

    // } 
    // if (startDate && endDate) {
    //   query = { created_at: { $gte: ISODate(startDate), $lt: ISODate(endDate) }, user_id: user }

    // }
    query["$and"] = [];
    query["$and"].push({ user_id: user })
    if (ids && ids.length > 0) {
      query['$and'].push({ _id: { $in: ids } });
    }
    if (startDate && endDate) {
      query['$and'].push({ created_at: { $gte: ISODate(startDate), $lt: ISODate(endDate) } });
    }


    user_documents = await UserDocument.find(query).lean()
    let files = [], archiveFiles = [];
    if (user_documents) {

      for (let i = 0; i < user_documents.length; i++) {
        const user_document = user_documents[i];
        files.push(user_document.s3_key)
        archiveFiles.push(i + "_" + user_document.originalname)

      }

      res.attachment("userDocument.zip");

      s3GetFileStream(files, archiveFiles, res);

    } else {
      return res.json({ message: 'user document is not avaliable for zip' })
    }



  } catch (error) {
    common.log("Error while generate zip content", error)
    return res.status(400).send({ success: false, message: error.message });
  }



}

module.exports.updateSubscription = function (req, res) {
  /*
     * Create a new Customer on Stripe DB
   */
  stripe.customers.create({
    card: req.body.stripeToken,
    email: req.body.email,
  }).then(function (customer) {
    return stripe.subscriptions.create({
      customer: customer.id,
      items: [
        {
          plan: process.env.STRIPE_PLAN || config.STRIPE_PLAN,
        },
      ],
    });
  }).then(function (subscribe) {
    User
      .findOne({ email: req.body.email })
      .exec(function (err, user) {
        if (!err) {
          if (user) {
            var subscription_expire_on = +new Date() + config.freeDaysTrail * 24 * 60 * 60 * 1000;
            User.update({ _id: user.id }, {
              $set: {
                subscription_expire_on: subscription_expire_on,
                subscription_type: 'pro',
                subscription: true,
                stripe_id: subscribe.customer
              }
            }, { upsert: true }, async function (err) {
              if (!err) {
                //subscription confirmed, send activation email.
                const url = req.headers.origin || process.env.PORTAL_URL;
                //common.sendActivationEmail(user, url);
                let activationToken = emailTemplateService.activationTokenGeneration(user);
                let activationLink = portalURL + '/public/activateUser?token=' + activationToken;
                await emailTemplateService.sendEmail('upgrade-subscription', { user }, user.email, activationLink);

                return res.status(200).json({ sucess: true, status: 200 });
              } else {
                common.log("user update error", err);
                res.status(400).send({ sucess: false, message: err.message });
                return;
              }
            });

          } else {
            common.log("user not found");
            return res.status(401).send({ message: 'UnauthorizedError: User not found' });
          }
        } else {
          common.log(err);
          return res.status(404).json(err);
        }

      });

  }).catch(function (err) {
    var errMsg;
    common.log("error updating subscription ", err);
    switch (err.type) {
      case 'StripeCardError':
        // A declined card error
        errMsg = err.message; // => e.g. "Your card's expiration year is invalid."
        break;
      case 'RateLimitError':
        // Too many requests made to the API too quickly
        errMsg = err.message
        break;
      case 'StripeInvalidRequestError':
        // Invalid parameters were supplied to Stripe's API
        errMsg = err.message
        break;
      case 'StripeAPIError':
        // An error occurred internally with Stripe's API
        errMsg = err.message
        break;
      case 'StripeConnectionError':
        // Some kind of error occurred during the HTTPS communication
        errMsg = err.message
        break;
      case 'StripeAuthenticationError':
        // You probably used an incorrect API key
        errMsg = err.message
        break;
      default:
        // Handle any other types of unexpected errors
        break;
    }
    return res.status(404).json(errMsg);
  });
};

module.exports.getProfileUploads = async function (req, res) {
  try {
    var limitResults = 10;
    var skipResults = 1;
    if (req.body.limit != null && req.body.limit != '') {
      limitResults = req.body.limit;
    }

    if (req.body.skip != null && req.body.skip != '') {
      skipResults = (req.body.skip - 1) * limitResults;
    }

    var sort_by = req.body.sort_by || 'created_at';
    var sort_direction = req.body.sort_direction == 'asc' ? 1 : -1;
    var sortObj = {};
    var sort_split = sort_by.split(',');
    for (let index = 0; index < sort_split.length; index++) {
      sortObj[sort_split[index].trim()] = sort_direction;
    }
    let search = { user_id: req.payload._id, company: null, transaction_id: null, is_delete: false };

    let [count, profileDocuments] = await Promise.all([
      UserDocument.count(search),
      UserDocument.find(search).sort(sortObj).skip(skipResults).limit(limitResults).lean(),
    ])
    return res.send({ totalRecords: count, data: profileDocuments })
  }
  catch (err) {
    common.log("Error while get profile upload documents list:-", err);
    return res.status(400).send({ success: false, message: err.message });
  }
}

module.exports.updatePaymentMethod = async function (req, res) {
  try {
    let uid = ''
    if (req.params.id) {
      uid = req.params.id;
    } else {
      uid = req.payload._id
    }
    if (!uid) return res.status(400).send({ success: false, message: "user id is required" });
    let user = await User.findById(uid).populate('subscription_package').lean();
    if (!user) return res.status(400).send({ success: false, message: "User not found" })
    if (user.stripe_id) {
      let stripe_req = [];
      if(req.body.new_pm_id){
        stripe_req.push(stripeHelper.attachPaymentMethod(user.stripe_id,req.body.new_pm_id))
      }
      if(req.body.old_pm_id){
        stripe_req.push(stripeHelper.removePaymentMethod(req.body.old_pm_id))
      }
      const [pm]= await Promise.all(stripe_req);
      let openInvoice=await stripeHelper.customerOpenInvoices(user.stripe_id);
      if(openInvoice && openInvoice.data && openInvoice.data.length>0){
        await stripeHelper.payInvoice(openInvoice.data[0].id,pm.id);
      }
      let payment_method = await stripeHelper.getPaymentMethods(user.stripe_id);
      return res.send({ success: true, data: payment_method });
    } else {
      let trial_for_plan = true;
      let trial_period_days;
      let price_id = user.subscription_package.stripe_price_id;;
      if (user.subscription_expire_on) {
        var currentDate = new Date();
        if (user.subscription_expire_on <= currentDate) {
          trial_for_plan = false;
          price_id = null;
        } else {
          const diffTime = Math.abs(user.subscription_expire_on - currentDate);
          trial_period_days = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
        }
        const customer = await stripeHelper.createCustomer(user.email, user.first_name + ' ' + user.last_name, req.body.new_pm_id);
        const subscription = await stripeHelper.createSubscription(customer.id, user.subscription_package.stripe_plan_id, trial_for_plan, trial_period_days, price_id);
        await User.updateOne({ _id: ObjectId(uid) }, {
          $set: {
            stripe_id: customer.id,
            payment_status: subscription.status
          }
        })
        let payment_method = await stripeHelper.getPaymentMethods(customer.id);
        return res.send({ success: true, data: payment_method });
      }
      return res.status(400).send({ success: false, message: "Stripe information missing" });
    }
  } catch (err) {
    common.log('Error while update payment method :--', err)
    return res.status(400).send({ success: false, message: err.message });
  }
}

/**
 * update user payment method function for admin\
 * @returns 
 */
 module.exports.updatePaymentMethodAdmin = async function (req, res) {
  try {
    let uid = ''
    if (req.params.id) {
      uid = req.params.id;
    }

    if (!uid) return res.status(400).send({ success: false, message: "user id is required" });
    let user = await User.findById(uid).populate('subscription_package').lean();
    if (!user) return res.status(400).send({ success: false, message: "User not found" })
    if (user.stripe_id) {
      let stripe_req = [];
      let _openInvoice={};
      if(req.body.new_pm_id){
        stripe_req.push(stripeHelper.attachPaymentMethod(user.stripe_id,req.body.new_pm_id))
      }
      if(req.body.old_pm_id){
        stripe_req.push(stripeHelper.removePaymentMethod(req.body.old_pm_id))
      }
      const [pm]= await Promise.all(stripe_req);
      let openInvoice=await stripeHelper.customerOpenInvoices(user.stripe_id);
      if(openInvoice && openInvoice.data && openInvoice.data.length>0){
      
        _openInvoice=openInvoice.data[0];
        _openInvoice["invoice_pending"]=true;
        // await stripeHelper.payInvoice(openInvoice.data[0].id,pm.id);

      }
      let payment_method = await stripeHelper.getPaymentMethods(user.stripe_id);
      payment_method["openInvoice"]=_openInvoice;
      return res.send({ success: true, data: payment_method });
    } else {
      let trial_for_plan = true;
      let trial_period_days;
      let price_id = user.subscription_package.stripe_price_id;;
      if (user.subscription_expire_on) {
        var currentDate = new Date();
        if (user.subscription_expire_on <= currentDate) {
          trial_for_plan = false;
          price_id = null;
        } else {
          const diffTime = Math.abs(user.subscription_expire_on - currentDate);
          trial_period_days = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
        }
        const customer = await stripeHelper.createCustomer(user.email, user.first_name + ' ' + user.last_name, req.body.new_pm_id);
        const subscription = await stripeHelper.createSubscription(customer.id, user.subscription_package.stripe_plan_id, trial_for_plan, trial_period_days, price_id);
        await User.updateOne({ _id: ObjectId(uid) }, {
          $set: {
            stripe_id: customer.id,
            payment_status: subscription.status
          }
        })
        let payment_method = await stripeHelper.getPaymentMethods(customer.id);
        return res.send({ success: true, data: payment_method });
      }
      return res.status(400).send({ success: false, message: "Stripe information missing" });
    }
  } catch (err) {
    common.log('Error while update payment method :--', err)
    return res.status(400).send({ success: false, message: err.message });
  }
}

module.exports.updateOldUsersSubscriptionStatus = async function (req, res) {
  try {
    let users = await User.find({
      $and: [
        { stripe_id: { $ne: null } },
        { subscription: true },
        { subscription_type: { $ne: "free" } },
        { role: { $ne: "administrator" } },
        { status: "active" },
        { source_data: { $ne: "posteverywhere" } },
        {
          subscription_expire_on: { $lte: (new Date()).toISOString() }
        }]
    });
    for (let i = 0; i < users.length; i++) {
      const user = users[i];
      try {
        let customer = await stripeHelper.getCustomer(user.stripe_id);
        if (customer && customer.subscriptions && customer.subscriptions.data && customer.subscriptions.data.length > 0) {
          await User.updateOne({ _id: user._id }, {
            $set: {
              subscription_expire_on: new Date(customer.subscriptions.data[0].current_period_end * 1000).toISOString()
            }
          });
        } else {
          await User.updateOne({ _id: user._id }, { $set: { subscription: false } });
        }
      } catch (err) {
        if (err.code == "resource_missing") {
          await User.updateOne({ _id: user._id }, {
            $set: {
              subscription: false,
              stripe_id: null
            }
          })
        } else {
          common.log('Error while get customer detail :--', err)
        }
      }
      await common.snooze(100);
    }
    return res.send({ success: true, count: users.length });
  } catch (err) {
    common.log('Error while updateOldUsersSubscriptionStatus :--', err)
    return res.status(400).send({ success: false, message: err.message });
  }
}

module.exports.cancelUpdateScheduleSubscriptionRequest = async function (req, res) {
  try {
    if (req.body.type != 'free' && req.body.schedule_id) {
      let schedule = await UserStripeSubscriptionSchedule.findById(req.body.schedule_id);
      await stripeHelper.cancelScheduleSubscription(schedule.stripe_schedule_id);
      await User.updateOne({ _id: schedule.user }, {
        $set: {
          schedule: null
        }
      })
      await UserStripeSubscriptionSchedule.updateOne({ _id: ObjectId(req.body.schedule_id) }, {
        $set: {
          is_delete: true
        }
      })
    } else {
       let user_id= req.body.user_id ? req.body.user_id: req.payload._id;
       common.log("user_id",user_id);
      let user = await User.findById(user_id).populate('subscription_package');
      if (user) {
        let customer = await stripeHelper.getCustomer(user.stripe_id);
        if (customer && customer.subscriptions && customer.subscriptions.data && customer.subscriptions.data.length > 0 && user.subscription_package && !user.subscription_package.is_free) {
          await stripeHelper.changeSubscription(customer.subscriptions.data[0].id, user.subscription_package.stripe_plan_id)
        }
        await User.updateOne({ _id: user._id }, {
          $set: {
            subscription: true,
            subscription_cancel_reason: '',
            free_subscription_package_after_cancel_paid: null
          }
        })
      }
    }
    return res.send({ success: true, message: 'Cancel schedule subscription.' });
  } catch (err) {
    common.log('Error while cancelScheduleSubscription :--', err)
    return res.status(400).send({ success: false, message: err.message });
  }
}

/**
 * Payment History api
 */
module.exports.customerPaymentHistory= async (req,res)=>{
  try{
    var uid = req.params.userID
    common.log('userid is ', uid)
    if (!uid || uid == '')
      res.status(400).send({ message: "User ID is missing" });

    let user = await User.findById(uid);
    // console.log("user:_____", user);
    if(user.stripe_id){
/** 
    // let customer_invoices= await stripeHelper.customerInvoices(user.stripe_id);
    // let customer_subscriptions= await stripeHelper.getCustomerSubscriptions(user.stripe_id);
    let customer_details = await stripeHelper.getCustomer(user.stripe_id);
    let events= await stripeHelper.getcustomerEvents(user.stripe_id);
    */
 
     let customer_invoices= await process_invoice_filter(user);
    
    // let events= await stripeHelper.getcustomerEvents(user.stripe_id);
    // let response_object= await processEvents(events,user);
    return res.status(200).send({status: true, data:customer_invoices});
    }else{
      res.status(200).send({status: false,message:"No record Found"});
    }
  }catch(error){
    return res.status(200).send({status:false,message:error.message});
  }
}
/**
 * function to validate email for updation of profile
 * if someone want to update his profile email...
 */

async function validate_update_email(user_email, user_id){

  if(common.validateEmail(user_email)){

    let user_info = await User.findOne({email:user_email});
    // if user email is exist and associated with other user
    if(user_info && user_info._id!=user_id){
      return {success: false, message:"Email is already exist"}
  }

    return {success:true};

  }else{

    return {success: false, message:"Invalid Email"};

  }
};

/**
 * update on stripe customer email
 */
 async function updateEmailOnStripe(email,user){
  if(user.stripe_id){
  let upd_resp= await stripeHelper.updateCustomerEmail(user.stripe_id,email);
  common.log(`customer ${user.stripe_id} email to be updated `,user.email);
  common.log("updated on customer stripe:_____", upd_resp);
  return;
  };
  return;
 }

 /**
  * process events and return neccessary objects 
  */
 async function processEvents(events,user_info){
   try{
     let {data}= events;
     let r_arr=[];
      data.map(async r=>{
         let obj= await build_obj_helper(r,user_info);
         r_arr.push(obj);
   });
      // r_arr.push(data_obj);
      return r_arr;
   }catch(error){
     common.log("ERROR in Process Events function:____", error);
     throw error;
   }
 }


 //const built data object for payment history

 const build_obj_helper= async (r,user_info)=>{
  let {type,created,data:{object}}=r;

  switch(type){
    // Occurs whenever a customer is signed up for a new plan.
    case "customer.subscription.created":
      let _subscribed_pkg_details= await SubscriptionPackages.findOne({stripe_plan_id:object.plan.id});
      return {
         activity: type.split('.').join(' '),
         created:  new Date(created * 1000).toISOString(),
         subscribed_package: _subscribed_pkg_details?.subscription_name,
         s_role : _subscribed_pkg_details?.role,
         user_role: user_info.role,
         plan_details: object.plan,
        //  subscription_details : {}, 
        //  invoice_details : {},
         details:object 
        };
       break;
      
    // case "invoice.finalized":
    //   break;
     // Occurs whenever a new customer is created;          
    case "customer.created":

      return {
        activity: type.split('.').join(' '),
        created:  new Date(created * 1000).toISOString(),
        subscribed_package: user_info.subscription_type,
        user_role: user_info.role, 
        // details:object 
       };    
      break;
     // Occurs whenever any property of a customer changes.
    case 'customer.updated':
      return {
        activity: type.split('.').join(' '),
        created:  new Date(created * 1000).toISOString(),
        subscribed_package: user_info.subscription_type,
        user_role: user_info.role, 
        // details:object 
       }; 
      break;
     // Occurs whenever a coupon is attached to a customer.
    // case "customer.discount.created ":
    //   break;
   // Occurs whenever a coupon is removed from a customer
    // case "customer.discount.deleted":

    //   break;
   // Occurs whenever a customer is switched from one coupon to another.
    // case "customer.discount.updated":
    
    //   break;
   // Occurs whenever a customer's subscription ends.
    // case "customer.subscription.deleted":
    //   break;
   // Occurs three days before a subscription's trial period is scheduled to end, or when a trial is ended immediately (using trial_end=now).
    // case "customer.subscription.trial_will_end":
    //   break;
   // Occurs whenever a subscription changes (e.g., switching from one plan to another, or changing the status from trial to active).
    case "customer.subscription.updated":
      let _subscribed_pkg_detail= await SubscriptionPackages.findOne({stripe_plan_id:object.plan.id});
      return {
         activity: type.split('.').join(' '),
         created:  new Date(created * 1000).toISOString(),
         subscribed_package: _subscribed_pkg_detail?.subscription_name,
         s_role : _subscribed_pkg_detail?.role,
         user_role: user_info.role,
         amount : object.plan.amount/100,
         plan_details: object.plan,
        //  subscription_details : {}, 
        //  invoice_details : {},
        //  details:object 
        };
      break;
     // Occurs whenever a new invoice is created. To learn how webhooks can be used with this event, and how they can affect it, see Using Webhooks with Subscriptions.
      case "invoice.created":
        // let _subscribed_pkg_details= await SubscriptionPackages.findOne({stripe_plan_id:object.plan.id});
        return {
          activity: type.split('.').join(' '),
          created:  new Date(created * 1000).toISOString(),
          subscribed_package: user_info.subscription_type,
          receipt_url: object.hosted_invoice_url,
          amount: object.amount_due/100,
          user_role: user_info.role, 
          // details:object 

         };   
        break;
    // Occurs whenever an invoice payment attempt succeeds or an invoice is marked as paid out-of-band.
    case "invoice.paid":
      return {
        activity: type.split('.').join(' '),
        created:  new Date(created * 1000).toISOString(),
        subscribed_package: user_info.subscription_type,
        receipt_url: object.hosted_invoice_url,
        amount: object.amount_paid/100,
        user_role: user_info.role, 
        // details:object 
       }; 
      break;
     // Occurs whenever an invoice payment attempt fails, due either to a declined payment or to the lack of a stored payment method.
    case "invoice.payment_failed":
      return {
        activity: type.split('.').join(' '),
        created:  new Date(created * 1000).toISOString(),
        subscribed_package: user_info.subscription_type,
        receipt_url: object.hosted_invoice_url,
        amount: object.amount_paid/100,
        user_role: user_info.role, 
        // details:object 
       }; 
      break;
     // Occurs whenever an invoice payment attempt succeeds.
    case "invoice.payment_succeeded":
      return {
        activity: type.split('.').join(' '),
        created:  new Date(created * 1000).toISOString(),
        subscribed_package: user_info.subscription_type,
        receipt_url: object.hosted_invoice_url,
        amount: object.amount_paid/100,
        user_role: user_info.role, 
        // details:object 
       }; 
      break;
    // Occurs whenever an invoice changes (e.g., the invoice amount).
    // case "invoice.updated":
    //   break;
    // Occurs whenever an invoice is voided
    // case "invoice.voided":
    //   break;

     default:
      return {
        activity: type.split('.').join(' '),
        created:  new Date(created * 1000).toISOString(),
        subscribed_package: user_info.subscription_type,
        // s_role : _subscribed_pkg_details?.role,
        user_role: user_info.role,
        // plan_details: object.plan,
        // subscription_details : {}, 
        // invoice_details : {},
        // details:object
       };
       
 };

 }

 //process invoices and return only relevant info
   const process_invoice_filter= async(user_info)=>{
         try{
         let {data}= await stripeHelper.customerInvoices(user_info.stripe_id);
         console.log("invoices ", data.length);
         //let plan of user 
           let plan="";
           let _subscribed_pkg_details=null;
           let arr_details=[];
         if(data.length>0){
             // let _subscribed_pkg_details= await SubscriptionPackages.findOne({stripe_plan_id:object.plan.id});
             data.map(async r=>{
         
               if(r.lines.data[0]?.plan?.id===plan){
                 plan= r.lines.data[0].plan.id;
             _subscribed_pkg_details= await SubscriptionPackages.findOne({stripe_plan_id:object.plan.id});
               }
               // console.log("r",r.lines.data[0]);
                arr_details.push({
                  "invoice_number": r.number,
                  "paid": r.paid,
                  "amount_paid": r.amount_paid/100,
                  "amount_due": r.amount_due/100,
                  "amount_remaining": r.amount_remaining/100,
                  "billing_reason": r.billing_reason,
                  "currency": r.currency,
                  "hosted_invoice_url": r.hosted_invoice_url,
                  "period_end" :  new Date(r.period_end * 1000).toISOString(),
                  "period_start":  new Date(r.period_start * 1000).toISOString(),
                  "status": r.status,
                  "subtotal" : r.subtotal/100,
                  "total": r.total/100,
                  "description": r.lines.data[0].description,
                  "created" : new Date(r.created * 1000).toISOString(),
                  "subscribed_package": _subscribed_pkg_details!==null?_subscribed_pkg_detail?.subscription_name:user_info.subscription_type,
                  "subscription_role" :  _subscribed_pkg_details!==null?_subscribed_pkg_detail?.role:user_info.role,
                  "user_role" : user_info.role
                });
                
             });
         
             return arr_details;
           }else{
             return arr_details;
           } 
         
            }catch(error){
               common.log("ERROR in processing incvoice filter",error)
               throw error;
           }
   }