var _ = require('util');


var mongoose = require('mongoose');
// set Promise provider to bluebird
mongoose.Promise = require('bluebird');
var Load = mongoose.model('Load');
var Truck = mongoose.model('Truck');
var User = mongoose.model('User');
var DatUser = mongoose.model('DatUser');
var config = require('../config/config');
var states_names = require('../helpers/states_name');
var common = require('../helpers/common');
var soap = require('soap');
var NodeGeocoder = require('node-geocoder');
var Randomstring = require("randomstring");
var moment = require('moment');

var options = {
  provider: 'google',
 
  // Optional depending on the providers
  httpAdapter: 'https', // Default
  apiKey: config.googleGoecoder_key, // for Mapquest, OpenCage, Google Premier
  //formatter: 'string',         // 'gpx', 'string', ...
  //formatterPattern: '%p%t%z'
};
 
var geocoder = NodeGeocoder(options);
 

module.exports.login = function(credentials, socket, callback){

  let tokenPrimary = config.DATtokenPrimary;
  let tokenSecondary = config.DATtokenSecondary;
  let tokenExpiration = config.DATtokenExpiration;

   if(Date.now() < Date.parse(tokenExpiration))
   {
    return callback({tokenPrimary : config.DATtokenPrimary, tokenSecondary : config.DATtokenSecondary,
     tokenExpiration : config.DATtokenExpiration });
    }

    var url = 'http://cnx.test.dat.com:9280/wsdl/TfmiFreightMatching.wsdl';
    var args = { 
              
            "loginOperation": {
                "loginId": "jmg_cnx1",
                "password": "logistics",
                "thirdPartyId": "ForkFreight-API",
                "apiVersion": "2"
            }  
         
    }
     //{loginId: 'jmg_cnx1', password: 'logistics', thirdPartyId : "ForkFreight-API",apiVersion : 2};
    soap.createClient(url, function(err, client) {
        if(err) common.log("error message: " , err);
        common.log(client);
        client.Login(args, function(err, result) {
            if(err) 
            {
              common.log("error message: " , err);
            }
            else{
            common.log(result);
            let tokenPrimary = result.loginResult.loginSuccessData.token.primary;
            let tokenSecondary = result.loginResult.loginSuccessData.token.secondary;
            let tokenExpiration = result.loginResult.loginSuccessData.expiration;
            config.DATtokenPrimary = tokenPrimary;
            config.DATtokenSecondary = tokenSecondary;
            config.DATtokenExpiration = tokenExpiration;
            //execute search with the login token.
            common.log(config);
            callback(result);
            }
        });
    });
};

function validateSearchFilter(search_object, socket)
{
  return new Promise(function(resolve,reject){
      var search_options = {};
      search_options["is_opened"] = true;
      search_options["created_by"] = {$ne : socket.decoded._id};

      if(search_object !== undefined) {

        if(search_object.pickup_address && search_object.pickup_address.indexOf(',') !== -1)
        {
          let address_parts = search_object.pickup_address.split(',');

          if (address_parts.length >1)
          {
            if(address_parts[0] && address_parts[0]!="") search_options.pickup_address = address_parts[0].trim();
            if(address_parts[0] && address_parts[0]!="") search_options.pickup_city = address_parts[0].trim();
            if(address_parts[1] && address_parts[1]!="") search_options.pickup_state = address_parts[1].trim();
          }
        }
        else 
        {
          // Pickup options
          if(search_object.pickup_address && search_object.pickup_address != "") {
            search_options["pickup_address"] = new RegExp(search_object.pickup_address, "i")
          }

          if(search_object.pickup_city && search_object.pickup_city != "") {
            search_options["pickup_city"] = search_object.pickup_city.trim();
          }

          if(search_object.pickup_state && search_object.pickup_state != "") {
            search_options["pickup_state"] = search_object.pickup_state.trim();
          }

          if(search_object.pickup_zip && search_object.pickup_zip != "") {
            search_options["pickup_zip"] = search_object.pickup_zip.trim();
          }

        }

        if(search_object.name && search_object.name != "") {
          search_options["name"] = new RegExp(search_object.name, "i")
        }

        if(search_object.weight && search_object.weight != "") {
          search_options["weight"] = search_object.weight;
        }

        if(search_object.pickup_date && search_object.pickup_date != "") {
          search_options["pickup_date"] = search_object.pickup_date;
        }

        if(search_object.delivery_date && search_object.delivery_date != "") {
          search_options["delivery_date"] = search_object.delivery_date.trim();
        }

        

        // Delivery options
        if(search_object.delivery_address && search_object.delivery_address.indexOf(',') !== -1)
        {
          let address_parts = search_object.delivery_address.split(',');

          if (address_parts.length >1)
          {
            if(address_parts[0] && address_parts[0]!="")search_options.delivery_address = address_parts[0].trim();
            if(address_parts[0] && address_parts[0]!="")search_options.delivery_city = address_parts[0].trim();
            if(address_parts[1] && address_parts[1]!="")search_options.delivery_state = address_parts[1].trim();
          }
        }
        else 
        {
          if(search_object.delivery_address && search_object.delivery_address != "") {
            search_options["delivery_address"] = new RegExp(search_object.delivery_address, "i")
          }

          if(search_object.delivery_city && search_object.delivery_city != "") {
            search_options["delivery_city"] = search_object.delivery_city.trim();
          }

          if(search_object.delivery_state && search_object.delivery_state != "") {
            search_options["delivery_state"] = search_object.delivery_state.trim();
          }

          if(search_object.delivery_zip && search_object.delivery_zip != "") {
            search_options["delivery_zip"] = search_object.delivery_zip.trim();
          }
        }
        //resolve(search_options);
      }
      resolve(search_options);
    }) 
}


function getCoordinates(address,city,state)
{
  return new Promise(function(resolve, reject){

    let pickup_coordinates = [];
    if (address && address !== 'undefined' )
    { 
      pickup_coordinates.push(address)
    }
    
    if (city && city !== 'undefined' )
    { 
      pickup_coordinates.push(city)
    }
    
    if (state && state !== 'undefined' )
    { 
      pickup_coordinates.push(state)
    }
   
    
    //convert address to coordinates , city and state
    let pickup_address_full = pickup_coordinates.join();
    let geocode_long;
    let geocode_lat;
    let geocode = {};
    
    geocoder.geocode(pickup_address_full, function(err,res) {
      if(err) {
        common.log(err);
        reject(err);
      }
      else
      {    
        geocode_lat = (res && res.length >0)? res[0].latitude : " ";
        geocode_long = (res && res.length >0)? res[0].longitude : " ";
        geocode["geocode_lat"] = geocode_lat;
        geocode["geocode_long"] = geocode_long;
        geocode["state_code"] = (res[0].administrativeLevels)? res[0].administrativeLevels.level1short : "";
        geocode["country_code"] = (res[0].countryCode)? res[0].countryCode : "US";
        geocode["city"] = (res[0].city)? res[0].city : (res[0].extra && res[0].extra.neighborhood)? res[0].extra.neighborhood : "";
        common.log(res);
        resolve(geocode);
      }
    })
  })
}
  


function buildSearchQuery(search_options)
{
  var args = { 
    "createSearchOperation": {
      "criteria": {
          "assetType": "Shipment",
          "equipmentTypes": ["Van", "Flatbed", "Reefer"],
          "ageLimitMinutes": 1200, //5940,
          "origin": {
            "radius":{
              "place":{
                "namedCoordinates": {
                  "latitude": search_options.pickup_coordinates.geocode_lat,
                  "longitude": search_options.pickup_coordinates.geocode_long,
                  "city": search_options.pickup_coordinates.city,
                  "stateProvince": search_options.pickup_coordinates.state_code
                }
            }
              ,
              "radius" : {
                "miles" : 50,
                "method" : "Road"
              }
            }
          },
            "destination": {
              "radius":{
                "place": {
                "namedCoordinates": {
                  "latitude": search_options.delivery_coordinates.geocode_lat,
                  "longitude": search_options.delivery_coordinates.geocode_long,
                  "city": search_options.delivery_coordinates.city,
                  "stateProvince": search_options.delivery_coordinates.state_code
                }
              },
              "radius" : {
                "miles" : 50, //TODO ask the client to pass in the search radius. default to 50 miles now.
                "method" : "Road"
              }
              }
            },
          //"availability": {
          //   "earliest": "2018-01-10T21:47:07.718Z",
          //   "latest": "2018-01-14T21:47:07.718Z"
          //},
          "includeLtls": "true",
          "includeFulls": "true",            
          "excludeOpenDestinationEquipment": "false"
      },
      "sortOrder": "None",
      "includeSearch": "true"
    }   
  }

  return args;

};


function CreateDatLoadObject(Datload)
{
  return new Promise(function(resolve, reject){

  let load = new Load();
  //load.name = Datload.asset.assetId;

  geocoder.reverse({lat:Datload.asset.shipment.origin.namedCoordinates.latitude, lon:Datload.asset.shipment.origin.namedCoordinates.longitude}, function(err, res){

    if(err) {
      common.log(err);
      reject(err);
    }
   
    common.log(res);
    load.pickup_addresses =  [];
    load.pickup_cities = [Datload.asset.shipment.origin.namedCoordinates.city];
    load.pickup_states = [Datload.asset.shipment.origin.namedCoordinates.stateProvince]; // need to convert state code to full name
    load.pickup_lat_long = { type: "MultiPoint", coordinates: [ [parseFloat(Datload.asset.shipment.origin.namedCoordinates.longitude),parseFloat(Datload.asset.shipment.origin.namedCoordinates.latitude)]]};
        
    load.pickup_zips = [parseInt(res[0].zipcode)];//TODO convert coordinates to address and zip;
    if(res[0].administrativeLevels.level1long) load.pickup_states.push(res[0].administrativeLevels.level1long);
    if(res[0].administrativeLevels.level2long) load.pickup_states.push(res[0].administrativeLevels.level2long);
    
    
    geocoder.reverse({lat:Datload.asset.shipment.destination.namedCoordinates.latitude, lon:Datload.asset.shipment.destination.namedCoordinates.longitude}, function(err, res2){

      if(err) {
        common.log(err);
        reject(err);
      }

      common.log(res2);      
      load.delivery_addresses = [];
      load.delivery_cities = [Datload.asset.shipment.destination.namedCoordinates.city];
      load.delivery_states = [Datload.asset.shipment.destination.namedCoordinates.stateProvince];
      //load.delivery_zips = data.delivery_zips;//TODO convert coordinates to address and zip;
      
      load.delivery_lat_long = { type: "MultiPoint", coordinates: [[parseFloat(Datload.asset.shipment.destination.namedCoordinates.longitude),parseFloat(Datload.asset.shipment.destination.namedCoordinates.latitude)]]};
      load.delivery_zips = [parseInt(res2[0].zipcode)];//TODO convert coordinates to address and zip;
      if(res2[0].administrativeLevels.level1long) load.delivery_states.push(res2[0].administrativeLevels.level1long);
      if(res2[0].administrativeLevels.level2long) load.delivery_states.push(res2[0].administrativeLevels.level2long);
      
      
      load.description = Datload.asset.assetId;
        load.load_weight = (Datload.asset.dimensions)? Datload.asset.dimensions.weightPounds : 0;
        load.unit_of_measurement = "pounds";
        load.load_length = (Datload.asset.dimensions)? Datload.asset.dimensions.lengthFeet : 0;
        load.pickup_date = (Datload.asset.availability)?Datload.asset.availability.earliest : new Date();
        load.delivery_date =(Datload.asset.availability)? Datload.asset.availability.latest : new Date();
        
        load.delivery_address = Datload.asset.shipment.destination.namedCoordinates.city;
        load.delivery_city = Datload.asset.shipment.destination.namedCoordinates.city;
        load.delivery_state = Datload.asset.shipment.destination.namedCoordinates.stateProvince;
        //load.delivery_zip = 12345//TODO convert coordinates to address and zip;


        load.phone = (Datload.callback && Datload.callback.phone) ? Datload.callback.phone.phone.number : null;
        load.expire_on = Datload.asset.status.endDate;
        load.is_opened = (!Datload.asset.status.expired);
        load.factor_load = true;
        load.internal = false;
        load.source = "DAT";

        
        load.equipment_type = (Datload.asset && Datload.asset.shipment)? Datload.asset.shipment.equipmentType : null;
        load.no_of_count = (Datload.asset)? parseInt(Datload.asset.count) : 0;
        load.target_rate = (Datload.asset && Datload.asset.shipment && Datload.asset.shipment.rate )? parseFloat(Datload.asset.shipment.rate.baseRateDollars) : 0.00;


        load.ready_date = (Datload.asset.availability)? Datload.asset.availability.earliest : null;
        load.pickup_date = (Datload.asset.availability)? Datload.asset.availability.latest : null;
        load.delivery_date = (Datload.asset.availability)? Datload.asset.availability.earliest : null;

        
        resolve(load);

    })

  })
})

}

module.exports.findLoads = function(search_object, socket,io) {
  'use strict';
  var search_options ={};
 var searchQuery;

    return new Promise(
      function(resolve, reject){

        search_options = common.validateSearchFilter(search_object);
       return getCoordinates(search_object.pickup_address,_.isArray(search_object.pickup_cities)?search_object.pickup_cities[0] : search_object.pickup_cities,
       _.isArray(search_object.pickup_states)?search_object.pickup_states[0] : search_object.pickup_states)
    .then(function(pickup_coordinates)
      {
        search_options["pickup_coordinates"] = pickup_coordinates ;//deliveryCoordinates;
       return getCoordinates(search_object.delivery_address,_.isArray(search_object.delivery_cities)?search_object.delivery_cities[0] : search_object.delivery_cities,
       _.isArray(search_object.delivery_states)?search_object.delivery_states[0]:search_object.delivery_states)
      })
    .then(function(deliveryCoordinates){        
        search_options["delivery_coordinates"] = deliveryCoordinates;
        searchQuery = buildSearchQuery(search_options);
        return searchQuery;
      })
      .then(function(searchQuery){

        // get DAT Search account
        DatUser.find({"environment": "test", "role": "search"})
        .exec(function(err,datuser){
    
          if (err) {
            common.log(err)
            reject(err);
          }
          else if(!datuser || datuser.length == 0 )
          {
            reject("error: DAT user not found", datuser);
          }
          
          return datuser;
        })
        .then(function(datuser) {

           //login to DAT API
          DATLoadLogin(datuser[0], function (user){

          var url = 'http://cnx.test.dat.com:9280/wsdl/TfmiFreightMatching.wsdl';
          //var client;
        //connect to api
          //{loginId: 'jmg_cnx1', password: 'logistics', thirdPartyId : "ForkFreight-API",apiVersion : 2};
          soap.createClient(url, function(err, client) {
            if(err)
            {
              common.log("error message: " , err);
              return callback(err, null);
            } 
    
            // common.log(client);
            //add authentication token{}
            client.addSoapHeader({
              "headers:sessionHeader": {
                "headers:sessionToken": {
                    "types:primary": user.primaryToken,
                    "types:secondary": user.secondaryToken
                }
              }
              });
              

              //execute search
              client.CreateSearch(searchQuery, function(err, result) {
                  
                if(err)
                {
                  common.log("error message: " , err);
                  reject(err);
                } 
                common.log(result);          
                //execute search with the login token.
                var loads = [];
                if(result)
                {
                    if(result.createSearchResult && result.createSearchResult.createSearchSuccessData && result.createSearchResult.createSearchSuccessData.totalMatches > 0 ){
                        result.createSearchResult.createSearchSuccessData.matches.forEach(function(Datload) {
                           
                          var load = new Load();
                          var newload =  CreateDatLoadObject(Datload);                   

                          newload.then(function(res){
                            load = res;

                            //find user / broker in our system
                            //we want to reach out later and get their actual details and convince them to signup
                            var useremail = "info@forkfreight.com"; //(Datload.callback && Datload.callback.email) ?
                                                  
                            var userpromise = User.find({email: useremail}).exec();

                            return userpromise;
                          })
                          .then(function(loaduser){
                              if(loaduser && loaduser.length > 0 )
                              {
                                //load.created_by = loaduser[0];
                                return loaduser[0];
                              }
                              else
                              {
                                var loaduser = new User();
                                loaduser.first_name = (Datload.callback && Datload.callback.name) ?Datload.callback.name.firstName : null;
                                loaduser.last_name = (Datload.callback && Datload.callback.name) ? Datload.callback.name.lastName : null;
                                
                                loaduser.email=(Datload.callback) ? Datload.callback.userId + "@forkfreight.com" : Randomstring.generate() + "@forkfreight.com";
                                loaduser.company =(Datload.callback) ?  Datload.callback.companyName : "DAT";
                                loaduser.address = " nw 1 st";
                                loaduser.phone = (Datload.callback.phone) ? Datload.callback.phone.phone.number : null;
                                loaduser.mc_number = (Datload.dotIds) ?  Datload.dotIds.brokerMcNumber: null;                    
                              
                                return loaduser.save();
                              }                       
                            }).then(function(loaduser){

                              if(loaduser)
                              {
                                load.created_by = loaduser;
                                //return load;
                                load.save();
                                common.log(load);
                                loads.push(load); //TODO: Make the caller in server.js use a promise
                                io.emit('loads_response', {type: 'new', data:load});
                              }
                              
                            })
                            .catch(function(err){
                              // just need one of these
                              common.log('error:', err);
                              resolve(err);
                            });
                      });

                      resolve(loads);
                    }
                }
                else
                { 
                  resolve(null);
                }
            });                      
       });

    })
 
  })
  .catch(function(err){
    common.log(err);
    reject(err);
  })
})
})
}  



function DATLoadLogin(credentials, callback)
{
  let tokenPrimary = credentials.primaryToken;
  let tokenSecondary = credentials.secondaryToken;
  let tokenExpiration = credentials.tokenexpiration;

   if(Date.now() <= Date.parse(tokenExpiration))
   {
    return callback(credentials);
   }

    var url = 'http://cnx.test.dat.com:9280/wsdl/TfmiFreightMatching.wsdl';
    var args = { 
              
            "loginOperation": {
                "loginId": credentials.loginId,// "jmg_cnx1",
                "password":credentials.password, // "logistics",
                "thirdPartyId": "ForkFreight-API",
                "apiVersion": "2"
            }  
         
    }
     //{loginId: 'jmg_cnx1', password: 'logistics', thirdPartyId : "ForkFreight-API",apiVersion : 2};
    soap.createClient(url, function(err, client) {
        if(err) 
        {
          common.log("error message: " , err);
          //reject(err);
        }
        common.log(client);
        client.Login(args, function(err, result) {
            if(err) 
            {
              common.log("error message: " , err);
              //reject(err);
            }
            else{
            common.log(result);
            let tokenPrimary = result.loginResult.loginSuccessData.token.primary;
            let tokenSecondary = result.loginResult.loginSuccessData.token.secondary;
            let tokenExpiration = result.loginResult.loginSuccessData.expiration;
            credentials.primaryToken = tokenPrimary;
            credentials.secondaryToken = tokenSecondary;
            credentials.tokenexpiration = tokenExpiration;
            //execute search with the login token.
            common.log(credentials);
            DatUser.update({_id: credentials._id}, credentials, {upsert: true}, function(err, user){

              if(err) { common.log(err);}
              else
              {
                 return callback(credentials);
              }
            });

            }
        });
    });

}


module.exports.postLoad = function(data, socket, callback) {

  'use strict';
  if (!socket.decoded._id) {
    socket.emit('add_load_response', {success: false, message: "UnauthorizedError: Private action"});
  } else {

  //   if(!data.name || !data.description 
  //   || !data.weight || !data.unit_of_measurement
  //   || !data.pickup_date || !data.delivery_date
  //   || !data.pickup_address || !data.pickup_city || !data.pickup_state || !data.pickup_zip 
  //   || !data.delivery_address || !data.delivery_city || !data.delivery_state || !data.delivery_zip
  //   || !data.phone //|| !data.images[0]
  // ) {
  //     socket.emit('add_load_response', {success: false, message: "All fields are required"});
  //     return;
  //   }

  var pickup_coordinates;
  var delivery_coordinates
  var pickup = getCoordinates(_.isArray(data.pickup_addresses)?data.pickup_addresses[0] : null,_.isArray(data.pickup_cities)?data.pickup_cities[0] : data.pickup_cities,
  _.isArray(data.pickup_states)?data.pickup_states[0] : data.pickup_states);

  pickup.then(function(res){
    pickup_coordinates = res;

    return getCoordinates(_.isArray(data.delivery_addresses)?data.delivery_addresses[0] : data.delivery_address,_.isArray(data.delivery_cities)?data.delivery_cities[0] : data.delivery_cities,
    _.isArray(data.delivery_states)?data.delivery_states[0] : data.delivery_states)
  }).then(function(res2){

      delivery_coordinates = res2;

      var load = new Load();
      //load.name = data.name;
      load.pickup_addresses = data.pickup_addresses;
      load.pickup_cities = data.pickup_cities;
      load.pickup_states = data.pickup_states;
      load.pickup_zips = data.pickup_zips;
      load.pickup_lat_long = { type: "MultiPoint", coordinates: data.pickup_lat_long};
  
      load.delivery_addresses = data.delivery_addresses;
      load.delivery_cities = data.delivery_cities;
      load.delivery_states = data.delivery_states;
      load.delivery_zips = data.delivery_zips;
      // load.delivery_lat_long = data.delivery_lat_long;
      load.delivery_lat_long = { type: "MultiPoint", coordinates: data.delivery_lat_long};
  
      load.equipment_type = data.equipment_type;
      load.no_of_count = data.no_of_count;
      load.target_rate = data.target_rate;
  
      load.load_weight = data.load_weight;
      load.load_length = data.load_length;
      //load.unit_of_measurement = data.unit_of_measurement;
  
      load.ready_date = data.ready_date;
      load.pickup_date = data.pickup_date;
      load.delivery_date = data.delivery_date;
  
      load.description = data.description;
      load.post_to = data.post_to;
  
      var postersReferenceId = Randomstring.generate(8);
  
       // get DAT Search account
      DatUser.find({"environment": "test", "role": "post"})
      .exec(function(err,datuser){
      
        if (err) {common.log(err)
        }else if
        //return if dat user not found
        (!datuser || datuser.length == 0 )
        {
        return callback(datuser);
        }
        //login to DAT API
        DATLoadLogin(datuser[0], function (user){
  
        var url = 'http://cnx.test.dat.com:9280/wsdl/TfmiFreightMatching.wsdl';
  
        var args = { 
          
            "postAssetOperations": {
              "shipment": {
                  "equipmentType":"Flatbed" ,//load.equipment_type,
                  "origin": {
                    "cityAndState": {
                        "city": pickup_coordinates.city,
                        "stateProvince": pickup_coordinates.state_code
                    }
                  },
                  "destination": {
                    "cityAndState": {
                        "city": delivery_coordinates.city,
                        "stateProvince": delivery_coordinates.state_code
                    }
                  },
                  "rate": {
                    "baseRateDollars": 2000,
                    "rateBasedOn": "Flat",
                    "rateMiles": 500
                  }
              },
              
              "postersReferenceId":  postersReferenceId,
              "ltl": "false",
              "comments": load.description,
              "count": load.no_of_count !== null ? load.no_of_count  :"1",
              "dimensions": {
                  "lengthFeet": load.load_length,
                  "weightPounds": load.load_weight,
                  "heightInches": 80,
                  "volumeCubicFeet": 3000
              },
              "stops": "1",
              //"availability": {
              //    "earliest": load.pickup_date,
             //     "latest": load.delivery_date
             // },
              
              "includeAsset": "true",
              "postToExtendedNetwork": "false"
            }
        }
      //connect to api
      //{loginId: 'jmg_cnx1', password: 'logistics', thirdPartyId : "ForkFreight-API",apiVersion : 2};
      soap.createClient(url, function(err, client) {
      if(err) common.log("error message: " , err);
      common.log(client);
      //add authentication token
      client.addSoapHeader({
        "headers:sessionHeader": {
          "headers:sessionToken": {
              "types:primary": user.primaryToken,
              "types:secondary": user.secondaryToken
          }
        }
      });
      //execute search
      client.PostAsset(args, function(err, result) {
          if(err) 
          {
            common.log("error message: " , err);
            return callback(result);
          }
          else{
          common.log(result); 
                   
          //execute search with the login token.
          return callback(result);
          }
      });
    });
  
  });
  
  }); 

  })

    
    
    
} 
  



};


module.exports.editLoad = function(load, data, socket, callback) {

  'use strict';
  if (!socket.decoded._id) {
    socket.emit('add_load_response', {success: false, message: "UnauthorizedError: Private action"});
  } else {

  var pickup_coordinates;
  var delivery_coordinates
  var pickup = getCoordinates(_.isArray(data.pickup_addresses)?data.pickup_addresses[0] : null,_.isArray(data.pickup_cities)?data.pickup_cities[0] : data.pickup_cities,
  _.isArray(data.pickup_states)?data.pickup_states[0] : data.pickup_states);

  pickup.then(function(res){
    pickup_coordinates = res;

    return getCoordinates(_.isArray(data.delivery_addresses)?data.delivery_addresses[0] : data.delivery_address,_.isArray(data.delivery_cities)?data.delivery_cities[0] : data.delivery_cities,
    _.isArray(data.delivery_states)?data.delivery_states[0] : data.delivery_states)
  }).then(function(res2){

      delivery_coordinates = res2;

      /**var load = new Load();
      //load.name = data.name;
      load.pickup_addresses = data.pickup_addresses;
      load.pickup_cities = data.pickup_cities;
      load.pickup_states = data.pickup_states;
      load.pickup_zips = data.pickup_zips;
      load.pickup_lat_long = { type: "MultiPoint", coordinates: data.pickup_lat_long};
  
      load.delivery_addresses = data.delivery_addresses;
      load.delivery_cities = data.delivery_cities;
      load.delivery_states = data.delivery_states;
      load.delivery_zips = data.delivery_zips;
      // load.delivery_lat_long = data.delivery_lat_long;
      load.delivery_lat_long = { type: "MultiPoint", coordinates: data.delivery_lat_long};
  
      load.equipment_type = data.equipment_type;
      load.no_of_count = data.no_of_count;
      load.target_rate = data.target_rate;
  
      load.load_weight = data.load_weight;
      load.load_length = data.load_length;
      //load.unit_of_measurement = data.unit_of_measurement;
  
      load.ready_date = data.ready_date;
      load.pickup_date = data.pickup_date;
      load.delivery_date = data.delivery_date;
  
      load.description = data.description;
      load.post_to = data.post_to;
  
      var postersReferenceId = Randomstring.generate(8);

      ***/
  
       // get DAT Search account
      DatUser.find({"environment": "test", "role": "post"})
      .exec(function(err,datuser){
      
        if (err) {common.log(err)
        }else if
        //return if dat user not found
        (!datuser || datuser.length == 0 )
        {
        return callback(datuser);
        }
        //login to DAT API
        DATLoadLogin(datuser[0], function (user){
  
        var url = 'http://cnx.test.dat.com:9280/wsdl/TfmiFreightMatching.wsdl';
  
        var args = { 
          
            "updateAssetOperation": {
              "assetId" : load.datAssetId,
              //"postersReferenceId": load.postersReferenceId,
              "shipmentUpdate": {
                "ltl": "false",
                "comments": load.description,
                "count": load.no_of_count !== null ? load.no_of_count  :"1",
                "dimensions": {
                    "lengthFeet": load.load_length,
                    "weightPounds": load.load_weight,
                    "heightInches": 80,
                    "volumeCubicFeet": 3000
                },
                "stops": "1"
              }
            }
        }
      //connect to api
      //{loginId: 'jmg_cnx1', password: 'logistics', thirdPartyId : "ForkFreight-API",apiVersion : 2};
      soap.createClient(url, function(err, client) {
      if(err) common.log("error message: " , err);
      common.log(client);
      //add authentication token
      client.addSoapHeader({
        "headers:sessionHeader": {
          "headers:sessionToken": {
              "types:primary": user.primaryToken,
              "types:secondary": user.secondaryToken
          }
        }
      });
      //execute search
      client.UpdateAsset(args, function(err, result) {
          if(err) 
          {
            common.log("error message: " , err);
            return callback(result);
          }
          else{
          common.log(result); 
                   
          //execute search with the login token.
          return callback(result);
          }
      });
    });
  
  });
  
  }); 

  })   
} 
};

module.exports.deleteLoad = function(load, data, socket, callback) {

  'use strict';
  if (!socket.decoded._id) {
    socket.emit('add_load_response', {success: false, message: "UnauthorizedError: Private action"});
  } else {

  var pickup_coordinates;
  var delivery_coordinates
  var pickup = getCoordinates(_.isArray(data.pickup_addresses)?data.pickup_addresses[0] : null,_.isArray(data.pickup_cities)?data.pickup_cities[0] : data.pickup_cities,
  _.isArray(data.pickup_states)?data.pickup_states[0] : data.pickup_states);

  pickup.then(function(res){
    pickup_coordinates = res;

    return getCoordinates(_.isArray(data.delivery_addresses)?data.delivery_addresses[0] : data.delivery_address,_.isArray(data.delivery_cities)?data.delivery_cities[0] : data.delivery_cities,
    _.isArray(data.delivery_states)?data.delivery_states[0] : data.delivery_states)
  }).then(function(res2){

      delivery_coordinates = res2;

        
       // get DAT Search account
      DatUser.find({"environment": "test", "role": "post"})
      .exec(function(err,datuser){
      
        if (err) {common.log(err)
        }else if
        //return if dat user not found
        (!datuser || datuser.length == 0 )
        {
        return callback(datuser);
        }
        //login to DAT API
        DATLoadLogin(datuser[0], function (user){
  
        var url = 'http://cnx.test.dat.com:9280/wsdl/TfmiFreightMatching.wsdl';
  
        var args = { 
          
            "deleteAssetsByAssetIds": {
              "assetId" : load.assetId
            }
        }
      //connect to api
      //{loginId: 'jmg_cnx1', password: 'logistics', thirdPartyId : "ForkFreight-API",apiVersion : 2};
      soap.createClient(url, function(err, client) {
      if(err) common.log("error message: " , err);
      common.log(client);
      //add authentication token
      client.addSoapHeader({
        "headers:sessionHeader": {
          "headers:sessionToken": {
              "types:primary": user.primaryToken,
              "types:secondary": user.secondaryToken
          }
        }
      });
      //execute search
      client.DeleteAsset(args, function(err, result) {
          if(err) 
          {
            common.log("error message: " , err);
            return callback(result);
          }
          else{
          common.log(result); 
                   
          //execute search with the login token.
          return callback(result);
          }
      });
    });
  
  });
  
  }); 

  })   
} 
};



module.exports.findTrucks = function(search_object, socket, callback) {
  'use strict';
  var search_options = {};
  search_options["is_opened"] = true;
  search_options["created_by"] = {$ne : socket.decoded._id};

  if(search_object !== undefined) {

    if(search_object.name && search_object.name != "") {
      search_options["name"] = new RegExp(search_object.name, "i")
    }

    if(search_object.dm_length && search_object.dm_length != "") {
      search_options["dm_length"] = search_object.dm_length;
    }

    if(search_object.dm_height && search_object.dm_height != "") {
      search_options["dm_height"] = search_object.dm_height;
    }

    if(search_object.dm_weight && search_object.dm_weight != "") {
      search_options["dm_weight"] = search_object.dm_weight;
    }

    if(search_object.dm_volume && search_object.dm_volume != "") {
      search_options["dm_volume"] = search_object.dm_volume;
    }

    // Origin 
    if(search_object.origin_address && search_object.origin_address != "") {
      search_options["origin_address"] = new RegExp(search_object.origin_address, "i")
    }

    if(search_object.origin_city && search_object.origin_city != "") {
      search_options["origin_city"] = search_object.origin_city;
    }

    if(search_object.origin_state && search_object.origin_state != "") {
      search_options["origin_state"] = search_object.origin_state;
    }

    if(search_object.origin_zip && search_object.origin_zip != "") {
      search_options["origin_zip"] = search_object.origin_zip;
    }

    // Destination
    if(search_object.destination_type && search_object.destination_type != "") {

      if(search_object.destination_type == 'location') {

        if(search_object.destination_address && search_object.destination_address != "") {
          search_options["destination_address"] = new RegExp(search_object.destination_address, "i")
        }

        if(search_object.destination_city && search_object.destination_city != "") {
          search_options["destination_city"] = search_object.destination_city;
        }

        if(search_object.destination_state && search_object.destination_state != "") {
          search_options["destination_state"] = search_object.destination_state;
        }

        if(search_object.destination_zip && search_object.destination_zip != "") {
          search_options["destination_zip"] = search_object.destination_zip;
        }

      } else {
        search_options["destination_type"] = search_object.destination_type;
      }
    }
  
  }
  Truck.find(search_options)
  .populate('created_by')    
  .exec(function(err, trucks) {
    callback(trucks);
  });

};


module.exports.postTruck = function(data, socket) {

  'use strict';
  
  if (!socket.decoded._id) {
    socket.emit('add_truck_response', {success: false, message: "UnauthorizedError: Private action"});
  } else {
    if(!data.name || !data.description 
    || !data.dm_length || !data.dm_height || !data.dm_weight 
    || !data.dm_volume || !data.eq_type || !data.is_ltl 
    || !data.sp_count || !data.no_of_stops || !data.availability
    || !data.origin_address || !data.origin_city || !data.origin_state || !data.origin_zip 
    || !data.destination_type || (data.destination_type == 'location' && (!data.destination_address || !data.destination_city || !data.destination_state 
    || !data.destination_zip)) || !data.phone //|| !data.images[0]
  ) {
      socket.emit('add_truck_response', {success: false, message: "All fields are required"});
      return;
    }

    var truck = new Truck();

    truck.name = data.name;
    truck.description = data.description;

    truck.dm_length = data.dm_length;
    truck.dm_height = data.dm_height;
    truck.dm_weight = data.dm_weight;
    truck.dm_volume = data.dm_volume;

    truck.eq_type = data.eq_type;
    truck.is_ltl = data.is_ltl;
    truck.sp_count = data.sp_count;
    truck.no_of_stops = data.no_of_stops;
    truck.availability = data.availability;

    truck.origin_address = data.origin_address;
    truck.origin_city = data.origin_city;
    truck.origin_state = data.origin_state;
    truck.origin_zip = data.origin_zip;
    
    truck.destination_type = data.destination_type;
    truck.destination_address = data.destination_address;
    truck.destination_city = data.destination_city;
    truck.destination_state = data.destination_state;
    truck.destination_zip = data.destination_zip;
    
    truck.phone = data.phone;
    User.findOne({_id : socket.decoded._id}, function(err, user) {

      truck.created_by = user;
      var images = data.images;

  if(images[0] != "" && images[0] != null){
        common.SaveImageToDisk(images[0], function (image_url){
      
          truck.images = [];
          if(image_url != "" && image_url != null) {
            truck.images.push(image_url);
          }
          addTruckFun(truck,socket);
        });
      }
      else if(data.image_url){
            truck.images=data.image_url;
            addTruckFun(truck,socket);
      }else{
         addTruckFun(truck,socket);
      }   
    
    });
  
  }

};

module.exports.Heatmapdata  = async function (equipment) {
 

  var usastates = states_names.usAllStates();

  search_options = [];
  var equipmentOptions = [];
  var equipmentType = equipment["type"];
 
  if(equipmentType =="Flatbed"){
   equipmentOptions.push("Flatbed/Step Deck","Flatbed/Van","Flatbed/Van/Reefer","B-Train Flatbed","Flatbed with sides",
   "Flatbed with Tarps","Flatbed with Pallet Exchange","Hazmat Flatbed","Maxi Flatbed","Team Flatbed","Hotshot Flatbed")
  }

  if(equipmentType == "Step Deck"){
   equipmentOptions.push("Flatbed/Step Deck","Hotshot Step Deck","Removable Gooseneck")
 }

 if(equipmentType == "Reefer"){
   equipmentOptions.push("Reefer with Pallet Exchange","Hazmat Reefer","Van/Reefer","Flatbed/Van/Reefer")
 }

 if(equipmentType == "Van")
 {
   equipmentOptions.push("Flatbed/Van/Reefer","Flatbed/Van","Air Ride Van","Van with curtains","Van with Pallet Exchange",
   "Hazmat Van","Vented Van","Team Van","Walking Floor Van","Van/Reefer")
 }

 
   equipmentOptions.push(equipmentType);
 


try
{
  
  var search = [];
  var _match = {}
  _match["$match"] = {};   
  _match["$match"]["equipment_type"] = {$in : equipmentOptions};
  _match["$match"]["is_opened"] = true;
   search.push(_match);
   var _project = {};
   _project["$project"] = {};
   _project["$project"]["pickup_states"] = 1;
   var _unwind = {};
   _unwind["$unwind"] = {};
   _unwind["$unwind"]["path"] = "$pickup_states";
   _unwind["$unwind"]["preserveNullAndEmptyArrays"] = true;
   search.push(_project);
   search.push(_unwind);
   var _group = {}
   _group["$group"] = {};
   _group["$group"]["_id"] ="$pickup_states";
   _group["$group"]["myCount"]  = { $sum: 1 };
   search.push(_group);

  let loads = await Load.aggregate(search);

   if( loads != null && loads.length > 0){
     
    var array = [];
    loads.forEach(element => { 
      if( element._id != ''){
        if( element._id !== undefined){
          var pickup_state = element._id;
          
            var i = 0;
            usastates.forEach(usastateselement => {               
                if( pickup_state == usastateselement.abbreviation ){
                  usastates[i].Loads = element.myCount;      
                }
                i++;
            });
        }
      } 
    })
  }

  let trucks = await  Truck.find({equipment_type:equipment.type, is_opened : true}); 

  if( trucks != null && trucks.length > 0){
      trucks.forEach(element2 => { 
        if( element2.pickup_states != null){
          if( element2.pickup_states[0] != null ){
            var pickup_state = element2.pickup_state;
              var a = 0;
              usastates.forEach(usastateselement2 => {               
                  if( pickup_state == usastateselement2.name || pickup_state == usastateselement2.abbreviation ){
                    usastates[a].truck++;        
                  }
                  a++;
              });
          }
        }
    });
  }

  return JSON.stringify(usastates);

}
catch(err)
{
  common.log(err);
}
        
      
};

