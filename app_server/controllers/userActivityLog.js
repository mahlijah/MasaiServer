const mongoose = require("mongoose");
const userActivity = mongoose.model("UserActivityInfo");
const User = mongoose.model("User");
const { ObjectId } = require("bson");
const config = require("../config/config");
const common = require("../helpers/common");
const jwt = require("jsonwebtoken");
const moment = require("moment");
const { Parser } = require('json2csv');
const isodate = require("isodate");
//mapping model
const ApiFunctionalityMapping = {
  //Read
  "GET": {
    //endpoints helps in validation of existance of route
    // "endpoints" : ["defaultExpensesTransactions","transactionCategories","findLoadDetail",
    //               "my-access-list","state","profile","getAuthuser","get_transactions_document_zip","download_user_document",
    //                "getChildUser","getDetailsFromInvitationToken","detail_truck","download_trucks_file","detail_load",
    //                "download_loads_file","addToTruckWatchList","removeToTruckWatchList","addToLoadWatchList"],
    "model": {
      "defaultExpensesTransactions": { "db_model": "Transaction", "message": "Expenses Page" },
      "transactionCategories": { "db_model": "TransactionCategories", "message": "Transaction Catagories" },
      "findLoadDetail": { "db_model": "Load and LoadWatchedUsers", "message": "Viewed Load Details" },
      "profile": { "db_model": "User and Role", "message": "Profile" },
      "get_transactions_document_zip": { "db_model": "UserDocument", "message": "Downloaded Selected Transactions" },
      "download_user_document": { "db_model": "UserDocument", "message": "Downloaded User Document" },
      "getChildUser": { "db_model": "User", "message": "Get Child User of Company" },
      "detail_truck": { "db_model": "Truck And TruckWatchedUsers", "message": "Viewed Truck Details" },
      "download_trucks_file": { "db_model": "TrucksByCsvUpload", "message": "Uploaded Trucks by CSV" },
      "detail_load": { "db_model": "Load", "message": "Get from Load" },
      "download_loads_file": { "db_model": "LoadsByCsvUpload", "message": "Get from LoadsByCsvUpload" },
      "addToTruckWatchList": { "db_model": "TruckWatchedUsers", "message": "Saved Truck in WatchList" },//add to truckwatch list should be post method
      "removeToTruckWatchList": { "db_model": "TruckWatchedUsers", "message": "Removed Truck in WatchList" },//update watching list should be delete method
      "addToLoadWatchList": { "db_model": "LoadWatchedUsers", "message": "Saved Load in WatchList" }//add to Load WatchUsers should be post method 
    }
  },
  //write
  "POST": {
    "model": {
      "checkAndLogin": { "db_model": "Users", "message": "Logged In" },
      "add_load": { "db_model": "Loads", "message": "Posted new Load" },// new Load
      "update_subscription": { "db_model": "Users", "message": "Updated User Subscription" },//update user subscription should be put 
      "cancel_subscription": { "db_model": "Users", "message": "Canceled User Subscription" },//update user subsciption shoudl be put
      "get_profile_uploads": { "db_model": "UserDocument", "message": "Get from UserDocument" }, //get
      "upload_document": { "db_model": "UserDocument", "message": "Uploaded Document in Fork Document" },//add
      "Userdocs": { "db_model": "Users", "message": "Updating Aggreement Document" },//updating aggreement document should be put
      "upload_profile_pic": { "db_model": "Users", "message": "Updated Profile Picture" }, //add
      "updatePaymentMethod": { "db_model": "Users", "message": "Updated Payment Method" },//updating payment method should be put
      "watchListTrucks": { "db_model": "Truck", "message": "View truck in watched List" }, //getting truck list 
      "watchListLoads": { "db_model": "LoadWatchedUsers", "message": "View Load in watched List" },  //get
      "login": { "db_model": "Users", "message": "LogIn" },  //without auth
      "logout": { "db_model": "Users", "message": "LogOut" },
      "send_reset_password_link": { "db_model": "Users", "message": "Forgot password" },  //without auth
      "reset_password": { "db_model": "Users and ResetPassword", "message": "Get from Users and ResetPassword " }, //without auth
      "findLoads": { "db_model": "Loads", "message": "Loads details Viewed" },  //without auth //getting Loads should be get 
      // "realTimeFindLoads"           :{"db_model" : "Loads",  "message": "Get from Loads"},//getting Loads Get
      "deleteSearch": { "db_model": "SearchObject", "message": "Delete Search in Dispatcher" }, //should be put (as it is updating) or delete for convention
      "getSearchList": { "db_model": "SearchObject", "message": "Viewed Loads in Dispatcher" }, //getting search list 
      "getHeatMapData": { "db_model": "Truck", "message": "HeatMap Viewed" },//get
      "getTransactions": { "db_model": "Transactions", "message": "Expenses Transaction List Viewed" },//get 
      "transaction": { "db_model": "Transactions", "message": "Expenses Added" },//write
      "getGraph": { "db_model": "Transactions", "message": "Viewed Expenses Graph" },//get
      "getPieChart": { "db_model": "Transactions", "message": "Viewed Income Chart in Expenses" },//get
      "getIncomeChart": { "db_model": "Transactions", "message": "Download Expenses report in PDF" }, //get
      "generatePDFReport": { "db_model": "Transactions", "message": "Download  Expenses report in CSV" },//get
      "exportTransactionsToCSV": { "db_model": "Transactions", "message": "Get from Transaction" },//get
      "upload_transaction_document": { "db_model": "Transactions and UserDocument", "message": "Update in Transactions and UserDocument" }, //update
      "get_company_uploads": { "db_model": "UserDocument", "message": "Searched in Fork Documents " },//get 
      "upload_company_document": { "db_model": "UserDocument", "message": "Upload documents in fork Documents" },//update user document
      "get_recipt_uploads": { "db_model": "UserDocument", "message": "Searched in Fork Documents " },//get
      "addChildUser": { "db_model": "Users", "message": "Child User added in Company Admin" },//added
      "getChildUsers": { "db_model": "Users", "message": "Child Users viewed in Company Admin" },//get
      "setPassword": { "db_model": "Users", "message": "Updated Password" },//update password
      "sendInvitationToJoinCompany": { "db_model": "Users", "message": "Sent Child User Invitation" },//get sent invitation
      "markedAsCompanyAdmin": { "db_model": "Users", "message": "Marked as Company Admin" },//update
      "signUpInOneStep": { "db_model": "Users", "message": "Clicked on Signup page" },//added 
      "add_truck": { "db_model": "Truck", "message": "Posted New Truck" },//added new Truck
      "my_truck": { "db_model": "Truck", "message": "Viewed Trucks Details" },//get
      "upload_trucks": { "db_model": "TrucksByCsvUpload", "message": "Added a new TrucksByCsvUpload" },//added
      "get_upload_trucks": { "db_model": "TrucksByCsvUpload", "message": "Trucks Upload from CSV" }, //get 
      "my_load": { "db_model": "Loads", "message": "Viewed Loads Details" }, //get
      "upload_loads": { "db_model": "LoadsByCsvUpload", "message": "Loads Upload from CSV" }, //added
      "get_upload_loads": { "db_model": "LoadsByCsvUpload", "message": "Viewed Loads from CSV" },//get
      "getLoadByUserId": { "db_model": "Loads", "message": "Get from Load" },
      "getTruckByUserId": { "db_model": "Truck", "message": "Get from Truck" }
    }
  },
  //update
  "PUT": {
    //endpoints helps in validation of existance of route
    //  "endpoints": ["update_account","transaction","editChildUser","refresh_age","close_truck","edit_truck","load_refresh_age","close_load","edit_load"],
    "model": {
      "update_account": { "db_model": "User", "message": "Profile Updated" },
      "editChildUser": { "db_model": "User", "message": "Child User Updated" },
      "refresh_age": { "db_model": "Truck", "message": "Truck Age Refreshed" },
      "close_truck": { "db_model": "Truck", "message": "Truck Closed" },
      "edit_truck": { "db_model": "Truck", "message": "Updated Truck" },
      "load_refresh_age": { "db_model": "Load", "message": "Load Age Refreshed" },
      "close_load": { "db_model": "Load", "message": "Load Closed" },
      "edit_load": { "db_model": "Load", "message": "Update Load" }
    }
  },
  //delete
  "DELETE": {
    //endpoints helps in validation of existance of route
    //"endpoints": ["upload_document","removeToLoadWatchList","transaction","upload_transaction_document","upload_company_document"],
    "model": {
      "upload_document": { "db_model": "UserDocument", "message": "Document Deleted from Fork Documents" },
      "removeToLoadWatchList": { "db_model": "LoadWatchedUsers", "message": "Removed Load From Watchlist" },
      "transaction": { "db_model": "Transaction", "message": "Deleted Transaction" },
    }
  }
};
// let insertion_message= "Added a new ";
/**
 * store the activity user performing and then pass
 */
const storeLogs = async (req, res, next) => {
  try {
    const { params, query, body, originalUrl, headers, method } = req;
    let user;
    const { password, ...rest_body } = body;
    if (headers?.authorization) {
      if (req.hasOwnProperty("payload")) {
        user = req.payload._id;
      }
    } else {
      const { _id } = await User.findOne(
        { email: body.email },
        { projection: { _id: 1 } }
      );
      common.log("id", _id);
      user = _id;
    };
    //get db model name 
    // let model= ApiFunctionalityMapping[method]["model"][originalUrl.split("/")[2]];
    //final object to save
    let object_to_save = {
      user_id: user,
      // event details
      event_details: {
        event_type: originalUrl,
        event_method: method,
        db_model: ApiFunctionalityMapping[method]["model"][originalUrl.split("/")[2]]["db_model"],
        event_message: ApiFunctionalityMapping[method]["model"][originalUrl.split("/")[2]]["message"],
        request_details: {
          params: params,
          query: query,
          body: rest_body,
        },
      },
    };

    /**
     * insert logs 
     */
    await userActivity.findOneAndUpdate(
      {
        user_id: user,
        log_date: {
          $gte: moment(
            new Date().setHours(00, 00, 00)
          ).format(),
          $lte: moment(
            new Date().setHours(23, 59, 59)
          ).format()
        },
      },
      {
        $push: { events: object_to_save['event_details'] },
      },
      { upsert: true, setDefaultsOnInsert: true }
    );
    next();
  } catch (error) {
    console.log("Error while storing logs:___", `url ${req.method} ${req.originalUrl}`, error);
    // return res.status("500").send({success: false, message:error.message})
    next();
  }
};
/**
 * retrieve logs for admins
 */
const userLogs = async (req, res) => {
  const { query: { user_id, start_date, end_date, limit = 5, offset = 0 } } = req;
  let query_filters = {
    user_id,
    created_by:{$ne:null},
    admin_events:{$ne:null}
  };

  // query_filters["created_by"]['$ne'] = null
  /**
   * date filters
   */
  if (start_date || end_date) {
    query_filters["log_date"] = {};
    if (start_date)
      query_filters["log_date"]['$gte'] = moment(start_date).format();
    if (end_date)
      query_filters["log_date"]['$lte'] = moment(end_date).format();
  }
  console.log(query_filters)
  try {
    let user_logs = await userActivity.find(query_filters,
      {
        _id: 0,
        user_id: 1,
        log_date: 1,
        created_by: 1
        //  "events.event_type": 1, 
        //  "events.created_at": 1, 
        //  "events.request_details": 1,
        //  "events.event_message" : 1
      }).sort({ log_date: -1 })
      .skip(Number(offset) > 0 ? ((Number(offset) - 1) * Number(limit)) : 0)
      .limit(Number(limit));

    common.log("user_logs:___", user_logs);
    await res.status(200).send({ success: true, data: user_logs });
    return;
  } catch (error) {
    common.log("Error :_____", error)
    res.status(500).send({ success: false, message: 'Internal server error' })
  }
}


/**
 * retrive events by log data
 */
const userLogDateEvents = async function (req, res) {
  try {
    let { body: { user_id, log_date, limit = 10, offset = 0 } } = req;
    limit = Number(limit);
    offset = Number(offset);

    let query_filters = {
      user_id
    };
    /**
     * date filters
     */
    if (log_date) {

      let start_date = new Date(log_date);
      start_date.setHours(0, 0, 0);

      let end_date = new Date(log_date);
      end_date.setHours(23, 59, 59);

      query_filters["log_date"] = {};
      query_filters["log_date"]['$gte'] = start_date
      query_filters["log_date"]['$lte'] = end_date
    }
    /** 
        let user_logs = await userActivity.findOne(query_filters, 
           { _id:0,
             user_id: 1,
             log_date: 1,
            "events":{$slice:[offset,limit]},
             count: { $size:"$events" }
            }).exec();
      common.log("user_logs_Events:___",user_logs);
     return res.status(200).send({ success: true, data: user_logs });
     */
    let user_logs = await findUserActivity(query_filters, limit, offset);
    //  let resp_data= JSON.stringify({ success: true, data: user_logs});
    //  common.log("response data:____", resp_data)
    //  return res.status(200).send(resp_data);
    return res.send({ success: true, data: user_logs }).end();
  } catch (error) {
    common.log("Error userLogDateEvents:_____", error)
    return res.status(500).send({ success: false, message: 'Internal server error' })
  }
}

/**
 * admin log events on user  
 */
/**
 * retrive events by log data
 */
 const userLogDateEventsAdmin = async function (req, res) {
  try {
    let { body: { user_id, log_date, limit = 10, offset = 0 } } = req;
    limit = Number(limit);
    offset = Number(offset);

    let query_filters = {
      user_id,
      created_by:{$ne:null}
    };
    /**
     * date filters
     */
    if (log_date) {

      let start_date = new Date(log_date);
      start_date.setHours(0, 0, 0);

      let end_date = new Date(log_date);
      end_date.setHours(23, 59, 59);

      query_filters["log_date"] = {};
      query_filters["log_date"]['$gte'] = start_date
      query_filters["log_date"]['$lte'] = end_date
    }
 
    let user_logs = await findAdminActivity(query_filters, limit, offset);

    return res.send({ success: true, data: user_logs }).end();
  } catch (error) {
    common.log("Error userLogDateEvents:_____", error)
    return res.status(500).send({ success: false, message: 'Internal server error' })
  }
}
/**
 * retrive events by log data
 */
const userCsvLogDateEvents = async (req, res) => {
  try {
    let user_id=req.query.user_id;
    console.log(user_id)
   // let userActivityData =await userActivity.find({user_id:ObjectId(user_id)},{events:1}).sort({ createdAt: -1 }).lean()
    let userActivityData =await userActivity.aggregate([
      {
        '$match': {
          'user_id': ObjectId(user_id)
        }
      }, {
        '$unwind': {
          'path': '$events'
        }
      }, {
        '$project': {
          'events._id': 1, 
          'events.event_type': 1, 
          'events.event_message': 1, 
          'events.created_at': {
            '$dateToString': {
              'format': '%Y-%m-%d %H:%M:%S', 
              'date': {
                '$subtract': [
                  '$events.created_at', 12 * 60 * 60 * 1000,
            
                ]
              }
            }
          }
        }
      }, {
        '$project': {
          'events': [
            '$events'
          ]
        }
      }, {
        '$sort': {
          'createdAt': -1
        }
      }
    ])
  
  	var fields = [
      { label: 'Event Type', value: 'event_type' },
      { label: 'Event Message', value: 'event_message' },
      { label: 'Created At', value: 'created_at' }
    ];
    var opts = {
      fields: fields
    }
    try {
      const parser = new Parser(opts);
      let csv;

      let filename = 'userActivity.csv';
      
      let arr=[];
      
      userActivityData.map(results => {
          arr.push(...results.events);
        })
        console.log(arr)  
      csv = parser.parse(arr);
      let dataCsv = [csv];
      res.set('Content-Disposition', ["attachment; filename=", filename].join(''));
      res.set('Content-Type', 'text/csv');
      res.end(dataCsv.toString());
    } catch (err) {
      console.error(err);
    }
  } catch (error) {
    common.log("Error userLogDateEvents:_____", error)
    res.status(500).send({ success: false, message: 'Internal server error' })
  }
}


// const userCsvLogDateEvents = async (req, res) => {
//   try {
//     let user_id=req.query.user_id;
//     let userActivityData =await userActivity.find({user_id:ObjectId(user_id)},{events:1}).sort({ createdAt: -1 }).lean()
    
//   	var fields = [
//       { label: 'Event Type', value: 'event_type' },
//       { label: 'Event Message', value: 'event_message' },
//       { label: 'Created At', value: 'created_at' }
//     ];
//     var opts = {
//       fields: fields
//     }
//     try {
//       const parser = new Parser(opts);
//       let csv;

//       let filename = 'userActivity.csv';
//       let arr = [];
//       userActivityData.map(results => {
//       results.events.map(res1=>{
//         const newDate = new Date(res1.created_at);
//         newDate.setHours(newDate.getHours() - 12)
//         res1.created_at=newDate.toISOString()
//         return res1.created_at
//       })
//         arr.push(...results.events);
//       })


//       csv = parser.parse(arr);
//       let dataCsv = [csv];
//       res.set('Content-Disposition', ["attachment; filename=", filename].join(''));
//       res.set('Content-Type', 'text/csv');
//       res.end(dataCsv.toString());
//     } catch (err) {
//       console.error(err);
//     }
//   } catch (error) {
//     common.log("Error userLogDateEvents:_____", error)
//     res.status(500).send({ success: false, message: 'Internal server error' })
//   }
// }
/**
 * retrive events by log data
 */
const userLogDateEvents_socket = async (data, socket) => {
  try {
    // let { query:{user_id,log_date,limit=10,offset=0}} = req;

    console.log("data requested from socket ", data);
    console.log("________________________________")
    let { user_id, log_date, limit = 10, offset = 0 } = data;

    limit = Number(limit);
    offset = Number(offset);


    let query_filters = {
      user_id
    };
    /**
     * date filters
     */
    if (log_date) {

      let start_date = new Date(log_date);
      start_date.setHours(0, 0, 0);

      let end_date = new Date(log_date);
      end_date.setHours(23, 59, 59);

      query_filters["log_date"] = {};
      query_filters["log_date"]['$gte'] = start_date
      query_filters["log_date"]['$lte'] = end_date
    }
    /** 
        let user_logs = await userActivity.findOne(query_filters, 
           { _id:0,
             user_id: 1,
             log_date: 1,
            "events":{$slice:[offset,limit]},
             count: { $size:"$events" }
            }).exec();
      common.log("user_logs_Events:___",user_logs);
     return res.status(200).send({ success: true, data: user_logs });
     */
    let user_logs = await findUserActivity(query_filters, limit, offset);
    //  let resp_data= JSON.stringify({ success: true, data: user_logs});
    //  common.log("response data:____", resp_data)
    //  return res.status(200).send(resp_data);
    return { status: true, data: user_logs };

  } catch (error) {
    common.log("Error userLogDateEvents:_____", error)
    return { success: false, message: error.message };
  }
}

async function findUserActivity(query_filters, limit, offset) {
  return await userActivity.findOne(query_filters,
    {
      _id: 0,
      user_id: 1,
      log_date: 1,
      created_by:1,
      "events": { $slice: [offset, limit] },
      count: { $size: "$events" }
    }).lean();
}

/**
 * for admins only 
 */
 async function findAdminActivity(query_filters, limit, offset) {
  return await userActivity.find(query_filters,
    {
      _id: 0,
      user_id: 1,
      log_date: 1,
      created_by:1,
      admin_events: { $slice: [offset, limit] },
      count: { $size: {"$cond": [ 
        { "$isArray": "$admin_events" }, 
        "$admin_events", 
        []
    ]} }
    }).populate({
      path: "created_by",
      // populate:{path:"User"}
      select: 'email first_name last_name role'
  }).lean();
}
/**
 * retrieve logs 
 */
 const userLogs1 = async (req, res) => {
  const { query: { user_id, start_date, end_date, limit = 5, offset = 0 } } = req;
  let startdate;
    let enddate
  let query_filters = {
    user_id:ObjectId(user_id)

  };
  

  if (start_date || end_date) {
    query_filters["log_date"] = {};
    if (start_date)
      startdate=moment(start_date).format()
      enddate=moment(end_date).format()
      query_filters["log_date"]['$gte'] = isodate(startdate);
    if (end_date)
      query_filters["log_date"]['$lte'] = isodate(enddate);
  }

  try {
    let user_logs = await userActivity.find(query_filters,
      {
        _id: 0,
        user_id: 1,
        log_date: 1,
        //  "events.event_type": 1, 
        //  "events.created_at": 1, 
        //  "events.request_details": 1,
        //  "events.event_message" : 1
      }).sort({ log_date: -1 })
      .skip(Number(offset) > 0 ? ((Number(offset) - 1) * Number(limit)) : 0)
      .limit(Number(limit));
 let lastLoginQuery = await userActivity.aggregate([{
        $match: {user_id:ObjectId(user_id)}

    }, {
        $unwind: '$events'
    }, {
        $match: {
            'events.event_type': '/api/checkAndLogin'
        }
    }, {
        $sort: {
            'events.created_at': -1
        }
    }, {
        $group: {
            _id: '$_id',
            doc: {
                $first: '$$ROOT'
            }
        }
    }, {
        $project: {
            latestlogin: '$doc.events.created_at'
        }
    }, {
        $sort: {
            latestlogin: -1
        }},
      ])  
  let lastLogin
  if(lastLoginQuery[0]!=null){
    lastLogin=lastLoginQuery[0].latestlogin
  }else{
    lastLogin='Record Not found'
  }
  
  
      
    common.log("user_logs:___", user_logs);
    await res.status(200).send({ success: true, data: user_logs,lastLogin });
    return;
  } catch (error) {
    common.log("Error :_____", error)
    res.status(500).send({ success: false, message: 'Internal server error' })
  }
}

const adminStoreLogs = async (req,prev_data=null,message,model=null) => {
  try {
    const { params, query, body, originalUrl, headers, method } = req;
    let user;
    let created_by;
    const { password, ...rest_body } = body;
 
    // console.log("_____other_______",headers,method,originalUrl);
    // console.log("\n___params:_____", params);
    // console.log("__req payload ", req.payload._id);
    // console.log("\n_____body:____", body);
   
    created_by=req.payload._id;

      if (params?.id) {
        user = params.id;
    }else if(body?.user_id){
        user= body.user_id
    }else {
      const { _id } = await User.findOne(
        { email: body.email },
        { projection: { _id: 1 } }
      );
      common.log("id", _id);
      user = _id;
    };
    // console.log("user:___", user, "created_by ", created_by);
    //get db model name 
    // let model= ApiFunctionalityMapping[method]["model"][originalUrl.split("/")[2]];
    // ApiFunctionalityMapping[method]["model"][originalUrl.split("/")[2]]["db_model"],
    //  ApiFunctionalityMapping[method]["model"][originalUrl.split("/")[2]]["message"],
    //final object to save
    let object_to_save = {
      user_id: user,
      // event details
      event_details: {
        event_type: originalUrl,
        event_method: method,
        db_model: model,
        event_message:message,
        request_details: {
          params: params,
          query: query,
          body: rest_body,
          prev_data: prev_data
        },
      },
    };

    /**
     * insert logs 
     */
    await userActivity.findOneAndUpdate(
      {
        user_id: user,
        log_date: {
          $gte: moment(
            new Date().setHours(00, 00, 00)
          ).format(),
          $lte: moment(
            new Date().setHours(23, 59, 59)
          ).format()
        },
        created_by: created_by
      },
      {
        $push: { admin_events: object_to_save['event_details'] },
      },
      { upsert: true, setDefaultsOnInsert: true }
    );
    return;
    // next();
  } catch (error) {
    console.log("Error while storing logs:___", `url ${req.method} ${req.originalUrl}`, error);
    // return res.status("500").send({success: false, message:error.message})
    // next();
    return;
  }
};


/**
 * store logs for dispatcher page socket 
 */
const store_sockets_logs= async (data,event_func,user)=>{
  try{
    if(user){
    console.log("data:___", data,"\nEvent function :__", event_func);
   let params;
   let message;
    if(event_func=='Delete Search'){
      const {users,...rest_data}= data?._doc;
       params= rest_data

       message="Deleted Search"
    }else{
      params= data.searchParams
      message="Added Search to dispatcher"
    }

    //final object to save
    let object_to_save = {
      user_id: user,
      // event details
      event_details: {
        event_type: event_func,
        event_method: 'Socket',
        db_model: "Search Object",
        event_message:message ,
        request_details: {
          params: params,
          // query: query,
          // body: rest_body,
        },
      },
    };
    
    /**
     * insert logs 
     */
    await userActivity.findOneAndUpdate(
      {
        user_id: user,
        log_date: {
          $gte: moment(
            new Date().setHours(00, 00, 00)
          ).format(),
          $lte: moment(
            new Date().setHours(23, 59, 59)
          ).format()
        },
      },
      {
        $push: { events: object_to_save['event_details'] },
      },
      { upsert: true, setDefaultsOnInsert: true }
    );
return 
    }else{
      return;
    }
  }catch(error){
    console.log("Error while storing logs for sockets:__", error);
    return;
  }
}

/**
 * export modules
 */
module.exports = {
  storeLogs,
  userLogs,
  userLogs1,
  userLogDateEvents,
  userLogDateEvents_socket,
  userCsvLogDateEvents,
  adminStoreLogs,
  userLogDateEventsAdmin,
  store_sockets_logs
};