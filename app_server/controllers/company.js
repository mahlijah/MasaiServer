
var mongoose = require('mongoose');
var Company = mongoose.model('Company');
var User = mongoose.model('User');
var UserDocument = mongoose.model('UserDocument');
let CompanyTransCreditDetails= mongoose.model('CompanyTransCreditDetails');

const common = require('../helpers/common');
const config = require('../config/config');
const emailTemplateService = require('../services/emailTemplateService');
const { ObjectId } = require('mongodb');
var _ = require('lodash');
const isodate = require('isodate');

module.exports.addCompany = async function (req, res) {
    try {
        let company = new Company();
        company.company_name = req.body.company_name;
        company.address = req.body.address;
        company.city = req.body.city;
        company.state = req.body.state;
        company.email = req.body.email;
        company.phone = req.body.phone;
        company.mc_number = req.body.mc_number;
 
        let _resp= await processTranscreditReport(req.body.company_name);
        company.save();
        await saveTranscreditReport(_resp);
        return res.send({ success: true, message: 'Company saved successful', data: company});
    }
    catch (err) {
        common.log("Error while add company--- ", err);
        return res.status(400).send({ success: false, message: err.message });
    }
}

module.exports.editCompany = async function (req, res) {
    try {
        let company = await Company.findById(req.params.id);
        let user = await User.findOne({ company_id: ObjectId(company._id) })

        let old_company = _.cloneDeep(company)

        if (company) {
            company.company_name = req.body.company_name;
            company.address = req.body.address;
            company.city = req.body.city;
            company.state = req.body.state;
            company.email = req.body.email;
            company.phone = req.body.phone;
            company.mc_number = req.body.mc_number;

            let companyUpsert = company.toObject();
            delete companyUpsert._id;

            await Company.updateOne({ _id: req.params.id }, { $set: companyUpsert }, { upsert: true });
            //change Company Success

            await emailTemplateService.sendEmail('change-company-success', { oldcp: old_company, newcp: company, user }, user.email)
            return res.send({ success: true, message: 'Company updated successful' });
        } else {
            return res.status(400).send({ success: false, message: 'Company not found' });
        }
    }
    catch (err) {
        common.log("Error while update company", err);
        return res.status(400).send({ success: false, message: err.message });
    }
}

module.exports.getCompanyById = async function (req, res) {
    try {
        let company = await Company.findById(req.params.id);
        if (company) {
            return res.send({ success: true, data: company });
        } else {
            return res.status(400).send({ success: false, message: 'Company not found' });
        }
    }
    catch (err) {
        common.log("Error while get company detail", err);
        return res.status(400).send({ success: false, message: err.message });
    }
}

module.exports.getCompanies = async function (req, res) {
    try {
        var search = {};
        if (req.body.company_name) {
            search["company_name"] = { '$regex': req.body.company_name, '$options': 'i' };
        }
        let companies = await Company.find(search).lean();
        if (companies) {
            return res.send({ success: true, data: companies });
        } else {
            return res.status(400).send({ success: false, message: 'No record found' });
        }
    }
    catch (err) {
        common.log("Error while get companies", err);
        return res.status(400).send({ success: false, message: err.message });
    }
}

module.exports.uploadCompanyDocument = async function (req, res) {
    if (!req.body.document_type) {
        return res.status(400).send({
            filed: 'document_type',
            message: 'document type is required'
        })
    }
    try {
        let user = await User.findById(req.payload._id);
        let company = await Company.findById(user.company_id);
        if (!user) {
            common.log("Error while save company document:", "User not found");
            return res.status(400).send({ message: "User not found" })
        }
        if (!company) {
            common.log("Error while save company document", "Company not found");
            return res.status(400).send({ message: "Company not found" })
        }
        var ud = new UserDocument();
        ud.user_id = user._id;
        ud.company = user.company_id;
        ud.document_path = req.file.location;
        ud.s3_key = req.file.key;
        ud.originalname = req.file.originalname;
        ud.mimetype = req.file.mimetype;
        ud.document_type = req.body.document_type;
        ud.save();
        await Promise.all([
            User.updateOne({ _id: user._id }, { $push: { upload_documents: ud._id } }),
            Company.updateOne({ _id: user.company_id }, { $push: { upload_documents: ud._id } })
        ]);
        return res.status(200).send(ud)
    }
    catch (err) {
        common.log("Error while save user document", err);
        res.status(400).send(err)
    }
}
module.exports.deleteCompanyDocument = async function (req, res) {
    if (!req.params.id) {
        return res.status(400).send({
            filed: 'id',
            message: 'Company document id is required'
        })
    }
    try {
        UserDocument.findById(req.params.id).exec(async function (err, userDocument) {
            if (err) {
                common.log("Error while delete company document", err);
                return res.status(400).send(err)
            }
            if (!userDocument) {
                common.log("Company document not found");
                return res.status(400).send({ message: "Company document not found" })
            }
            await Promise.all([
                UserDocument.updateOne({ _id: req.params.id }, { $set: { is_delete: true } }),
                Company.updateOne({ _id: userDocument.company }, { $pull: { upload_documents: req.params.id } }),
                User.updateOne({ _id: userDocument.user_id }, { $pull: { upload_documents: req.params.id } })
            ])
            return res.status(200).send({ message: "Company document deleted successfull" });
        })
    }
    catch (err) {
        common.log("Error while delet user document", err);
        res.status(400).send(err)
    }
}


module.exports.getCompanyUploads = async function (req, res) {
    try {
        
        var limitResults = 10;
        var skipResults = 0;
        let search;
        let regex=RegExp('.*'+req.body.document_name+'.*')
        
        let startDate = new Date(req.body.startDate)
        startDate.setHours(00, 00, 00)
        let endDate = new Date(req.body.endDate)
        endDate.setHours(23, 59, 59)
        if (req.body.limit != null && req.body.limit != '') {
            limitResults = req.body.limit;
        }

        if (req.body.skip != null && req.body.skip != '') {
            skipResults = (req.body.skip - 1) * limitResults;
        }

        var sort_by = req.body.sort_by || 'created_at';
        var sort_direction = req.body.sort_direction == 'asc' ? 1 : -1;
        var sortObj = {};
        var sort_split = sort_by.split(',');
        for (let index = 0; index < sort_split.length; index++) {
            sortObj[sort_split[index].trim()] = sort_direction;
        }

        let user = await User.findById(req.payload._id).lean();
        if (!user) {
            common.log("User not found ");
            return res.status(400).send({ success: false, message: "User detail not found" })
        }
        
        if (req.body.document_name) {

         search = { document_type: { $in: config.CompanyEnums },originalname:{$regex:regex,$options:"i"}, is_delete: false, user_id: ObjectId(req.payload._id) };
        }
        
        if ((req.body.startDate==undefined || req.body.startDate == '') && (req.body.endDate==undefined || req.body.endDate == '') && (req.body.document_name==undefined || req.body.document_name == '')) {
           
            search = { document_type: { $in: config.CompanyEnums }, is_delete: false, user_id: req.payload._id, company: user.company_id };
        }else 
        {
            
            search = { document_type: { $in: config.CompanyEnums },originalname:{$regex:regex,$options:"i"}, is_delete: false, user_id: req.payload._id, company: user.company_id };
        }
        

        if (req.body.startDate && req.body.endDate && !req.body.document_name ) {
            search = { document_type: { $in: config.CompanyEnums },created_at: { $gte: startDate, $lte: endDate }, is_delete: false, user_id: req.payload._id };
        }
        

        let [count, companyDocuments] = await Promise.all([
            UserDocument.count(search),
            UserDocument.find(search).populate('company').sort(sortObj).skip(skipResults).limit(limitResults).lean(),

        ])
        return res.send({ totalRecords: count, data: companyDocuments })

    }
    catch (err) {
        common.log("Error while get company upload documents list:-", err);
        return res.status(400).send({ success: false, message: err.message });
    }
}


module.exports.getReceiptDetails = async (req, res) => {
    try {
        var limitResults = 10;
        var skipResults = 0;
        let search;
        let regex=RegExp('.*'+req.body.document_name+'.*')
       
        let document_type;
        let startDate = new Date(req.body.startDate)
        startDate.setHours(00, 00, 00)
        let endDate = new Date(req.body.endDate)
        endDate.setHours(23, 59, 59)

        if (req.body.limit != null && req.body.limit != '') {
            limitResults = req.body.limit;
        }

        if (req.body.skip != null && req.body.skip != '') {
            skipResults = (req.body.skip - 1) * limitResults;
        }

        var sort_by = req.body.sort_by || 'created_at';
        var sort_direction = req.body.sort_direction == 'asc' ? 1 : -1;
        var sortObj = {};
        var sort_split = sort_by.split(',');
        for (let index = 0; index < sort_split.length; index++) {
            sortObj[sort_split[index].trim()] = sort_direction;
        }
        let user = await User.findById(req.payload._id).lean();
        if (!user) {
            common.log("user not found ");
            return res.status(400).send({ success: false, message: "User detail not found" })
        }
        if (req.body.document_name) {
            search = { document_type: { $in: config.ReciptEnums },originalname: {$regex:regex,$options:'i'}, is_delete: false, user_id: ObjectId(req.payload._id) };
           }
       
        if ((req.body.startDate==undefined || req.body.startDate == '') && (req.body.endDate==undefined || req.body.endDate == '') && (req.body.document_name==undefined || req.body.document_name == '')) {
           
            search = { document_type: { $in: config.ReciptEnums }, is_delete: false, user_id: req.payload._id };;
        }else 
        {
            search = { document_type: { $in: config.ReciptEnums },originalname:{$regex:regex,$options:'i'}, is_delete: false, user_id: req.payload._id };;
        }

        if (req.body.startDate && req.body.endDate && !req.body.document_name) {
            search = { document_type: { $in: config.ReciptEnums },created_at: { $gte: startDate, $lte: endDate }, is_delete: false, user_id: req.payload._id };
        }


  




        let [count, reciptDocuments] = await Promise.all([
            UserDocument.count(search),
            UserDocument.find(search).sort(sortObj).skip(skipResults).limit(limitResults).lean(),

        ])

        return res.send({ totalRecords: count, data: reciptDocuments })
    }
    catch (err) {
        common.log("Error while get Receipt Details upload documents list:-", err);
        return res.status(400).send({ success: false, message: err.message });
    }
}

/**
 * process for transcredit report of company
 */

const processTranscreditReport= async (company_name)=>{
    try{

        let resp = await common.xml_request("POST",null,null,null,company_name);
        let _resp= await xmlreader(resp);
        if(_resp.ListOfCompany.length>0)
        {
             console.log("report id is ",_resp.ListOfCompany[0].CompanyDetails[0].ReportID[0]);
             if(_resp.ListOfCompany[0].CompanyDetails[0].ReportID[0]){

             let xml_body=`<?xml version="1.0" encoding="utf-8"?>
             <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
             <soap:Body>
             <getFullReportV3 xmlns="http://tempuri.org/">
             <API_key>${config.TRANSCREDIT_KEY}</API_key>
             <ReportID>${_resp.ListOfCompany[0].CompanyDetails[0].ReportID[0]}</ReportID>
             </getFullReportV3>
             </soap:Body>
             </soap:Envelope>`;



            let fullreport= await common.xml_request('POST',xml_body,null,null,null);
            let _fullreport= await xmlreader(fullreport);
             return _fullreport;
        }
    }

    }catch(error){
        common.log("Error while fetching report:____",error.message);
        return;
    }
}

  
  const xmlreader= async (data)=>{
    try{
      let parseString = require('xml2js').parseString;  
  return new Promise((resolve,reject)=>{
    parseString(data, function (err, result) {
      console.log("result",result);
      if(err)
       reject(err);

       //return reponse for v2 report 
       if(result['soap:Envelope']['soap:Body'][0]['getReportSearchV2Response'])
       resolve(result['soap:Envelope']['soap:Body'][0]['getReportSearchV2Response'][0]['getReportSearchV2Result'][0]);

      
       resolve(result['soap:Envelope']['soap:Body'][0])
    });
  }) 
  
  }catch(error){
      console.log("error in xml reader :__", error.message);
     throw error.message;
    }
  }
  /**
   * save transcredit report 
   */
const saveTranscreditReport= async(report)=>{
    try{

        let t_report= new CompanyTransCreditDetails();
        // console.log("report received",report);
        if(report.getFullReportV3Response[0].getFullReportV3Result.length>0){
        //   console.log("Now parse each value of :___",report.getFullReportV3Response[0].getFullReportV3Result[0])
          let r_obj= report.getFullReportV3Response[0].getFullReportV3Result[0];
          let _report= await parseReport(r_obj);

      Object.assign(t_report,_report);


      common.log("finally:_", t_report);
      t_report.save();
        }
    }catch(error){
        common.log("Error in saving Transcredit Report:___", error.message)
    }
}

/**
 * parse report version 3 for transcredit
 */
const parseReport= async(r_obj)=>{
    try{
        let t_report={};
        Object.entries(r_obj).forEach(([key ,value])=>{
            console.log("now :__", key, "__value:___", value.join(","));
            t_report[key]=value.join(",");
        });
        console.log("com:__", t_report);
        return t_report;
    }catch(error){
        console.log("error in parsing :____",error.message);
    }
}