var mongoose = require('mongoose');
var Transaction = mongoose.model('Transaction');
var TransactionCategories = mongoose.model('TransactionCategories');
var User = mongoose.model('User');
var UserDocument = mongoose.model('UserDocument');
var fs = require('fs');
var ejs = require('ejs');
const { ObjectId } = require('mongodb');
const { PieChart, GroupBarChart, MixedBarAndLineChart } = require('../helpers/d3ChartDraw');
const { Parser } = require('json2csv');
const { PDFtoBuffer } = require('../helpers/pdfGenerate');
const common = require('../helpers/common');
const { s3GetFileStream } = require('../helpers/s3FileDownload');
const archiver = require('archiver');

const { getGraphData, getIncomePaidUnpaidGraphData, getPieChartData, addDecimal } = require('../services/transactionService');


module.exports.AddTransaction = async function (data, socket) {
  'use strict';

  if (!socket.decoded._id) {
    socket.emit('add_transaction_response', { success: false, message: "UnauthorizedError: Private action" });
  }

  if (!data.description || !data.amount || !data.transactionType
  ) {
    socket.emit('add_transaction_response', { success: false, message: "complete all required fields. description, amount, date" });
    return;
  }

  try {
    var transaction = new Transaction();
    transaction.transactionType = data.transactionType;
    transaction.amount = data.amount;
    transaction.transactionDate = data.transactionDate;
    transaction.description = data.description;
    transaction.userId = socket.decoded._id;
    let result = await transaction.save();
    socket.emit('add_transaction_response', { success: true, message: "Income / Expense saved." });
  }
  catch (err) {
    socket.emit('add_transaction_response', { success: false, message: err.message });
  }

};

module.exports.getTransaction = function (data, socket) {
  'use strict';

  if (!socket.decoded._id) {
    socket.emit('get_transaction_response', { success: false, message: "UnauthorizedError: Private action" });
  }

  try {
    let search_object = data;
    common.log("search object: ", search_object);
    var search_options = {};
    var limitResults = 500;
    search_options["isDeleted"] = false;
    search_options["userId"] = socket.decoded._id;

    if (search_object !== undefined) {

      //date range
      if (search_object.start_date != null
        && search_object.end_date != null
      ) {
        search_options["$expr"] =
        {
          "$and": [
            { $gte: [{ "$dateFromString": { "dateString": "$transactionDate" } }, new Date(search_object.start_date)] },
            { $lte: [{ "$dateFromString": { "dateString": "$transactionDate" } }, new Date(search_object.end_date)] }
          ]
        }
      }

      if (search_object.start_date != null
        && search_object.end_date == null) {

        search_options["$expr"] =
        {
          $gte: [{ "$dateFromString": { "dateString": "$transactionDate" } }, new Date(search_object.start_date)]
        }
      }

      if (search_object.end_date != null
        && search_object.start_date == null) {

        search_options["$expr"] =
        {
          $lte: [{ "$dateFromString": { "dateString": "$transactionDate" } }, new Date(search_object.end_date)]
        }
      }

    }
    Transaction.find(search_options)
      .limit(limitResults)
      .exec(function (err, transactions) {
        if (err) {
          common.log(err);
          socket.emit('get_transaction_response', { success: false, message: err.message });
        } else {
          socket.emit('get_transaction_response', { type: 'new', data: transactions });
        }
      });
  }
  catch (err) {
    common.log(err);
    socket.emit('get_transaction_response', { success: false, message: err.message });
  }
};

module.exports.getTransactionHttp = async function (req, res) {
  try {
    var limitResults = 10;
    var skipResults = 1;
    if (req.body.limit != null && req.body.limit != '') {
      limitResults = req.body.limit;
    }

    if (req.body.skip != null && req.body.skip != '') {
      skipResults = (req.body.skip - 1) * limitResults;
    }

    var sort_by = req.body.sort_by || 'created_at';
    var sort_direction = req.body.sort_direction == 'asc' ? 1 : -1;
    var sortObj = {};
    var sort_split = sort_by.split(',');
    for (let index = 0; index < sort_split.length; index++) {
      sortObj[sort_split[index].trim()] = sort_direction;
    }

    let search_object = req.body;
    common.log("search object: ", search_object);
    var search_options = {};
    search_options["isDeleted"] = false;
    search_options["userId"] = ObjectId(req.payload._id);
    if (search_object !== undefined) {
      if (search_object.start_date != null
        && search_object.end_date != null
      ) {
        search_options["$expr"] =
        {
          "$and": [
            { $gte: [{ "$dateFromString": { "dateString": "$transactionDate"}},{ "$dateFromString": { "dateString": search_object.start_date} }] },
            { $lte: [{ "$dateFromString": { "dateString": "$transactionDate"}},{ "$dateFromString": { "dateString": search_object.end_date} }] }
          ]
        }
      }

      if (search_object.start_date != null
        && search_object.end_date == null) {
        search_options["$expr"] =
        {
          $gte: [{ "$dateFromString": { "dateString": "$transactionDate" } },{ "$dateFromString": { "dateString": search_object.start_date} }]
        }
      }

      if (search_object.end_date != null
        && search_object.start_date == null) {

        search_options["$expr"] =
        {
          $lte: [{ "$dateFromString": { "dateString": "$transactionDate"}},{ "$dateFromString": { "dateString": search_object.end_date} }]
        }
      }
      if (search_object.description) {
        search_options["description"] = { $regex: search_object.description, $options: 'g' };
      }
      if (search_object.amount) {
        search_options["amount"] = { $eq: parseFloat(search_object.amount) };
      }
      if (search_object.transactionCategoryId) {
        search_options["transaction_category_id"] = ObjectId(search_object.transactionCategoryId);
      }
      if (search_object.transactionType) {
        search_options["transactionType"] = search_object.transactionType;
      }
      if (search_object.isPaid === 'true' || search_object.isPaid === 'false') {
        search_options["isPaid"] = { $eq: search_object.isPaid === 'true' };
      }
      if (search_object.due_start_date != null
        && search_object.due_end_date != null
      ) {
        search_options["$expr"] =
        {
          "$and": [
            { $gte: [{ "$dateFromString": { "dateString": "$dueDate" } },{ "$dateFromString": { "dateString": search_object.due_start_date } } ] },
            { $lte: [{ "$dateFromString": { "dateString": "$dueDate" } }, { "$dateFromString": { "dateString": search_object.due_end_date } } ] }
          ]
        }
      }

      if (search_object.due_start_date != null
        && search_object.due_end_date == null) {

        search_options["$expr"] =
        {
          $gte:  [{ "$dateFromString": { "dateString": "$dueDate" } },{ "$dateFromString": { "dateString": search_object.due_start_date } } ]
        }
      }

      if (search_object.due_end_date != null
        && search_object.due_start_date == null) {

        search_options["$expr"] =
        {
          $lte: [{ "$dateFromString": { "dateString": "$dueDate" } }, { "$dateFromString": { "dateString": search_object.due_end_date } } ]
        }
      }
    }
    common.log(JSON.stringify(search_options));
    let aggregate = [
      {
        $match: search_options
      },
      {
        $lookup: {
          from: "transactioncategories", localField: "transaction_category_id", foreignField: "_id", as: "transaction_category_id"
        }
      },
      { $unwind: { path: "$transaction_category_id", preserveNullAndEmptyArrays: true } },
      {
        $project: {
            _id: 1,
            amount: 1,
            transactionDate: {
                $dateFromString: {
                    dateString: '$transactionDate'
                }
            },
            isDeleted: 1,
            isPaid: 1,
            upload_documents: 1,
            created_at: 1,
            dueDate: 1,
            transactionType: 1,
            description: 1,
            userId: 1,
            transaction_category_id: 1,
    
    
        }
    },
      { "$sort": sortObj },
      { "$skip": skipResults },
      { "$limit": limitResults }

    ];
    
    common.log(JSON.stringify(aggregate));
    let totalcount = await Transaction.find(search_options).count();
    var transactions = await Transaction.aggregate(aggregate)
    return res.send({
      totalcount,
      data: transactions
    })
  }
  catch (err) {
    common.log("Error While get transactions", err);
    return res.status(400).send(err);
  }
}

module.exports.defaultExpensesTransactionsHttp = async function (req, res) {
  try {

    var limitResults =  10;
    var skipResults = 0;
    var search_options = {};
    search_options["isDeleted"] = false;
    search_options["userId"] = ObjectId(req.payload._id);
   
    let transactions = await Transaction.aggregate([{
      $match: search_options
    }, {
      $lookup: {
        from: 'transactioncategories',
        localField: '_id',
        foreignField: 'transactions',
        as: 'transaction_category_id'
      }
    }, {
      $unwind: {
        path: '$transaction_category_id',

      }
    }, {
      $project: {
          _id: 1,
          amount: 1,
          transactionDate: {
              $dateFromString: {
                  dateString: '$transactionDate'
              }
          },
          isDeleted: 1,
          isPaid: 1,
          upload_documents: 1,
          created_at: 1,
          dueDate: 1,
          transactionType: 1,
          description: 1,
          userId: 1,
          transaction_category_id: 1,
  
  
      }
  },{
      $sort: {'transactionDate':-1}
    }, { $skip: skipResults },
    { $limit: limitResults }])
    return res.send({
      totalcount: transactions.length,
      data: transactions
    });
  }
  catch (err) {
    common.log("Error While get default transactions", err);
    return res.status(400).send(err);
  }
}

module.exports.addTransactionHttp = async function (req, res) {
  var data = req.body;
  if (!data.description || !data.amount || !data.transactionType) {
    return res.send({ success: false, message: "complete all required fields. description, amount, date" });
  }
  if (data.transactionCategory && !data.transactionCategoryId) {
    try {
      var tc = new TransactionCategories();
      tc.name = data.transactionCategory;
      tc.save();
      data.transactionCategoryId = tc._id;
    } catch (err) {
      common.log("Error While save transaction Category", err);
      return res.status(400).send(err.message);
    }
  }

  try {
    var transaction = new Transaction();
    transaction.transactionType = data.transactionType;
    transaction.amount = data.amount;
    transaction.transactionDate = data.transactionDate;
    transaction.dueDate = data.dueDate;
    transaction.isPaid = data.isPaid;
    transaction.description = data.description;
    transaction.userId = req.payload._id;
    if (data.transactionCategoryId) transaction.transaction_category_id = data.transactionCategoryId
    var result = await transaction.save();
    await TransactionCategories.updateOne({ _id: transaction.transaction_category_id }, { $push: { transactions: transaction._id } })
    return res.send({ success: true, message: "Income / Expense saved.", data: result });
  }
  catch (err) {
    common.log("Error while add transaction :-", err);
    return res.send({ success: false, message: err.message });
  }
}

module.exports.editTransactionHttp = async function (req, res) {
  if (!req.params.id) {
    return res.send({ success: false, message: "Transaction id is required" });
  }
  var data = req.body;
  if (!data.description || !data.amount || !data.transactionType) {
    return res.send({ success: false, message: "complete all required fields. description, amount, date" });
  }

  if (data.transactionCategory && !data.transactionCategoryId) {
    try {
      var tc = new TransactionCategories();
      tc.name = data.transactionCategory;
      tc.save();
      data.transactionCategoryId = tc._id;
    } catch (err) {
      common.log("Error While save transaction Category", err);
      return res.status(400).send(err.message);
    }
  }

  try {
    let transaction = await Transaction.findById(req.params.id);
    transaction.transactionType = data.transactionType;
    transaction.amount = data.amount;
    transaction.transactionDate = data.transactionDate;
    transaction.description = data.description;
    transaction.dueDate = data.dueDate;
    transaction.isPaid = data.isPaid;
    if (data.transactionCategoryId) {
      if (transaction.transaction_category_id != data.transactionCategoryId) {
        await TransactionCategories.updateOne({ _id: transaction.transaction_category_id }, { $pull: { transactions: transaction._id } })
        transaction.transaction_category_id = data.transactionCategoryId
      }
    } else if (transaction.transaction_category_id && !data.transactionCategoryId) {
      await TransactionCategories.updateOne({ _id: transaction.transaction_category_id }, { $pull: { transactions: transaction._id } })
      transaction.transaction_category_id = null;
    }
    let upsertData = transaction.toObject();
    delete upsertData._id;
    let result= await Transaction.findOneAndUpdate({ _id: req.params.id }, upsertData, { upsert: true });
    await TransactionCategories.updateOne({ _id: transaction.transaction_category_id }, { $push: { transactions: transaction._id } })
    return res.send({ success: true, data: result ,message: "transaction update successfull." });

  }
  catch (err) {
    common.log("Error while update transaction :-", err);
    return res.send({ success: false, message: err.message });
  }
}

module.exports.deleteTransactionHttp = async function (req, res) {
  if (!req.params.id) {
    return res.send({ success: false, message: "Transaction id is required" });
  }
  try {
    let result = await Transaction.update({ _id: req.params.id }, { $set: { isDeleted: true } }, { useFindAndModify: true });
    return res.send({ success: true, message: "transaction delete successfull." });
  }
  catch (err) {
    common.log("Error while delete transaction :-", err);
    return res.send({ success: false, message: err.message });
  }
}

module.exports.getGraphDataHttp = async function (req, res) {
  if (!req.body.start_date || !req.body.end_date) {
    return res.status(400).send("start_date and end_date is required");
  }
  try {
    var data = await getGraphData(req.body, req.payload._id);
    return res.send(data);
  } catch (err) {
    common.log("Error While get load data", err);
    return res.status(400).send(err.message);
  }
}

module.exports.generatePDFReport = async function (req, res) {
  if (!req.body.start_date || !req.body.end_date) {
    return res.status(400).send("start_date and end_date is required");
  }
  try {
    var showProfitAndLossGraph = req.body.showProfitAndLossGraph == 'true';
    var showBarGraph = req.body.showBarGraph == 'true' && showProfitAndLossGraph == false;
    var showIncomeAndExpensesGraph = req.body.showIncomeAndExpensesGraph == 'true'
    var start_date = req.body.start_date;
    var end_date = req.body.end_date;
    var search_object = req.body;
    var search_options = {};
    search_options["isDeleted"] = false;
    search_options["userId"] = ObjectId(req.payload._id);
    if(!req.body.is_default){
      search_options["$expr"] =
      {
        "$and": [
          { $gte: [{ "$dateFromString": { "dateString": "$transactionDate"}},{ "$dateFromString": { "dateString": start_date} }] },
          { $lte: [{ "$dateFromString": { "dateString": "$transactionDate" } }, { "$dateFromString": { "dateString": end_date} }] }
        ]
      };
    }
    if (search_object.description) {
      search_options["description"] = { $regex: search_object.description, $options: 'g' };
    }
    if (search_object.amount) {
      search_options["amount"] = { $eq: parseFloat(search_object.amount) };
    }
    if (search_object.transactionCategoryId) {
      search_options["transaction_category_id"] = ObjectId(search_object.transactionCategoryId);
    }
    if (search_object.transactionType) {
      search_options["transactionType"] = search_object.transactionType;
    }
    if (search_object.isPaid === 'true' || search_object.isPaid === 'false') {
      search_options["isPaid"] = { $eq: search_object.isPaid === 'true' };
    }
    if (search_object.due_start_date != null
      && search_object.due_end_date != null
    ) {
      search_options["$expr"] =
      {
        "$and": [
          { $gte: [{ "$dateFromString": { "dateString": "$dueDate" } },{ "$dateFromString": { "dateString": search_object.due_start_date } } ] },
          { $lte: [{ "$dateFromString": { "dateString": "$dueDate" } }, { "$dateFromString": { "dateString": search_object.due_end_date } } ] }
        ]
      }
    }

    if (search_object.due_start_date != null
      && search_object.due_end_date == null) {

      search_options["$expr"] =
      {
        $gte:  [{ "$dateFromString": { "dateString": "$dueDate" } },{ "$dateFromString": { "dateString": search_object.due_start_date } } ]
      }
    }

    if (search_object.due_end_date != null
      && search_object.due_start_date == null) {

      search_options["$expr"] =
      {
        $lte: [{ "$dateFromString": { "dateString": "$dueDate" } }, { "$dateFromString": { "dateString": search_object.due_end_date } } ]
      }
    }
    var user = await User.findById(req.payload._id).lean();


    let grid_aggregate = [{
      $match: search_options
    },
    {
      $lookup: {
        from: "transactioncategories", localField: "transaction_category_id", foreignField: "_id", as: "transaction_category_id"
      }
    },
    { $unwind: { path: "$transaction_category_id", preserveNullAndEmptyArrays: true } }, {
      $project: {
        transactionDate: 1,
        transactionType: 1,
        amount: 1,
        description: 1,
        category: '$transaction_category_id.name'
      }
    }, {
      $sort: { transactionDate: 1 }
    }];
    if(req.body.is_default){
      grid_aggregate.push({$limit:10})
    }
    var data = await Transaction.aggregate(grid_aggregate);
    var html = await fs.readFileSync('./app_server/template/expenses.ejs', 'utf8')

    var graph_data = await getGraphData(req.body, req.payload._id);

    var igData = await getIncomePaidUnpaidGraphData(req.body.start_date, req.body.end_date, req.payload._id);

    var pie_data = [], total_expenses = 0, total_income = 0;
    var groupData = [];
    var profitLossChart = [];

    graph_data.forEach(function (obj) {
      total_expenses += obj.expenses;
      total_income += obj.income;
      groupData.push({
        key: obj.label,
        values: [
          {
            grpName: 'Income',
            grpValue: obj.income
          },
          {
            grpName: 'Expenses',
            grpValue: obj.expenses
          }
        ]
      })
      profitLossChart.push({
        key: obj.label,
        Income: obj.income,
        Expenses: obj.expenses * -1
      })
    })
    // pie_data.push(total_expenses);
    // pie_data.push(total_income);
    pie_data = await getPieChartData(req.body.start_date, req.body.end_date, req.payload._id, req.body.transactionCategoryIds);
    html = ejs.render(html, {
      data: data,
      start_date: start_date,
      end_date: end_date,
      total_expenses: total_expenses,
      total_income: total_income,
      company: user.company,
      showBarGraph: showBarGraph,
      showProfitAndLossGraph: showProfitAndLossGraph,
      showIncomeAndExpensesGraph: showIncomeAndExpensesGraph,
      currentDate: req.body.reportExecuteDateTime,
      ...igData,
      expenses: pie_data.map(obj => { return { value: obj.expenses, label: obj.label } }),
      addDecimal: addDecimal
    });
    //common.log(JSON.stringify(incomeGraphData));
    if (groupData.length > 0 && showBarGraph) {
      html = GroupBarChart(html, groupData);
    }

    if (pie_data.length > 0 && showIncomeAndExpensesGraph) {
      // html = await HorizontalStackedBar(html, incomeGraphData, '#incomePieChart');
      html = PieChart(html, pie_data.map(obj => { return { value: obj.expenses, label: obj.label } }), '#expensesPieChart');
    }

    if (profitLossChart.length > 0 && showProfitAndLossGraph) {
      // common.log(JSON.stringify(profitLossChart));
      html = await MixedBarAndLineChart(html, profitLossChart, "#profitlossBarChart");
    }

    PDFtoBuffer(html, user, function (err, buffer) {
      res.status(200);
      res.set({
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/pdf',
        'Content-Length': buffer.length,
        'Content-Disposition': 'attachment; filename=expenses-report-' + (new Date()).getTime() + '.pdf'
      });
      return res.send(buffer);
    });
  }
  catch (err) {
    common.log("Error While generate pdf", err);
    return res.status(400).send(err.message);
  }
}

module.exports.getTransactionCategoriesHttp = async function (req, res) {
  try {
    var cat = await TransactionCategories.find({
      isDeleted: false
    }, {
      name: 1,
      _id: 1
    }).lean()
    return res.send(cat);
  }
  catch (err) {
    common.log("Error while get transaction categories", err);
    return res.status(400).send(err.message);
  }
}

module.exports.getPieChartDataHttp = async function (req, res) {
  if (!req.body.start_date || !req.body.end_date) {
    return res.status(400).send("start_date and end_date is required");
  }
  try {
    var data = await getPieChartData(req.body.start_date, req.body.end_date, req.payload._id, req.body.transactionCategoryIds);
    return res.send(data);
  } catch (err) {
    common.log("Error while getting pie chart data", err);
    return res.status(400).send(err.message);
  }

}

module.exports.exportTransactionsToCSV = async function (req, res) {

  try {
    var limitResults = 10;
    var skipResults = 0;
   
    
    let search_object = req.body;
    var search_options = {};
    var transactions = [];
    search_options["isDeleted"] = false;
    search_options["userId"] = ObjectId(req.payload._id);
  
    if (search_object !== undefined) {
      if(search_object.is_default){
        transactions = await Transaction.find(search_options, {
          transactionDate: 1,
          transactionType: 1,
          amount: 1,
          description: 1,
          category: '$transaction_category_id.name'
        }).populate({
          path: 'transaction_category_id',
          select: 'name'
        }).sort({ "transactionDate":1 }).skip(skipResults).limit(limitResults).lean();
      }
      else{

        if (search_object.start_date != null
          && search_object.end_date != null
        ) {
          search_options["$expr"] =
          {
            "$and": [
              { $gte: [{ "$dateFromString": { "dateString": "$transactionDate"}},{ "$dateFromString": { "dateString": search_object.start_date} }] },
              { $lte: [{ "$dateFromString": { "dateString": "$transactionDate"}},{ "$dateFromString": { "dateString": search_object.end_date} }] }
            ]
          }
        }
        if (search_object.start_date != null
          && search_object.end_date == null) {
          search_options["$expr"] =
          {
            $gte: [{ "$dateFromString": { "dateString": "$transactionDate"}},{ "$dateFromString": { "dateString": search_object.start_date} }]
          }
        }
        if (search_object.end_date != null
          && search_object.start_date == null) {
          search_options["$expr"] =
          {
            $lte: [{ "$dateFromString": { "dateString": "$transactionDate"}},{ "$dateFromString": { "dateString": search_object.end_date} }]
          }
        }

        if (search_object.description) {
          search_options["description"] = { $regex: search_object.description, $options: 'g' };
        }
        if (search_object.amount) {
          search_options["amount"] = { $eq: parseFloat(search_object.amount) };
        }
        if (search_object.transactionCategoryId) {
          search_options["transaction_category_id"] = ObjectId(search_object.transactionCategoryId);
        }
        if (search_object.transactionType) {
          search_options["transactionType"] = search_object.transactionType;
        }
        if (search_object.isPaid === 'true' || search_object.isPaid === 'false') {
          search_options["isPaid"] = { $eq: search_object.isPaid === 'true' };
        }
        if (search_object.due_start_date != null
          && search_object.due_end_date != null
        ) {
          search_options["$expr"] =
          {
            "$and": [
              { $gte: [{ "$dateFromString": { "dateString": "$dueDate" } },{ "$dateFromString": { "dateString": search_object.due_start_date } } ] },
              { $lte: [{ "$dateFromString": { "dateString": "$dueDate" } }, { "$dateFromString": { "dateString": search_object.due_end_date } } ] }
            ]
          }
        }
  
        if (search_object.due_start_date != null
          && search_object.due_end_date == null) {
  
          search_options["$expr"] =
          {
            $gte:  [{ "$dateFromString": { "dateString": "$dueDate" } },{ "$dateFromString": { "dateString": search_object.due_start_date } } ]
          }
        }
  
        if (search_object.due_end_date != null
          && search_object.due_start_date == null) {
  
          search_options["$expr"] =
          {
            $lte: [{ "$dateFromString": { "dateString": "$dueDate" } }, { "$dateFromString": { "dateString": search_object.due_end_date } } ]
          }
        }

        transactions = await Transaction.find(search_options, {
          transactionDate: 1,
          transactionType: 1,
          amount: 1,
          description: 1,
          category: '$transaction_category_id.name'
        }).populate({
          path: 'transaction_category_id',
          select: 'name'
        }).lean();
      }
    }
    
     var expenses = 0, income = 0;
    transactions.forEach((tran) => {
      if (tran.transactionType.indexOf('Expense') >= 0) expenses += tran.amount;
      if (tran.transactionType.indexOf('Income') >= 0) income += tran.amount;
    })
    transactions.push({
      description: 'Total',
      transactionType: 'Total',
      income: income,
      expenses: expenses
    });
    transactions.push({
      description: 'Net Income',
      transactionType: 'netIncome',
      netIncome: (income - expenses)
    });

    var filename = 'Transactions.csv';
    var fields = [
      { label: 'Date', value: 'transactionDate' },
      { label: 'Description', value: 'description' },
      { label: 'Category', value: 'transaction_category_id.name' },
      {
        label: 'Income', value: (row) => {
          if (row.transactionType == 'Income') {
            return addDecimal(row.amount);
          } else if (row.transactionType == 'Total') {
            return addDecimal(row.income);
          } else if (row.transactionType == 'netIncome') {
            return addDecimal(row.netIncome);
          }
          return null
        }
      },
      {
        label: 'Expenses', value: (row) => {
          if (row.transactionType.indexOf('Expense') >= 0) {
            return addDecimal(row.amount);
          } else if (row.transactionType == 'Total') {
            return addDecimal(row.expenses);
          }
          return null
        }
      },

      // { label: 'Amount', value: 'amount' },
      // { label: 'Type', value: 'transactionType' },
    ];
    var opts = {
      fields: fields
    }
    try {
      const parser = new Parser(opts);
      const csv = parser.parse(transactions);
      //common.log(csv);
      res.set('Content-Disposition', ["attachment; filename=", filename].join(''));
      res.set('Content-Type', 'text/csv');
      return res.end(csv);
    } catch (err) {
      console.error("Error while generate csv file", err);
      throw err;
    }


  }
  catch (err) {
    common.log("Error While generate CSV file", err);
    return res.status(400).send(err);
  }
}

module.exports.incomeGraphDataHttp = async function (req, res) {
  if (!req.body.start_date || !req.body.end_date) {
    return res.status(400).send("start_date and end_date is required");
  }
  try {
    var result = await getIncomePaidUnpaidGraphData(req.body.start_date, req.body.end_date, req.payload._id);
    return res.send(result);
  } catch (err) {
    common.log("Error while getting pie chart data", err);
    return res.status(400).send(err.message);
  }
}

module.exports.uploadTransactionDocument = async function (req, res) {
  if (!req.body.document_type) {
    return res.status(400).send({
      filed: 'document_type',
      message: 'document type is required'
    })
  }
  try {
    let user = await User.findById(req.payload._id);
    if (!user) {
      common.log("Error while save transaction document:", "User not found");
      return res.status(400).send({ message: "User not found" })
    }
    let transaction = await Transaction.findById(req.params.transactionid);
    if (!transaction) {
      common.log("Error while save transaction document:", "Transaction not found");
      return res.status(400).send({ message: "Transaction not found" })
    }
    var ud = new UserDocument();
    ud.user_id = user._id;
    ud.transaction_id = req.params.transactionid;
    ud.document_path = req.file.location;
    ud.s3_key = req.file.key;
    ud.originalname = req.file.originalname;
    ud.mimetype = req.file.mimetype;
    ud.document_type = req.body.document_type;
    ud.expire_at = req.body.expire_at;
    ud.save();
    await Promise.all([
      User.updateOne({ _id: user._id }, { $push: { upload_documents: ud._id } }),
      Transaction.updateOne({ _id: req.params.transactionid }, { $push: { upload_documents: ud._id } })
    ]);
    return res.status(200).send(ud);

  }
  catch (err) {
    common.log("Error while save transaction document", err);
    return res.status(400).send(err)
  }
}

module.exports.deleteTransactionDocument = function (req, res) {
  if (!req.params.documentid) {
    return res.status(400).send({
      filed: 'documentid',
      message: 'document id is required'
    })
  }
  try {
    UserDocument.findById(req.params.documentid).exec(async function (err, userDocument) {
      if (err) {
        common.log("Error while delete user document", err);
        return res.status(400).send(err)
      }
      if (!userDocument) {
        common.log("User document not found");
        return res.status(400).send({ message: "User document not found" })
      }
      await Promise.all([
        await UserDocument.updateOne({ _id: req.params.documentid }, { $set: { is_delete: true } }),
        await User.updateOne({ _id: req.params.documentid }, { $pull: { upload_documents: req.params.documentid } }),
        await Transaction.updateOne({ _id: userDocument.transaction_id }, { $pull: { upload_documents: req.params.documentid } }),
      ])
      return res.status(200).send({ message: "User document deleted successfull" });
    })
  }
  catch (err) {
    common.log("Error while delet user document", err);
    res.status(400).send(err)
  }
}

module.exports.getTransactionUploads = async function (req, res) {
  try {
    var limitResults = 10;
    var skipResults = 1;
    if (req.body.limit != null && req.body.limit != '') {
      limitResults = req.body.limit;
    }

    if (req.body.skip != null && req.body.skip != '') {
      skipResults = (req.body.skip - 1) * limitResults;
    }

    var sort_by = req.body.sort_by || 'originalname';
    var sort_direction = req.body.sort_direction == 'asc' ? 1 : -1;
    var sortObj = {};
    var sort_split = sort_by.split(',');
    for (let index = 0; index < sort_split.length; index++) {
      sortObj[sort_split[index].trim()] = sort_direction;
    }
    let search = { user_id: req.payload._id, transaction_id: { $ne: null }, is_delete: false };

    let [count, transactionDocuments] = await Promise.all([
      UserDocument.count(search),
      UserDocument.find(search).populate("transaction_id").sort(sortObj).skip(skipResults).limit(limitResults).lean(),
    ])
    return res.send({ totalRecords: count, data: transactionDocuments })
  }
  catch (err) {
    common.log("Error while get transaction upload documents list:-", err);
    return res.status(400).send({ success: false, message: err.message });
  }
}

module.exports.getTransactionDocumentsZip = async function (req, res) {
  try {
    let ids = (req.body.documentIds || []).map(function (id) {
      return ObjectId(id)
    })
    let transactions = await UserDocument.find(
      { _id: { $in: ids } }
    ).populate('upload_documents').lean();
    let files = [], archiveFiles = [];
    for (let i = 0; i < transactions.length; i++) {
      const transaction = transactions[i];
      for (let j = 0; j < transaction.upload_documents.length; j++) {
        const document = transaction.upload_documents[j];
        files.push(document.s3_key)
        archiveFiles.push(i + "_" + j + "_" + document.originalname)
      }
    }
    console.log(files);
    res.attachment("transactions.zip");
    s3GetFileStream(files, archiveFiles, res);
  } catch (error) {
    common.log("Error while generate zip content", error)
    return res.status(400).send({ success: false, message: error.message });
  }

}


