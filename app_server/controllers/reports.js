const mongoose = require('mongoose');
const User = mongoose.model('User');
const TemporaryUsers = mongoose.model('TemporaryUsers');
const common = require('../helpers/common');
const config = require('../config/config');
const stripe = require("stripe")(config.stripe_key);
const { Parser } = require('json2csv');
const multer = require('multer')
const fileSystem = require('fs')
const _ = require('lodash');
const stripeHelper = require('../helpers/stripe');
const emailTemplateService = require('../services/emailTemplateService');
const SubscriptionService = require('../services/subscriptionServices');
const roleServices = require('../services/roleServices');
const ObjectId = require('mongodb').ObjectID;
const UserOfferHistory = mongoose.model('UserOfferHistory');

const incompleteToComplete= async(req,res)=>{
    try {
        let diff_with=  req.params.hours || 2;
        console.log(diff_with);
        let users = await User.aggregate([
            {
              '$match': {
                'registration_started': {
                  '$ne': null
                }
              }
            }, {
              '$project': {
                'created_at': 1, 
                'first_name': 1, 
                'last_name': 1, 
                'email': 1, 
                'phone': 1, 
                'company': 1, 
                'stripe_id': 1, 
                'role': 1, 
                'source_data': 1, 
                'subscription_type': 1, 
                'status': 1, 
                'payment_status': 1, 
                'last_login_at': 1, 
                'equipment_type': 1, 
                'difference': {
                  '$divide': [
                    {
                      '$subtract': [
                        '$createdAt', '$registration_started'
                      ]
                    }, 3600000
                  ]
                }
              }
            }, {
              '$group': {
                '_id': '$_id', 
                'totalDifference': {
                  '$sum': '$difference'
                },
                'created_at':{
                    '$first':'$created_at'
                }, 
                'first_name': {
                  '$first': '$first_name'
                }, 
                'last_name': {
                  '$first': '$last_name'
                }, 
                'email': {
                  '$first': '$email'
                }, 
                'phone': {
                  '$first': '$phone'
                }, 
                'company': {
                  '$first': '$company'
                }, 
                'stripe_id': {
                  '$first': '$stripe_id'
                }, 
                'source_data': {
                  '$first': '$source_data'
                }, 
                'role': {
                  '$first': '$role'
                }, 
                'subscription_type': {
                  '$first': '$subscription_type'
                }, 
                'status': {
                  '$first': '$status'
                }, 
                'payment_status': {
                  '$first': '$payment_status'
                }, 
                'last_login_at': {
                  '$first': '$last_login_at'
                }, 
                'equipment_type': {
                  '$first': '$equipment_type'
                }
              }
            }, {
              '$match': {
                'totalDifference': {
                  '$gte': Number(diff_with)
                }
              }
            }
          ]);

 

        let filename = 'IncompleteToComplete.csv';
        let fields = [
            {
                label: 'Sign Up Date', value: function (row) {
                    return row.created_at.toLocaleString()
                }
            },
            {
                label: 'First Name', value: 'first_name'
            },
            {
                label: 'Last Name', value: 'last_name'
            },
            { label: 'Phone Number', value: 'phone' },
            { label: 'Email', value: 'email' },
            { label: 'Role', value: 'role' },//roles are added in csv
            { label: 'Payment', value: 'payment_status' },
            { label: 'Subscription Type', value: 'subscription_type' }
        ];
        let opts = {
            fields: fields
        }
            const parser = new Parser(opts);
            const csv = parser.parse(users);
            //common.log(csv);
            res.set('Content-Disposition', ["attachment; filename=", filename].join(''));
            res.set('Content-Type', 'text/csv');
            return res.end(csv);
    
    }catch(error){
        common.log("ERROR in incompleteToComplete:___", error.message);
        return res.status(400).send({ success: false, message: err.message });
    }
}

const exportUserCSV = async function (req, res) {
	let tabStatus = req.params.status;
	let projection = {
    _id:1,
		created_at: 1,
    created_by: 1,
		first_name: 1,
		last_name: 1,
		email: 1,
		phone: 1,
		company: 1,
		stripe_id: 1,
		role: 1,
		source_data: 1,
		subscription_type: 1,
		status: 1,
    totalspent:1,
    card_type:1,
    sign_up_promotion:1,
		payment_status: 1,
        last_login_at: 1,
        equipment_type:1
	}

		var [freeSubsIds, publicRoles] = await Promise.all([
			SubscriptionService.getFreeSubscriptionsId(),
			roleServices.getPublicRoles()
		]);
		let privateRoles = await roleServices.getPrivateRoles();
        
		let today = new Date();
		today.setHours(0, 0, 0);
		let query = {};
		query['$and'] = [];
		if (tabStatus === 'active') {
			query["$and"].push(
				{ "source_data": { $ne: "posteverywhere" } }
			);
			query['$and'].push(
				{ $or: [{ is_internal: false }, { is_internal: null }] }, { "role": { $in: publicRoles } }

			);
			query['$and'].push(
				{
					status: 'active', $or: [
						{ "subscription_package": { $in: freeSubsIds } },
						{ "subscription_expire_on": { $gte: today } }
					],
          "payment_status": {$nin:["past_due"]} 
				}
			);
		}
		if (tabStatus === 'inactive') {
			query['$and'].push(
				{ $or: [{ is_internal: false }, { is_internal: null }] }, { "role": { $in: publicRoles } },
				{ "source_data": { $ne: "posteverywhere" } }

			);
			query['$and'].push(
				{
					$or: [{
						$and: [
							{ "subscription_package": { $nin: freeSubsIds } },
							{ "subscription_expire_on": { $lt: today } }
						]
					},
					{ status: 'inactive' },
          { "payment_status": {$in:["past_due"]}}
					]
				}
			);

		}
		try {
			let users = await User.find(query, projection)
            .populate('subscription_package')
            .populate('company_id')
            .populate('ref_Id')
            .populate('created_by')
            .sort({ created_at: -1 }).lean();
            // users= await process_stripe_customers(users);
            // let users1= await find_coupons_used(users)
            // return res.status(200).send(users);
			convertCSV(null, users, res)
		} catch (err) {
			common.log("Error while export user", err);
			return res.status(400).send({ success: false, message: err.message })
		}
}

const convertCSV = (err, users, res) => {
	if (err) return res.status(400).send(err);

	var fields = [
		{
			label: 'Sign Up Date', value: function (row) {
				return row?.created_at?.toLocaleString()
			}
		},
		{ label: 'First Name', value: 'first_name' },
		{ label: 'Last name', value: 'last_name' },
		{ label: 'Email', value: 'email' },
		{ label: 'Phone Number', value: 'phone' },
        { label: 'Role', value: 'role' },//roles are added in csv
	
		{ label: 'Subscription Type', value: 'subscription_type' },
        { label: 'Subscription Price', value: 'subscription_package.price' },

        { label: 'Equipment Type', value: (row)=>{
            return row.equipment_type?.toString()
        } },

        {
			label: 'CC on File', value: function (row) {
				if (row.stripe_id) return 'Yes'
				return 'No'
			}
		},
        {label:'Type of Card',value:'card_type'},
		    { label: 'Payment', value: 'payment_status' },
        { label: 'Status', value: 'status' },
        {label:'Sign Up Promotion',value:'sign_up_promotion'},
        {label:'Coupon Code Used',value:''},
        {label:'Registered by',value:'created_by.email'},
        {label:'Total Amount Spent',value: 'totalspent'},
        {label:'Total Months Subscribed',value:(row)=>{
        //   let end= new Date();
        //   let start= new Date(row.created_at);
        //   var timeDiff = Math.abs(end.getTime() - start.getTime());
        let total_months=  Math.round(Math.abs(new Date().getTime() - new Date(row.created_at).getTime()) / (2e3 * 3600 * 365.25))
          return total_months;
        }},
        { label: 'Company Name', value: 'company_id.company_name' },
        {label:'Company Address', value: 'company_id.address'},
        {label:'Company City', value: 'company_id.city'},
        {label:'Company Sate', value: 'company_id.state'},
        {label:'Business Email', value: 'company_id.email'},
        {label:'Company Phone', value: 'company_id.phone'},
        {label:'MC Number', value: 'company_id.mc_number'},
        {label:'Last Time Logged in', value: (row)=>{
          return row.last_login_at
        }},
	];
	var opts = {
		fields: fields
	}
	try {
		const parser = new Parser(opts);
		let csv;
		let filename = 'user.csv';
		csv = parser.parse(users);
		res.set('Content-Disposition', ["attachment; filename=", filename].join(''));
		res.set('Content-Type', 'text/csv');
		res.end(csv);
	} catch (err) {
		console.error(err);
        return res.status(400).send(err);
	}
}

const subscriptionsByStatus = async function (req, res) {
	try {
		let possibleStatus = ["active", "past_due", "unpaid", "canceled", "incomplete", "incomplete_expired", "trialing", "all", "ended"];
		if (possibleStatus.indexOf(req.params.status) == -1) {
			return res.status(400).send({ success: false, message: "Status is not valid value", possible_value: possibleStatus });
		}
		let users = [];
		let subscriptions = await stripeHelper.getSubscriptionsByStatus(req.params.status);
		for (let i = 0; i < subscriptions.length; i++) {
			const subscription = subscriptions[i];
			subscription.current_period_end = subscription.current_period_end ? new Date(subscription.current_period_end * 1000) : subscription.current_period_end;
			subscription.current_period_start = subscription.current_period_start ? new Date(subscription.current_period_start * 1000) : subscription.current_period_start;
			subscription.billing_cycle_anchor = subscription.billing_cycle_anchor ? new Date(subscription.billing_cycle_anchor * 1000) : subscription.billing_cycle_anchor;
			subscription.created = subscription.created ? new Date(subscription.created * 1000) : subscription.created;
			subscription.start_date = subscription.start_date ? new Date(subscription.start_date * 1000) : subscription.start_date;
			subscription.trial_end = subscription.trial_end ? new Date(subscription.trial_end * 1000) : subscription.trial_end;
			subscription.trial_start = subscription.trial_start ? new Date(subscription.trial_start * 1000) : subscription.trial_start;
			const user = await User.find({ stripe_id: subscription.customer }).lean();
			if (!user) {
				const customer = await stripeHelper.getCustomer(subscription.customer);
			}
		}
		const parser = new Parser();
		let csv;
		let filename = 'user.csv';
		csv = parser.parse(users);
		res.set('Content-Disposition', ["attachment; filename=", filename].join(''));
		res.set('Content-Type', 'text/csv');
		return res.end(csv.toString());
	} catch (err) {
		common.log('Error while exportUserToCSVByStripeStatus :--', err)
		return res.status(400).send({ success: false, message: err.message });
	}
}


// month diff between dates
const diffInMonths = (end, start) => {
    var timeDiff = Math.abs(end.getTime() - start.getTime());
    return Math.round(timeDiff / (2e3 * 3600 * 365.25));
 };
//total spending of user
const calculate_total_spent= async(customer_id)=>{
  //process invoices and return only relevant info
  let totalspent=0;
    try{
    let {data}= await stripeHelper.customerInvoices(customer_id);
    console.log("invoices ", data.length);
    
    if(data.length>0){
        data.map(async r=>{
          totalspent+= r.amount_paid/100;
        });
        return totalspent;
      }else{
        return totalspent;
      } 
}catch(error){
      common.log("ERROR in processing incvoice filter",error)
    return totalspent;
}
}

const process_stripe_customers= async(users)=>{
    try{
        let user_count= users.length;
        console.log(user_count);
        let opt={}
        let customers=[];
        if(user_count<100){
         customers= await stripeHelper.getCustomerList(opt);
          console.log("customers ",customers);
        }else{
            for(let i=0;i<user_count/100;i++){
             customers= await stripeHelper.getCustomerList(opt);
               console.log("customers ",customers);
               await common.snooze(100)
            }
        }
        return customers;
    }catch(error){
        console.log("ERROR in process stripe customers",error)
        return users;
    }
}


const referal_signed_up= async(req,res)=>{
  try {
      let users = await User.find({ref_Id:{$ne:null}})
      .populate({
        path: "ref_Id",
        populate: {
          path: "influencerId",
          model: "Influencer",
          select:"_id name email phone is_deleted created_at created_by"
        }
      }).populate({
        path: "ref_Id",
        populate: {
          path: "userId",
          model: "User",
          select:"_id first_name last_name email phone created_at created_by"
        }
      });

      let filename = 'referal.csv';
      let fields = [
          {
              label: 'Sign Up Date', value: function (row) {
                return row?.created_at?.toLocaleString()
              }
          },
          {
              label: 'First Name', value: 'first_name'
          },
          {
              label: 'Last Name', value: 'last_name'
          },
          { label: 'Phone Number', value: 'phone' },
          { label: 'Email', value: 'email' },
          { label: 'Role', value: 'role' },//roles are added in csv
          { label: 'Payment', value: 'payment_status' },
          { label: 'Subscription Type', value:  'subscription_type' },
          {label: 'ReferedBy Name', value: (row)=>{
            if(row?.ref_Id?.influencerId?.name)
            return row?.ref_Id?.influencerId?.name

            if(row?.ref_Id?.userId?.first_name)
            return row?.ref_Id?.userId?.first_name + ' ' + row?.ref_Id?.userId?.last_name 
          }},
          {label: 'ReferedBy Phone', value:(row)=>{
            if(row?.ref_Id?.influencerId?.phone)
            return row?.ref_Id?.influencerId?.phone

            if(row?.ref_Id?.userId?.phone)
            return row?.ref_Id?.userId?.phone
          }},
          {label: 'ReferedBy Email', value: (row)=>{
            if(row?.ref_Id?.influencerId?.email)
            return row?.ref_Id?.influencerId?.email

            if(row?.ref_Id?.userId?.email)
            return row?.ref_Id?.userId?.email
          }}
      ];
      let opts = {
          fields: fields
      }
          const parser = new Parser(opts);
          const csv = parser.parse(users);
          //common.log(csv);
          res.set('Content-Disposition', ["attachment; filename=", filename].join(''));
          res.set('Content-Type', 'text/csv');
          return res.end(csv);
  
  }catch(error){
      common.log("ERROR in incompleteToComplete:___", error.message);
      return res.status(400).send({ success: false, message: err.message });
  }
}


/**
 * find each user coupons used
 */
async function find_coupons_used(users){
try{
  let _user_arr=[];
 for(let i=0;i<users.length;i++){
    let offers_used=[];
   let coupons_used= await UserOfferHistory.find({user:users[i]._id})
    .populate('offer');

    console.log("offers used:___", coupons_used);
    users[i].coupons=offers_used;
    _user_arr.push(users[i]);
  }
return _user_arr;
}catch(error){
return users
}
}

const sales_report = async (req, res) => {
  try {
    if(!req.query.user_id)
     return res.status(400).send({status: false, message:"User id is required"})

    let users = await User.aggregate([
     { $facet:{
       data:[{
        "$lookup": {
          "from": "referals",
          "as": "referal",
          "localField": "ref_Id",
          "foreignField": "_id"
        }
      },
      {
        "$lookup": {
          "from": "companies",
          "as": "company",
          "localField": "company_id",
          "foreignField": "_id"
        }
      },
      { "$unwind": "$referal" },
      //match conditions
        {'$match':{'referal.userId':ObjectId(req.query.user_id)
        }},
      {
        '$project': {
          created_at: 1,
          first_name: 1,
          last_name: 1,
          email: 1,
          phone: 1,
          company: 1,
          stripe_id: 1,
          role: 1,
          source_data: 1,
          subscription_type: 1,
          status: 1,
          payment_status: 1,
        }
      }
    ],
    user_created_by:[
 
      {
        "$lookup": {
          "from": "companies",
          "as": "company",
          "localField": "company_id",
          "foreignField": "_id"
        }
      },
      //match conditions
        {'$match':{'created_by':ObjectId(req.query.user_id)
        }},
      {
        '$project': {
          created_at: 1,
          first_name: 1,
          last_name: 1,
          email: 1,
          phone: 1,
          company: 1,
          stripe_id: 1,
          role: 1,
          source_data: 1,
          subscription_type: 1,
          status: 1,
          payment_status: 1,
        }
      }
    ]
    
    }
  }
    ]);


    // console.log("users:__", users);
   users=[...users[0].data,...users[0].user_created_by]

    let filename = "salesReport.csv";
    let fields = [
      {
        label: "Sign Up Date", value: function (row) {
          return row?.created_at?.toLocaleString();
        }
      },
      { label: "First Name", value: "first_name" },
      { label: "Last Name", value: "last_name" },
      { label: "Phone Number", value: "phone" },
      { label: "Company", value: "company[0].company_name" },
      { label: "Email", value: "email" },
      { label: "Role", value: "role" },
      {
        label: "CC entered",
        value: function (row) {
          if (row.stripe_id) return "Yes";
          return "No";
        },
      },
      { label: "Subscription status", value: "subscription_type" },
      { label: "Status", value: "status" },
      { label: "Payment status", value: "payment_status" }
    ];
    let options = {
      fields: fields
    }
    const parser = new Parser(options);
    const csv = parser.parse(users);
    res.set('Content-Disposition', ["attachment; filename=", filename].join(''));
    res.set('Content-Type', 'text/csv');
    return res.end(csv);
  } catch (err) {
    console.log("Error in Sales Report:", err)
    return res.status(400).send({ success: false, message: err.message })
  }
}

module.exports={
    incompleteToComplete,
    exportUserCSV,
    referal_signed_up,
    sales_report
}