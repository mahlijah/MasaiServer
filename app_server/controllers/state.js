var mongoose = require('mongoose');
var State = mongoose.model('State');
var common = require('../helpers/common');
var config = require('../config/config');
var fs = require('fs');
var multer = require('multer')
var path = require('path');
var async = require("async");

module.exports.stateList = function (equipment, socket, callback) {
  'use strict';
  if (socket.decoded._id) {
   
    if(equipment.type.name !== undefined){
      var  equipmen = equipment.type.name;
    }else{
      var  equipmen = equipment.type;
    }

    var pdata= [];
    pdata.trucks= [];
    Truck.find({equipment_type: equipmen},function(err, ress){
        if(!err){
          ress.forEach(function(val){
            pdata.trucks.push(val.destination_bycity_state);
          });
        }        
        Load.find({equipment_type: equipmen},function(error, respo){
          if(!error){
            
            respo.forEach(function(val2){
              common.log(val2.delivery_states)
              //pdata.loads.push(val2.delivery_states);
            });
          }
        });
     });
    // Truck.aggregate([         
    //       {
    //         $lookup: { 
    //           from: "loads",
    //           localField: "equipment_type", 
    //           foreignField: "equipment_type", 
    //           as: "loads_data"
    //         }
    //       }
    //     ],
    //     function (err, state) {
    //       common.log(state)
    //       if (err) {
    //         callback(err);
    //       } else {
    //         state.forEach(function(data) {
    //           data.truck = 0;
    //           data.Loads = 0
    //           data.trucks.forEach(function(val){
    //               val.equipment_type.forEach(function(v){
    //                  if(v == equipment.type ) {
    //                     data.truck++;
    //                  }
    //               })
    //           })
    //          common.log(data.truck)
  
    //          data.loads.forEach(function(val){
    //             if(val.equipment_type == equipment.type ) {
    //               data.Loads++;
    //             }
    //           })
    //         });
    //         callback(state);
    //       }
    //     });




      // State.aggregate([
      //   {
      //     "$match": { from: "trucks", localField: "name", foreignField: "origin_bystate_states", as: "truck" }
      //   },
      //   {
      //     "$match": { from: "loads", localField: "abbreviation", foreignField: "pickup_states", as: "load" }
      //   },
      //   {
      //     $project: { name: 1, abbreviation: 1, trucks: "$truck", loads: "$load" }
      //   }],
      //   function (err, state) {
      //     if (err) {
      //       callback(err);
      //     } else {
      //       state.forEach(function(data) {
      //         data.truck = 0;
      //         data.Loads = 0
      //         data.trucks.forEach(function(val){
      //             val.equipment_type.forEach(function(v){
      //                if(v == equipment.type ) {
      //                   data.truck++;
      //                }
      //             })
      //         })
      //        common.log(data.truck)
  
      //        data.loads.forEach(function(val){
      //           if(val.equipment_type == equipment.type ) {
      //             data.Loads++;
      //           }
      //         })
      //       });
      //       callback(state);
      //     }
      //   });
  }
};




// module.exports.stateList = function (equipment, socket, callback) {
//   'use strict';
//   if (socket.decoded._id) {

//     State.aggregate([
//       {$match:{ from: 'trucks', localField:'state', foreignField:'origin_bystate_states',as:'truck'}},
//       {$match:{ from: 'loads', localField:'state2', foreignField:'pickup_states',as:'load'}},
//       {$project: { state: 1, state2: 1, trucks: "$truck", loads: "$state2" }}
//     ],
//       function( err, data ) {
//         if ( err )
//           throw err;
//         common.log( JSON.stringify( data, undefined, 2 ) );
    
//       }
//     );
//   }
// };
module.exports.create = function (req, res) {
  common.log("creation")
  var obj = {};
  obj.name = req.body.name;
  obj.abbreviation = req.body.abbreviation;
  var state = new State(obj);
  state.save(function (err, data) {
    if (err) {
      return res.status(400).send(err);
    } else {
      common.log(data, 'testing');
      return res.status(200).json(data, "State created successfully.");
    }

  });

};

module.exports.list = function (req, res) {
  State.find().exec(function (err, State) {
    if (err) {
      return res.status(200).json(err);
    } else {
      return res.status(200).json(State);
    }
  });
};


