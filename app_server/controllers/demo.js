const { ObjectId } = require('bson');
var mongoose = require('mongoose');
var VideoDemo=mongoose.model('Demo');

module.exports.addVideoDemo = async (req, res) => {
    'use strict';
    try{
        let {page_name,video_popup_title,video_title,video_link,video_description,video_instructions} = req.body;
        //finding video with page_name
        let findVideo = await VideoDemo.findOne({page_name:page_name}).lean()
        if(!findVideo){
            let videoDemo = new VideoDemo({
                page_name,
                video_popup_title,
                video_title,
                video_link,
                video_description,
                video_instructions         
       })
       //data save in new collection               
      let save_video=await videoDemo.save();
      res.status(200).json({success:true,data:save_video})
    }else{
        res.status(400).json({success:false,message:`on this page video already exist`})
    }
         
    }catch(error){
        let arr=[];
        Object.keys(req.body).filter(resData=>{
            Object.keys(error.errors).filter(requiredValidation=>{
                if(resData.includes(requiredValidation)){
                   arr.push(resData)
                }
            })
        })
        res.status(400).json({success:false,error:`${arr} is required`})
    }
     
}



module.exports.getVideoAllDemoRecords = async (req, res) => {
    'use strict';
    try{
        // Similar to 'limit'
        let page=req.body.skip || 1;
        let limit=req.body.limit || 10;
         
        
        if (limit != null && limit != '') {
            limit = limit;
          }
      
          if (page != null && page != '') {
              // For page 1, the skip is: (1 - 1) * 20 => 0 * 20 = 0
            page = (page - 1) * limit;
          }
           
       let findAllVideoRecords=await VideoDemo.find({}).skip(page).limit(limit).lean()
       if(findAllVideoRecords.length>0){
           res.status(200).json({success:true,data:findAllVideoRecords})
       }else{
        res.status(400).json({error:'There is no video records on any page'})
       }
    }
    catch(error){
        res.status(400).json({success:false,error})
    }
}




module.exports.getOneVideoDemoRecords = async (req, res) => {
    'use strict';
    let {page_name}=req.params;
    try{
       let videoRecord=await VideoDemo.findOne({page_name:page_name}).lean()
       if(videoRecord){
           res.status(200).json({success:true,data:videoRecord})
       }else{
        res.status(200).json({success:false,error:'This page name is not found'})
       }
    }
    catch(error){
        res.status(400).json({success:false,error})
    }
}


module.exports.updateOneVideoDemoRecords = async (req, res) => {
    'use strict';
    
    try{
        let {_id}=req.params;
        let {page_name,video_popup_title,video_title,video_link,video_description,video_instructions} = req.body;
        
        //implementing Validation
        const emptyishProperties = Object.entries(req.body)
        .filter(([, val]) => val === null || val === undefined || val === '')
        .map(([key]) => key);

        
    if(emptyishProperties.length<=0){
        let updateVideoRecord=await VideoDemo.findOne({_id:ObjectId(_id)}).lean()
        if(updateVideoRecord){
           let updateOne= await VideoDemo.findOneAndUpdate({_id:ObjectId(_id)},{$set:{page_name:page_name,video_popup_title:video_popup_title,video_title:video_title,video_link:video_link,video_description:video_description,video_instructions:video_instructions}},{upsert:true,omitUndefined: true,new:true}).exec()
           if(updateOne){
             res.status(200).json({data:updateOne})
           }
           
        }else{
         res.status(400).json({error:'There is no video records on this page'})
        }
    }else{
           res.status(400).json({message:`${emptyishProperties} is required`})
    } 
       
    }
    catch(error){
        
        res.status(400).json({error})
    }
}

