var mongoose = require('mongoose');
var AccountInfo = mongoose.model('AccountInfo');
var User = mongoose.model('User');
var Load = mongoose.model('Load');
var Message = mongoose.model('Message');
var config = require('../config/config');
var common = require('../helpers/common');
var FactoringRequest = mongoose.model('FactoringRequest');
const emailTemplateService=require('../services/emailTemplateService');

module.exports.accountInfo = function (data, socket) {
  'use strict';

  if (!socket.decoded._id) {
    socket.emit('account_info_response', { success: false, message: "UnauthorizedError: Private action" });
  } else {
    if (!data.account_holder_name || !data.account_number
      || !data.ifsc_code
    ) {
      socket.emit('account_info_response', { success: false, message: "All fields are required" });
      return;
    }
    var info = new AccountInfo();
    info.user_id = socket.decoded._id;
    info.account_holder_name = data.account_holder_name;
    info.account_number = data.account_number;
    info.ifsc_code = data.ifsc_code;
    info.save(function (err) {
      if (err) {
        socket.emit('account_info_response', { success: false, message: err.message });
      } else {
        User.findOneAndUpdate({ _id: socket.decoded._id }, { $set: { account_info: true } }, { 'new': true })
          .exec(function (err, load) {
            if (err) {
              socket.emit('account_info_response', { success: false, message: err.message }); s
            } else {
              Load.find({ created_by: socket.decoded._id })
                .populate('bids.user')
                .populate('accepted_bid_user')
                .populate('created_by')
                .exec(function (err, loads) {
                  socket.broadcast.emit('bidswon_response', { type: 'new', loads });
                });
              socket.emit('account_info_response', { success: true, message: "Account info saved." });

              // common.updateLoads(load, socket);  
            }
          });
      }
    });

  }

};

module.exports.factoringRequest = async function (req, res) {

  try {
    var data = req.body;
    common.log(data);
    if (!data.email && !data.phone) {
      res.status(400).send({ message: "either email or phone number is required" });
      return;
    }
    var request = new FactoringRequest();
    request.name = data.name;
    request.mcNumber = data.mc_Number;
    request.phone = data.phone;
    request.email = data.email;
    request.company = data.company;
    request.message = data.message;
    var result = await request.save();
    if (result) {
      await Promise.all([ 
              emailTemplateService.sendEmail('factor-request-for-support',{request},config.supportEmail),
              emailTemplateService.sendEmail('factor-request-for-user',{request},request.email)
            ])
      res.status(200).json({ message: "Request has been submitted successfully" });

    }
    else {
      common.log("error saving quote request.")
      res.status(400).send({ message: "error saving quote" });
      return;
    }

  }
  catch (error) {
    common.log("error occured", error);
    res.status(400).send({ message: "error saving quote" });

  }
}


module.exports.contactMessage = async function (req, res) {

  try {
    var data = req.body;
    common.log(data);
    if (!data.email && !data.phone) {
      res.status(400).send({ message: "either email or phone number is required" });
      return;
    }
    var msg = new Message();
    msg.first_name = data.first_name;
    msg.last_name = data.last_name;
    msg.phone = data.phone;
    var user = null;
    if (data.email != null) {
      user = await User.findOne({ email: data.email }).exec()
      if (user != null) msg.userId = user._id;
    }
    msg.email = data.email;
    msg.content = data.content;
    var result = await msg.save();
    if (result) {
      msg.content = msg.content.replace(/\n/g, "<br />");
     await Promise.all([
      emailTemplateService.sendEmail('fork-freight-support',{msg},config.supportEmail),
      emailTemplateService.sendEmail('contact-message-received',{msg},msg.email)
     ]) ;
      res.status(200).json({ message: "Thank you! Your message has been sent." });

    }
    else {
      common.log("error saving message.")
      res.status(400).send({ message: "error saving message" });
      return;
    }

  }
  catch (error) {
    common.log("error occured", error);
    res.status(400).send({ message: "error saving message" });

  }
}


