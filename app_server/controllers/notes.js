var mongoose = require('mongoose');
var Note = mongoose.model('Note');
var User = mongoose.model('User');
var TemporaryUsers = mongoose.model('TemporaryUsers');
var TempNotes = mongoose.model('TempNotes');
const {adminStoreLogs}= require('../controllers/userActivityLog');

module.exports.saveNote = function(req,res){
    if(!req.payload._id){
    return res.status(401).send({message:'UnauthorizedError: Private profile'})
    }
    if(!req.body.comment || !req.body.user_id){
        return res.status(400).send({message:'comment and user_id is required.'})
    }
    var note = new Note();
    note.comment= req.body.comment;
    note.user= req.body.user_id;
    note.created_by= req.payload._id;
    note.created_at= Date.now();
    
    note.save(function(err){
        if(err){
            console.error("Error when save note ----" , err);
            return res.status(400).send(err.message);
        }
        User.update({_id:req.body.user_id},{$push:{notes:note._id}},async function(err){
            if(err){
                return res.status(400).send(err.message);
            }

            await adminStoreLogs(req,null,"added user note","User")
            return res.status(200).send(note);
        });
        
    })
}


module.exports.saveNoteForTemporaryUser = function(req,res){
    
    if(!req.body.comment || !req.body.user_id){
        return res.status(400).send({message:'comment and user_id is required.'})
    }
    var note = new TempNotes();
    note.comment= req.body.comment;
    note.user= req.body.user_id;
    note.created_by= req.payload._id;
    note.created_at= Date.now();
    
    note.save(function(err){
        if(err){
            console.error("Error when save note ----" , err);
            return res.status(400).send(err.message);
        }
        TemporaryUsers.updateOne({_id:req.body.user_id},{$push:{notes:note._id}},function(err){
            if(err){
                return res.status(400).send(err.message);
            }
             
            return res.status(200).json(note);
        });
        
    })
}

