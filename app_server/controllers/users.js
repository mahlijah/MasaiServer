var mongoose = require("mongoose");
var User = mongoose.model("User");
var ResetPassword = mongoose.model("ResetPassword");
var TemporaryUsers = mongoose.model("TemporaryUsers");
const UserStripeDetail = mongoose.model('UserStripeDetail');
var UserStripeSubscriptionSchedule = mongoose.model(
  "UserStripeSubscriptionSchedule"
);
var randomstring = require("randomstring");
var common = require("../helpers/common");
var config = require("../config/config");
var stripe = require("stripe")(config.stripe_key);
const { Parser } = require("json2csv");
var multer = require("multer");
var path = require("path");
var fileSystem = require("fs");
const _ = require("lodash");
const { populate } = require("../models/price");
const stripeHelper = require("../helpers/stripe");
const emailTemplateService = require("../services/emailTemplateService");
const SubscriptionService = require("../services/subscriptionServices");
const roleServices = require("../services/roleServices");
const {adminStoreLogs}= require('../controllers/userActivityLog');
const { ObjectId } = require("mongodb");


module.exports.getAllUsersWithoutFilter = async function (req, res) {
  if (!req.payload._id) {
    return res
      .status(401)
      .send({ message: "UnauthorizedError: Private profile" });
  } else {
    try {
      let users = await User.find(
        {},
        {
          _id: 1,
          first_name: 1,
          last_name: 1,
          username: 1,
          avatar: 1,
          email: 1,
          company: 1,
          address: 1,
          phone: 1,
          subscription_type: 1,
          subscription_expire_on: 1,
          status: 1,
          role: 1,
          subscription: 1,
          stripe_id: 1,
        }
      )
        .sort({ created_at: -1 })
        .exec();
      return res.send(users);
    } catch (err) {
      console.error("ERROR:Find User", err);
      return res.status(400).send({ message: err.message });
    }
  }
};

module.exports.getIncompeleteUserById = async function (req, res) {
  let { _id } = req.params;
  let notes = [];
  let users;
  let sort = { created_at: -1 };
  try {
    users = await TemporaryUsers.findOne({ _id: ObjectId(_id) }).sort(sort).populate('company_id');

    if (users.notes.length > 0) {
      let tempusers = await TemporaryUsers.aggregate([
        { $match: { _id: ObjectId(_id) } },
        {
          $lookup: {
            from: "tempnotes",
            localField: "_id",
            foreignField: "user",
            as: "notes",
          },
        },
        { $unwind: "$notes" },
        {
          $lookup: {
            from: "users",
            localField: "notes.created_by",
            foreignField: "_id",
            as: "users",
          },
        },
        { $unwind: "$users" },
      ]);

      let results = tempusers.map((result) => {
        let { _id, created_at, comment, user, created_by } = result.notes;
        let { first_name, last_name } = result.users;
        notes.push({
          _id,
          created_at,
          comment,
          user,
          created_by: { _id: created_by, first_name, last_name },
        });
        delete result.users;
        delete result.notes;
        result.notes = notes;
        result.notes.reverse();
        return result;
      });
      return res.status(200).json(results[0]);
    } else {
      return res.status(200).json(users);
    }
  } catch (err) {
    console.error("ERROR:Find User", err);
    return res.status(400).send({ message: err.message });
  }
};

module.exports.getAllUsers = async function (req, res) {
  if (!req.payload._id) {
    return res
      .status(401)
      .send({ message: "UnauthorizedError: Private profile" });
  } else {
   if(req.body.created_by){
     let resp= await user_refered_by(req.body);
    return res.status(200).send(resp);
   }else{

    try {
      let [freeSubsIds, publicRoles] = await Promise.all([
        SubscriptionService.getFreeSubscriptionsId(),
        roleServices.getPublicRoles(),
      ]);
      let query = await common.getUserSearchObject(
        req.body,
        false,
        false,
        freeSubsIds,
        publicRoles
      );
      //query['is_internal'] = false;
      var limitResults = 20;
      var skipResults = 1;
      if (req.body.limit != null && req.body.limit != "") {
        limitResults = req.body.limit;
      }

      if (req.body.skip != null && req.body.skip != "") {
        skipResults = (req.body.skip - 1) * limitResults;
      }
      var sort_by = req.body.sort_by || "created_at";
      var sort_direction = req.body.sort_direction == "asc" ? 1 : -1;
      var sortObj = {};
      var sort_split = sort_by.split(",");
      for (let index = 0; index < sort_split.length; index++) {
        sortObj[sort_split[index].trim()] = sort_direction;
      }
      common.log(JSON.stringify(query));

      let totalcount = await User.find(query).count().exec();
      let users = await User.find(query, {
        _id: 1,
        first_name: 1,
        last_name: 1,
        username: 1,
        avatar: 1,
        email: 1,
        company: 1,
        address: 1,
        phone: 1,
        subscription_type: 1,
        subscription_expire_on: 1,
        status: 1,
        role: 1,
        subscription: 1,
        stripe_id: 1,
        created_at: 1,
        subscription_package: 1,
        payment_status: 1,
		    canceled_by : 1,
        user_status: {
          $switch: {
            branches: [
              {
                case: {
                  $and: [
                    { $eq:["$status", "active"] },
                    {
                      $or: [
                        {
                            $in: ["$subscription_package", freeSubsIds],
                        }
                      ],
                    },
                    {
                      $gte: ["$subscription_expire_on",new Date()],
                  },
                    { $ne: ["$payment_status", "past_due"] },
                    { $ne: ["$payment_status", "trialing"] },
                    { $ne: ["$payment_status", null] } // {}
                  ],
                },
                then: "paying",
              },
              {
                case: {
                  $and: [
                    { $eq:["$status", "active"] },
                    {
                      $or: [
                        {
                            $in: ["$subscription_package", freeSubsIds],
                        }, {
                          $gte: ["$subscription_expire_on",new Date()],
                      },
                      ],
                    },
                    { $eq: ["$payment_status", "active"] },
                  ],
                },
                then: "paying",
              },
              {
                case: {
                  $and: [
                    { $eq:["$status", "active"] },
                    {
                      $or: [
                        {
                          subscription_package: {
                            $in: ["$subscription_package", freeSubsIds],
                          },
                        },
                        {
                          subscription_expire_on: {
                            $gte: ["$subscription_expire_on", new Date()],
                          },
                        },
                      ],
                    },
                    {
                        $gte: ["$subscription_expire_on", new Date()],
                    },
                    { $eq: ["$payment_status", "trialing"] }, // {}
                  ],
                },
                then: "trialing",
              },
              {
                case: {
                  $and: [
                    { $eq:["$status", "active"] },
                    {
                      $or: [
                        {
                          subscription_package: {
                            $in: ["$subscription_package", freeSubsIds],
                          },
                        },
                        {
                          subscription_expire_on: {
                            $gte: ["$subscription_expire_on", new Date()],
                          },
                        },
                      ],
                    },
                    { $eq: ["$payment_status", "canceled"] }, // {}
                  ],
                },
                then: "canceled",
              },
              {
                case: {
                 $eq:["$status", "inactive"]
                },
                then: "Deactivated_by_admin",
              },
			        {case:{ $eq: ["$payment_status", "past_due"] },
			         then:"past_due"},
               {case:{$and:[ {$gte:["$subscription_expire_on",new Date()]}, { $eq: ["$payment_status", "active"] } ]},
               then: "paying"
               },
              {case:{$lt:["$subscription_expire_on",new Date()]},
              then: "subscription expired"
              },
             
                  ],
            default: "$payment_status",
          },
        },


        /** 
		 $or: [
				// {subscription:false},
				{$and:[{"subscription_package":{$nin:freeSubscriptionId}},
				{ subscription_expire_on: { $lt: today } }]},
				{ status: "inactive" },
				{ "payment_status": {$in:["past_due"]}}
				// {"subscription_package.is_free":false}
			  ],
				payment_status: {
					$ifNull: [
						'$payment_status',
						{
							$cond: [
								{ $lt: ['$subscription_expire_on', new Date()] },
								'free trial expired',
								'free trial no cc'
							]
						}
					]
				}
				*/
      })
        .populate("subscription_package")
        .populate("canceled_by")
        .sort(sortObj)
        .limit(limitResults)
        .skip(skipResults)
        .lean();
      return res.send({
        count: totalcount,
        data: users,
      });
    } catch (err) {
      console.error("ERROR:Find User", err);
      return res.status(400).send({ message: err.message });
    }
  }
}
};
module.exports.getAllInternalUsers = async function (req, res) {
  if (!req.payload._id) {
    return res
      .status(401)
      .send({ message: "UnauthorizedError: Private profile" });
  } else {
    if(req.body.created_by){
      req.body.status='internal';
let resp= await user_refered_by(req.body);
return res.status(200).send(resp);
    }else{
    try {
      let publicRoles = await roleServices.getPrivateRoles();
      let query = await common.getUserSearchObject(
        req.body,
        false,
        true,
        [],
        publicRoles
      );
      var limitResults = 20;
      var skipResults = 0;
      if (req.body.limit != null && req.body.limit != "") {
        limitResults = req.body.limit;
      }

      if (req.body.skip != null && req.body.skip != "") {
        skipResults = (req.body.skip - 1) * limitResults;
      }
      var sort_by = req.body.sort_by || "created_at";
      var sort_direction = req.body.sort_direction == "asc" ? 1 : -1;
      var sortObj = {};
      var sort_split = sort_by.split(",");
      for (let index = 0; index < sort_split.length; index++) {
        sortObj[sort_split[index].trim()] = sort_direction;
      }
      let totalcount = await User.find(query).count().exec();

      let users = await User.find(query, {
        _id: 1,
        first_name: 1,
        last_name: 1,
        username: 1,
        avatar: 1,
        email: 1,
        company: 1,
        address: 1,
        phone: 1,
        subscription_type: 1,
        subscription_expire_on: 1,
        status: 1,
        role: 1,
        subscription: 1,
        stripe_id: 1,
        created_at: 1,
        payment_status: 1,
      })
        .sort(sortObj)
        .limit(limitResults)
        .skip(skipResults)
        .exec();
      return res.send({
        count: totalcount,
        data: users,
      });
    } catch (err) {
      console.error("ERROR:Find User", err);
      return res.status(400).send({ message: err.message });
    }
  }
}
};

module.exports.getAllInCompleteUsers = async function (req, res) {
  if (!req.payload._id) {
    return res
      .status(401)
      .send({ message: "UnauthorizedError: Private profile" });
  } else {
    if(req.body.created_by){
      req.body.status='incomplete';
      let resp = await user_refered_by(req.body);
      return res.status(200).send(resp);
    }else{
    try {
      let query = await common.getUserSearchObject(req.body);
      var limitResults = 20;
      var skipResults = 1;
      if (req.body.limit != null && req.body.limit != "") {
        limitResults = req.body.limit;
      }

      if (req.body.skip != null && req.body.skip != "") {
        skipResults = (req.body.skip - 1) * limitResults;
      }
      var sort_by = req.body.sort_by || "created_at";
      var sort_direction = req.body.sort_direction == "asc" ? 1 : -1;
      var sortObj = {};
      var sort_split = sort_by.split(",");
      for (let index = 0; index < sort_split.length; index++) {
        sortObj[sort_split[index].trim()] = sort_direction;
      }
      let totalcount = await TemporaryUsers.find(query).count().exec();

      let users = await TemporaryUsers.find(query, {
        _id: 1,
        first_name: 1,
        last_name: 1,
        email: 1,
        company: 1,
        address: 1,
        phone: 1,
        role: 1,
        created_at: 1,
      })
        .sort(sortObj)
        .limit(limitResults)
        .skip(skipResults)
        .exec();

      return res.send({
        count: totalcount,
        data: users,
      });
    } catch (err) {
      console.error("ERROR:Find TemporaryUsers", err);
      return res.status(400).send({ message: err.message });
    }
  }
}
};

module.exports.getPostEveryWhereUsers = async function (req, res) {
  if (!req.payload._id) {
    return res
      .status(401)
      .send({ message: "UnauthorizedError: Private profile" });
  } else {
    try {
      let query = await common.getUserSearchObject(req.body, true);
      var limitResults = 20;
      var skipResults = 1;
      if (req.body.limit != null && req.body.limit != "") {
        limitResults = req.body.limit;
      }

      if (req.body.skip != null && req.body.skip != "") {
        skipResults = (req.body.skip - 1) * limitResults;
      }
      var sort_by = req.body.sort_by || "created_at";
      var sort_direction = req.body.sort_direction == "asc" ? 1 : -1;
      var sortObj = {};
      var sort_split = sort_by.split(",");
      for (let index = 0; index < sort_split.length; index++) {
        sortObj[sort_split[index].trim()] = sort_direction;
      }
      let totalcount = await User.find(query).count().exec();

      let users = await User.find(query, {
        _id: 1,
        first_name: 1,
        last_name: 1,
        email: 1,
        company: 1,
        address: 1,
        phone: 1,
        role: 1,
        created_at: 1,
        payment_status: 1,
        source_data: 1,
      })
        .sort(sortObj)
        .limit(limitResults)
        .skip(skipResults)
        .exec();
      return res.send({
        count: totalcount,
        data: users,
      });
    } catch (err) {
      console.error("ERROR:Find TemporaryUsers", err);
      return res.status(400).send({ message: err.message });
    }
  }
};

module.exports.exportUserToCSV = async function (req, res) {
  let projection = {
    created_at: 1,
    first_name: 1,
    last_name: 1,
    email: 1,
    phone: 1,
    company: 1,
    role: 1,
    stripe_id: 1,
    subscription_type: 1,
    status: 1,
    payment_status: 1,
    /** 
		payment_status: {
			$ifNull: [
				'$payment_status',
				{
					$cond: [
						{ $lt: ['$subscription_expire_on', new Date()] },
						'free trial expired',
						'free trial no cc'
					]
				}
			]
		}
		*/
  };
  let registerUser = User.find({}, projection).sort({ created_at: -1 });
  let temporaryUser = TemporaryUsers.find({}, projection).sort({
    created_at: -1,
  });
  // let unSubscribeUser = User.find({ email: { $regex: 'pels.*', $options: 'i' } }, projection).sort({ created_at: -1 })
  Promise.all([registerUser, temporaryUser]).then((results) => {
    if (results) {
      convertAllTabDataToCSV(undefined, results, res);
    } else {
      convertAllTabDataToCSV("error", "", "");
    }
  });
};

module.exports.exportUserToCSVByStripeStatus = async function (req, res) {
  try {
    let possibleStatus = [
      "active",
      "past_due",
      "unpaid",
      "canceled",
      "incomplete",
      "incomplete_expired",
      "trialing",
      "all",
      "ended",
    ];
    if (possibleStatus.indexOf(req.params.status) == -1) {
      return res
        .status(400)
        .send({
          success: false,
          message: "Status is not valid value",
          possible_value: possibleStatus,
        });
    }
    let users = [];
    let subscriptions = await stripeHelper.getSubscriptionsByStatus(
      req.params.status
    );
    for (let i = 0; i < subscriptions.length; i++) {
      const subscription = subscriptions[i];
      subscription.current_period_end = subscription.current_period_end
        ? new Date(subscription.current_period_end * 1000)
        : subscription.current_period_end;
      subscription.current_period_start = subscription.current_period_start
        ? new Date(subscription.current_period_start * 1000)
        : subscription.current_period_start;
      subscription.billing_cycle_anchor = subscription.billing_cycle_anchor
        ? new Date(subscription.billing_cycle_anchor * 1000)
        : subscription.billing_cycle_anchor;
      subscription.created = subscription.created
        ? new Date(subscription.created * 1000)
        : subscription.created;
      subscription.start_date = subscription.start_date
        ? new Date(subscription.start_date * 1000)
        : subscription.start_date;
      subscription.trial_end = subscription.trial_end
        ? new Date(subscription.trial_end * 1000)
        : subscription.trial_end;
      subscription.trial_start = subscription.trial_start
        ? new Date(subscription.trial_start * 1000)
        : subscription.trial_start;
      const user = await User.find({ stripe_id: subscription.customer }).lean();
      if (!user) {
        const customer = await stripeHelper.getCustomer(subscription.customer);
      }
    }
    const parser = new Parser();
    let csv;
    let filename = "user.csv";
    csv = parser.parse(users);
    res.set(
      "Content-Disposition",
      ["attachment; filename=", filename].join("")
    );
    res.set("Content-Type", "text/csv");
    return res.end(csv.toString());
  } catch (err) {
    common.log("Error while exportUserToCSVByStripeStatus :--", err);
    return res.status(400).send({ success: false, message: err.message });
  }
};

module.exports.exportToCSVOnTab = async function (req, res) {
  let tabStatus = req.params.tabStatus;
  let projection = {
    created_at: 1,
    first_name: 1,
    last_name: 1,
    email: 1,
    phone: 1,
    company: 1,
    stripe_id: 1,
    role: 1,
    source_data: 1,
    subscription_type: 1,
    status: 1,
    payment_status: 1,
    /** 
		payment_status: {
			$ifNull: [
				'$payment_status',
				{
					$cond: [
						{ $lt: ['$subscription_expire_on', new Date()] },
						'free trial expired',
						'free trial no cc'
					]
				}
			]
		}
		*/
  };

  if (tabStatus === "getInCompleteUsers") {
    TemporaryUsers.find({}, projection)
      .sort({ created_at: -1 })
      .populate('company_id')
      .exec(function (err, users) {
        convertCSV(err, users, res);
      });
  } else {
    var [freeSubsIds, publicRoles] = await Promise.all([
      SubscriptionService.getFreeSubscriptionsId(),
      roleServices.getPublicRoles(),
    ]);
    let privateRoles = await roleServices.getPrivateRoles();
    let today = new Date();
    today.setHours(0, 0, 0);
    let query = {};
    query["$and"] = [];
    if (tabStatus === "getActiveUsers") {
      query["$and"].push({ source_data: { $ne: "posteverywhere" } });
      query["$and"].push(
        { $or: [{ is_internal: false }, { is_internal: null }] },
        { role: { $in: publicRoles } }
      );
      query["$and"].push({
        status: "active",
        $or: [
          { subscription_package: { $in: freeSubsIds } },
          { subscription_expire_on: { $gte: today } },
        ],
        "payment_status": {$nin:["past_due"]} 
      });
    }
    if (tabStatus === "getInactiveUsers") {
      query["$and"].push(
        { $or: [{ is_internal: false }, { is_internal: null }] },
        { role: { $in: publicRoles } },
        { source_data: { $ne: "posteverywhere" } }
      );
      query["$and"].push({
        $or: [
          {
            $and: [
              { subscription_package: { $nin: freeSubsIds } },
              { subscription_expire_on: { $lt: today } },
            ],
          },
          { status: "inactive" },
          { "payment_status": {$in:["past_due"]}}
        ],
      });
    }
    if (tabStatus === "getInternalUsers") {
      query["$and"].push(
        { $or: [{ is_internal: true }, { role: { $in: privateRoles } }] },
        { source_data: { $ne: "posteverywhere" } }
      );
    }
    if (tabStatus === "getPostEveryWhereUsers") {
      query["$and"].push(
        { $or: [{ is_internal: false }, { is_internal: null }] },
        { source_data: { $eq: "posteverywhere" } }
      );
    }
    try {
      let users = await User.find(query, projection)
        .populate('company_id')
        .sort({ created_at: -1 })
        .lean();

      convertCSV(null, users, res);
    } catch (err) {
      common.log("Error while export user", err);
      return res.status(400).send({ success: false, message: err.message });
    }
  }
};

const convertCSV = (err, users, res) => {
  if (err) return res.status(400).send(err);

  var fields = [
    {
      label: "Sign Up Date",
      value: function (row) {
        return row.created_at.toLocaleString();
      },
    },
    { label: "First Name", value: "first_name" },
    { label: "Last name", value: "last_name" },
    { label: "email", value: "email" },
    { label: "phone", value: "phone" },
    { label: "company", value: "company_id.company_name" },
    { label: "role", value: "role" }, //roles are added in csv
    {
      label: "CC entered",
      value: function (row) {
        if (row.stripe_id) return "Yes";
        return "No";
      },
    },
    { label: "Subscription status", value: "subscription_type" },
    { label: "Status", value: "status" },
    { label: "Payment status", value: "payment_status" },
  ];
  var opts = {
    fields: fields,
  };
  try {
    const parser = new Parser(opts);
    let csv;
    let filename = "user.csv";
    csv = parser.parse(users);
    res.set(
      "Content-Disposition",
      ["attachment; filename=", filename].join("")
    );
    res.set("Content-Type", "text/csv");
    res.end(csv);
  } catch (err) {
    console.error(err);
  }
};

const convertAllTabDataToCSV = (err, users, res) => {
  if (err) return res.status(400).send(err);

  var fields = [
    {
      label: "Sign Up Date",
      value: function (row) {
        return row.created_at.toLocaleString();
      },
    },
    { label: "First Name", value: "first_name" },
    { label: "Last name", value: "last_name" },
    { label: "email", value: "email" },
    { label: "phone", value: "phone" },
    { label: "company", value: "company" },
    { label: "role", value: "role" },
    {
      label: "CC entered",
      value: function (row) {
        if (row.stripe_id) return "Yes";
        return "No";
      },
    },
    { label: "Subscription status", value: "subscription_type" },
    { label: "Status", value: "status" },
  ];
  var opts = {
    fields: fields,
  };
  try {
    const parser = new Parser(opts);
    let csv;
    let csv1;
    let csv2;
    let filename = "user.csv";
    csv = parser.parse(users[0]);
    csv1 = parser.parse(users[1]);
    csv2 = parser.parse(users[2]);
    let dataCsv = [csv, csv1, csv2];
    res.set(
      "Content-Disposition",
      ["attachment; filename=", filename].join("")
    );
    res.set("Content-Type", "text/csv");
    res.end(dataCsv.toString());
  } catch (err) {
    console.error(err);
  }
};

module.exports.getUser = async function (req, res) {
  if (!req.payload._id) {
    return res
      .status(401)
      .send({ message: "UnauthorizedError: Private profile" });
  } else if (!req.params.id) {
    return res.status(400).send({ message: "User id is required" });
  } else {
    let user = await User.findById(req.params.id, { salt: 0, hash: 0, __v: 0 })
      .populate({
        path: "notes",
        options: {
          sort: { created_at: -1 },
        },
        select: "_id created_at created_by comment",
        populate: {
          path: "created_by",
          model: "User",
          select: "first_name last_name ",
        },
      })
      .populate({
        path: "created_by",
        model: "User",
      })
      .populate({
        path: "schedule",
        populate: {
          path: "subscription_package",
        },
      })
      .populate({
        path: "upload_documents",
        match: { is_delete: 0 },
      })
      .populate("company_id")
      .populate("subscription_package")
      .populate("free_subscription_package_after_cancel_paid")
      .populate({
        path: "ref_Id",
        populate: {
          path: "influencerId",
          model: "Influencer",
          select:"_id name email phone is_deleted created_at created_by"
        }
      })
      .populate({
        path: "ref_Id",
        populate: {
          path: "userId",
          model: "User",
          select:"_id first_name last_name email phone is_deleted created_at created_by role"
        }
      })

      .lean();
    if (!user)
      return res
        .status(404)
        .send({ success: false, message: "User nor found" });
    if (user.stripe_id) {
      try {
        user.payment_methods = await stripeHelper.getPaymentMethods(
          user.stripe_id
        );
        const customer = await stripeHelper.getCustomer(user.stripe_id);
        user.customer = customer;
        if (
          customer &&
          customer.subscriptions &&
          customer.subscriptions.data &&
          customer.subscriptions.total_count > 0
        ) {
          user.subscriptions = customer.subscriptions.data[0];
          user.payment_status = user.subscriptions.status;
          await User.updateOne(
            { _id: user._id },
            {
              $set: {
                payment_status: user.subscriptions.status,
              },
            }
          );
        } else {
          const subscriptions =
            await stripeHelper.getCustomerCancelSubscriptions(user.stripe_id);
          if (subscriptions.data && subscriptions.data.length > 0) {
            user.subscriptions = subscriptions.data[0];
            user.payment_status = user.subscriptions.status;
            await User.updateOne(
              { _id: user._id },
              {
                $set: {
                  payment_status: user.subscriptions.status,
                },
              }
            );
          }
        }
      } catch (err) {
        // await User.updateOne({ _id: user._id }, { $set: { stripe_id: null } });
        // user.stripe_id = null;
      }
    }
    return res.send(user);
  }
};

module.exports.userActivate = function (req, res) {
  if (!req.payload._id) {
    return res
      .status(401)
      .send({ message: "UnauthorizedError: Private profile" });
  } else {
    let prev_data;
    User.findById(req.params.id).exec(function (err, user) {
      if (err) {
        return res.status(404).send({ message: "User not found" });
      }
      if (user != null && user.status == "active") {
        return res.status(200).send({ message: "user already active" });
      }
      User.updateOne(
        { _id: user._id },
        { $set: { status: "active" } },
        async function (err) {
          if (err) return res.status(400).send({ message: err.message });

          await adminStoreLogs(req,user,"set User to active","User")
          return res.status(200).send({ message: "activation success" });
        }
      ).exec();
    });
  }
};

module.exports.cancelSubscription = async function (req, res) {
  try {
    if (!req.payload._id) {
      return res
        .status(401)
        .send({ message: "UnauthorizedError: Private profile" });
    }
    if (!req.body.reason) {
      return res
        .status(400)
        .send({ field: "reason", message: "cancel reason is required." });
    }
    //get user info from db
    var user_info = await User.findOne({ _id: req.params.id })
      .populate("subscription_package")
      .exec();

    //if no user found
    if (!user_info)
      return res
        .status(404)
        .send({ success: false, message: "User not found" });
    //if user have free package
    if (
      user_info.subscription_package &&
      user_info.subscription_package.is_free
    ) {
      await User.updateOne(
        { _id: user._id },
        {
          $set: {
            subscription_expire_on: new Date(),
            subscription: false,
            subscription_cancel_reason: req.body.reason,
            subscription_canceled_on: new Date(),
          },
        }
      );
      await emailTemplateService.sendEmail(
        "admin-cancel-user-subscription",
        { user },
        user.email
      );
      return res.status(200).json({ success: true, status: 200 });
    }

    //if user have subscription and stripe id in db
    if (user_info && user_info.stripe_id) {
      //get customer info from stripe
      const customer_stripe_info = await stripeHelper.getCustomer(
        user_info.stripe_id
      );
      if (!customer_stripe_info) {
        return res
          .status(400)
          .send({
            success: false,
            message: "Failed to retrieve customer info",
          });
      }

      if (
        !customer_stripe_info.deleted &&
        customer_stripe_info.subscriptions &&
        customer_stripe_info.subscriptions.data &&
        customer_stripe_info.subscriptions.data[0]
      ) {
        //cancel user subscription from stripe
        const subscription = await stripeHelper.cancelSubscription(
          customer_stripe_info.subscriptions.data[0].id
        );
        let subscription_expire = new Date(
          subscription.current_period_end * 1000
        ).toISOString();
        //update  user info
        await User.updateOne(
          { _id: user_info._id },
          {
            $set: {
              subscription_expire_on: subscription_expire,
              subscription: false,
              subscription_cancel_reason: req.body.reason,
              subscription_canceled_on: new Date(
                subscription.canceled_at * 1000
              ).toISOString(),
            },
          }
        );
        //send email to user
        await emailTemplateService.sendEmail(
          "admin-cancel-user-subscription",
          { user: user_info },
          user_info.email
        );

        return res.status(200).json({ success: true, status: 200 });
      } else {
        return res
          .status(400)
          .send({ success: false, message: "Subscription already cancel" });
      }
    } else {
      return res.status(400).send({ message: "User subscription not found" });
    }

    /**
     * below commented code now depricated
     */
    // User.findOne({ _id: req.params.id }).populate('subscription_package').exec(async function (err, user) {
    // 	if (!err) {

    // 		if(user.subscription_package && user.subscription_package.is_free){
    // 			await User.updateOne({ _id: user._id }, {
    // 			  $set: {
    // 				subscription_expire_on: new Date(),
    // 				subscription: false,
    // 				subscription_cancel_reason: req.body.reason,
    // 				subscription_canceled_on : new Date()
    // 			  }
    // 			});
    // 			await emailTemplateService.sendEmail('admin-cancel-user-subscription', { user }, user.email)
    // 			return res.status(200).json({ success: true, status: 200 });
    // 		  }
    // 		if (user && user.stripe_id) {
    // 			stripe.customers.retrieve(user.stripe_id, function (err, confirmation) {
    // 				if (err) {
    // 					return res.status(400).send({ success: false, message: err.message });
    // 				}  else if(!confirmation.deleted && confirmation.subscriptions && confirmation.subscriptions.data && confirmation.subscriptions.data[0]) {
    // 					var subscription_expire = new Date(confirmation.subscriptions.data[0].current_period_end * 1000).toISOString();
    // 					//cancel user subscription
    // 					console.log()
    // 					// const subscription = await stripeHelper.cancelSubscription(customer.subscriptions.data[0].id);
    // 					User.update({ _id: user.id }, { $set: { subscription_expire_on: subscription_expire, subscription: false, subscription_cancel_reason: req.body.reason } }, async function (err) {
    // 						if (!err) {
    // 							stripe.customers.del(user.stripe_id, async function (err, confirmation) {
    // 								if (!err) {

    // 									await emailTemplateService.sendEmail('admin-cancel-user-subscription', { user }, user.email)

    // 									return res.status(200).json({ success: true, status: 200 });
    // 								} else {
    // 									common.log("error deleting user from stripe", err);
    // 									res.status(400).send({ success: false, message: err.message });
    // 									return;
    // 								}
    // 							});
    // 						} else {
    // 							res.status(400).send({ success: false, message: err.message });
    // 							return;
    // 						}
    // 					});

    // 				} else {
    // 					return res.status(400).send({ success: false, message: "Subscription already cancel" });
    // 				}
    // 			});
    // 		} else {
    // 			return res.status(400).send({ message: 'User subscription not found' });
    // 		}
    // 	} else {
    // 		return res.status(404).json(err);
    // 	}
    // });
  } catch (err) {
    return res.status(400).send({ success: false, message: err.message });
  }finally{
    if(req?.params?.id)
    await adminStoreLogs(req,user_info,"cancel user subscription","User")
  }
};

module.exports.userInactive = function (req, res) {
  if (!req.payload._id) {
    return res
      .status(401)
      .send({ message: "UnauthorizedError: Private profile" });
  } else {
    User.findById(req.params.id).exec(function (err, user) {
      if (err) {
        return res.status(404).send({ message: "User not found" });
      }
      if (user != null && user.status == "inactive") {
        return res.status(200).send({ message: "user already inactive" });
      }
      User.updateOne(
        { _id: user._id },
        { $set: { status: "inactive" } },
       async function (err) {
          if (err) return res.status(400).send({ message: err.message });

          await adminStoreLogs(req,user,"Deactivate user","User")
          return res.status(200).send({ message: "deactivation success" });
        }
      ).exec();
    });
  }
};

module.exports.setAsInternal = function (req, res) {
  User.findById(req.params.id).exec(function (err, user) {
    if (err) {
      return res.status(404).send({ message: "User not found" });
    }
    if (user != null && user.is_internal == true) {
      return res.status(200).send({ message: "user already is internal" });
    }
    User.updateOne(
      { _id: user._id },
      { $set: { is_internal: true } },
      async function (err) {
        if (err) return res.status(400).send({ message: err.message });

        await adminStoreLogs(req,user,"Mark user as internal","User")
        return res
          .status(200)
          .send({ message: "Successfully set as internal user" });
      }
    ).exec();
  });
};

module.exports.unSetAsInternal = function (req, res) {
  User.findById(req.params.id).exec(function (err, user) {
    if (err) {
      return res.status(404).send({ message: "User not found" });
    }
    if (user != null && user.is_internal == false) {
      return res.status(200).send({ message: "User is not an internal user." });
    }
    User.updateOne(
      { _id: user._id },
      { $set: { is_internal: false } },
      async function (err) {
        if (err) return res.status(400).send({ message: err.message });

        await adminStoreLogs(req,user,"remove User from Internal category","User")
        return res
          .status(200)
          .send({ message: "Successfully unset as internal user" });
      }
    ).exec();
  });
};

module.exports.changePassword = async function (req, res) {
  try {
    var user = await User.findById(req.params.id).lean();
    if (!user) {
      return res
        .status(401)
        .send({ success: false, message: "UnauthorizedError: User not found" });
    }
    const url = req.headers.origin || process.env.PORTAL_URL;
    var date = new Date();
    date.setHours(date.getHours() + 2);
    var resetPassword = new ResetPassword();
    resetPassword.user_id = user._id;
    resetPassword.expired_at = date;
    resetPassword.generateResetToken();
    resetPassword.save();
    console.log(resetPassword);
    var resetPasswordLink =
      url + "/public/reset-password/" + resetPassword.reset_token;
    await emailTemplateService.sendEmail(
      "reset-password",
      { user },
      user.email,
      resetPasswordLink
    );
    return res
      .status(200)
      .json({
        success: true,
        message: "Password has been sent to registered email",
      });
  } catch (err) {
    common.log("user not found");
    return res.status(401).send({ success: false, message: err.message });
  }finally{
    await adminStoreLogs(req,user,"Regenerate Password","User")
  }
};

module.exports.updateUser = async function (req, res) {
	try {    
		let stripeupdateflag= false;
		var user = await User.findById(req.params.id).populate('company_id').lean();

    var prev_data= Object.assign({},user);

		if (!user) return res.status(404).send({ message: "User not found" });
		// if (req.body.email && req.body.email != user.email) {
		// 	user.email = req.body.email;
		// 	user.emailVerified = false;
		// }
		if (req.body.first_name) user.first_name = req.body.first_name;
		if (req.body.last_name) user.last_name = req.body.last_name;
		if (req.body.phone && req.body.phone != user.phone) {
			user.phone = req.body.phone;
			user.phoneVerified = false;
		}
		if (req.body.company_id) user.company_id = req.body.company_id;

		if (req.body.role && req.body.role != user.role) {
			user.role = req.body.role;
		}

    //equipment type update
    if(req.body.equipment_type){
      user.equipment_type=req.body.equipment_type
    }

    if (req.body.user_created_by) {
      user.created_by = req.body.user_created_by
    }    

		//update email for user
		if(req.body.email){

			let Email_validation= await validate_update_email(req.body.email, user._id);
			if(Email_validation.success){
			  if(req.body.email!==user.email){
				user.email= req.body.email;
				stripeupdateflag= true;
			  }
	
			}else{
			  common.log("ERROR in email validation:__", Email_validation);
			   return res.status(400).send(Email_validation);
			}
		}

    
		if (req.body.subscription_package && req.body.subscription_package != user.subscription_package) {
			let sp = await SubscriptionService.getSubscriptionPackageById(req.body.subscription_package);
			// console.log("subscription user want:__", sp)
			if (user.stripe_id) {
				const customer = await stripeHelper.getCustomer(user.stripe_id);
				//when updating user to free package
				if(sp.is_free){
		    	if (!customer.deleted && customer.subscriptions && customer.subscriptions.data && customer.subscriptions.data[0]) {
				//cancel user subscription from stripe 
				const subscription = await stripeHelper.cancelSubscription(customer.subscriptions.data[0].id);
				let subscription_expire = new Date(subscription.current_period_end * 1000).toISOString();
				//update  user info
	            user.subscription_package = sp._id;
				user.subscription_type = sp.subscription_name;
				user.subscription_expire_on= subscription_expire,
				user.subscription= true,
				user.subscription_cancel_reason= "Updated subscription to Free",
				user.subscription_canceled_on= new Date(subscription.canceled_at * 1000).toISOString();
				//send email to user 
				// await emailTemplateService.sendEmail('admin-cancel-user-subscription', { user: user_info }, user_info.email)
				
				}else{
					let subscription_expire = new Date().toISOString();
					//update  user info
					user.subscription_package = sp._id;
					user.subscription_type = sp.subscription_name;
					user.subscription_expire_on= subscription_expire,
					user.subscription= true,
					user.subscription_cancel_reason= "Updated subscription to Free",
					user.subscription_canceled_on= new Date().toISOString();
					//send email to user 
					// await emailTemplateService.sendEmail('admin-cancel-user-subscription', { user: user_info }, user_info.email)
					
				}
			}

		    	if(sp.stripe_plan_id && sp.stripe_price_id){
				if (customer.subscriptions && customer.subscriptions.data && customer.subscriptions.data.length > 0) {
					const subscription = await stripeHelper.changeSubscription(customer.subscriptions.data[0].id, sp.stripe_plan_id);
				} else {
					let trial_for_plan = true;
					let trial_period_days;
					if (user.subscription_expire_on) {
						var currentDate = new Date();
						if (user.subscription_expire_on <= currentDate) {
							trial_for_plan = false;
						} else {
							const diffTime = Math.abs(user.subscription_expire_on - currentDate);
							trial_period_days = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
						}
					}
					const subscription = await stripeHelper.createSubscription(user.stripe_id, sp.stripe_plan_id, trial_for_plan, trial_period_days);
				}
			}

			}else{
        if (req.body.paymentMethod && req.body.paymentMethod.id) {          
          const customer = await stripeHelper.createCustomer(req.body.email, req.body.first_name + ' ' + req.body.last_name, req.body.paymentMethod.id);
          //const paymentMethod = await stripeHelper.attachPaymentMethod(customer.id,req.body.paymentMethod.id)
          const subscription = await stripeHelper.createSubscription(customer.id, sp.stripe_plan_id, true, null, sp.stripe_price_id, req.body?.coupon);
          // console.log("subscription response:___", subscription,"Json stringify:___\n");   
          user.stripe_id = customer.id;
          user.subscription_expire_on = new Date(subscription.current_period_end * 1000);
          user.payment_status = subscription.status
          // throw "wait"    
          let usd = new UserStripeDetail();
          usd.created_by = req.payload._id;
          usd.updated_by = req.payload._id;
          usd.user = user._id;
          usd.customer = customer;
          usd.paymentMethod = req.body.paymentMethod
          usd.subscription = subscription;
          usd.save();
        }
      }

			user.subscription_package = sp._id;
			user.subscription_type = sp.subscription_name;
			user.updated_by= req.payload._id;

		}else{      
      if (req.body.subscription_package) {
        const sp = await SubscriptionService.getSubscriptionPackageById(req.body.subscription_package)
        user.subscription_package = req.body.subscription_package;
        user.subscription_type = sp.subscription_name;

        if (req.body.paymentMethod && req.body.paymentMethod.id) {              
          const customer = await stripeHelper.createCustomer(req.body.email, req.body.first_name + ' ' + req.body.last_name, req.body.paymentMethod.id);
          //const paymentMethod = await stripeHelper.attachPaymentMethod(customer.id,req.body.paymentMethod.id)
          const subscription = await stripeHelper.createSubscription(customer.id, sp.stripe_plan_id, true, null, sp.stripe_price_id, req.body?.coupon);
          // console.log("subscription response:___", subscription,"Json stringify:___\n");   
          user.stripe_id = customer.id;
          user.subscription_expire_on = new Date(subscription.current_period_end * 1000);
          user.payment_status = subscription.status
          // throw "wait"    
          let usd = new UserStripeDetail();
          usd.created_by = req.payload._id;
          usd.updated_by = req.payload._id;
          usd.user = user._id;
          usd.customer = customer;
          usd.paymentMethod = req.body.paymentMethod
          usd.subscription = subscription;
          usd.save();
        }
      }
    }
		delete user._id;
		let updated= await User.updateOne({ _id: ObjectId(req.params.id) }, user);
		common.log("updated:____",updated)
		
		if(stripeupdateflag)
		await updateEmailOnStripe(req.body.email,user);
    
    return res.send({ success: true, message: "User information updated successfully" })
	}
	catch (err) {
		common.log("Error while update user", err);
		return res.status(400).send({ message: err.message })
	}finally{
    console.log("user data ", prev_data, "data", req.body);
    await adminStoreLogs(req,prev_data,"Updated user Info","User")
  }
}

function updateProfile(req, files, res) {
  User.findById(req.params.id).exec(function (err, user) {
    if (!err) {
      if (user) {
        if (req.body.password) {
          user.setPassword(req.body.password);
        }
        if (files) {
          _.forEach(files, function (file) {
            var ext = path.extname(file.originalname);
            if (ext == ".docx" || ext == ".doc" || ext == ".pdf") {
              user.agreement_doc.push({ filename: file.filename });
            }
          });
        }
        user.first_name = req.body.first_name;
        user.email = req.body.email;
        user.last_name = req.body.last_name;
        user.company = req.body.company;
        user.address = req.body.address;
        user.mc_Number = req.body.mc_Number;
        if (user.phone != req.body.phone) {
          user.phoneVerified = false;
          user.phone = req.body.phone;
        }

        if (req.body.role) user.role = req.body.role;
        if (req.body.sign_file) {
          user.electronic_signature = req.body.sign_file;
        }
        var upsertData = user.toObject();
        delete upsertData._id;

        if (req.body.avatar != "" && req.body.avatar != null) {
          common.SaveImageToDisk(req.body.avatar, function (image_url) {
            if (image_url != "" && image_url != null) {
              upsertData.avatar = image_url;
            }

            User.update(
              { _id: user.id },
              upsertData,
              { upsert: true },
              function (err) {
                if (!err) {
                  res.status(200).json(user);
                } else {
                  res.status(400).send({ message: err.message });
                  return;
                }
              }
            );
          });
        } else if (
          req.body.profile_image != "" &&
          req.body.profile_image != null
        ) {
          upsertData.avatar = req.body.profile_image;
          User.update(
            { _id: user.id },
            upsertData,
            { upsert: true },
            function (err) {
              if (!err) {
                res.status(200).json(user);
              } else {
                res.status(400).send({ message: err.message });
                return;
              }
            }
          );
        } else {
          common.log(upsertData);
          User.update({ _id: user.id }, upsertData, function (err) {
            if (!err) {
              res.status(200).json(user);
            } else {
              res.status(400).send({ message: err.message });
              return;
            }
          });
        }
      } else {
        res.status(404).send({ message: "User not found" });
      }
    } else {
      res.status(404).json(err);
    }
  });
}

module.exports.updatePaymentStatus = async function (req, res) {
  try {
    let psl = await SubscriptionService.getPaidSubscriptionsId();
    let users = await User.find({
      stripe_id: { $ne: null },
      subscription_package: { $in: psl },
      payment_status: null,
    }).lean();
    let i = 0;
    while (i < users.length) {
      let _user = users[i];
      let customer = await stripeHelper.getCustomer(_user.stripe_id);
      let subscriptions = customer.subscriptions;
      if (subscriptions && subscriptions.data.length > 0) {
        await User.updateOne(
          { stripe_id: customer.id },
          { $set: { payment_status: subscriptions.data[0].status } }
        );
      }
      // private function for thread sleep so that stripe api limit will not throw exceed error test mode 25 req/sec and live mode 100 req/sec
      await common.snooze(500);
      i++;
    }
    return res.send({ success: true, data: users.length });
  } catch (error) {
    common.log("Error while execute updatePaymentStatus", error);
    return res.status(400).send({ success: false, message: error.message });
  }
};

module.exports.updateScheduleSubscriptionStatus = async function (req, res) {
  try {
    let schedules = await UserStripeSubscriptionSchedule.find({
      is_delete: false,
    }).lean();
    for (let i = 0; i < schedules.length; i++) {
      const schedule = schedules[i];
      let ss = await stripeHelper.retrieveScheduleSubscription(
        schedule.stripe_schedule_id
      );
      if (ss.status == "released" || ss.status == "completed") {
        await User.updateOne(
          { _id: schedule.user },
          {
            subscription_package: schedule.subscription_package,
            role: schedule.role,
            schedule: null,
          }
        );
        await UserStripeSubscriptionSchedule.updateOne(
          { _id: schedule._id },
          {
            $set: {
              is_delete: true,
            },
          }
        );
      }
      await common.snooze(500);
    }
    return res.send("ok");
  } catch (err) {
    common.log("Error while updateScheduleSubscriptionStatus :--", err);
    return res.status(400).send({ success: false, message: err.message });
  }
};

/**
 * api for users signedup csv
 */
module.exports.getSignedUpUsersCsv = async function (req, res) {
  let { from_date = null, to_date = null } = req.query;

  let query_filters = {};
  /**
   * date filters
   */
  if (from_date != null && to_date != null) {
    common.log("query from date");
    let start_date = new Date(from_date);
    start_date.setHours(0, 0, 0);

    let end_date = new Date(to_date);
    end_date.setHours(23, 59, 59);

    query_filters["createdAt"] = {};
    query_filters["createdAt"]["$gte"] = start_date;
    query_filters["createdAt"]["$lte"] = end_date;
  } else {
    //by default previous month
    let start_date = new Date();
    start_date.setDate(1);
    start_date.setMonth(start_date.getMonth() - 1);
    start_date.setHours(0, 0, 0);

    let end_date = new Date();
    end_date.setHours(23, 59, 59);

    query_filters["createdAt"] = {};
    query_filters["createdAt"]["$gte"] = start_date;
    query_filters["createdAt"]["$lte"] = end_date;
  }

  console.log("Query filters:____", query_filters);
  try {
    let users = await User.find(query_filters, {
      first_name: 1,
      last_name: 1,
      email: 1,
      role: 1,
      phone: 1,
      last_login_at: 1,
      subscription: 1,
      payment_status: 1,
      subscription_type: 1,
      subscription_expire_on: 1,
      subscription_cancel_reason: 1,
      subscription_canceled_on: 1,
      status: 1,
      createdAt: 1,
    })
      .sort({ created_at: 1 })
      .exec();

    signedUpCSV(null, users, res);
    // return res.send(users);
  } catch (err) {
    console.error("ERROR:Find User", err);
    return res.status(400).send({ message: err.message });
  }
};

/**
 * function to validate email for updation of profile
 * if someone want to update his profile email...
 */

async function validate_update_email(user_email, user_id) {
  if (common.validateEmail(user_email)) {
    let user_info = await User.findOne({ email: user_email });
    // if user email is exist and associated with other user
    if (user_info && !_.isEqual(user_info._id, user_id)) {
      return { success: false, message: "Email is already exist" };
    }

    return { success: true };
  } else {
    return { success: false, message: "Invalid Email" };
  }
}

/**
 * update on stripe customer email
 */
async function updateEmailOnStripe(email, user) {
  if (user.stripe_id) {
    let upd_resp = await stripeHelper.updateCustomerEmail(
      user.stripe_id,
      email
    );
    common.log(`customer ${user.stripe_id} email to be updated `, user.email);
    common.log("updated on customer stripe:_____", upd_resp);
    return;
  }
  return;
}

const signedUpCSV = (err, users, res) => {
  if (err) return res.status(400).send(err);

  var fields = [
    {
      label: "Created At",
      value: function (row) {
      	return row?.createdAt.toLocaleString()
      }
    },
    { label: "First Name", value: "first_name" },
    { label: "Last name", value: "last_name" },
    { label: "Email", value: "email" },
    { label: "Role", value: "role" },
    { label: "Phone", value: "phone" },
    { label: "Package", value: "subscription_type" },
    {
      label: "Last Login",
      value: function (row) {
        // let d = new Date(0); // The 0 there is the key, which sets the date to the epoch
        //   return d.setUTCSeconds(row.last_login_at);
        if (row.last_login_at) {
          return (new Date(row.last_login_at * 1000)).toLocaleString();
        } else {
          return "";
        }
      },
    }, //roles are added in csv
    // {
    // 	label: 'CC entered', value: function (row) {
    // 		if (row.stripe_id) return 'Yes'
    // 		return 'No'
    // 	}
    // },
    { label: "Payment status", value: "payment_status" },
    { label: "Subscription", value: "subscription" },
    {
      label: "Subscription Cancel Reason",
      value: "subscription_cancel_reason",
    },
    {label: "Subscription Canceled On", value: function (row) {
      return row?.subscription_canceled_on?.toLocaleString();
    }},
    {
    label: "Subscription Expire On", value: function (row) {
      return row?.subscription_expire_on?.toLocaleString();
    }}
  ];
  var opts = {
    fields: fields,
  };
  try {
    const parser = new Parser(opts);
    let csv;
    let filename = "usersignedup.csv";
    csv = parser.parse(users);
    res.set(
      "Content-Disposition",
      ["attachment; filename=", filename].join("")
    );
    res.set("Content-Type", "text/csv");
    res.end(csv);
  } catch (err) {
    console.error(err);
  }
};

//get all user refered by person
module.exports.getUserRefSignedUp= async(req, res)=>{
  try{

  const { body: {status='active' } } = req;

if(status!='incomplete'){
      let [freeSubsIds, publicRoles] = await Promise.all([
      SubscriptionService.getFreeSubscriptionsId(),
      roleServices.getPublicRoles(),
    ]);

    let query = await common.getUserSearchObject(
      req.body,
      false,
      false,
      freeSubsIds,
      publicRoles
    );

    let users= await User.aggregate([
      //filter condition on source file    
      { '$match': query },
        {
          $facet: {
            //pipeline user registered through referal
        data: [
                //lookup for referals
          {"$lookup":{
            "from":"referals",
            "as": "referal",
            "localField": "ref_Id",
            "foreignField": "_id"
          }},
          {"$unwind":"$referal"},
//match conditions
        {'$match':{'referal.userId':ObjectId(req.payload._id)
          // "$or":[{'referal.userId':ObjectId(req.payload._id)},
          // {'created_by':ObjectId(req.payload._id)}]
        }},
//lookup for subscription
        {'$lookup':{
          "from": "subscriptionpackages",
          "as":"subscriptionpackages",
          "localField": "subscription_package",
          "foreignField": "_id"
        }},
//project
        {'$project':{
            // 'referal':0,
            _id: 1,
            first_name: 1,
            last_name: 1,
            username: 1,
            avatar: 1,
            email: 1,
            company: 1,
            address: 1,
            phone: 1,
            subscription_type: 1,
            subscription_expire_on: 1,
            status: 1,
            role: 1,
            subscription: 1,
            stripe_id: 1,
            created_at: 1,
            subscription_package: 1,
            subscriptionpackages:1,
            payment_status: 1,
            canceled_by : 1,
            user_status: {
              $switch: {
                branches: [
                  {
                    case: {
                      $and: [
                        { $eq:["$status", "active"] },
                        {
                          $or: [
                            {
                                $in: ["$subscription_package", freeSubsIds],
                            }
                          ],
                        },
                        {
                          $gte: ["$subscription_expire_on",new Date()],
                      },
                        { $ne: ["$payment_status", "past_due"] },
                        { $ne: ["$payment_status", "trialing"] },
                        { $ne: ["$payment_status", null] } // {}
                      ],
                    },
                    then: "paying",
                  },
                  {
                    case: {
                      $and: [
                        { $eq:["$status", "active"] },
                        {
                          $or: [
                            {
                                $in: ["$subscription_package", freeSubsIds],
                            }, {
                              $gte: ["$subscription_expire_on",new Date()],
                          },
                          ],
                        },
                        { $eq: ["$payment_status", "active"] },
                      ],
                    },
                    then: "paying",
                  },
                  {
                    case: {
                      $and: [
                        { $eq:["$status", "active"] },
                        {
                          $or: [
                            {
                              subscription_package: {
                                $in: ["$subscription_package", freeSubsIds],
                              },
                            },
                            {
                              subscription_expire_on: {
                                $gte: ["$subscription_expire_on", new Date()],
                              },
                            },
                          ],
                        },
                        {
                            $gte: ["$subscription_expire_on", new Date()],
                        },
                        { $eq: ["$payment_status", "trialing"] }, // {}
                      ],
                    },
                    then: "trialing",
                  },
                  {
                    case: {
                      $and: [
                        { $eq:["$status", "active"] },
                        {
                          $or: [
                            {
                              subscription_package: {
                                $in: ["$subscription_package", freeSubsIds],
                              },
                            },
                            {
                              subscription_expire_on: {
                                $gte: ["$subscription_expire_on", new Date()],
                              },
                            },
                          ],
                        },
                        { $eq: ["$payment_status", "canceled"] }, // {}
                      ],
                    },
                    then: "canceled",
                  },
                  {
                    case: {
                     $eq:["$status", "inactive"]
                    },
                    then: "Deactivated_by_admin",
                  },
                  {case:{ $eq: ["$payment_status", "past_due"] },
                   then:"past_due"},
                   {case:{$and:[ {$gte:["$subscription_expire_on",new Date()]}, { $eq: ["$payment_status", "active"] } ]},
                   then: "paying"
                   },
                  {case:{$lt:["$subscription_expire_on",new Date()]},
                  then: "subscription expired"
                  },
                 
                      ],
                default: "$payment_status",
              },
            },
        }},
        //sort
        {
          '$sort': {
            'created_at': -1
          }
        },
    //pagination
  ],
  //pipeline for users created by admin or sales from register user marketing page
user_created_by:[


//match conditions
{'$match':{'created_by':ObjectId(req.payload._id)
// "$or":[{'referal.userId':ObjectId(req.payload._id)},
// {'created_by':ObjectId(req.payload._id)}]
}},
//lookup for subscription
{'$lookup':{
"from": "subscriptionpackages",
"as":"subscriptionpackages",
"localField": "subscription_package",
"foreignField": "_id"
}},
//project
{'$project':{
  // 'referal':0,
  _id: 1,
  first_name: 1,
  last_name: 1,
  username: 1,
  avatar: 1,
  email: 1,
  company: 1,
  address: 1,
  phone: 1,
  subscription_type: 1,
  subscription_expire_on: 1,
  status: 1,
  role: 1,
  subscription: 1,
  stripe_id: 1,
  created_at: 1,
  subscription_package: 1,
  subscriptionpackages:1,
  payment_status: 1,
  canceled_by : 1,
  user_status: {
    $switch: {
      branches: [
        {
          case: {
            $and: [
              { $eq:["$status", "active"] },
              {
                $or: [
                  {
                      $in: ["$subscription_package", freeSubsIds],
                  }
                ],
              },
              {
                $gte: ["$subscription_expire_on",new Date()],
            },
              { $ne: ["$payment_status", "past_due"] },
              { $ne: ["$payment_status", "trialing"] },
              { $ne: ["$payment_status", null] } // {}
            ],
          },
          then: "paying",
        },
        {
          case: {
            $and: [
              { $eq:["$status", "active"] },
              {
                $or: [
                  {
                      $in: ["$subscription_package", freeSubsIds],
                  }, {
                    $gte: ["$subscription_expire_on",new Date()],
                },
                ],
              },
              { $eq: ["$payment_status", "active"] },
            ],
          },
          then: "paying",
        },
        {
          case: {
            $and: [
              { $eq:["$status", "active"] },
              {
                $or: [
                  {
                    subscription_package: {
                      $in: ["$subscription_package", freeSubsIds],
                    },
                  },
                  {
                    subscription_expire_on: {
                      $gte: ["$subscription_expire_on", new Date()],
                    },
                  },
                ],
              },
              {
                  $gte: ["$subscription_expire_on", new Date()],
              },
              { $eq: ["$payment_status", "trialing"] }, // {}
            ],
          },
          then: "trialing",
        },
        {
          case: {
            $and: [
              { $eq:["$status", "active"] },
              {
                $or: [
                  {
                    subscription_package: {
                      $in: ["$subscription_package", freeSubsIds],
                    },
                  },
                  {
                    subscription_expire_on: {
                      $gte: ["$subscription_expire_on", new Date()],
                    },
                  },
                ],
              },
              { $eq: ["$payment_status", "canceled"] }, // {}
            ],
          },
          then: "canceled",
        },
        {
          case: {
           $eq:["$status", "inactive"]
          },
          then: "Deactivated_by_admin",
        },
        {case:{ $eq: ["$payment_status", "past_due"] },
         then:"past_due"},
         {case:{$and:[ {$gte:["$subscription_expire_on",new Date()]}, { $eq: ["$payment_status", "active"] } ]},
         then: "paying"
         },
        {case:{$lt:["$subscription_expire_on",new Date()]},
        then: "subscription expired"
        },
       
            ],
      default: "$payment_status",
    },
  },
}},
//sort
{
'$sort': {
  'created_at': -1
}
}
],

         }
}

]);

let returnObj = {
  users_data: []
}

if (users && users[0]?.data) {
  returnObj.users_data = [...users[0].data,...users[0].user_created_by];
}
return res.status(200).send({status:true, data: returnObj});

}else if(status=='incomplete'){
  let query = await common.getUserSearchObject(req.body);
  let users= await TemporaryUsers.aggregate([
    //filter condition on source file    
    { '$match': query },
      {
        $facet: {
          //pipeline user registered through referal
      data: [
              //lookup for referals
        {"$lookup":{
          "from":"referals",
          "as": "referal",
          "localField": "ref_Id",
          "foreignField": "_id"
        }},
        {"$unwind":"$referal"},
//match conditions
      {'$match':{'referal.userId':ObjectId(req.payload._id)
        // "$or":[{'referal.userId':ObjectId(req.payload._id)},
        // {'created_by':ObjectId(req.payload._id)}]
      }},
//lookup for subscription
      {'$lookup':{
        "from": "subscriptionpackages",
        "as":"subscriptionpackages",
        "localField": "subscription_package",
        "foreignField": "_id"
      }},
//project
      {'$project':{
          // 'referal':0,
          _id: 1,
          first_name: 1,
          last_name: 1,
          username: 1,
          avatar: 1,
          email: 1,
          company: 1,
          address: 1,
          phone: 1,
          subscription_type: 1,
          subscription_expire_on: 1,
          status: 1,
          role: 1,
          subscription: 1,
          stripe_id: 1,
          created_at: 1,
          subscription_package: 1,
          subscriptionpackages:1,
          payment_status: 1,
          canceled_by : 1
      }},
      //sort
      {
        '$sort': {
          'created_at': -1
        }
      },
  //pagination
],
//pipeline for users created by admin or sales from register user marketing page
user_created_by:[


//match conditions
{'$match':{'created_by':ObjectId(req.payload._id)
// "$or":[{'referal.userId':ObjectId(req.payload._id)},
// {'created_by':ObjectId(req.payload._id)}]
}},
//lookup for subscription
{'$lookup':{
"from": "subscriptionpackages",
"as":"subscriptionpackages",
"localField": "subscription_package",
"foreignField": "_id"
}},
//project
{'$project':{
// 'referal':0,
_id: 1,
first_name: 1,
last_name: 1,
username: 1,
avatar: 1,
email: 1,
company: 1,
address: 1,
phone: 1,
subscription_type: 1,
subscription_expire_on: 1,
status: 1,
role: 1,
subscription: 1,
stripe_id: 1,
created_at: 1,
subscription_package: 1,
subscriptionpackages:1,
payment_status: 1,
canceled_by : 1
}},
//sort
{
'$sort': {
'created_at': -1
}
}
],

 }
}

]);

let returnObj = {
users_data: []
}

if (users && users[0]?.data) {
returnObj.users_data = [...users[0].data,...users[0].user_created_by];
}
return res.status(200).send({status:true, data: returnObj});
}

  }catch(error){
    common.log("Error in getUserRefSignedUp:___", error);
    return res.status(500).send({status:false,message:error.message});
  }
}

/**
 * get sales users only
 */
 module.exports.get_all_sales_person = async (req, res) => {
  try {
    let users = await User.find({ $or: [{ 'role': "Sales" }, { 'role': "administrator" }] })
    return res.status(200).json({status: true, data: users})
  } catch (err) {
    console.log("Error in Sales Person:", err)
    return res.status(400).send({ success: false, message: err.message })
  }
}

/**
 * common function
 */
const user_refered_by= async(body)=>{
try{
  
if(body.status=='active' || body.status=='inactive'){
  let [freeSubsIds, publicRoles] = await Promise.all([
  SubscriptionService.getFreeSubscriptionsId(),
  roleServices.getPublicRoles(),
]);

let query = await common.getUserSearchObject(
   body,
  false,
  false,
  freeSubsIds,
  publicRoles
);

let users= await User.aggregate([
  //filter condition on source file    
  { '$match': query },
    {
      $facet: {
        //pipeline user registered through referal
    data: [
            //lookup for referals
      {"$lookup":{
        "from":"referals",
        "as": "referal",
        "localField": "ref_Id",
        "foreignField": "_id"
      }},
      {"$unwind":"$referal"},
//match conditions
    {'$match':{'referal.userId':ObjectId(body.created_by)
      // "$or":[{'referal.userId':ObjectId(req.payload._id)},
      // {'created_by':ObjectId(req.payload._id)}]
    }},
//lookup for subscription
    {'$lookup':{
      "from": "subscriptionpackages",
      "as":"subscriptionpackages",
      "localField": "subscription_package",
      "foreignField": "_id"
    }},
//project
    {'$project':{
        // 'referal':0,
        _id: 1,
        first_name: 1,
        last_name: 1,
        username: 1,
        avatar: 1,
        email: 1,
        company: 1,
        address: 1,
        phone: 1,
        subscription_type: 1,
        subscription_expire_on: 1,
        status: 1,
        role: 1,
        subscription: 1,
        stripe_id: 1,
        created_at: 1,
        subscription_package: 1,
        subscriptionpackages:1,
        payment_status: 1,
        canceled_by : 1,
        user_status: {
          $switch: {
            branches: [
              {
                case: {
                  $and: [
                    { $eq:["$status", "active"] },
                    {
                      $or: [
                        {
                            $in: ["$subscription_package", freeSubsIds],
                        }
                      ],
                    },
                    {
                      $gte: ["$subscription_expire_on",new Date()],
                  },
                    { $ne: ["$payment_status", "past_due"] },
                    { $ne: ["$payment_status", "trialing"] },
                    { $ne: ["$payment_status", null] } // {}
                  ],
                },
                then: "paying",
              },
              {
                case: {
                  $and: [
                    { $eq:["$status", "active"] },
                    {
                      $or: [
                        {
                            $in: ["$subscription_package", freeSubsIds],
                        }, {
                          $gte: ["$subscription_expire_on",new Date()],
                      },
                      ],
                    },
                    { $eq: ["$payment_status", "active"] },
                  ],
                },
                then: "paying",
              },
              {
                case: {
                  $and: [
                    { $eq:["$status", "active"] },
                    {
                      $or: [
                        {
                          subscription_package: {
                            $in: ["$subscription_package", freeSubsIds],
                          },
                        },
                        {
                          subscription_expire_on: {
                            $gte: ["$subscription_expire_on", new Date()],
                          },
                        },
                      ],
                    },
                    {
                        $gte: ["$subscription_expire_on", new Date()],
                    },
                    { $eq: ["$payment_status", "trialing"] }, // {}
                  ],
                },
                then: "trialing",
              },
              {
                case: {
                  $and: [
                    { $eq:["$status", "active"] },
                    {
                      $or: [
                        {
                          subscription_package: {
                            $in: ["$subscription_package", freeSubsIds],
                          },
                        },
                        {
                          subscription_expire_on: {
                            $gte: ["$subscription_expire_on", new Date()],
                          },
                        },
                      ],
                    },
                    { $eq: ["$payment_status", "canceled"] }, // {}
                  ],
                },
                then: "canceled",
              },
              {
                case: {
                 $eq:["$status", "inactive"]
                },
                then: "Deactivated_by_admin",
              },
              {case:{ $eq: ["$payment_status", "past_due"] },
               then:"past_due"},
               {case:{$and:[ {$gte:["$subscription_expire_on",new Date()]}, { $eq: ["$payment_status", "active"] } ]},
               then: "paying"
               },
              {case:{$lt:["$subscription_expire_on",new Date()]},
              then: "subscription expired"
              },
             
                  ],
            default: "$payment_status",
          },
        },
    }},
    //sort
    {
      '$sort': {
        'createdAt': -1
      }
    },
//pagination
],
//pipeline for users created by admin or sales from register user marketing page
user_created_by:[


//match conditions
{'$match':{'created_by':ObjectId(body.created_by)
// "$or":[{'referal.userId':ObjectId(req.payload._id)},
// {'created_by':ObjectId(req.payload._id)}]
}},
//lookup for subscription
{'$lookup':{
"from": "subscriptionpackages",
"as":"subscriptionpackages",
"localField": "subscription_package",
"foreignField": "_id"
}},
//project
{'$project':{
// 'referal':0,
_id: 1,
first_name: 1,
last_name: 1,
username: 1,
avatar: 1,
email: 1,
company: 1,
address: 1,
phone: 1,
subscription_type: 1,
subscription_expire_on: 1,
status: 1,
role: 1,
subscription: 1,
stripe_id: 1,
created_at: 1,
subscription_package: 1,
subscriptionpackages:1,
payment_status: 1,
canceled_by : 1,
user_status: {
$switch: {
  branches: [
    {
      case: {
        $and: [
          { $eq:["$status", "active"] },
          {
            $or: [
              {
                  $in: ["$subscription_package", freeSubsIds],
              }
            ],
          },
          {
            $gte: ["$subscription_expire_on",new Date()],
        },
          { $ne: ["$payment_status", "past_due"] },
          { $ne: ["$payment_status", "trialing"] },
          { $ne: ["$payment_status", null] } // {}
        ],
      },
      then: "paying",
    },
    {
      case: {
        $and: [
          { $eq:["$status", "active"] },
          {
            $or: [
              {
                  $in: ["$subscription_package", freeSubsIds],
              }, {
                $gte: ["$subscription_expire_on",new Date()],
            },
            ],
          },
          { $eq: ["$payment_status", "active"] },
        ],
      },
      then: "paying",
    },
    {
      case: {
        $and: [
          { $eq:["$status", "active"] },
          {
            $or: [
              {
                subscription_package: {
                  $in: ["$subscription_package", freeSubsIds],
                },
              },
              {
                subscription_expire_on: {
                  $gte: ["$subscription_expire_on", new Date()],
                },
              },
            ],
          },
          {
              $gte: ["$subscription_expire_on", new Date()],
          },
          { $eq: ["$payment_status", "trialing"] }, // {}
        ],
      },
      then: "trialing",
    },
    {
      case: {
        $and: [
          { $eq:["$status", "active"] },
          {
            $or: [
              {
                subscription_package: {
                  $in: ["$subscription_package", freeSubsIds],
                },
              },
              {
                subscription_expire_on: {
                  $gte: ["$subscription_expire_on", new Date()],
                },
              },
            ],
          },
          { $eq: ["$payment_status", "canceled"] }, // {}
        ],
      },
      then: "canceled",
    },
    {
      case: {
       $eq:["$status", "inactive"]
      },
      then: "Deactivated_by_admin",
    },
    {case:{ $eq: ["$payment_status", "past_due"] },
     then:"past_due"},
     {case:{$and:[ {$gte:["$subscription_expire_on",new Date()]}, { $eq: ["$payment_status", "active"] } ]},
     then: "paying"
     },
    {case:{$lt:["$subscription_expire_on",new Date()]},
    then: "subscription expired"
    },
   
        ],
  default: "$payment_status",
},
},
}},
//sort
{
'$sort': {
'createdAt': -1
}
}
],

     }
}

]);

let returnObj = {
users_data: []
}

if (users && users[0]?.data) {
returnObj.users_data = [...users[0].data,...users[0].user_created_by];
}
return returnObj;

}else if(body.status=='incomplete'){
  
let query = await common.getUserSearchObject(body);
let users= await TemporaryUsers.aggregate([
  //filter condition on source file    
  { '$match': query },
    {
      $facet: {
        //pipeline user registered through referal
    data: [
            //lookup for referals
      {"$lookup":{
        "from":"referals",
        "as": "referal",
        "localField": "ref_Id",
        "foreignField": "_id"
      }},
      {"$unwind":"$referal"},
//match conditions
    {'$match':{'referal.userId':ObjectId(body.created_by)
      // "$or":[{'referal.userId':ObjectId(req.payload._id)},
      // {'created_by':ObjectId(req.payload._id)}]
    }},
//lookup for subscription
    {'$lookup':{
      "from": "subscriptionpackages",
      "as":"subscriptionpackages",
      "localField": "subscription_package",
      "foreignField": "_id"
    }},
//project
    {'$project':{
        // 'referal':0,
        _id: 1,
        first_name: 1,
        last_name: 1,
        username: 1,
        avatar: 1,
        email: 1,
        company: 1,
        address: 1,
        phone: 1,
        subscription_type: 1,
        subscription_expire_on: 1,
        status: 1,
        role: 1,
        subscription: 1,
        stripe_id: 1,
        created_at: 1,
        subscription_package: 1,
        subscriptionpackages:1,
        payment_status: 1,
        canceled_by : 1
    }},
    //sort
    {
      '$sort': {
        'createdAt': -1
      }
    },
//pagination
],
//pipeline for users created by admin or sales from register user marketing page
user_created_by:[


//match conditions
{'$match':{'created_by':ObjectId(body.created_by)
// "$or":[{'referal.userId':ObjectId(req.payload._id)},
// {'created_by':ObjectId(req.payload._id)}]
}},
//lookup for subscription
{'$lookup':{
"from": "subscriptionpackages",
"as":"subscriptionpackages",
"localField": "subscription_package",
"foreignField": "_id"
}},
//project
{'$project':{
// 'referal':0,
_id: 1,
first_name: 1,
last_name: 1,
username: 1,
avatar: 1,
email: 1,
company: 1,
address: 1,
phone: 1,
subscription_type: 1,
subscription_expire_on: 1,
status: 1,
role: 1,
subscription: 1,
stripe_id: 1,
created_at: 1,
subscription_package: 1,
subscriptionpackages:1,
payment_status: 1,
canceled_by : 1
}},
//sort
{
'$sort': {
'createdAt': -1
}
}
],
}
}

]);

let returnObj = {
users_data: []
}

if (users && users[0]?.data) {
returnObj.users_data = [...users[0].data,...users[0].user_created_by];
}
return returnObj;

}else if(body.status=='internal'){
  common.log("hit for internal")
let publicRoles = await roleServices.getPrivateRoles();
let query = await common.getUserSearchObject(
  body,
  false,
  true,
  [],
  publicRoles
);

let users= await User.aggregate([
  //filter condition on source file    
  { '$match': query },
    {
      $facet: {
        //pipeline user registered through referal
    data: [
            //lookup for referals
      {"$lookup":{
        "from":"referals",
        "as": "referal",
        "localField": "ref_Id",
        "foreignField": "_id"
      }},
      {"$unwind":"$referal"},
//match conditions
    {'$match':{'referal.userId':ObjectId(body.created_by)
      // "$or":[{'referal.userId':ObjectId(req.payload._id)},
      // {'created_by':ObjectId(req.payload._id)}]
    }},
//lookup for subscription
    {'$lookup':{
      "from": "subscriptionpackages",
      "as":"subscriptionpackages",
      "localField": "subscription_package",
      "foreignField": "_id"
    }},
//project
    {'$project':{
        // 'referal':0,
        _id: 1,
        first_name: 1,
        last_name: 1,
        username: 1,
        avatar: 1,
        email: 1,
        company: 1,
        address: 1,
        phone: 1,
        subscription_type: 1,
        subscription_expire_on: 1,
        status: 1,
        role: 1,
        subscription: 1,
        stripe_id: 1,
        created_at: 1,
        subscription_package: 1,
        subscriptionpackages:1,
        payment_status: 1,
        canceled_by : 1
    }},
    //sort
    {
      '$sort': {
        'createdAt': -1
      }
    },
//pagination
],
//pipeline for users created by admin or sales from register user marketing page
user_created_by:[


//match conditions
{'$match':{'created_by':ObjectId(body.created_by)
// "$or":[{'referal.userId':ObjectId(req.payload._id)},
// {'created_by':ObjectId(req.payload._id)}]
}},
//lookup for subscription
{'$lookup':{
"from": "subscriptionpackages",
"as":"subscriptionpackages",
"localField": "subscription_package",
"foreignField": "_id"
}},
//project
{'$project':{
// 'referal':0,
_id: 1,
first_name: 1,
last_name: 1,
username: 1,
avatar: 1,
email: 1,
company: 1,
address: 1,
phone: 1,
subscription_type: 1,
subscription_expire_on: 1,
status: 1,
role: 1,
subscription: 1,
stripe_id: 1,
created_at: 1,
subscription_package: 1,
subscriptionpackages:1,
payment_status: 1,
canceled_by : 1
}},
//sort
{
'$sort': {
'createdAt': -1
}
}
],

     }
}

]);

let returnObj = {
users_data: []
}

if (users && users[0]?.data) {
returnObj.users_data = [...users[0].data,...users[0].user_created_by];
}
return returnObj;

}

}catch(error){
  common.log("error while getting users refered", error);
  return {
    count:0,
    users_data:[]
  }
}
}

/** udpate temporary user data */
module.exports.updateTempUser = async function (req, res) {
	try {
		let stripeupdateflag= false;
		var user = await TemporaryUsers.findById(req.params.id).lean();
		if (!user) return res.status(404).send({ message: "User not found" });
		// if (req.body.email && req.body.email != user.email) {
		// 	user.email = req.body.email;
		// 	user.emailVerified = false;
		// }
		if (req.body.first_name) user.first_name = req.body.first_name;
		if (req.body.last_name) user.last_name = req.body.last_name;
		if (req.body.phone && req.body.phone != user.phone) {
			user.phone = req.body.phone;
			user.phoneVerified = false;
		}
		if (req.body.company_id) user.company_id = req.body.company_id;

		if (req.body.role && req.body.role != user.role) {
			user.role = req.body.role;
		}

    //equipment type update
    if(req.body.equipment_type){
      user.equipment_type=req.body.equipment_type
    }

		//update email for user
		if(req.body.email){

			let Email_validation= await validate_updatetemp_email(req.body.email, user._id);
			if(Email_validation.success){
			  if(req.body.email!==user.email){
				user.email= req.body.email;
				// stripeupdateflag= true;
			  }
	
			}else{
			  common.log("ERROR in email validation:__", Email_validation);
			   return res.status(400).send(Email_validation);
			}
		}
    user.updated_by= req.payload._id;

		delete user._id;
		let updated= await TemporaryUsers.updateOne({ _id: ObjectId(req.params.id) }, user);
		common.log("updated:____",updated)
		
		// if(stripeupdateflag)
		// await updateEmailOnStripe(req.body.email,user);

		return res.send({ success: true, message: "User information updated successfully" })
	}
	catch (err) {
		common.log("Error while update user", err);
		return res.status(400).send({ message: err.message })
	}
}

/**
 * 
 */
 async function validate_updatetemp_email(user_email, user_id) {
  if (common.validateEmail(user_email)) {
    let user_info = await TemporaryUsers.findOne({ email: user_email });
    // if user email is exist and associated with other user
    if (user_info && !_.isEqual(user_info._id, user_id)) {
      return { success: false, message: "Email is already exist" };
    }

    return { success: true };
  } else {
    return { success: false, message: "Invalid Email" };
  }
}