var mongoose = require('mongoose');
var Notification = mongoose.model('Notification');
var User = mongoose.model('User');
var common = require('../helpers/common');
var NotificationService = require('../services/notificationService');
var _ = require('util');



module.exports.myNotifications = function (req, res) {
  'use strict';

    var data = req.body;
    Notification.find({ created_by: data.created_by})
      .exec(function (err, notifications) {
          if(err){
              common.log(err);
          }
        res.status(200).send(notifications);
    });
  
}

   module.exports.createNotification = async function (req, res) {
	    
    var notificationID;
    var newNotification;
    var data = req.body;
    common.log(data);
    var search_Options = {};
    search_Options = await common.generateSearchObject(data.searchCriteria); 
    //data["convertedSearchCriteria"]   = search_Options;    
    Notification.create(data).then((newSub) => {
        newNotification = newSub;
        notificationID = newSub.id;
        return NotificationService.createJob(newSub);
    }).then(job => {
        let jobOpts = job ? job.opts : null;
        newNotification["jobOptions"] = jobOpts;
        // Add job options to notification so that it can be retrieved later
        return Notification.update({_id: notificationID}, {jobOptions: jobOpts});
    }).then(updatedNotifications => {
        res.status(200).send(newNotification);
    }).catch(err => {
        common.log('Error creating notification.', err);
        res.status(400).send('Error creating notification.');
    });
},

module.exports.updateNotification = async function (req, res) {

    const subId = req.param('id');
    let oldNotification;
    Notification.findOne({id: subId}).then(sub => {
        oldNotification = sub;
        var data = req.body;
        var search_Options = {};
        search_Options = common.generateSearchObject(data); 
        data["SearchCriteria"]   = search_Options; 
        return Notification.update({_id: subId}, data)
    }).then(updatedSub => {
        // updates job data
        return NotificationService.updateJob(oldNotification, updatedSub[0]);
    }).then(job => {
        let jobOpts = job ? job.opts : null;
        // Add job options to Notification so that it can be retrieved later
        return Notification.update({_id: subId}, {jobOptions: jobOpts});
    }).then(updatedNotifications => {
        common.log(`Notification job with id:${subId} was successfullly updated!`);
        res.status(200).send(updatedNotifications[0]);
    }).catch(err => {
        common.log('Error updating Notification.', err);
        res.status(400).send('Error updating Notification.');
    });
},

module.exports.deleteNotification = function (req, res) {
    
    const subId = req.param('id');    
    let notification;
    Notification.findOne({_id: subId}).then(sub => {
        notification = sub;
        return Notification.deleteOne({_id: subId});
    }).then(removedNotification => {
        return NotificationService.removeJob(notification);
    }).then(job => {
        common.log(`notification job with id:${subId} was successfullly removed!`);
        res.ok(notification);
    }).catch(err => {
        common.log('Error removing notification.', err);
        res.status(400).send('Error removing notification.');
    });
},

module.exports.suspendNotification = function(req, res) {
    const subId = req.param('id');
    // remove the job from the queue
    // accessing a job from outside only give you the ability of deleting it....
    Notification.findOne({_id: subId}).then(notification => {
        if (!notification) {
            res.status(400).send('Couldn\'t find requested notification');
        }
        
        return NotificationService.removeJob(notification);
    }).then(() => {
        common.log('notification job with id ' + subId + 'was successfullly deleted!');
        return Notification.update({_id: subId}, {is_active: false});
    }).then(updatedNotifications => {
        res.ok(updatedNotifications[0]);
    }).catch(err => {
        common.log('Error suspending notification', err);
        res.status(200).send('Error suspending notification');
    });
},

module.exports.resumeNotification = function (req, res) {

    const subId = req.param('id');

    Notification.findOne({_id: subId}).then(notification => {
        if (!notification) {
            res.status(400).send('Couldn\'t find requested notification');
        }
        
        return NotificationService.createJob(notification);
    }).then(job => {
        common.log('notification job with id ' + subId + 'was successfully resumed!');
        let updateData = {is_active: true};
        if (job) {
            // job might be null if the job already exists for some reason
            updateData.jobOptions = job.opts;
        }
        return Notification.update({_id: subId}, updateData);
    }).then(updatedNotifications => {
        res.ok(updatedNotifications[0]);
    }).catch(err => {
        common.log('Error resuming notification.', err);
        res.status(400).send('Error resuming notification.');
    });
}
