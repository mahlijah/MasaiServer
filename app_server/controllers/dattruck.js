var _ = require('util');


var mongoose = require('mongoose');
// set Promise provider to bluebird
mongoose.Promise = require('bluebird');
var Truck = mongoose.model('Truck');
var User = mongoose.model('User');
var DatUser = mongoose.model('DatUser');
var config = require('../config/config');
var common = require('../helpers/common');
var soap = require('soap');
var NodeGeocoder = require('node-geocoder');
var Randomstring = require("randomstring");

var options = {
  provider: 'google',
 
  // Optional depending on the providers
  httpAdapter: 'https', // Default
  apiKey: config.googleGoecoder_key, // for Mapquest, OpenCage, Google Premier
  //formatter: 'string',         // 'gpx', 'string', ...
  //formatterPattern: '%p%t%z'
};
 
var geocoder = NodeGeocoder(options);
 

module.exports.login = function(credentials, socket, callback){

  let tokenPrimary = config.DATtokenPrimary;
  let tokenSecondary = config.DATtokenSecondary;
  let tokenExpiration = config.DATtokenExpiration;

   if(Date.now() < Date.parse(tokenExpiration))
    callback({tokenPrimary : config.DATtokenPrimary, tokenSecondary : config.DATtokenSecondary,
     tokenExpiration : config.DATtokenExpiration });

    var url = 'http://cnx.test.dat.com:9280/wsdl/TfmiFreightMatching.wsdl';
    var args = { 
              
            "loginOperation": {
                "loginId": "jmg_cnx1",
                "password": "logistics",
                "thirdPartyId": "ForkFreight-API",
                "apiVersion": "2"
            }  
         
    }
     //{loginId: 'jmg_cnx1', password: 'logistics', thirdPartyId : "ForkFreight-API",apiVersion : 2};
    soap.createClient(url, function(err, client) {
        if(err) common.log("error message: " , err);
        common.log(client);
        client.Login(args, function(err, result) {
            if(err) 
            {
              common.log("error message: " , err);
            }
            else{
            common.log(result);
            let tokenPrimary = result.loginResult.loginSuccessData.token.primary;
            let tokenSecondary = result.loginResult.loginSuccessData.token.secondary;
            let tokenExpiration = result.loginResult.loginSuccessData.expiration;
            config.DATtokenPrimary = tokenPrimary;
            config.DATtokenSecondary = tokenSecondary;
            config.DATtokenExpiration = tokenExpiration;
            //execute search with the login token.
            common.log(config);
            callback(result);
            }
        });
    });
};

function validateSearchFilter(search_object, socket)
{
  return new Promise(function(resolve,reject){
      var search_options = {};
      search_options["is_opened"] = true;
      search_options["created_by"] = {$ne : socket.decoded._id};

      if(search_object !== undefined) {

        if(search_object.pickup_address && search_object.pickup_address.indexOf(',') !== -1)
        {
          let address_parts = search_object.pickup_address.split(',');

          if (address_parts.length >1)
          {
            if(address_parts[0] && address_parts[0]!="") search_options.pickup_address = address_parts[0].trim();
            if(address_parts[0] && address_parts[0]!="") search_options.pickup_city = address_parts[0].trim();
            if(address_parts[1] && address_parts[1]!="") search_options.pickup_state = address_parts[1].trim();
          }
        }
        else 
        {
          // Pickup options
          if(search_object.pickup_address && search_object.pickup_address != "") {
            search_options["pickup_address"] = new RegExp(search_object.pickup_address, "i")
          }

          if(search_object.pickup_city && search_object.pickup_city != "") {
            search_options["pickup_city"] = search_object.pickup_city.trim();
          }

          if(search_object.pickup_state && search_object.pickup_state != "") {
            search_options["pickup_state"] = search_object.pickup_state.trim();
          }

          if(search_object.pickup_zip && search_object.pickup_zip != "") {
            search_options["pickup_zip"] = search_object.pickup_zip.trim();
          }

        }

        if(search_object.name && search_object.name != "") {
          search_options["name"] = new RegExp(search_object.name, "i")
        }

        if(search_object.weight && search_object.weight != "") {
          search_options["weight"] = search_object.weight;
        }

        if(search_object.pickup_date && search_object.pickup_date != "") {
          search_options["pickup_date"] = search_object.pickup_date;
        }

        if(search_object.delivery_date && search_object.delivery_date != "") {
          search_options["delivery_date"] = search_object.delivery_date.trim();
        }

        

        // Delivery options
        if(search_object.delivery_address && search_object.delivery_address.indexOf(',') !== -1)
        {
          let address_parts = search_object.delivery_address.split(',');

          if (address_parts.length >1)
          {
            if(address_parts[0] && address_parts[0]!="")search_options.delivery_address = address_parts[0].trim();
            if(address_parts[0] && address_parts[0]!="")search_options.delivery_city = address_parts[0].trim();
            if(address_parts[1] && address_parts[1]!="")search_options.delivery_state = address_parts[1].trim();
          }
        }
        else 
        {
          if(search_object.delivery_address && search_object.delivery_address != "") {
            search_options["delivery_address"] = new RegExp(search_object.delivery_address, "i")
          }

          if(search_object.delivery_city && search_object.delivery_city != "") {
            search_options["delivery_city"] = search_object.delivery_city.trim();
          }

          if(search_object.delivery_state && search_object.delivery_state != "") {
            search_options["delivery_state"] = search_object.delivery_state.trim();
          }

          if(search_object.delivery_zip && search_object.delivery_zip != "") {
            search_options["delivery_zip"] = search_object.delivery_zip.trim();
          }
        }
        //resolve(search_options);
      }
      resolve(search_options);
    }) 
}


function getCoordinates(address,city,state)
{
  return new Promise(function(resolve, reject){

    let pickup_coordinates = [];
    if (address && address !== 'undefined' )
    { 
      pickup_coordinates.push(address)
    }
    
    if (city && city !== 'undefined' )
    { 
      pickup_coordinates.push(city)
    }
    
    if (state && state !== 'undefined' )
    { 
      pickup_coordinates.push(state)
    }
   
    
    //convert address to coordinates , city and state
    let pickup_address_full = pickup_coordinates.join();
    let geocode_long;
    let geocode_lat;
    let geocode = {};
    
    if( !pickup_address_full || pickup_address_full === null) resolve(null);
    geocoder.geocode(pickup_address_full, function(err,res) {
      if(err) {
        common.log(err);
        reject(err);
      }
      else
      {    
        geocode_lat = (res && res.length >0)? res[0].latitude : " ";
        geocode_long = (res && res.length >0)? res[0].longitude : " ";
        geocode["geocode_lat"] = geocode_lat;
        geocode["geocode_long"] = geocode_long;
        geocode["state_code"] = (res[0].administrativeLevels)? res[0].administrativeLevels.level1short : "";
        geocode["country_code"] = (res[0].countryCode)? res[0].countryCode : "US";
        geocode["city"] = (res[0].city)? res[0].city : "";
        common.log(res);
        resolve(geocode);
      }
    })
  })
}
  


function buildSearchQuery(search_options)
{
   if(!search_options || !search_options.pickup_coordinates) return;
  var args = { 
    "createSearchOperation": {
      "criteria": {
          "assetType": "Equipment",
          "equipmentTypes": ["Van", "Flatbed", "Reefer"],
          "ageLimitMinutes": 1200, //5940,
          "origin": {
            "radius":{
              "place":{
                "namedCoordinates": {
                  "latitude": search_options.pickup_coordinates.geocode_lat,
                  "longitude": search_options.pickup_coordinates.geocode_long,
                  "city": search_options.pickup_coordinates.city,
                  "stateProvince": search_options.pickup_coordinates.state_code
                }
            }
              ,
              "radius" : {
                "miles" : 50,
                "method" : "Road"
              }
            }
          },
            "destination": {
              "radius":{
                "place": {
                "namedCoordinates": {
                  "latitude": search_options.delivery_coordinates.geocode_lat,
                  "longitude": search_options.delivery_coordinates.geocode_long,
                  "city": search_options.delivery_coordinates.city,
                  "stateProvince": search_options.delivery_coordinates.state_code
                }
              },
              "radius" : {
                "miles" : 50, //TODO ask the client to pass in the search radius. default to 50 miles now.
                "method" : "Road"
              }
              }
            },
          //"availability": {
          //   "earliest": "2018-01-10T21:47:07.718Z",
          //   "latest": "2018-01-14T21:47:07.718Z"
          //},
          "includeLtls": "true",
          "includeFulls": "true",            
          "excludeOpenDestinationEquipment": "false"
      },
      "sortOrder": "None",
      "includeSearch": "true"
    }   
  }

  return args;

};

function getAddressFromCoordinates(location){

    return new Promise(function(resolve, reject){
        if(location.open)
        {
           return resolve("Any");
        }
        if(location.area !== null && location.area !== undefined)
        {
          return  resolve(location.area.stateProvinces);
        }

        if(location.namedCoordinates === null || location.namedCoordinates === undefined) 
        {
           return resolve("Any");

        }
    
  

    // Or using Promise
    geocoder.reverse({lat:location.namedCoordinates.latitude, lon:location.namedCoordinates.longitude})
    .then(function(res) {
        common.log(res);
       return resolve(res);
    })
    .catch(function(err) {
        common.log(err);
        reject(err);
    });

    });
};

function CreateDatTruckObject(Datload)
{
  return new Promise(function(resolve, reject){

  let truck = new Truck();
  //load.name = Datload.asset.assetId;

  var origin = getAddressFromCoordinates(Datload.asset.equipment.origin);

  origin.then(function(res){
        common.log(res);        
        var destination = getAddressFromCoordinates(Datload.asset.equipment.destination);    
        destination.then(function(res2){

            //handle partial address or only states
            if(res === "Any" || res[0] && res[0].zipcode === null ){
                
                if(res.stateProvinces){
                    truck.destination_bycity_state = res.stateProvinces[0];
                    truck.destination_bystate_states = res.stateProvinces;
                }
            }
            else
            {
            truck.origin_bycity_city = Datload.asset.equipment.origin.namedCoordinates.city; //data.origin_bycity_city;
            truck.origin_bycity_state = (res[0].administrativeLevels.level1long)? res[0].administrativeLevels.level1long : Datload.asset.equipment.origin.namedCoordinates.stateProvince; //data.origin_bycity_state;
            truck.origin_bycity_zip = parseInt(res[0].zipcode); //data.origin_bycity_zip;
            truck.origin_bycity_lat_long = { type: "Point", coordinates: [ parseFloat(Datload.asset.equipment.origin.namedCoordinates.longitude),parseFloat(Datload.asset.equipment.origin.namedCoordinates.latitude)]}; // { type: "Point", coordinates: data.origin_bycity_lat_long};
            //truck.origin_bycity_range = data.origin_bycity_range;
            truck.origin_bystate_states = Datload.asset.equipment.origin.namedCoordinates.stateProvince; //data.origin_bystate_states;
            }
    
            if(res2 === "Any" || res2[0] && res2[0].zipcode === null ){
                
                if(res2.stateProvinces){
                    truck.destination_bycity_state = res2.stateProvinces[0];
                    truck.destination_bystate_states = res2.stateProvinces;
                }
            }
            else{
            //truck.destination_bycity_address = data.destination_bycity_address;
            truck.destination_bycity_city = Datload.asset.equipment.destination.namedCoordinates.city;// data.destination_bycity_city;
            truck.destination_bycity_state = (res2[0].administrativeLevels.level1long)? res2[0].administrativeLevels.level1long : Datload.asset.equipment.destination.namedCoordinates.stateProvince; // data.destination_bycity_state;
            truck.destination_bycity_zip = parseInt(res2[0].zipcode) ; //data.destination_bycity_zip;
            truck.destination_bycity_lat_long = { type: "Point", coordinates: [ parseFloat(Datload.asset.equipment.destination.namedCoordinates.longitude),parseFloat(Datload.asset.equipment.destination.namedCoordinates.latitude)]}; //{ type: "Point", coordinates: data.destination_bycity_lat_long};
            //truck.destination_bycity_range = data.destination_bycity_range;
            truck.destination_bystate_states = Datload.asset.equipment.destination.namedCoordinates.stateProvince;//data.destination_bystate_states;
            }
    
            truck.equipment_type = (Datload.asset && Datload.asset.equipment)? Datload.asset.equipment.equipmentType : null; // data.equipment_type;
            truck.weight = (Datload.asset.dimensions)? Datload.asset.dimensions.weightPounds : 0; // data.weight;
            truck.length = (Datload.asset.dimensions)? Datload.asset.dimensions.lengthFeet : 0; // data.length;
            truck.rate_per_mile = (Datload.asset && Datload.asset.equipment && Datload.asset.equipment.rate )? parseFloat(Datload.asset.equipment.rate.baseRateDollars) : 0.00;// data.rate_per_mile;
        
            truck.ready_start = (Datload.asset.availability)? Datload.asset.availability.earliest : null; //data.ready_start;
            truck.ready_end = (Datload.asset.availability)? Datload.asset.availability.latest : null;// data.ready_end;
        
            truck.description = Datload.asset.assetId; //data.description;
            truck.internal = false;
            truck.source = "DAT";
             return resolve(truck);
        })
  })
   
    //res = res1;
         
     /** load.delivery_addresses = [];
      load.delivery_cities = [Datload.asset.shipment.destination.namedCoordinates.city];
      load.delivery_states = [Datload.asset.shipment.destination.namedCoordinates.stateProvince];
      //load.delivery_zips = data.delivery_zips;//TODO convert coordinates to address and zip;
      
      load.delivery_lat_long = { type: "MultiPoint", coordinates: [[parseFloat(Datload.asset.shipment.destination.namedCoordinates.longitude),parseFloat(Datload.asset.shipment.destination.namedCoordinates.latitude)]]};
      load.delivery_zips = [parseInt(res2[0].zipcode)];//TODO convert coordinates to address and zip;
      if(res2[0].administrativeLevels.level1long) load.delivery_states.push(res2[0].administrativeLevels.level1long);
      if(res2[0].administrativeLevels.level2long) load.delivery_states.push(res2[0].administrativeLevels.level2long);
      
      
      load.description = Datload.asset.assetId;
        load.load_weight = (Datload.asset.dimensions)? Datload.asset.dimensions.weightPounds : 0;
        load.unit_of_measurement = "pounds";
        load.load_length = (Datload.asset.dimensions)? Datload.asset.dimensions.lengthFeet : 0;
        load.pickup_date = (Datload.asset.availability)?Datload.asset.availability.earliest : new Date();
        load.delivery_date =(Datload.asset.availability)? Datload.asset.availability.latest : new Date();
        
        load.delivery_address = Datload.asset.shipment.destination.namedCoordinates.city;
        load.delivery_city = Datload.asset.shipment.destination.namedCoordinates.city;
        load.delivery_state = Datload.asset.shipment.destination.namedCoordinates.stateProvince;
        //load.delivery_zip = 12345//TODO convert coordinates to address and zip;


        load.phone = (Datload.callback && Datload.callback.phone) ? Datload.callback.phone.phone.number : null;
        load.expire_on = Datload.asset.status.endDate;
        load.is_opened = (!Datload.asset.status.expired);
        load.factor_load = true;
        load.internal = false;
        load.source = "DAT";

        
        load.equipment_type = (Datload.asset && Datload.asset.shipment)? Datload.asset.shipment.equipmentType : null;
        load.no_of_count = (Datload.asset)? parseInt(Datload.asset.count) : 0;
        load.target_rate = (Datload.asset && Datload.asset.shipment && Datload.asset.shipment.rate )? parseFloat(Datload.asset.shipment.rate.baseRateDollars) : 0.00;


        load.ready_date = (Datload.asset.availability)? Datload.asset.availability.earliest : null;
        load.pickup_date = (Datload.asset.availability)? Datload.asset.availability.latest : null;
        load.delivery_date = (Datload.asset.availability)? Datload.asset.availability.earliest : null;

        **/
       
        // truck.name = data.name;
        //truck.origin_bycity_address = data.origin_bycity_address;
        

    });

}

module.exports.findtrucks = function(search_object, socket,io) {
  'use strict';
  var search_options ={};
 var searchQuery;

    return new Promise(
      function(resolve, reject){

        search_options = common.validateSearchFilter(search_object);
       return getCoordinates(search_object.pickup_address,_.isArray(search_object.pickup_cities)?search_object.pickup_cities[0] : search_object.pickup_cities,
       _.isArray(search_object.pickup_states)?search_object.pickup_states[0] : search_object.pickup_states)
    .then(function(pickup_coordinates)
      {
        search_options["pickup_coordinates"] = pickup_coordinates ;//deliveryCoordinates;
       return getCoordinates(search_object.delivery_address,_.isArray(search_object.delivery_cities)?search_object.delivery_cities[0] : search_object.delivery_cities,
       _.isArray(search_object.delivery_states)?search_object.delivery_states[0]:search_object.delivery_states)
      })
    .then(function(deliveryCoordinates){        
        search_options["delivery_coordinates"] = deliveryCoordinates;
        searchQuery = buildSearchQuery(search_options);
        return searchQuery;
      })
      .then(function(searchQuery){

        // get DAT Search account
        DatUser.find({"environment": "test", "role": "search"})
        .exec(function(err,datuser){
    
          if (err) {
            common.log(err)
            reject(err);
          }
          else if(!datuser || datuser.length == 0 )
          {
            reject("error: DAT user not found", datuser);
          }
          
          return datuser;
        })
        .then(function(datuser) {

           //login to DAT API
          DATLoadLogin(datuser[0], function (user){

          var url = 'http://cnx.test.dat.com:9280/wsdl/TfmiFreightMatching.wsdl';
          //var client;
        //connect to api
          //{loginId: 'jmg_cnx1', password: 'logistics', thirdPartyId : "ForkFreight-API",apiVersion : 2};
          soap.createClient(url, function(err, client) {
            if(err)
            {
              common.log("error message: " , err);
              return callback(err, null);
            } 
    
            // common.log(client);
            //add authentication token{}
            client.addSoapHeader({
              "headers:sessionHeader": {
                "headers:sessionToken": {
                    "types:primary": user.primaryToken,
                    "types:secondary": user.secondaryToken
                }
              }
              });
              

              //execute search
              client.CreateSearch(searchQuery, function(err, result) {
                  
                if(err)
                {
                  common.log("error message: " , err);
                  reject(err);
                } 
                common.log(result);          
                //execute search with the login token.
                var trucks = [];
                if(result)
                {
                    if(result.createSearchResult && result.createSearchResult.createSearchSuccessData && result.createSearchResult.createSearchSuccessData.totalMatches > 0 ){
                        result.createSearchResult.createSearchSuccessData.matches.forEach(function(Datload) {
                           
                          var truck = new Truck();
                          var newtruck =  CreateDatTruckObject(Datload);                   

                          newtruck.then(function(res){
                            truck = res;

                            //find user / broker in our system
                            //we want to reach out later and get their actual details and convince them to signup
                            var useremail = "info@forkfreight.com"; //(Datload.callback && Datload.callback.email) ?
                                                  
                            var userpromise = User.find({email: useremail}).exec();

                            return userpromise;
                          })
                          .then(function(loaduser){
                              if(loaduser && loaduser.length > 0 )
                              {
                                //load.created_by = loaduser[0];
                                return loaduser[0];
                              }
                              else
                              {
                                var loaduser = new User();
                                loaduser.first_name = (Datload.callback && Datload.callback.name) ?Datload.callback.name.firstName : null;
                                loaduser.last_name = (Datload.callback && Datload.callback.name) ? Datload.callback.name.lastName : null;
                                
                                loaduser.email=(Datload.callback) ? Datload.callback.userId + "@forkfreight.com" : Randomstring.generate() + "@forkfreight.com";
                                loaduser.company =(Datload.callback) ?  Datload.callback.companyName : "DAT";
                                loaduser.address = " nw 1 st";
                                loaduser.phone = (Datload.callback.phone) ? Datload.callback.phone.phone.number : null;
                                loaduser.mc_number = (Datload.dotIds) ?  Datload.dotIds.brokerMcNumber: null;                    
                              
                                return loaduser.save();
                              }                       
                            }).then(function(loaduser){

                              if(loaduser)
                              {
                                truck.created_by = loaduser;
                                //return load;
                                truck.save();
                                common.log(truck);
                                trucks.push(truck); //TODO: Make the caller in server.js use a promise
                                io.emit('trucks_response', {type: 'new', data:truck});
                              }
                              
                            })
                            .catch(function(err){
                              // just need one of these
                              common.log('error:', err);
                              resolve(err);
                            });
                      });

                      resolve(trucks);
                    }
                }
                else
                { 
                  resolve(null);
                }
            });                      
       });

    })
 
  })
  .catch(function(err){
    common.log(err);
    reject(err);
  })
})
})
}  



function DATLoadLogin(credentials, callback)
{
  let tokenPrimary = credentials.primaryToken;
  let tokenSecondary = credentials.secondaryToken;
  let tokenExpiration = credentials.tokenexpiration;

  if(Date.now() <= Date.parse(tokenExpiration))
  {
   return callback(credentials);
  }

    var url = 'http://cnx.test.dat.com:9280/wsdl/TfmiFreightMatching.wsdl';
    var args = { 
              
            "loginOperation": {
                "loginId": credentials.loginId,// "jmg_cnx1",
                "password":credentials.password, // "logistics",
                "thirdPartyId": "ForkFreight-API",
                "apiVersion": "2"
            }  
         
    }
     //{loginId: 'jmg_cnx1', password: 'logistics', thirdPartyId : "ForkFreight-API",apiVersion : 2};
    soap.createClient(url, function(err, client) {
        if(err) 
        {
          common.log("error message: " , err);
          //reject(err);
        }
        common.log(client);
        client.Login(args, function(err, result) {
            if(err) 
            {
              common.log("error message: " , err);
              //reject(err);
            }
            else{
            common.log(result);
            let tokenPrimary = result.loginResult.loginSuccessData.token.primary;
            let tokenSecondary = result.loginResult.loginSuccessData.token.secondary;
            let tokenExpiration = result.loginResult.loginSuccessData.expiration;
            credentials.primaryToken = tokenPrimary;
            credentials.secondaryToken = tokenSecondary;
            credentials.tokenexpiration = tokenExpiration;
            //execute search with the login token.
            common.log(credentials);
            DatUser.update({_id: credentials._id}, credentials, {upsert: true}, function(err, user){

              if(err) { common.log(err);}
              else
              {
                callback(credentials);
              }
            });

            }
        });
    });

}


module.exports.postTruck = function(truck, data, socket, callback) {

    'use strict';
    if (!socket.decoded._id) {
      socket.emit('add_load_response', {success: false, message: "UnauthorizedError: Private action"});
    } else {
  
    //   if(!data.name || !data.description 
    //   || !data.weight || !data.unit_of_measurement
    //   || !data.pickup_date || !data.delivery_date
    //   || !data.pickup_address || !data.pickup_city || !data.pickup_state || !data.pickup_zip 
    //   || !data.delivery_address || !data.delivery_city || !data.delivery_state || !data.delivery_zip
    //   || !data.phone //|| !data.images[0]
    // ) {
    //     socket.emit('add_load_response', {success: false, message: "All fields are required"});
    //     return;
    //   }
  
    var pickup_coordinates;
    var delivery_coordinates
    var pickup = getCoordinates(data.origin_bycity_address, data.origin_bycity_city,
    data.origin_bycity_state);
  
    pickup.then(function(res){
      pickup_coordinates = res;
  
      return getCoordinates(data.destination_bycity_address, data.destination_bycity_city,
        data.destination_bycity_state)
    }).then(function(res2){
  
        delivery_coordinates = res2;
  
       
    
        var postersReferenceId = Randomstring.generate(8);
    
         // get DAT Search account
        DatUser.find({"environment": "test", "role": "post"})
        .exec(function(err,datuser){
        
          if (err) {common.log(err)
          }else if
          //return if dat user not found
          (!datuser || datuser.length == 0 )
          {
          return callback(datuser);
          }
          //login to DAT API
          DATLoadLogin(datuser[0], function (user){
    
          var url = 'http://cnx.test.dat.com:9280/wsdl/TfmiFreightMatching.wsdl';
    
          var args = { 
            
              "postAssetOperations": {
                "equipment": {
                    "equipmentType":"Flatbed" ,//load.equipment_type,
                    "origin": {
                        "namedCoordinates": {
                            "latitude": pickup_coordinates.geocode_lat, // "29.76306",
                            "longitude": pickup_coordinates.geocode_long, // "-95.36306",
                            "city": pickup_coordinates.city,
                            "stateProvince": pickup_coordinates.state_code
                          }
                    },
                    "destination": {
                        "place": {
                                "namedCoordinates": {
                                    "latitude": delivery_coordinates.geocode_lat, // "29.76306",
                                    "longitude": delivery_coordinates.geocode_long, // "-95.36306",
                                    "city": delivery_coordinates.city,
                                    "stateProvince": delivery_coordinates.state_code
                                }
                            }
                    }
                    /**,
                    "rate": {
                      "baseRateDollars": 2000,
                      "rateBasedOn": "Flat",
                      "rateMiles": 500
                    }
                    **/
                },
                
                "postersReferenceId":  postersReferenceId,
                "ltl": "false",
                "comments": truck.description,
                "count": "1",
                "dimensions": {
                    "lengthFeet": truck.length,
                    "weightPounds": truck.weight,
                    "heightInches": 80,
                    "volumeCubicFeet": 3000
                },
                "stops": "1",
                //"availability": {
                //    "earliest": load.pickup_date,
               //     "latest": load.delivery_date
               // },
                
                "includeAsset": "true",
                "postToExtendedNetwork": "false"
              }
          }
        //connect to api
        //{loginId: 'jmg_cnx1', password: 'logistics', thirdPartyId : "ForkFreight-API",apiVersion : 2};
        soap.createClient(url, function(err, client) {
        if(err) common.log("error message: " , err);
        common.log(client);
        //add authentication token
        client.addSoapHeader({
          "headers:sessionHeader": {
            "headers:sessionToken": {
                "types:primary": user.primaryToken,
                "types:secondary": user.secondaryToken
            }
          }
        });
        //execute search
        client.PostAsset(args, function(err, result) {
            if(err) 
            {
              common.log("error message: " , err);
              return callback(result);
            }
            else{
            common.log(result); 
                     
            //execute search with the login token.
            return callback(result);
            }
        });
      });
    
    });
    
    }); 
  
    })  
  } 
 };

 module.exports.editTruck = function(truck, data, socket, callback) {

  'use strict';
  if (!socket.decoded._id) {
    socket.emit('add_load_response', {success: false, message: "UnauthorizedError: Private action"});
  } else {

  //   if(!data.name || !data.description 
  //   || !data.weight || !data.unit_of_measurement
  //   || !data.pickup_date || !data.delivery_date
  //   || !data.pickup_address || !data.pickup_city || !data.pickup_state || !data.pickup_zip 
  //   || !data.delivery_address || !data.delivery_city || !data.delivery_state || !data.delivery_zip
  //   || !data.phone //|| !data.images[0]
  // ) {
  //     socket.emit('add_load_response', {success: false, message: "All fields are required"});
  //     return;
  //   }

  var pickup_coordinates;
  var delivery_coordinates
  var pickup = getCoordinates(data.origin_bycity_address, data.origin_bycity_city,
  data.origin_bycity_state);

  pickup.then(function(res){
    pickup_coordinates = res;

    return getCoordinates(data.destination_bycity_address, data.destination_bycity_city,
      data.destination_bycity_state)
  }).then(function(res2){

      delivery_coordinates = res2;

     
  
      var postersReferenceId = Randomstring.generate(8);
  
       // get DAT Search account
      DatUser.find({"environment": "test", "role": "post"})
      .exec(function(err,datuser){
      
        if (err) {common.log(err)
        }else if
        //return if dat user not found
        (!datuser || datuser.length == 0 )
        {
        return callback(datuser);
        }
        //login to DAT API
        DATLoadLogin(datuser[0], function (user){
  
        var url = 'http://cnx.test.dat.com:9280/wsdl/TfmiFreightMatching.wsdl';
  
        var args = { 
          
            "updateAssetOperation": {
              "assetId" : truck.datAssetId,
              //"postersReferenceId":  truck.postersReferenceId,
              "equipmentUpdate": {
                "ltl": "false",
                "comments": truck.description,
                "count": "1",
                "dimensions": {
                    "lengthFeet": truck.length,
                    "weightPounds": truck.weight,
                    "heightInches": 80,
                    "volumeCubicFeet": 3000
                },
                "stops": "1",
                  /**"equipmentType":"Flatbed",
                  "origin": {
                      "namedCoordinates": {
                          "latitude": pickup_coordinates.geocode_lat, 
                          "longitude": pickup_coordinates.geocode_long, 
                          "city": pickup_coordinates.city,
                          "stateProvince": pickup_coordinates.state_code
                        }
                  },
                  "destination": {
                      "place": {
                              "namedCoordinates": {
                                  "latitude": delivery_coordinates.geocode_lat, // "29.76306",
                                  "longitude": delivery_coordinates.geocode_long, // "-95.36306",
                                  "city": delivery_coordinates.city,
                                  "stateProvince": delivery_coordinates.state_code
                              }
                          }
                  } **/
                  /**,
                  "rate": {
                    "baseRateDollars": 2000,
                    "rateBasedOn": "Flat",
                    "rateMiles": 500
                  }
                  **/
              },
              
              

              //"availability": {
              //    "earliest": load.pickup_date,
             //     "latest": load.delivery_date
             // },
              
              //"includeAsset": "true",
              //"postToExtendedNetwork": "true"
            }
        }
      //connect to api
      //{loginId: 'jmg_cnx1', password: 'logistics', thirdPartyId : "ForkFreight-API",apiVersion : 2};
      soap.createClient(url, function(err, client) {
      if(err) common.log("error message: " , err);
      common.log(client);
      //add authentication token
      client.addSoapHeader({
        "headers:sessionHeader": {
          "headers:sessionToken": {
              "types:primary": user.primaryToken,
              "types:secondary": user.secondaryToken
          }
        }
      });
      //execute search
      client.UpdateAsset(args, function(err, result) {
          if(err) 
          {
            common.log("error message: " , err);
            return callback(result);
          }
          else{
          common.log(result); 
                   
          //execute search with the login token.
          return callback(result);
          }
      });
    });
  
  });
  
  }); 

  })  
} 
};




module.exports.deleteTruck = function(truck, data, socket, callback) {

  'use strict';
  if (!socket.decoded._id) {
    socket.emit('add_load_response', {success: false, message: "UnauthorizedError: Private action"});
  } else {

  var pickup_coordinates;
  var delivery_coordinates
  var pickup = getCoordinates(_.isArray(data.pickup_addresses)?data.pickup_addresses[0] : null,_.isArray(data.pickup_cities)?data.pickup_cities[0] : data.pickup_cities,
  _.isArray(data.pickup_states)?data.pickup_states[0] : data.pickup_states);

  pickup.then(function(res){
    pickup_coordinates = res;

    return getCoordinates(_.isArray(data.delivery_addresses)?data.delivery_addresses[0] : data.delivery_address,_.isArray(data.delivery_cities)?data.delivery_cities[0] : data.delivery_cities,
    _.isArray(data.delivery_states)?data.delivery_states[0] : data.delivery_states)
  }).then(function(res2){

      delivery_coordinates = res2;

        
       // get DAT Search account
      DatUser.find({"environment": "test", "role": "post"})
      .exec(function(err,datuser){
      
        if (err) {common.log(err)
        }else if
        //return if dat user not found
        (!datuser || datuser.length == 0 )
        {
        return callback(datuser);
        }
        //login to DAT API
        DATLoadLogin(datuser[0], function (user){
  
        var url = 'http://cnx.test.dat.com:9280/wsdl/TfmiFreightMatching.wsdl';
  
        var args = { 
          
            "deleteAssetsByAssetIds": {
              "assetId" : load.assetId
            }
        }
      //connect to api
      //{loginId: 'jmg_cnx1', password: 'logistics', thirdPartyId : "ForkFreight-API",apiVersion : 2};
      soap.createClient(url, function(err, client) {
      if(err) common.log("error message: " , err);
      common.log(client);
      //add authentication token
      client.addSoapHeader({
        "headers:sessionHeader": {
          "headers:sessionToken": {
              "types:primary": user.primaryToken,
              "types:secondary": user.secondaryToken
          }
        }
      });
      //execute search
      client.DeleteAsset(args, function(err, result) {
          if(err) 
          {
            common.log("error message: " , err);
            return callback(result);
          }
          else{
          common.log(result); 
                   
          //execute search with the login token.
          return callback(result);
          }
      });
    });
  
  });
  
  }); 

  })   
} 
};


module.exports.findTrucks = function(search_object, socket, callback) {
  'use strict';
  var search_options = {};
  search_options["is_opened"] = true;
  search_options["created_by"] = {$ne : socket.decoded._id};

  if(search_object !== undefined) {

    if(search_object.name && search_object.name != "") {
      search_options["name"] = new RegExp(search_object.name, "i")
    }

    if(search_object.dm_length && search_object.dm_length != "") {
      search_options["dm_length"] = search_object.dm_length;
    }

    if(search_object.dm_height && search_object.dm_height != "") {
      search_options["dm_height"] = search_object.dm_height;
    }

    if(search_object.dm_weight && search_object.dm_weight != "") {
      search_options["dm_weight"] = search_object.dm_weight;
    }

    if(search_object.dm_volume && search_object.dm_volume != "") {
      search_options["dm_volume"] = search_object.dm_volume;
    }

    // Origin 
    if(search_object.origin_address && search_object.origin_address != "") {
      search_options["origin_address"] = new RegExp(search_object.origin_address, "i")
    }

    if(search_object.origin_city && search_object.origin_city != "") {
      search_options["origin_city"] = search_object.origin_city;
    }

    if(search_object.origin_state && search_object.origin_state != "") {
      search_options["origin_state"] = search_object.origin_state;
    }

    if(search_object.origin_zip && search_object.origin_zip != "") {
      search_options["origin_zip"] = search_object.origin_zip;
    }

    // Destination
    if(search_object.destination_type && search_object.destination_type != "") {

      if(search_object.destination_type == 'location') {

        if(search_object.destination_address && search_object.destination_address != "") {
          search_options["destination_address"] = new RegExp(search_object.destination_address, "i")
        }

        if(search_object.destination_city && search_object.destination_city != "") {
          search_options["destination_city"] = search_object.destination_city;
        }

        if(search_object.destination_state && search_object.destination_state != "") {
          search_options["destination_state"] = search_object.destination_state;
        }

        if(search_object.destination_zip && search_object.destination_zip != "") {
          search_options["destination_zip"] = search_object.destination_zip;
        }

      } else {
        search_options["destination_type"] = search_object.destination_type;
      }
    }
  
  }
  Truck.find(search_options)
  .populate('created_by')    
  .exec(function(err, trucks) {
    callback(trucks);
  });

};


