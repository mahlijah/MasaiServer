const mongoose = require('mongoose');
const Role =  mongoose.model('Role');
const common = require('../helpers/common');

module.exports.getAllRoles = async function(req,res){
    try{
        let roles = await Role.find({is_deleted:false}).lean()
        return res.send({success:true,data:roles})
    } catch(err){
        common.log('Error while get all role :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}

module.exports.getPublicRoles = async function(req,res){
    try{
        let roles = await Role.find({is_deleted:false,is_public:true},{
            is_public:0,
            is_deleted:0,
            __v:0
        }).lean();
        return res.send({success:true,data:roles})
    } catch(err){
        common.log('Error while get all role :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}

module.exports.addRole = async function(req,res){
    try{
        if(!req.body.name){
            return res.status(400).send({success:false,message:"name is required"});
        }
        let role =  new Role();
        role.name = req.body.name;
        role.is_public = req.body.is_public;
        await role.save();
        return res.send({success:true,data:role})
    } catch(err){
        common.log('Error while add role :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}

module.exports.editRole = async function(req,res){
    try{
        let role = await Role.findById(req.params.id).lean();
        if(!role){
            return res.status(404).send({success:false,message:'Role not found.'});
        }
        if(req.body.name) role.name = req.body.name;
        role.is_public = req.body.is_public;
        delete role._id;
        await Role.updateOne({_id:req.params.id},role);
        return res.send({success:true,message:"Role updated successfully."})
    } catch(err){
        common.log('Error while get all role :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}

module.exports.markAsPublicRole = async function(req,res){
    try{
        let role = await Role.findById(req.params.id).lean();
        if(!role){
            return res.status(404).send({success:false,message:'Role not found.'});
        }
        role.is_public = req.params.status == 'true';
        delete role._id;
        await Role.updateOne({_id:req.params.id},role);
        return res.send({success:true,message:"Role updated successfully."})
    } catch(err){
        common.log('Error while get all role :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}

module.exports.deleteRole = async function(req,res){
    try{
        let role = await Role.findById(req.params.id).lean();
        if(!role){
            return res.status(404).send({success:false,message:'Role not found.'});
        }
        role.is_deleted = true;
        delete role._id;
        await Role.updateOne({_id:req.params.id},role);
        return res.send({success:true,message:"Role deleted successfully."})
    } catch(err){
        common.log('Error while get all role :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}