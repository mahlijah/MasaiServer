var mongoose = require('mongoose');
var smsAlert = mongoose.model('SmsAlert');
const { ObjectId } = require('mongodb');

module.exports.dispatcherloadsSmsAlert = async (req, res) => {
    try {
        let data = req.body.data;
        var query= {
            user: req.payload._id,
            searchObjectId: data._id
         }  
        let userSmsData = await smsAlert.findOne({ $and: [{ user: ObjectId(req.payload._id), searchObjectId: ObjectId(data._id) }] }).lean();
        // checking the user and searchObject for same id is not avaialble then add new object in new collection 
        if(typeof req.body.checkedin == "boolean"){
             query['alert']=req.body.checkedin;
        }
        if(typeof req.body.emailCheckedIn == "boolean"){
            query['emailAlert']=req.body.emailCheckedIn;
        }
        
        if (!userSmsData) {
            let smsAlertSaveData = new smsAlert(query)
            let result = await smsAlertSaveData.save();
            if (result) {
                return res.status(200).json({ save: true, message: 'Alert save', result })
            }
        } else {
            let update = {};
             if(typeof req.body.checkedin == "boolean"){
                 update['alert']=req.body.checkedin;
             }
             if(typeof req.body.emailCheckedIn == "boolean"){
                update['emailAlert']=req.body.emailCheckedIn;
            } 
            let updated = await smsAlert.findOneAndUpdate({ $and: [{ user: ObjectId(req.payload._id), searchObjectId: ObjectId(data._id) }] }, { $set: update }, { upsert: true})
            return res.status(200).json({ update: true, message: "Alert is updated", updated })
            
        }
    }
    catch (err) {
        return res.status(400).send({ success: false, message: err.message });
    }

}
