const { lookup } = require('geoip-lite');
const { ObjectId } = require('bson');
const mongoose = require('mongoose');
const Advertisement = mongoose.model('Advertisement');
const AdvertisementClick = mongoose.model('AdvertisementClick');
const User = mongoose.model('User');
const Offer = mongoose.model('Offer');
const common = require('../helpers/common');

module.exports = {
    getAllAdvertisements: async function(req,res){
        try{
            let sort_by = req.body.sort_by || 'createdAt';
            let sort_direction = req.body.sort_direction == "asc" ? 1 : -1
            let sortObj={};
            sortObj[sort_by]=sort_direction;
            let limit = req.body.limit||10;
            let skip = ((req.body.skip||1) - 1) * limit  ;
            const [count,data]= await Promise.all([
                Advertisement.countDocuments(),
                Advertisement.find({ is_delete: false }).sort(sortObj).skip(skip).limit(limit).lean()
            ]);
            return res.send({success:true,count,data});
        }
        catch(err){
            common.log("Error While get All advertisements:---",err);
            return res.status(400).send({success:false,message:err.message});
        }
    },
    getActiveAdvertisements: async function(req,res){
        try{
            let cd = new Date();
            const user = await User.findById(req.payload._id,{subscription_package:1}).lean();
            let offer_query = {
                start_date: { $lte: cd },
                end_date: { $gte: cd },
            }
            if(user && user.subscription_package){
                offer_query["offer_on"]={$in:user.subscription_package};
            }
            const offer_ids = (await Offer.find(offer_query,{
                _id:1
            }).lean()).map(obj=>{
                return obj._id.toString()
            });
            const data = await Advertisement.aggregate([
                {   
                    $match : {
                        $and :[
                            { is_active: true}, 
                            {is_delete: false},
                            {start_date: { $lte: cd }},
                            {expire_at: { $gte: cd }},
                            {
                                $or:[
                                    {action:{$ne:'offer'}},
                                    {$and:[
                                        {action:'offer'},
                                        {value:{$in:offer_ids}}
                                    ]}
                                ]
                            }
                        ]
                    }
                },
              {  $sample : { size: 10 }}
            ])
            return res.send({success:true,data});
        }
        catch(err){
            common.log("Error While get Rendom advertisements:---",err);
            return res.status(400).send({success:false,message:err.message});
        }
    },
    addAdvertisement: async function(req,res){
        try{
            if(!req.file || !req.file.location){
                return res.status(400).send({success:false,message:"file is required"});
            }
            const advertisement = new Advertisement();
            advertisement.title = req.body.title;
            advertisement.file_url = req.file.location;  
            advertisement.s3_key = req.file.key;
            advertisement.originalname = req.file.originalname;
            advertisement.mimetype = req.file.mimetype;
            advertisement.is_active = req.body.is_active == false ? false : true;
            advertisement.start_date = req.body.start_date;
            advertisement.external_link = req.body.external_link;
            advertisement.action = req.body.action;
            advertisement.value = req.body.value;
            advertisement.type = req.body.type;
            advertisement.description = req.body.description;
            if(req.body.end_date){
                advertisement.expire_at = req.body.end_date;
            }
            const data = await advertisement.save();
            return res.send({success:true,data});
        }
        catch(err){
            common.log("Error While get Rendom advertisements:---",err);
            return res.status(400).send({success:false,message:err.message});
        }
    },
    editAdvertisement: async function(req,res){
        try{
            let advertisement = await Advertisement.findById(req.params.id);
            if(advertisement){
                if(req.body.title) advertisement.title = req.body.title;
                if(req.file && req.file.location){ 
                    advertisement.file_url = req.file.location;         
                    advertisement.s3_key = req.file.key;
                    advertisement.originalname = req.file.originalname;
                    advertisement.mimetype = req.file.mimetype;
                }
                if(req.body.expire_at) advertisement.expire_at = req.body.expire_at;
                advertisement.is_active = req.is_active == false ? false : true;
                advertisement.start_date = req.body.start_date || new Date();
                if(req.body.end_date){
                    advertisement.expire_at = req.body.end_date;
                }
                advertisement.external_link = req.body.external_link;
                advertisement.action = req.body.action;
                advertisement.value = req.body.value;
                advertisement.type = req.body.type;
                advertisement.description = req.body.description;
                let upsert = advertisement.toObject();
                delete upsert._id;
                const data = await Advertisement.findOneAndUpdate({_id:ObjectId(req.params.id)},upsert);
                return res.send({success:true,data});
            }else{
                return res.status(400).send({success:false,message:"Advertisement not found"});
            }
        }
        catch(err){
            common.log("Error While get Rendom advertisements:---",err);
            return res.status(400).send({success:false,message:err.message});
        }
    },
    deleteAdvertisement: async function(req,res){
        try{
            let advertisement = await Advertisement.findById(req.params.id);
            if(advertisement){
                const data = await Advertisement.findOneAndUpdate({_id:ObjectId(req.params.id)},{$set:{is_delete:true}});
                return res.send({success:true,message: 'Deleted Successfully'});
            }else{
                return res.status(400).send({success:false,message:"Advertisement not found"});
            }
        }
        catch(err){
            common.log("Error While get Rendom advertisements:---",err);
            return res.status(400).send({success:false,message:err.message});
        }
    },
    onAdvertisementClick: async function(req,res){
        try{
            const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;;
            let ac = new AdvertisementClick();
            ac.user=req.payload._id;
            ac.advertisement=req.params.id;
            ac.ip=ip;
            ac.lookup=lookup(ip);
            ac.save();
            return res.send({success:true,data:ac});
        }
        catch(err){
            common.log("Error While on Advertisement Click:---",err);
            return res.status(400).send({success:false,message:err.message});
        }
    }
}