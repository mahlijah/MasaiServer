var mongoose = require('mongoose');
var Load = mongoose.model('Load');
var LoadHistory = mongoose.model('LoadHistory');
var Truck = mongoose.model('Truck');
var User = mongoose.model('User');
var LoadsByCsvUpload = mongoose.model('LoadsByCsvUpload');
var SearchObject = mongoose.model('SearchObject');
var TemporaryUsers = mongoose.model('TemporaryUsers');
const { Parser } = require('json2csv');
var config = require('../config/config');
var common = require('../helpers/common');
var LoadWatchedUsers = mongoose.model('LoadWatchedUsers');
var SmsAlert = mongoose.model('SmsAlert');
let validateAddress=require('../controllers/posteverywhere')
var _ = require('util');
var lodash = require('lodash');
var url = require('url');
var async = require("async");
let request = require('request')
const REDIS_URL = config.loadJobs.redisUri;
const redis_uri = url.parse(REDIS_URL);
const redisOptions = REDIS_URL.includes("rediss://")
  ? {
    port: Number(redis_uri.port),
    host: redis_uri.hostname,
    password: redis_uri.auth.split(":")[1],
    maxRetriesPerRequest: 0,
    tls: {
      rejectUnauthorized: false,
      servername: redis_uri.hostname
    },
    enableReadyCheck: false
  }
  : REDIS_URL;

var Queue = require('bull');
var newLoadQueue = new Queue(config.loadJobs.newloadQueue.name, { redis: redisOptions });
var newCSVLoadQueue = new Queue(config.loadJobs.newCsvLoadsQueue.name, { redis: redisOptions });
var deleteInternalLoadQueue = new Queue(config.loadJobs.deleteloadQueue.name, { redis: redisOptions });
var ObjectId = require('mongodb').ObjectID;
var Company = mongoose.model('Company');
let companyTransCreditDetails= mongoose.model('CompanyTransCreditDetails')
var states_names = require('../helpers/states_name');
var response = require('http-status-codes');
const csvtojson = require('csvtojson');
var s3FileDownload = require('../helpers/s3FileDownload').s3FileDownload;
const { ObjectID } = require('mongodb');
const {store_sockets_logs} = require('./userActivityLog');

module.exports.myLoads = async function (req, res) {
  'use strict';
  var limitResults = req.body.limit || 10;
  var skipResults = ((req.body.skip || 1) - 1) * limitResults;
  var sort_by = req.body.sort_by || 'updatedAt';
  var sort_direction = req.body.sort_direction == 'asc' ? 1 : -1;
  var sortObj = {};
  var sort_split = sort_by.split(',');
  for (let index = 0; index < sort_split.length; index++) {
    sortObj[sort_split[index].trim()] = sort_direction;
  }
  let [count, loads] = await Promise.all([
    Load.count({ created_by: req.payload._id, "is_opened": true }),
    Load.find({ created_by: req.payload._id, "is_opened": true },{
      _id:1,
      ready_date: { $dateToString: { format: "%m/%d/%Y", date: "$ready_date" } },
      delivery_addresses: 1,
      delivery_cities: 1,
      delivery_date: 1,
      delivery_lat_long: 1,
      delivery_states: 1,
      delivery_zips: 1,
      description: 1,
      equipment_type: { $cond: { if: { "equipment_type" : { $type : 2 } }, then:   { $split: ["$equipment_type", ","] }  , else: "$equipment_type" } },
      load_weight: 1,
      load_length: 1,
      no_of_count: 1,
      pickup_addresses: 1,
      pickup_date: 1,
      pickup_cities: 1,
      pickup_lat_long: 1,
      pickup_states: 1,
      pickup_zips: 1,
      target_rate: 1,
      updatedAt:1,
    })
      .sort(sortObj).skip(skipResults).limit(limitResults).lean()
  ])

  return res.status(response.StatusCodes.OK).json({ count: count, load: loads })

};


module.exports.myLoadsWithId = async function (req, res) {
  'use strict';
  try {

  let {userId,limit,skip,sort_by,sort_direction}=req.body;
  let limitResults = limit || 10;
  let skipResults = ((skip || 1) - 1) * limitResults;
  let sortBy = sort_by || 'updatedAt';
  let sortDirection=sort_direction == 'asc' ? 1 : -1;
  let sortObj = {};
  let sort_split = sortBy.split(',');
  if(!userId){
    return res.status(response.StatusCodes.BAD_REQUEST).send("UserId is required");
  }

  for (let index = 0; index < sort_split.length; index++) {
    sortObj[sort_split[index].trim()] = sortDirection ;
  }
  let [totalcount, loads] = await Promise.all([
    Load.find({ created_by: ObjectId(userId), "is_opened": true }).count(),
    Load.find({ created_by: ObjectId(userId), "is_opened": true },{
      _id:1,
      ready_date:{ $cond:

        {

        if : { $eq : [{$type:'$ready_date'},'date']},

        then: { $dateToString: { format: "%m/%d/%Y", date: "$ready_date" } },

        else: {

        $cond :{

        if : {$eq:['$ready_date',null]},

        then : '',

        else :"$ready_date"



        }

        }

        }

        },
      delivery_addresses: 1,
      delivery_cities: 1,
      delivery_date: 1,
      delivery_lat_long: 1,
      delivery_states: 1,
      delivery_zips: 1,
      description: 1,
      equipment_type: 1,
      load_weight: 1,
      load_length: 1,
      no_of_count: 1,
      pickup_addresses: 1,
      pickup_date: 1,
      pickup_cities: 1,
      pickup_lat_long: 1,
      pickup_states: 1,
      pickup_zips: 1,
      target_rate: 1,
      updatedAt:1,
    })
        .sort(sortObj).skip(skipResults).limit(limitResults).lean()

  ])


  return res.status(response.StatusCodes.OK).json({ count: totalcount, load: loads })
}catch(error){
  common.log("Error while get my trucks", error);
  return res.status(response.StatusCodes.INTERNAL_SERVER_ERROR).send({ message: error.messgae });
}
};


module.exports.watchlist = function (socket, callback) {
  'use strict';
  if (socket.decoded._id) {
    Load.find({ watched_users: socket.decoded._id, is_opened: true })
      .populate('bids.user')
      .populate('accepted_bid_user')
      .populate('created_by')
      .exec(function (err, loads) {
        callback(loads);
      });
  }

};
module.exports.watchlistHttp = async function (req, res) {
  try {
    var limitResults = 20;
    var skipResults = 0;
    var search_object = req.body;

    var sort_by = req.body.sort_by || 'updatedAt';
    var sort_direction = req.body.sort_direction == 'asc' ? 1 : -1;
    var sortObj = {};
    var sort_split = sort_by.split(',');
    for (let index = 0; index < sort_split.length; index++) {
      sortObj[sort_split[index].trim()] = sort_direction;
    }

    //convert search parameters to mongodb search criteria
    let search_options = await common.generateSearchObject(search_object);

    let loadWatchedList = await LoadWatchedUsers.find({ userId: ObjectId(req.payload._id) }).lean();
    let loadIds = loadWatchedList.map(function (obj) { return obj.loads })[0];
    //convert search parameters to mongodb search criteria
    let loadIdArray
    loadIds ? loadIdArray = loadIds : null;
    // search_options["$and"].push({ "watched_users": req.payload._id });

    search_options["$and"].push(
      {
        '_id': { $in: loadIdArray }

      }
    );

    if (search_object.limit != null && search_object.limit != '') {
      limitResults = search_object.limit;
    }

    if (search_object.skip != null && search_object.skip != '') {
      skipResults = (search_object.skip - 1) * limitResults;
    }

    //var mongoQuery = Load.find(search_options);
    let [totalcount, loads] = await Promise.all([
      Load.count(search_options),
      Load.find(search_options,
        {
          _id:1,
          ready_date: { $dateToString: { format: "%m/%d/%Y", date: "$ready_date" } },
          delivery_addresses: 1,
          delivery_cities: 1,
          delivery_date: 1,
          delivery_lat_long: 1,
          delivery_states: 1,
          delivery_zips: 1,
          description: 1,
          equipment_type: 1,
          load_weight: 1,
          load_length: 1,
          no_of_count: 1,
          pickup_addresses: 1,
          pickup_date: 1,
          pickup_cities: 1,
          pickup_lat_long: 1,
          pickup_states: 1,
          pickup_zips: 1,
          target_rate: 1,
          updatedAt:1,
        })
        .sort(sortObj)
        .limit(limitResults)
        .skip(skipResults).lean()
    ])

    let result = {};
    result["count"] = totalcount;
    result["data"] = loads;

    return res.send(result);
  }
  catch (err) {
    common.log("Error while get watch list", err);
    return res.status(500).send(err);
  }

};
module.exports.bidsWon = function (socket, callback) {
  'use strict';
  if (socket.decoded._id) {
    Load.find({ accepted_bid_user: socket.decoded._id })
      .populate('bids.user')
      .populate('accepted_bid_user')
      .populate('created_by')
      .exec(function (err, loads) {
        callback(loads);
      });
  }

};

module.exports.addToWatchlist = async function (load_id, socket) {
  'use strict';

  if (socket.decoded._id) {
    try {
      var load = await Load.findOne({ _id: load_id._id });
      if (!load.watched_users || load.watched_users.indexOf(socket.decoded._id) === -1) {
        load.watched_users.push(socket.decoded._id);
        var update = await Load.updateOne({ _id: load_id._id }, { $set: { watched_users: load.watched_users } });
        if (update) {
          socket.emit('add_to_watchlist_response', { success: true, message: "Load has been added to watchlist" });
        }
      } else {
        socket.emit('add_to_watchlist_response', { success: false, message: "You have already added this load to watchlist" });
      }
    }
    catch (err) {
      common.log(err);
    };
  }
}

module.exports.addToWatchlistHttp = async function (req, res) {
  'use strict';
  var addToWatchlist = true
  if (req.payload._id) {
    try {
      let LoadWatchedUser = await LoadWatchedUsers.findOne({ userId: ObjectId(req.payload._id) }).lean();
      if (!LoadWatchedUser) {
        let _loadWatchedUser = new LoadWatchedUsers();
        _loadWatchedUser.loads = [req.params.id],
          _loadWatchedUser.userId = req.payload._id;
        _loadWatchedUser.save();
        return res.status(200).json({ success: true, message: "Loads has been added to watchlist", addToWatchlist });
      } else {

        let hasMatch = LoadWatchedUser.loads.filter(id => id.toString() == req.params.id)
        if (hasMatch.length > 0) {
          return res.status(400).json({ success: false, message: "Loads has been already added to watchlist", addToWatchlist });
        }
        else {
          let updated = await LoadWatchedUsers.findOneAndUpdate({ userId: req.payload._id }, { $push: { loads: ObjectId(req.params.id) } });
          return res.status(200).json({ success: true, message: "Loads has been added to watchlist", addToWatchlist })
        }
      }
    }
    catch (err) {
      common.log("Error while add user into watchlist", err);
      res.status(400).json({ success: false, message: err.message });
    }
  }
}




module.exports.removeFromWatchlistHttp = async function (req, res) {
  'use strict';
  if (req.payload._id) {
    let updateWatchList = await LoadWatchedUsers.findOneAndUpdate({ $and: [{ userId: ObjectId(req.payload._id), loads: ObjectId(req.params.id) }] }, { $pull: { loads: ObjectId(req.params.id) } })
    if (!updateWatchList) {
      return res.status(400).json({ success: false, message: "Load id or user id is not found" });
    } else {
      return res.status(200).json({ success: true, message: "Load has been removed from watchlist" });
    }
  } else {
    res.status(400).json({ success: false, message: " user id is not found" });
  }
}




module.exports.closeLoad = function (req, res) {
  'use strict';
  let loadItem = req.body;
  if (!req.payload._id) {
    res.status(401).json({ success: false, message: "UnauthorizedError: Private action" });
  } else {
    //is_opened is not returned from frontend so no need to check
    //if (loadItem.is_opened) {
    Load.findOneAndUpdate({ _id: req.params.id }, { $set: { is_opened: false } }, { 'new': true })
      .populate('bids.user')
      .populate('accepted_bid_user')
      .populate('created_by')
      .exec(function (err, load) {
        if (err) {
          res.status(404).json({ success: false, message: err.message });
        } else {
          deleteInternalLoadQueue.add({ id: load._id });
          res.status(200).json({ success: true, message: "Load has been closed successfully", data: load });

        }
      });
    //}
  }
}



// To find Load
module.exports.findLoads = async function (search_object, socket, callback) {
  try {
    var limitResults = 20;
    var skipResults = 0;


    var decoded = socket.decoded;

    // common.log(decoded);

    await store_sockets_logs(search_object, "added search load", decoded._id);

    if (search_object.searchParams._id) {
      let updateId = search_object.searchParams._id;
      search_object._id = search_object.searchParams._id;
      delete search_object.searchParams._id
      await SearchObject.updateOne({ _id: ObjectId(updateId) }, { $set: { searchParams: search_object.searchParams } })
    }

    let search_options = await common.generateSearchObject(search_object.searchParams);
    // console.log("\n_________search options generated________________",JSON.stringify(search_options),"\n_____________searchend______________________");
    let searchList = null;
    if (search_object._id != null) {
      searchList = await SearchObject.findOne({ _id: search_object._id });
    }
    else {
      searchList = await SearchObject.findOne({ searchParams: search_object.searchParams });
    }

    let userid = ObjectId(decoded._id);
    if (searchList != null) {
      // common.log(searchList);
      let users = searchList.users.map(u => {
        if (u == null) {
          return;
        }
        else {
          return u.toString();
        }
      })

      if (lodash.indexOf(users, decoded._id) == -1) {
        searchList.users.push(userid);
        searchList.save();
      }
    }
    else {
      searchList = new SearchObject();
      searchList.searchParams = search_object.searchParams;
      searchList.users = [decoded._id];
      let search = await searchList.save();
      if (search == null) { common.log("failure to save search list"); }
    }


    if (search_object.limit != null && search_object.limit != '') {
      limitResults = search_object.limit;
    }

    if (search_object.skip != null && search_object.skip != '') {
      skipResults = (search_object.skip - 1) * limitResults;
    }

    var sort_by = search_object.searchParams.load_weight ? 'load_weight' : search_object.sort_by || 'updatedAt';
    var sort_direction = search_object.searchParams.load_weight ? -1 : search_object.sort_direction == 'asc' ? 1 : -1;
    // var sort_by = search_object.sort_by || 'updatedAt';
    // var sort_direction = search_object.sort_direction == 'asc' ? 1 : -1;
    var sortObj = {};
    sortObj[sort_by] = sort_direction;
    //var mongoQuery = Load.find(search_options);
    // let totalcount = await Load.find(search_options)
    //   .collation({ locale: 'en', strength: 2 })
    //   .count()
    //   .exec();

    // let loads = await Load.find(search_options)
    //   .collation({ locale: 'en', strength: 2 })
    //   .populate('bids.user')
    //   .populate('accepted_bid_user')
    //   .populate('created_by')
    //   .sort({ updatedAt: -1 })
    //   .limit(limitResults)
    //   .skip(skipResults)
    //   .exec();
    // console.log("before load aggregate:_____\n", search_options.$and,"\n^^^^^^^^^^^^^^^^")

    var loads = await Load.aggregate(
      [{
        $match: search_options
      },
      {
        $facet: {
          "loads_count": [{ "$group": { _id: null, count: { $sum: 1 } } }],
          "loads": [{
            $project: {
              _id: 1,
              //ready_date: { $dateToString: { format: "%m/%d/%Y", date: "$ready_date" } },
              ready_date: 1,
              delivery_addresses: 1,
              delivery_cities: 1,
              delivery_date: 1,
              delivery_lat_long: 1,
              delivery_states: 1,
              delivery_zips: 1,
              description: 1,
              distance: 1,
              equipment_type: 1,
              load_weight: 1,
              load_length: 1,
              no_of_count: 1,
              pickup_addresses: 1,
              pickup_date: 1,
              pickup_cities: 1,
              pickup_lat_long: 1,
              pickup_states: 1,
              pickup_zips: 1,
              target_rate: 1,
              updatedAt: 1,
            }
          }, {
            $sort: sortObj
          }, { "$skip": skipResults }, { "$limit": limitResults }]
        }
      },
      { $unwind: "$loads_count" },
      {
        $project: {
          count: "$loads_count.count",
          data: "$loads"
        }
      }
      ]).exec(function (err, load) {
        if (err) {
          console.log(err)
          return res.send({ err });
        } else {
          let result = {};

          if (load.length > 0) {
            async.waterfall([
              function (doneq) {
                async.map(load[0].data, function (adddata_a, done2) {

                  if (parseInt(adddata_a.distance) == 0) {

                    var start_load = adddata_a.pickup_lat_long.coordinates[0][1] + ',' + adddata_a.pickup_lat_long.coordinates[0][0];
                    var end_load = adddata_a.delivery_lat_long.coordinates[0][1] + ',' + adddata_a.delivery_lat_long.coordinates[0][0];
                    getdistance(start_load, end_load, function (responsess) {
                      if (responsess.status === 1) {
                        adddata_a.distance = parseInt(responsess.data.distanceValue);
                        var ratepm = (adddata_a.target_rate / adddata_a.distance).toFixed(2);
                        adddata_a.load_distance = adddata_a.distance;
                        adddata_a.rpm = ratepm;

                        if ((typeof search_object.currentLat != 'undefined' && search_object.currentLat != '') && (typeof search_object.currentLng != 'undefined' && search_object.currentLng != '')) {
                          var start = search_object.currentLat + ',' + search_object.currentLng;
                          var end = adddata_a.pickup_lat_long.coordinates[0][1] + ',' + adddata_a.pickup_lat_long.coordinates[0][0];
                          getdistance(start, end, function (responsess) {
                            if (responsess.status === 1) {
                              adddata_a.dh_distance = parseInt(responsess.data.distanceValue);
                              done2(null, adddata_a)
                            } else {
                              adddata_a.dh_distance = 0;
                              done2(null, adddata_a)
                            }
                          });
                        } else {
                          adddata_a.dh_distance = 0;
                          done2(null, adddata_a)
                        }
                      } else {
                        adddata_a.distance = 0;
                        adddata_a.load_distance = 0;
                        adddata_a.rpm = 0;
                        done2(null, adddata_a)
                      }
                    });

                  } else {
                    adddata_a.distance = parseInt(adddata_a.distance);
                    var ratepm = (adddata_a.target_rate / adddata_a.distance).toFixed(2);
                    adddata_a.load_distance = adddata_a.distance;
                    adddata_a.rpm = ratepm;
                    if ((typeof search_object.currentLat != 'undefined' && search_object.currentLat != '') && (typeof search_object.currentLng != 'undefined' && search_object.currentLng != '')) {
                      var start = search_object.currentLat + ',' + search_object.currentLng;
                      var end = adddata_a.pickup_lat_long.coordinates[0][1] + ',' + adddata_a.pickup_lat_long.coordinates[0][0];
                      getdistance(start, end, function (responsess) {
                        if (responsess.status === 1) {
                          adddata_a.dh_distance = parseInt(responsess.data.distanceValue);
                          done2(null, adddata_a)
                        } else {
                          adddata_a.dh_distance = 0;
                          done2(null, adddata_a)
                        }
                      });
                    } else {
                      adddata_a.dh_distance = 0;
                      done2(null, adddata_a)
                    }
                  }


                  // adddata_a.distance = parseInt(adddata_a.distance);
                  // var ratepm = (adddata_a.target_rate / adddata_a.distance).toFixed(2);
                  // adddata_a.load_distance = adddata_a.distance;
                  // adddata_a.rpm = ratepm;
                  // if ((typeof search_object.currentLat != 'undefined' && search_object.currentLat != '') && (typeof search_object.currentLng != 'undefined' && search_object.currentLng != '')) {
                  //   var start = search_object.currentLat + ',' + search_object.currentLng;
                  //   var end = adddata_a.pickup_lat_long.coordinates[0][1] + ',' + adddata_a.pickup_lat_long.coordinates[0][0];
                  //   getdistance(start, end, function (responsess) {
                  //     if (responsess.status === 1) {
                  //       adddata_a.dh_distance = parseInt(responsess.data.distanceValue);
                  //       done2(null, adddata_a)
                  //     } else {
                  //       adddata_a.dh_distance = 0;
                  //       done2(null, adddata_a)
                  //     }
                  //   });
                  // } else {
                  //   adddata_a.dh_distance = 0;
                  //   done2(null, adddata_a)
                  // }

                },
                  function (err, adddata_a) {                    
                    if (err) {
                      doneq(err);
                    } else {
                      doneq(null, adddata_a);
                    }
                  }
                );
              }
            ], function (err, results) {

              result["count"] = load.length > 0 ? load[0].count : 0;
              result["data"] = load.length > 0 ? results : [];
              delete searchList.users;
              result["searchObject"] = load;
              // console.log("Result:_____", JSON.stringify(result));
              socket.emit('loads_response', result);
              //res.send(result);
              //send searchObject back
              callback(searchList);

            });

          } else {

            result["count"] = load.length > 0 ? load[0].count : 0;
            result["data"] = load.length > 0 ? load[0].data : [];
            delete searchList.users;
            result["searchObject"] = load;
            // console.log("Result:_____", JSON.stringify(result));
            socket.emit('loads_response', result);
            //res.send(result);
            //send searchObject back
            callback(searchList);
          }

        }
      });
  } catch (err) {
    common.log("error occured in database:", err);
    //res.send({message : err});
    callback()
  }

};


// To find Load
///find loads with a subset of destination states
//this is used on dispatcher page in frontend
//dont update search list in this request
module.exports.findLoadsLimitedList = async function (search_object, socket, callback) {
  try {

    var limitResults = 20;
    var skipResults = 0;


    //convert search parameters to mongodb search criteria
    let search_options = await common.generateSearchObject(search_object.searchParams);


    if (search_object.limit != null && search_object.limit != '') {
      limitResults = search_object.limit;
    }

    if (search_object.skip != null && search_object.skip != '') {
      skipResults = (search_object.skip - 1) * limitResults;
    }

    //var mongoQuery = Load.find(search_options);
    let totalcount = await Load.find(search_options)
      .collation({ locale: 'en', strength: 2 })
      .count()
      .exec();
    let loads = await Load.find(search_options)
      .collation({ locale: 'en', strength: 2 })
      .populate('bids.user')
      .populate('accepted_bid_user')
      .populate('created_by')
      .sort({ updatedAt: -1 })
      .limit(limitResults)
      .skip(skipResults)
      .exec();

    let result = {};
    result["count"] = totalcount;
    result["data"] = loads;

    result["searchObject"] = search_object;
    socket.emit('dispatcherloadslimited_response', result);

    callback();
  } catch (err) {
    common.log("error occured:", err);

    callback()
  }

};

module.exports.removeSearchItem = async function (req, res) {
  'use strict';
  var search_object = req.body;
  common.log(search_object);

  try {
    await SearchObject.updateOne({ _id: search_object._id, users: req.payload._id }, { $pull: { users: req.payload._id } });
    await SmsAlert.deleteMany({ searchObjectId: ObjectId(search_object._id), user: req.payload._id});
    return res.send({ message: "success" });
  }
  catch (err) {
    common.log(err);
    return res.send({ message: err });
  }
}

module.exports.removeSearchItemws = async function (search_object, socket, callback) {
  'use strict';

  common.log("delete search call from dispatcher:___",search_object);
  await store_sockets_logs(await SearchObject.findById(search_object._id),"Delete Search",socket.decoded._id);
  try {

    const decoded = socket.decoded;
    common.log(decoded);
    
    await SearchObject.updateOne({ _id: search_object._id, users: decoded._id }, { $pull: { users: decoded._id } });
    await SmsAlert.deleteMany({ searchObjectId: ObjectId(search_object._id), user: decoded._id});
    socket.emit('deleteSearch_response', { message: "success" });
    callback();
  }
  catch (err) {
    common.log(err);
    socket.emit('deleteSearch_response', { message: "error" });
    callback()
  }
}

module.exports.getSearchList = async function (req, res) {
  'use strict';
  var search_object = req.body;
  common.log(search_object);

  try {
    let list = await SearchObject.find({ users: req.payload._id }, { searchParams: 1, created_at: 1 });
    let result = {};
    if (list != null) {
      let updatedList = []
      let promises = [];
      for (let i = 0; i < list.length; i++) {
        let searchObject = list[i];
        let searchItem = searchObject.toObject();
        if (searchItem == null || searchItem.searchParams == null) { continue; }
        let searchOptions = await common.generateSearchObject(searchItem.searchParams);
        // let totalcount = await Load.find(searchOptions)
        //   .collation({ locale: 'en', strength: 2 })
        //   .count();

        let count_detail = await Load.aggregate([
          { $match: searchOptions },
          { $unwind: "$delivery_states" },
          {
            $group: {
              _id: {
                id: "$id",
                title: "$delivery_states",

              },
              total: {
                $sum: 1
              }
            }
          }, {
            $group: {
              _id: "$_id.id",
              delivery_states: {
                $push: {
                  state: "$_id.title",
                  total: "$total"
                }
              },
              total: {
                $sum: '$total'
              }
            }
          }, {
            $project: {
              "total_count": "$total",
              "sub_count": "$delivery_states"
            }
          }
        ]);
        // promises.push(count_detail);
        //searchItem.totalcount = totalcount;
        searchItem.recordCount = count_detail.length > 0 ? count_detail[0].total_count : 0;
        searchItem.count_detail = count_detail.length > 0 ? count_detail[0].sub_count : [];
        updatedList.push(searchItem);
        searchOptions = null;
        //totalcount=null;
        count_detail = null;
      }

      //const results = await Promise.all(promises);

      result["data"] = updatedList;
      //result["results"] = results;
    }
    else {
      result["data"] = [];
    }
    res.send(result);

  }
  catch (err) {
    common.log(err);
    res.send({ message: err });
  }
}


module.exports.getSearchListV2 = async function (req, res) {
  'use strict';
  let query = [{
    $match: {
      "users": {
        "$in": [ObjectId(req.payload._id)]
      }
    }
  }, {
    $lookup: {
      "from": "smsalerts",
      "as": "smsalerts",
      let: { keywordId: "$_id" },
      "pipeline": [{
        "$match": {
          $expr: { $eq: ['$$keywordId', '$searchObjectId'] },
          "user": ObjectId(req.payload._id)
        }
      }]
    }
  }, {
    $unwind: {
      "path": "$smsalerts",
      "preserveNullAndEmptyArrays": true
    }
  }, {
    $match: {
      $or: [
        { smsalerts: null },
        { "smsalerts.user": ObjectId(req.payload._id) }
      ]
    }
  }, {
    $project: {
      "_id": 1,
      "searchParams": 1,
      "smsalert": {
        $switch: {
          "branches": [
            { "case": { "$eq": ["$smsalerts.alert", true] }, "then": true }
          ],
          "default": false
        }
      },
      "emailalert": {
        $switch: {
          "branches": [
            { "case": { "$eq": ["$smsalerts.emailAlert", true] }, "then": true }
          ],
          "default": false
        }
      }
    }
  }]

  try {
    let list = await SearchObject.aggregate(query);
    res.send(list);
  }
  catch (err) {
    common.log(err);
    res.send({ message: err });
  }
}

module.exports.getSearchListV2_count = async function (req, res) {
  'use strict';
  var search_object = req.body;
  common.log(search_object);

  try {
    let searchOptions = await common.generateSearchObject(search_object);
    common.log(JSON.stringify(searchOptions));
    //let count_detail = await Load.find(searchOptions).explain();
    let count_detail = await Load.aggregate([
      { $match: searchOptions },
      { $unwind: "$delivery_states" },
      {
        $group: {
          _id: {
            id: "$id",
            title: "$delivery_states"
          },
          total: {
            $sum: 1
          }
        }
      }, {
        $group: {
          _id: "$_id.id",
          delivery_states: {
            $push: {
              state: "$_id.title",
              total: "$total"
            }
          },
          total: {
            $sum: '$total'
          }
        }
      }, {
        $project: {
          "total_count": "$total",
          "sub_count": "$delivery_states"
        }
      }
    ]);
    let result = []
    if (count_detail.length > 0) {
      result = count_detail;
    } else {
      result.push({
        total_count: 0,
        sub_count: []
      })
    }

    res.send(result);
  }
  catch (err) {
    common.log(err);
    res.send({ message: err });
  }
}


module.exports.getSearchListws = async function (search_object, socket, callback) {
  'use strict';

  common.log(search_object);

  try {

    const decoded = socket.decoded;
    common.log(decoded);
    const list = await SearchObject.find({ users: decoded._id }, { searchParams: 1, created_at: 1 });

    // const searchCache = new JSONCache(redis, {prefix: 'user'});
    // let list = await searchCache.get(decoded._id);
    let result = {};
    if (list != null) {
      let updatedList = []
      for (let i = 0; i < list.length; i++) {
        let searchObject = list[i];
        if (searchObject == null || searchObject.searchParams == null) { continue; }
        let searchOptions = await common.generateSearchObject(searchObject.searchParams);
        // let totalcount = await Load.find(searchOptions)
        //   .collation({ locale: 'en', strength: 2 })
        //   .count()
        //   .exec();

        let count_detail = await Load.aggregate([
          { $match: searchOptions },
          { $unwind: "$delivery_states" },
          {
            $group: {
              _id: {
                id: "$id",
                title: "$delivery_states"
              },
              total: {
                $sum: 1
              }
            }
          }, {
            $group: {
              _id: "$_id.id",
              delivery_states: {
                $push: {
                  state: "$_id.title",
                  total: "$total"
                }
              },
              total: {
                $sum: '$total'
              }
            }
          }, {
            $project: {
              "total_count": "$total",
              "sub_count": "$delivery_states"
            }
          }
        ]).collation({ locale: 'en', strength: 2 }).exec();
        let searchItem = searchObject.toObject();
        searchItem.recordCount = count_detail.length > 0 ? count_detail[0].total_count : 0;
        searchItem.count_detail = count_detail.length > 0 ? count_detail[0].sub_count : [];
        updatedList.push(searchItem);
        searchOptions = null;
        count_detail = null;
        //totalcount = null;
      }
      result["data"] = updatedList;
    }
    else {
      result["data"] = [];
    }
    socket.emit('searchList_response', result);
    callback(result["data"]);


  }
  catch (err) {
    common.log(err);
    socket.emit('searchList_response', "error occured");
    callback();
  }
}

module.exports.realTimeFindLoads = async function (req, res) {
  'use strict';
  var search_object = req.body;
  common.log(search_object);

  try {

    var limitResults = 20;
    var skipResults = 0;
    var sort_by = req.body.sort_by || 'updatedAt';
    var sort_direction = req.body.sort_direction == 'asc' ? 1 : -1;
    var sortObj = {};
    sortObj[sort_by] = sort_direction;
    if (search_object.limit != null && search_object.limit != '') {
      limitResults = search_object.limit;
    }
    if (search_object.skip != null && search_object.skip != '') {
      skipResults = (search_object.skip - 1) * limitResults;
    }
    let search_options = await common.generateSearchObject(search_object);
    let date = new Date();
    date.setHours(date.getHours() - 1);
    search_options["$and"].push({ updatedAt: { $lte: date } });
    common.log(JSON.stringify(search_options));
    const [count, data] = await Promise.all([
      Load.count(search_options),
      Load.find(search_options,{
        _id:1,
        ready_date:{ $cond:
          {
            if : { $eq : [{$type:'$ready_date'},'date']},
            then: { $dateToString: { format: "%m/%d/%Y", date: "$ready_date" } },
            else: {
              $cond :{
                if : {$eq:['$ready_date',null]},
                then : '',
                else :"$ready_date"
                // {
                //   "$dateFromString": {
                //     "dateString": "$ready_date",
                //     "format": "%m/%d/%Y"
                //   }
                // }
              }
            }
          }
        },
        delivery_addresses: 1,
        delivery_cities: 1,
        delivery_date: 1,
        delivery_lat_long: 1,
        delivery_states: 1,
        delivery_zips: 1,
        description: 1,
        equipment_type: 1,
        load_weight: 1,
        load_length: 1,
        no_of_count: 1,
        pickup_addresses: 1,
        pickup_date: 1,
        pickup_cities: 1,
        pickup_lat_long: 1,
        pickup_states: 1,
        pickup_zips: 1,
        target_rate: 1,
        updatedAt:1,
      }).sort(sortObj).skip(skipResults).limit(limitResults).lean()
    ]);
    return res.send({ count, data });
  } catch (err) {
    common.log("error occured in database:", err);
    res.send({ message: err });
  }
};

module.exports.findLoadshttp = async function (req, res) {
  'use strict';
  var search_object = req.body;

  try {

    var limitResults = 20;
    var skipResults = 0;
    // var sort_by = req.body.sort_by || 'updatedAt';
    // var sort_direction = req.body.sort_direction == 'asc' ? 1 : -1;
    var sort_by = req.body.load_weight ? 'load_weight' : req.body.sort_by || 'updatedAt';
    var sort_direction = req.body.load_weight ? -1 : req.body.sort_direction == 'asc' ? 1 : -1;
    var sortObj = {};
    // sortObj[sort_by] = sort_direction;
    //sortObj['updatedAt'] = -1;

    if (req.body.sort_by == 'equipment_type') {
      sortObj['equipment_type'] = (req.body.sort_by == 'equipment_type') ? req.body.sort_direction == 'asc' ? 1 : -1 : -1;
    }

    if (req.body.sort_by == 'pickup_addresses') {
      sortObj['pickup_addresses'] = (req.body.sort_by == 'pickup_addresses') ? req.body.sort_direction == 'asc' ? 1 : -1 : -1;
    }
    if (req.body.sort_by == 'delivery_addresses') {
      sortObj['delivery_addresses'] = (req.body.sort_by == 'delivery_addresses') ? req.body.sort_direction == 'asc' ? 1 : -1 : -1;
    }

    if (req.body.sort_by == 'equipment_type') {
      sortObj['equipment_type'] = (req.body.sort_by == 'equipment_type') ? req.body.sort_direction == 'asc' ? 1 : -1 : -1;
    }

    if (req.body.sort_by == 'load_weight') {
      sortObj['load_weight'] = (req.body.sort_by == 'load_weight') ? req.body.sort_direction == 'asc' ? 1 : -1 : -1;
    }

    if (req.body.sort_by == 'load_length') {
      sortObj['load_length'] = (req.body.sort_by == 'load_length') ? req.body.sort_direction == 'asc' ? 1 : -1 : -1;
    }

    if (req.body.sort_by == 'ready_date') {
      sortObj['ready_date'] = (req.body.sort_by == 'ready_date') ? req.body.sort_direction == 'asc' ? 1 : -1 : -1;
    }

    if (req.body.sort_by == 'updatedAt') {
      sortObj['updatedAt'] = (req.body.sort_by == 'updatedAt') ? req.body.sort_direction == 'asc' ? 1 : -1 : -1;
    }

    if (req.body.sort_by == 'deadHead') {
      sortObj['distcalculated'] = (req.body.sort_by == 'deadHead') ? req.body.sort_direction == 'asc' ? 1 : -1 : -1;
    }

    if (req.body.sort_by == 'rpm') {
      sortObj['rpm'] = (req.body.sort_by == 'rpm') ? req.body.sort_direction == 'asc' ? 1 : -1 : -1;
    }

    if (req.body.sort_by == 'distance') {
      sortObj['distance'] = (req.body.sort_by == 'distance') ? req.body.sort_direction == 'asc' ? 1 : -1 : -1;
    }

    if (req.body.sort_by == 'target_rate') {
      sortObj['target_rate'] = (req.body.sort_by == 'target_rate') ? req.body.sort_direction == 'asc' ? 1 : -1 : -1;
    }

    if (!req.body.sort_by) {
      sortObj['updatedAt'] = -1;
    }

    let search_options = await common.generateSearchObject(search_object);
    
    if (search_object.limit != null && search_object.limit != '') {
      limitResults = search_object.limit;
    }

    if (search_object.skip != null && search_object.skip != '') {
      skipResults = (search_object.skip - 1) * limitResults;
    }

    common.log(JSON.stringify(search_options));
    
      var query = Load.aggregate([
        // {
        //   $geoNear: {
        //     near: {
        //       type: "Point",
        //       coordinates: [Number(search_object.pickup_long), Number(search_object.pickup_lat)]
        //     },
        //     distanceField: "distcalculated",
        //     maxDistance: distanceRediuan,
        //     distanceMultiplier: 0.001,
        //     key: 'pickup_lat_long',
        //     spherical: true
        //   }
        // },
        {
          "$match": search_options
        },
        {
          $project: {
            _id: 1,
            ready_date: 1,
            rpm:1,
            delivery_addresses: 1,
            delivery_cities: 1,
            delivery_date: 1,
            delivery_lat_long: 1,
            delivery_states: 1,
            delivery_zips: 1,
            description: 1,
            equipment_type: 1,
            load_weight: 1,
            load_length: 1,
            no_of_count: 1,
            pickup_addresses: 1,
            pickup_date: 1,
            pickup_cities: 1,
            pickup_lat_long: 1,
            route_distance: 1,
            pickup_states: 1,
            pickup_zips: 1,
            distance: 1,
            distcalculated: 1,
            target_rate: 1,
            updatedAt: 1,
          }
        },
        {
          $sort: sortObj
        },
        {
          $facet: {
            count: [{ "$group": { _id: null, count: { $sum: 1 } } }],
            data: [
              {
                $project: {
                  _id: 1,
                  ready_date: 1,
                  rpm: 1,
                  delivery_addresses: 1,
                  delivery_cities: 1,
                  delivery_date: 1,
                  delivery_lat_long: 1,
                  delivery_states: 1,
                  delivery_zips: 1,
                  description: 1,
                  equipment_type: 1,
                  load_weight: 1,
                  load_length: 1,
                  no_of_count: 1,
                  pickup_addresses: 1,
                  pickup_date: 1,
                  pickup_cities: 1,
                  pickup_lat_long: 1,
                  route_distance: 1,
                  pickup_states: 1,
                  pickup_zips: 1,
                  distance: 1,
                  distcalculated: 1,
                  target_rate: 1,
                  updatedAt: 1,
                }
              },
              {
                $skip: skipResults
              },
              {
                $limit: limitResults
              }
            ]
          }
        },
      ]).allowDiskUse(true)
    

    query.exec(function (err, result) {
      if (err) {
        console.log(err)
        return res.send({ err });
      } else {
        var count = (result[0] && result[0].count && result[0].count[0] && result[0].count[0].count) ? result[0].count[0].count : 0;
        // return res.send({
        //   count: count,
        //   data: result[0].data
        // });


        async.waterfall([
          function (doneq) {
            async.map(result[0].data, function (adddata_a, done2) {

              if (parseInt(adddata_a.distance) == 0) {

                var start_load = adddata_a.pickup_lat_long.coordinates[0][1] + ',' + adddata_a.pickup_lat_long.coordinates[0][0];
                var end_load = adddata_a.delivery_lat_long.coordinates[0][1] + ',' + adddata_a.delivery_lat_long.coordinates[0][0];
                getdistance(start_load, end_load, function (responsess) {
                  if (responsess.status === 1) {
                    adddata_a.distance = parseInt(responsess.data.distanceValue);
                    var ratepm = (adddata_a.target_rate / adddata_a.distance).toFixed(2);
                    adddata_a.load_distance = adddata_a.distance;
                    adddata_a.rpm = ratepm;

                    if ((typeof search_object.currentLat != 'undefined' && search_object.currentLat != '') && (typeof search_object.currentLng != 'undefined' && search_object.currentLng != '')) {
                      var start = search_object.currentLat + ',' + search_object.currentLng;
                      var end = adddata_a.pickup_lat_long.coordinates[0][1] + ',' + adddata_a.pickup_lat_long.coordinates[0][0];
                      getdistance(start, end, function (responsess) {
                        if (responsess.status === 1) {
                          adddata_a.dh_distance = parseInt(responsess.data.distanceValue);
                          done2(null, adddata_a)
                        } else {
                          adddata_a.dh_distance = 0;
                          done2(null, adddata_a)
                        }
                      });
                    } else {
                      adddata_a.dh_distance = 0;
                      done2(null, adddata_a)
                    }


                  } else {
                    adddata_a.distance = 0;
                    adddata_a.load_distance = 0;
                    adddata_a.rpm = 0;
                    done2(null, adddata_a)
                  }
                });

              } else {
                adddata_a.distance = parseInt(adddata_a.distance);
                var ratepm = (adddata_a.target_rate / adddata_a.distance).toFixed(2);
                adddata_a.load_distance = adddata_a.distance;
                adddata_a.rpm = ratepm;
                if ((typeof search_object.currentLat != 'undefined' && search_object.currentLat != '') && (typeof search_object.currentLng != 'undefined' && search_object.currentLng != '')) {
                  var start = search_object.currentLat + ',' + search_object.currentLng;
                  var end = adddata_a.pickup_lat_long.coordinates[0][1] + ',' + adddata_a.pickup_lat_long.coordinates[0][0];
                  getdistance(start, end, function (responsess) {
                    if (responsess.status === 1) {
                      adddata_a.dh_distance = parseInt(responsess.data.distanceValue);
                      done2(null, adddata_a)
                    } else {
                      adddata_a.dh_distance = 0;
                      done2(null, adddata_a)
                    }
                  });
                } else {
                  adddata_a.dh_distance = 0;
                  done2(null, adddata_a)
                }
              }

             
            },
              function (err, adddata_a) {
                if (err) {
                  doneq(err);
                } else {
                  // Array.prototype.sortOn = function (key) {
                  //   this.sort(function (a, b) {
                  //     if (a[key] < b[key]) {
                  //       return -1;
                  //     } else if (a[key] > b[key]) {
                  //       return 1;
                  //     }
                  //     return 0;
                  //   });
                  // }
                  // adddata_a.sortOn("distance");
                  doneq(null, adddata_a);
                }
              }
            );
          }
        ], function (err, result) {
          return res.send({
            count: count,
            data: result
          });
        });
      }
    });


    // if (companies && companies[0] && companies[0].count && companies[0].count[0] && companies[0].count[0].count) {
    //   returnObj.count = companies[0].count[0].count;
    // }
    // if (companies && companies[0].data) {
    //   returnObj.data = companies[0].data;
    // }

    // Load.count(pickupadd, search_options),
    // Load.find(pickupadd, search_options,
    //   {
    //     _id:1,
    //     ready_date:
    //     { $cond:
    //       {
    //         if : { $eq : [{$type:'$ready_date'},'date']},
    //         then: { $dateToString: { format: "%m/%d/%Y", date: "$ready_date" } },
    //         else: {
    //           $cond :{
    //             if : {$eq:['$ready_date',null]},
    //             then : '',
    //             else :"$ready_date"
    //             // {
    //             //   "$dateFromString": {
    //             //     "dateString": "$ready_date",
    //             //     "format": "%m/%d/%Y"
    //             //   }
    //             // }
    //           }
    //         }
    //       }
    //     }, //{ $dateToString: { format: "%m/%d/%Y", date: "$ready_date" } },
    //     delivery_addresses: 1,
    //     delivery_cities: 1,
    //     delivery_date: 1,
    //     delivery_lat_long: 1,
    //     delivery_states: 1,
    //     delivery_zips: 1,
    //     description: 1,
    //     equipment_type: { $cond: { if: { "equipment_type" : { $type : 2 } }, then:   { $split: ["$equipment_type", ","] }  , else: "$equipment_type" } },
    //     load_weight: 1,
    //     load_length: 1,
    //     no_of_count: 1,
    //     pickup_addresses: 1,
    //     pickup_date: 1,
    //     pickup_cities: 1,
    //     pickup_lat_long: 1,
    //     route_distance: 1,
    //     pickup_states: 1,
    //     pickup_zips: 1,
    //     distance:1,
    //     distcalculated:1,
    //     target_rate: 1,
    //     updatedAt:1,
    //   }).sort(sortObj).skip(skipResults).limit(limitResults).allowDiskUse(true).lean()






  } catch (err) {
    common.log("error occured in database:", err);
    res.send({ message: err });
  }
};

if (typeof (Number.prototype.toRad) === "undefined") {
  Number.prototype.toRad = function () {
    return this * Math.PI / 180;
  }
}

function getdistance(start, end, callback) {

  var startsplit = start.split(',');
  var endsplit = end.split(',');

  var origin = parseFloat(startsplit[0]) + ',' + parseFloat(startsplit[1]);
  var destination = parseFloat(endsplit[0]) + ',' + parseFloat(endsplit[1]);

  // var origin = origin_add;
  // var destination = destination_add;
  if (origin && destination) {
    let url = 'http://www.mapquestapi.com/directions/v2/route?key=' + config.maprequest_key + '&from=' + origin + '&to=' + destination;
    let options = {
      method: 'get',
      json: true,
      url: url
    }
    request(options, function (err, rest, body) {      
      if (err) {
        var data = {
          distanceValue: 0
        };
        callback({ 'status': 0, 'data': data });
      } else {        
        if (body && body.route && body.route.distance) {
          var data = {
            distanceValue: body.route.distance
          };
          callback({ 'status': 1, 'data': data });
        } else {
          var data = {
            distanceValue: 0
          };
          callback({ 'status': 0, 'data': data });
        }
      }
    })

    // var dist = geodist(startsplit, endsplit, { exact: true, unit: 'km' })

    // var decimals = 2;
    //  var earthRadius = 6371; // km
    //  lat1 = parseFloat(startsplit[0]);
    //  lat2 = parseFloat(endsplit[0]);
    //  lon1 = parseFloat(startsplit[1]);
    //  lon2 = parseFloat(endsplit[1]);

    //  var dLat = (lat2 - lat1).toRad();
    //  var dLon = (lon2 - lon1).toRad();
    //  var lat1 = lat1.toRad();
    //  var lat2 = lat2.toRad();

    //  var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    //  Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
    //  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    //  var d = earthRadius * c;
    //  var dist = Math.round(d * Math.pow(10, decimals)) / Math.pow(10, decimals);

    // var numberdata = Math.round(dist * 100) / 100;
    // var data = {
    //   distance: numberdata,
    //   distanceValue: numberdata
    // };
    // callback({ 'status': 1, 'data': data });
  } else {
    var data = {
      distanceValue: 0
    };
    callback({ 'status': 0, 'data': data });
  }



}

module.exports.findLoadDetail = async function (req, res) {
  'use strict';
  var load_detail
  if (!req.params.id) {
    return res.status(400).send({ message: "load id is required" });
  }

  try {
    load_detail = await Load.findById(req.params.id)
      .collation({ locale: 'en', strength: 2 })
      .populate('bids.user')
      .populate('accepted_bid_user')
      .populate('created_by')
      .exec();


    


      //for transcredit
    let CreditScore=0;
    let Days_To_Pay=0;
    let ID=null;

      let _fullreport;
      //let transcredit= await findCompanyTransCredit(load_detail._doc.created_by.company_id);
      if(load_detail._doc.created_by.company_id){
        let _company= await Company.findById(load_detail._doc.created_by.company_id);
        console.log("company:__", _company._doc)
        _fullreport= await processTranscreditReport(_company._doc.company_name.replace(/&/g, "&amp;"))
        common.log(_fullreport)
        //days to pay
        if(_fullreport?.getCompleteCreditV2Response[0]?.getCompleteCreditV2Result[0]?.Days_To_Pay)
        Days_To_Pay= _fullreport?.getCompleteCreditV2Response[0]?.getCompleteCreditV2Result[0]?.Days_To_Pay[0]

        //credit score
        if(_fullreport?.getCompleteCreditV2Response[0]?.getCompleteCreditV2Result[0]?.CreditScore)
        CreditScore= _fullreport?.getCompleteCreditV2Response[0]?.getCompleteCreditV2Result[0]?.CreditScore[0]
        //Mc number
        if(_fullreport?.getCompleteCreditV2Response[0]?.getCompleteCreditV2Result[0]?.ID)
        ID= _fullreport?.getCompleteCreditV2Response[0]?.getCompleteCreditV2Result[0]?.ID[0]

      }else if(load_detail._doc.created_by.company){
        _fullreport= await processTranscreditReport(load_detail._doc.created_by.company.replace(/&/g, "&amp;"));
        common.log(_fullreport);
        //days to pay
        if(_fullreport?.getCompleteCreditV2Response[0]?.getCompleteCreditV2Result[0]?.Days_To_Pay)
        Days_To_Pay= _fullreport?.getCompleteCreditV2Response[0]?.getCompleteCreditV2Result[0]?.Days_To_Pay[0]

        //credit score
        if(_fullreport?.getCompleteCreditV2Response[0]?.getCompleteCreditV2Result[0]?.CreditScore)
        CreditScore= _fullreport?.getCompleteCreditV2Response[0]?.getCompleteCreditV2Result[0]?.CreditScore[0]
        //Mc number
        if(_fullreport?.getCompleteCreditV2Response[0]?.getCompleteCreditV2Result[0]?.ID)
        ID= _fullreport?.getCompleteCreditV2Response[0]?.getCompleteCreditV2Result[0]?.ID[0]
      }

    let LoadWatched = await LoadWatchedUsers.findOne({ userId: ObjectId(req.payload._id), loads: ObjectId(req.params.id) }).lean();
    

    if (parseInt(load_detail.distance) == 0) {
      var start_load = load_detail.pickup_lat_long.coordinates[0][1] + ',' + load_detail.pickup_lat_long.coordinates[0][0];
      var end_load = load_detail.delivery_lat_long.coordinates[0][1] + ',' + load_detail.delivery_lat_long.coordinates[0][0];
      
      getdistance(start_load, end_load, function (responsess) {
        
        if (responsess.status === 1) {
          load_detail.distance = parseInt(responsess.data.distanceValue);
          if (LoadWatched) {
            let loadDetails = { ...load_detail._doc }
            loadDetails.addToWatchlist = true;
            // if(transcredit.length>0){
            loadDetails.transCredit = {
              CreditScore: CreditScore,
              Days_To_Pay: Days_To_Pay,
              MC_Number: ID
            }
            // }
            return res.status(200).json(loadDetails);

          } else {
            let loadDetails = { ...load_detail._doc }
            loadDetails.addToWatchlist = false;
            loadDetails.transCredit = {
              CreditScore: CreditScore,
              Days_To_Pay: Days_To_Pay,
              MC_Number: ID
            }
            return res.status(200).json(loadDetails);
          }

        }else{
          if (LoadWatched) {
            let loadDetails = { ...load_detail._doc }
            loadDetails.addToWatchlist = true;
            // if(transcredit.length>0){
            loadDetails.transCredit = {
              CreditScore: CreditScore,
              Days_To_Pay: Days_To_Pay,
              MC_Number: ID
            }
            // }
            return res.status(200).json(loadDetails);

          } else {
            let loadDetails = { ...load_detail._doc }
            loadDetails.addToWatchlist = false;
            loadDetails.transCredit = {
              CreditScore: CreditScore,
              Days_To_Pay: Days_To_Pay,
              MC_Number: ID
            }
            return res.status(200).json(loadDetails);
          }

        }
      })
    }else{
      if (LoadWatched) {
        let loadDetails = { ...load_detail._doc }
        loadDetails.addToWatchlist = true;
        // if(transcredit.length>0){
        loadDetails.transCredit = {
          CreditScore: CreditScore,
          Days_To_Pay: Days_To_Pay,
          MC_Number: ID
        }
        // }
        return res.status(200).json(loadDetails);

      } else {
        let loadDetails = { ...load_detail._doc }
        loadDetails.addToWatchlist = false;
        loadDetails.transCredit = {
          CreditScore: CreditScore,
          Days_To_Pay: Days_To_Pay,
          MC_Number: ID
        }
        return res.status(200).json(loadDetails);
      }
    }

    

  } catch (err) {
    common.log("error occured in database:", err);
    res.send({ message: err });
  }
};


module.exports.postLoad = async (req, res) => {
  common.log("inside postload - - >");
  'use strict';
  let data = req.body;
  //common.log('socket---->', socket)
  if (!req.payload._id) {
    res.status(401).json({ success: false, message: "UnauthorizedError: Private action" });

  } else {


    let load = await loadPost(data)
    setTimeout(() => {
      User.findOne({ _id: req.payload._id }, function (err, user) {
        load.created_by = user;
          load.save(function (err, loadData) {

            if (err) {

              res.status(400).json({ success: false, message: err.message });
            } else {
              res.status(200).json({ success: true, data: loadData });
              //io.emit('new_loads_response', { type: 'new', data: loadData });

              //queue it for history collection and broadcast to connected clients
              const authHeader = req.headers.authorization;
              const token = authHeader.split(' ')[1];
              newLoadQueue.add({ id: load._id, token: token }).then(function (res) {
                common.log("queue redis added success", res);
              }).catch(function (err) {
                common.log("Error while add queue redis", err);
              });

              // ctrlDATLoads.postLoad(data,socket, function(res){
              //     if(res.postAssetResults && res.postAssetResults.postAssetSuccessData){
              //       Load.update({_id: load._id}, { $set :{ datAssetId : res.postAssetResults.postAssetSuccessData.assetId}}, {upsert: true}, function(err) {
              //         if (err) {
              //           common.log(err);
              //         }
              //       });
              //     }

              // });
              // common.updateLoads(load, socket);
            }
          });
      });
    }, 1000);

  }

};


async function loadPost(data){
  var load = new Load();
  let validateAdresses
  if (data.pickup_addresses && data.pickup_addresses != '') {
    var pickupAddressArray = data.pickup_addresses[0];

   validateAdresses=await validateAddress.validateAddress(pickupAddressArray)

    //var geocoordinates = await common.getCoordinatesAsync(search_object.pickup_address);
    if ( validateAdresses.city != null &&  validateAdresses.state!=null) {
      load.pickup_cities.push(validateAdresses.city);
      load.pickup_states.push(validateAdresses.state_code);
    }

  }
  if (data.delivery_addresses && data.delivery_addresses != '') {
    var deliveryAddressArray = data.delivery_addresses[0];
    validateAdresses=await validateAddress.validateAddress(deliveryAddressArray)

    //var geocoordinates = await common.getCoordinatesAsync(search_object.pickup_address);
    if ( validateAdresses.city != null &&  validateAdresses.state!=0) {
      load.delivery_cities.push( validateAdresses.city);
      load.delivery_states.push( validateAdresses.state_code);
    }

  }
  load.is_opened = true
  load.pickup_addresses = data.pickup_addresses;
  load.pickup_zips = data.pickup_zips;
  load.pickup_lat_long = { type: "MultiPoint", coordinates: [data.pickup_lat_long] };
  load.delivery_addresses = data.delivery_addresses;

  load.delivery_zips = data.delivery_zips;
  // load.delivery_lat_long = data.delivery_lat_long;
  load.delivery_lat_long = { type: "MultiPoint", coordinates: [data.delivery_lat_long] };
  load.equipment_type = data.equipment_type.toString();
  load.no_of_count = data.no_of_count;
  load.target_rate = data.target_rate;

  load.load_weight = data.load_weight;
  load.load_length = data.load_length;
  //load.unit_of_measurement = data.unit_of_measurement;

  load.ready_date = data.ready_date;
  load.pickup_date = data.pickup_date;
  load.delivery_date = data.delivery_date;

  load.description = data.description;
  load.post_to = data.post_to;

  var origin = data.pickup_addresses;
  var destination = data.delivery_addresses;

  let url = 'http://www.mapquestapi.com/directions/v2/route?key='+config.maprequest_key+'&from=' + origin + '&to=' + destination;
  let options = {
    method: 'get',
    json: true,
    url: url
  }
  request(options, function (err, rest, body) {
    if (err) {
      console.error('error posting json: ', err)
    } else {
      if (body && body.route && body.route.distance) {
        load.distance = body.route.distance;
        load.route_distance = body.route.distance;
        load.rpm = (data.target_rate / load.distance).toFixed(2);
      }else{
        load.distance = 0;
        load.route_distance = 0;
        load.rpm = 0;
      }
    }
  })

  // var origins = [data.pickup_addresses, data.pickup_lat_long];
  // var destinations = [data.delivery_addresses, data.delivery_lat_long];
  // distance.key('AIzaSyC6WZGzkjs5t3wi64t-1fIkDdmRjm0wdjs');
  // distance.matrix(origins, destinations, function (err, distances) {
  //   if (distances.status == 'OK') {
  //     if (distances.rows[0].elements[0].status == 'OK') {
  //       var distance = distances.rows[0].elements[0].distance.value;
  //       var miles = (distance / 1609).toFixed(2);

  //       load.route_distance = miles;
  //     } else {

  //     }
  //   } else {

  //   }
  // });

  // load.phone = data.phone;
  //common.log("myimages",data.images[0],data.post_to)
  if (data.images && data.images.length > 0) {
    load.agreement_signed = true;

    if (data.post_to == 0) {

      load.carrier_signed_signature = data.images[0]
    } else if (data.post_to == 1) {
      load.shipper_signed_agreement = data.images[0]
    } else if (data.post_to == 2) {
      load.carrier_signed_signature = data.images[0]
      load.shipper_signed_agreement = data.images[0]
    }

  }


  return load;


}
module.exports.freePostLoad = async (req, res) => {
  common.log("inside postload - - >");
  let {data,email,phone} = req.body;
  let tempUser=await TemporaryUsers.findOne({email:email })
  let user=await User.findOne({email:email})
  if(user){
    return res.status(200).json({code:400,message:'email already signup'})
  }
  let load=loadPost(data)
  var saveUser;
  if(!tempUser){
  let user= new TemporaryUsers({
       email,
       phone
   })
    saveUser = await user.save();
  }
  let user_id=tempUser?tempUser._id:saveUser._id
      load.created_by = user_id;
      load.source='free'
      load.save(function (err, loadData) {
        if (err) {
          res.status(400).json({ success: false, message: err.message });
        } else {
          res.status(200).json({ success: true, data: loadData });

        }
        newLoadQueue.add({ id: load._id }).then(function (res) {
          common.log("queue redis added success", res);
        }).catch(function (err) {
          common.log("Error while add queue redis", err);
        });
});
};

module.exports.uploadCSVLoads = async function (req, res) {
  try {
    if (!req.file) {
      return res.status(400).send("file is required");
    }
    let loadsByCsvUpload = new LoadsByCsvUpload();
    loadsByCsvUpload.s3_key = req.file.key;
    loadsByCsvUpload.file_location = req.file.location;
    loadsByCsvUpload.original_name = req.file.originalname;
    loadsByCsvUpload.mimetype = req.file.mimetype;
    loadsByCsvUpload.user = req.payload._id;
    await loadsByCsvUpload.save();
    await newCSVLoadQueue.add({ id: loadsByCsvUpload._id })
    return res.send({ success: true, message: "uploading request in process" });
  } catch (err) {
    common.log('Error while upload csv loads :--', err)
    return res.status(400).send({ success: false, message: err.message });
  }
}

module.exports.getUploadCSVLoads = async function (req, res) {
  try {
    let sort_direction = req.body.sort_direction == 'asc' ? 1 : -1;
    let sort_by = req.body.sort_by || 'createdAt';
    let sortObj = {};
    sortObj[sort_by] = sort_direction;
    let limit = req.body.limit || 10;
    let skip = ((req.body.skip || 1) - 1) * limit;
    let searchObject = { user: ObjectId(req.payload._id) };
    const [count, data] = await Promise.all([
      LoadsByCsvUpload.count(searchObject),
      LoadsByCsvUpload.find(searchObject).sort(sortObj).skip(skip).limit(limit).lean()
    ])
    return res.send({ success: true, count, data });
  } catch (err) {
    common.log('Error while get upload csv loads :--', err)
    return res.status(400).send({ success: false, message: err.message });
  }
}

module.exports.downloadFile = async function (req, res) {
  if (!req.params.id) {
    return res.status(400).send({ param: 'id', message: 'Load file id is required' });
  }
  try {
    let document = await LoadsByCsvUpload.findById(req.params.id)
    if (document) {
      s3FileDownload(document, req, res);
    } else {
      common.log("Loads csv file not found");
      return res.status(400).send({ message: "Load csv file not found" });
    }
  }
  catch (err) {
    common.log("Error while get loads csv document detail", err);
    return res.status(400).send(err);
  }
}

module.exports.editLoad = function (req, res) {
  'use strict';
  let data = req.body;

  if (!req.payload._id) {
    res.status(401).json({ success: false, message: "UnauthorizedError: Private action" });
  } else {



    if (data.images && data.images.length > 0) {
      data.agreement_signed = true;
    }
    if (data.post_to == 0) {
      //data.carrier_signed_signature = data.images[0]
      data.shipper_signed_agreement = "";
    } else if (data.post_to == 1) {
      //data.shipper_signed_agreement = data.images[0]
      data.carrier_signed_signature = "";
    } else if (data.post_to == 2) {
      //data.carrier_signed_signature = data.images[0]
      //data.shipper_signed_agreement = data.images[0]
    }

    var orignal_load_id = req.params.id;
    delete data._id;
    data.updated_at = Date.now();
    data.pickup_lat_long = { type: "MultiPoint", coordinates: [data.pickup_lat_long] };
    data.delivery_lat_long = { type: "MultiPoint", coordinates: [data.delivery_lat_long] };
    data.equipment_type=data.equipment_type.toString()
    Load.update({ _id: orignal_load_id }, data, function (err) {

      if (err) {
        res.status(400).json({ success: false, message: err.message });
      } else {

        data._id = orignal_load_id;

        Load.findOne({ _id: req.params.id })
          .populate('bids.user')
          .populate('accepted_bid_user')
          .populate('created_by')
          .exec(function (err, load) {
            if (!err) {
              //load.created_at = Date.now();

              res.status(200).json({ message: "Edit load  successfully", data: load })


              // ctrlDATLoads.editLoad(load, data,req, res, function (result) {
              //   common.log(result);

              //   if (result.updateAssetResult && result.updateAssetResult.updateAssetSuccessData) {
              //     Load.update({ _id: load._id }, {
              //       $set: {
              //         datAssetId: result.updatetAssetResults.updateAssetSuccessData.assetId,
              //         postersReferenceId: result.updateAssetResult.updateAssetSuccessData.postersReferenceId
              //       }
              //     }, { upsert: true }, function (err) {
              //       if (err) {
              //         common.log(err);
              //       }
              //     });
              //   }
              // });
            }
          });
      }
    });

  }

};

module.exports.detailLoad = async (req, res) => {
  if (!req.payload._id) {
    res.status(401).json({ success: false, message: "UnauthorizedError: Private action" });
  } else {
    try {
      let load_details = await Load.findOne({ _id: req.params.id })
      res.status(200).json({ message: "Load detail send succesfully", data: load_details })
    } catch (error) {
      res.status(404).json({ error });
    }
  }

};


module.exports.LoadRefreshAge = function (req, res) {

  let data = req.body;

  'use strict';
  if (!req.payload._id) {
    res.status(401).json({ success: false, message: "UnauthorizedError: Private action" });

    return;
  } else {


    var orignal_load_id = req.params.id;
    // delete data._id;
    var refAge = {
      updated_at: Date.now()
    }




    Load.update({ _id: orignal_load_id }, refAge, { upsert: true }, function (err) {
      if (err) {
        res.status(404).json({ success: false, message: err.message });
      } else {
        data.load_id = orignal_load_id;

        Load.findOne({ _id: data.load_id })
          .populate('bids.user')
          .populate('accepted_bid_user')
          .populate('created_by')
          .exec(function (err, load) {
            if (!err) {
              res.status(200).json({ message: "refresh load age successfully", data: load })
            }
          });
      }

    });
  }
};



module.exports.acceptBid = function (load_id, bid_user_id, socket) {

  'use strict';

  if (socket.decoded._id) {
    Load.findOne({ _id: load_id }, function (err, loadItem) {
      if (loadItem.created_by != socket.decoded._id) {
        socket.emit('accept_bid_response', { success: false, message: "UnauthorizedError: Private action" });
      } else {
        if (loadItem.is_opened) {
          Load.findOneAndUpdate({ _id: loadItem._id }, { $set: { is_opened: false, accepted_bid_user: bid_user_id } }, { 'new': true })
            .populate('bids.user')
            .populate('accepted_bid_user')
            .populate('created_by')
            .exec(async function (err, load) {
              if (err) {
                socket.emit('accept_bid_response', { success: false, message: err.message });
              } else {
                socket.emit('accept_bid_response', { success: true, message: "Bid has been accepted successfully" });
                common.updateLoads(load, socket);

                // Send notification
                var subject = 'Bid on "' + load.name + '" Won';
                var message = '<p>Hi ' + load.accepted_bid_user.first_name + ',</p><p>You were chosen for the load named "' + load.name + '" on Fork Freight!</p><p>If you have any questions or need support, please email us at ' + config.supportEmail + '</p><p>Best wishes,<br>Fork Freight Team</p>';
                // common.sendEmail(subject, message, config.supportEmail, load.accepted_bid_user.email);
                await emailTemplateService.sendEmail('Bid-On', { message }, load.accepted_bid_user.email);
              }
            });
        } else {
          socket.emit('accept_bid_response', { success: false, message: "Bid could not be accepted as the post has been closed" });
        }
      }
    });
  }

};

module.exports.postBid = function (load_id, bid, socket) {

  'use strict';

  if (!socket.decoded._id) {
    socket.emit('post_bid_response', { success: false, message: "UnauthorizedError: Access denied" });
  } else {

    Load.findOne({ _id: load_id })
      .populate('bids.user')
      .populate('accepted_bid_user')
      .populate('created_by')
      .exec(function (err, load) {

        if (load.created_by._id == socket.decoded._id) {
          socket.emit('post_bid_response', { success: false, message: "You are not allowed to post bid on your own load." });
          return;
        }

        if (load.bids.filter(function (e) { return e.user._id == socket.decoded._id }).length > 0) {
          socket.emit('post_bid_response', { success: false, message: "You have already posted the bid on this load." });
          return;
        }

        if (!bid.amount || !bid.description) {
          socket.emit('post_bid_response', { success: false, message: "All fields are required" });
          return;
        }

        User.findOne({ _id: socket.decoded._id }, function (err, user) {

          //            load.bids.push({ amount: bid.amount},{
          //             description: bid.description},{
          //             user: user
          //           });
          load.bids.push({
            amount: bid.amount,
            description: bid.description,
            user: user
          });

          load.save(async function (err) {

            if (err) {
              socket.emit('post_bid_response', { success: false, message: err.message });
            } else {
              socket.emit('post_bid_response', { success: true });
              common.updateLoads(load, socket);

              // Send notification
              var subject = 'New-Bid-Posted';
              var message = '<p>Hi ' + load.created_by.first_name + ',</p><p>You have received a new bid for the load named "' + load.name + '" on Fork Freight!</p><p>If you have any questions or need support, please email us at ' + config.supportEmail + '</p><p>Best wishes,<br>Fork Freight Team</p>';

              //common.sendEmail(subject, message, config.supportEmail, load.created_by.email);
              await emailTemplateService.sendEmail('New-Bid-Posted', { message }, load.created_by.email);
            }

          });

        });

      });

  }

};

module.exports.factorLoad = function (loadItem, socket) {

  'use strict';

  if (!socket.decoded._id) {
    socket.emit('factor_load_response', { success: false, message: "UnauthorizedError: Access denied" });
  } else {
    Load.findOneAndUpdate({ _id: loadItem._id }, { $set: { factor_load: true } }, { 'new': true })
      .exec(async function (err, load) {
        if (err) {
          socket.emit('factor_load_response', { success: false, message: err.message }); s
        } else {
          socket.emit('factor_load_response', { success: true, message: "Your request load as factoring accepted." });
          common.updateLoads(load, socket);
          // Send notification
          var subject = 'Factor Load Request';
          var message = '<h2>User Details:</h2>';
          message += '<p><b>Id</b> : ' + loadItem.accepted_bid_user._id + '</p>';
          message += '<p><b>Name</b> : ' + loadItem.accepted_bid_user.first_name + ' ' + loadItem.accepted_bid_user.last_name + '</p>';
          message += '<p><b>Phone Number</b> : ' + loadItem.accepted_bid_user.phone + '</p><br>';
          message += '<h2>Load Details:</h2>';
          message += '<p><b>Load Id</b> : ' + loadItem._id + '</p>';
          message += '<p><b>Name</b> : ' + loadItem.name + '</p>';
          message += '<p><b>Phone Number</b> : ' + loadItem.phone + '</p>';
          // common.sendEmail(subject, message, loadItem.accepted_bid_user.email, config.supportEmail);
          await emailTemplateService.sendEmail('save-alerts-for-loads', { message }, loadItem.accepted_bid_user.email);
        }
      });

  }
}

module.exports.acceptAgreement = function (loadItem, socket) {
  'use strict';
  common.log(loadItem)
  if (!socket.decoded._id) {
    socket.emit('accept_agreement_response', { success: false, message: "UnauthorizedError: Access denied" });
  } else {
    Load.findOneAndUpdate({ _id: loadItem._id }, { $set: { agreement_signed: true, shipper_signed_agreement: loadItem.created_by.agreement_doc, carrier_signed_signature: loadItem.accepted_bid_user.electronic_signature } }, { 'new': true })
      .exec(function (err, load) {
        if (err) {
          socket.emit('accept_agreement_response', { success: false, message: err.message }); s
        } else {
          socket.emit('accept_agreement_response', { success: true });
          // common.updateLoads(load, socket);
        }
      });
  }
};

module.exports.loginLoadsfind = async function (req, res) {

  try {
    const loads = await Load.find(
      { is_opened: true }, {
      equipment_type: 1, pickup_addresses: 1, delivery_addresses: 1,
      load_weight: 1, load_length: 1, ready_date: 1, expire_on: 1
    })
      .sort({ created_at: -1 })
      .limit(10)
      .exec()

    return res.status(200).json(loads);
  }
  catch (err) {
    res.status(400).send({ sucess: false, message: err.message });
  }

};


module.exports.loginLocationLoads = async function (req, res) {

  try {
    var lat = req.body.lat;
    var lng = req.body.long;
    var radius = 100;

    if (req.body.radius != null && req.body.radius > 0) {
      radius = req.body.radius;
    }


    var search_options = {};
    search_options["$and"] = [];
    let isOpened = { "is_opened": true };
    search_options["$and"].push(isOpened);

    //  Pickup options
    let pickupItems = {};
    pickupItems["$or"] = [];

    if (lat != null
      && lng != null) {
      let pickup_lat_long = {};
      pickup_lat_long["pickup_lat_long"] =
      {
        $geoWithin:
          { $centerSphere: [[Number(lng), Number(lat)], Number(radius) / 3963.2] }
      }

      pickupItems["$or"].push(pickup_lat_long);
    }

    if (pickupItems["$or"].length > 0) { search_options["$and"].push(pickupItems); }

    let loads = await Load.find(search_options, {
      equipment_type: 1, pickup_addresses: 1, delivery_addresses: 1,
      load_weight: 1, load_length: 1, ready_date: 1, expire_on: 1
    })
      .collation({ locale: 'en', strength: 2 })
      .sort({ updatedAt: -1 })
      .limit(10)
      .exec();

    if (loads == null || loads.length == 0) {

      loads = await Load.find(
        { is_opened: true }, {
        equipment_type: 1, pickup_addresses: 1, delivery_addresses: 1,
        load_weight: 1, load_length: 1, ready_date: 1, expire_on: 1
      })
        .sort({ created_at: -1 })
        .limit(10)
        .exec()

    }

    return res.status(200).json(loads);
  }
  catch (err) {
    res.status(400).send({ sucess: false, message: err.message });
  }

};

module.exports.heatMapData = async function (req, res) {
  'use strict';

  common.log(req.body);
  let equipment = req.body.equipment;
  var usastates = states_names.usAllStates();
  var equipmentOptions = [];
  // var equipmentType = equipment;
/**
  if (equipmentType == "Flatbed") {
    equipmentOptions.push("Flatbed/Step Deck", "Flatbed/Van", "Flatbed/Van/Reefer", "B-Train Flatbed", "Flatbed with sides",
      "Flatbed with Tarps", "Flatbed with Pallet Exchange", "Hazmat Flatbed", "Maxi Flatbed", "Team Flatbed", "Hotshot Flatbed")
  }

  if (equipmentType == "Step Deck") {
    equipmentOptions.push("Flatbed/Step Deck", "Hotshot Step Deck", "Removable Gooseneck")
  }

  if (equipmentType == "Reefer") {
    equipmentOptions.push("Reefer with Pallet Exchange", "Hazmat Reefer", "Van/Reefer", "Flatbed/Van/Reefer")
  }

  if (equipmentType == "Van") {
    equipmentOptions.push("Flatbed/Van/Reefer", "Flatbed/Van", "Air Ride Van", "Van with curtains", "Van with Pallet Exchange",
      "Hazmat Van", "Vented Van", "Team Van", "Walking Floor Van", "Van/Reefer", "Box Truck")
  }
*/
  equipmentOptions.push(new RegExp(`${equipment}`));

  try {
    var currentDate = new Date();
    currentDate.setHours(-96,0,0);

    var search = [];
    var _match = {}

    _match["$match"] = {};
    if(equipment != '') _match["$match"]["equipment_type"] = { $in: equipmentOptions };
    _match["$match"]["is_opened"] = true;
   // _match["$match"]["distance"] = { $nin: [0] };
    _match["$match"]["ready_date"] = {$gte:currentDate};
    search.push(_match);
    var _project = {};
    _project["$project"] = {};
    _project["$project"]["pickup_states"] = 1;
    var _unwind = {};
    _unwind["$unwind"] = {};
    _unwind["$unwind"]["path"] = "$pickup_states";
    _unwind["$unwind"]["preserveNullAndEmptyArrays"] = true;
    search.push(_project);
    search.push(_unwind);
    var _group = {}
    _group["$group"] = {};
    _group["$group"]["_id"] = "$pickup_states";
    _group["$group"]["myCount"] = { $sum: 1 };
    search.push(_group);
    let loads = await Load.aggregate(search);

    if (loads != null && loads.length > 0) {

      var array = [];
      loads.forEach(element => {
        if (element._id != '') {
          if (element._id !== undefined) {
            var pickup_state = element._id;

            var i = 0;
            usastates.forEach(usastateselement => {
              if (pickup_state == usastateselement.abbreviation) {
                usastates[i].Loads += element.myCount;
              }
              i++;
            });
          }
        }
      })
    }

    var searchTruck = [];
    var matchTruck = {}
    matchTruck["$match"] = {};
    matchTruck["$match"]["equipment_type"] = { $in: equipmentOptions };
    matchTruck["$match"]["is_opened"] = true;
    matchTruck["$match"]["ready_date"] = {$gte: currentDate};
    searchTruck.push(matchTruck);
    var project = {};
    project["$project"] = {};
    project["$project"]["origin_bystate_states"] = 1;
    project["$project"]["origin_bycity_state"] = 1;
    var unwind = {};
    unwind["$unwind"] = {};
    unwind["$unwind"]["path"] = "$origin_bystate_states";
    unwind["$unwind"]["preserveNullAndEmptyArrays"] = true;
    searchTruck.push(project);
    searchTruck.push(unwind);
    var group = {}
    group["$group"] = {};
    group["$group"]["_id"] = {};
    group["$group"]["_id"]["origin_bystate_states"] = "$origin_bystate_states";
    group["$group"]["_id"]["origin_bycity_state"] = "$origin_bycity_state";
    group["$group"]["myCount"] = { $sum: 1 };
    searchTruck.push(group);

    let trucks = await Truck.aggregate(searchTruck)
    if (trucks != null && trucks.length > 0) {

      var array = [];
      trucks.forEach(element => {

        if (element._id != '') {
          if (element._id !== undefined) {
            var origin_bystate_states = element._id.origin_bystate_states;
            var origin_bycity_state = element._id.origin_bycity_state;

            var i = 0;
            usastates.forEach(usastateselement => {
              if (origin_bystate_states == usastateselement.name || origin_bycity_state == usastateselement.name) {
                usastates[i].truck += element.myCount;
              }
              if (origin_bystate_states == usastateselement.abbreviation || origin_bycity_state == usastateselement.abbreviation) {
                usastates[i].truck += element.myCount;
              }
              i++;
            });
          }
        }
      })
    }


    return res.status(200).json(usastates);

  }
  catch (err) {
    common.log("heat map error: ", + err);
    res.status(400).send({ sucess: false, message: err.message });
  }

};

module.exports.getSearchListGroupBy = async function (req, res) {
  'use strict';
  var search_object = req.body;
  common.log(search_object);

  try {
    let list = await SearchObject.find({ users: req.payload._id }, { searchParams: 1, created_at: 1 });
    let result = {};
    if (list != null) {
      let updatedList = []
      for (let searchObject of list) {
        if (searchObject == null || searchObject.searchParams == null) { continue; }
        let searchOptions = await common.generateSearchObject(searchObject.searchParams);
        let totalcount = await Load.aggregate([
          { $match: searchOptions },
          { $unwind: "$delivery_states" },
          {
            $group: {
              _id: {
                id: "$id",
                title: "$delivery_states"
              },
              total: {
                $sum: 1
              }
            }
          }, {
            $group: {
              _id: "$_id.id",
              delivery_states: {
                $push: {
                  state: "$_id.title",
                  total: "$total"
                }
              },
              total: {
                $sum: '$total'
              }
            }
          }, {
            $project: {
              "total_count": "$total",
              "sub_count": "$delivery_states"
            }
          }
        ]).collation({ locale: 'en', strength: 2 }).exec();

        let searchItem = searchObject.toObject();
        searchItem.recordCount = totalcount;
        updatedList.push(searchItem);
        searchOptions = null;
        totalcount = null;
      }
      result["data"] = updatedList;
    }
    else {
      result["data"] = [];
    }
    res.send(result);

  }
  catch (err) {
    common.log(err);
    res.send({ message: err });
  }
}

function getDateStartMonth(monthsAfter){
  var moment = require('moment');
  let currentDate = new Date();
  currentDate.setDate(currentDate.getDate()-monthsAfter);
  // console.log(moment(currentDate).startOf('day'));
  return moment(currentDate).startOf('day');
}

function getDateEndMonth(monthsAfter){
  var moment = require('moment');
  let currentDate = new Date();
  currentDate.setDate(currentDate.getDate()-monthsAfter);
  // console.log(
  //   moment(currentDate).endOf("day")
  // );
  return moment(currentDate).endOf("day");
}

function getLoadReduction(a,b,c){
  if( (a*.7)>c || (b*.7)>c ) return 'Yes'
  return 'No'
}

module.exports.getLoadsCountPeUsers = async function(req,res){
  try{

    let users = await User.find({source_data:'posteverywhere'},{_id:1, email:1, role:1,company:1}).lean();
    for (let i = 0; i < users.length; i++) {
      const user = users[i];
      let [f,g,h,a,b,c,d,e] = await Promise.all([
        LoadHistory.countDocuments({created_by:user._id, updatedAt: {$gte: getDateStartMonth(7)},updatedAt: {$lte: getDateEndMonth(7)}}),
        LoadHistory.countDocuments({created_by:user._id, updatedAt: {$gte: getDateStartMonth(6)},updatedAt: {$lte: getDateEndMonth(6)}}),
        LoadHistory.countDocuments({created_by:user._id, updatedAt: {$gte: getDateStartMonth(5)},updatedAt: {$lte: getDateEndMonth(5)}}),
        LoadHistory.countDocuments({created_by:user._id, updatedAt: {$gte: getDateStartMonth(4)},updatedAt: {$lte: getDateEndMonth(4)}}),
        LoadHistory.countDocuments({created_by:user._id, updatedAt: {$gte: getDateStartMonth(3)},updatedAt: {$lte: getDateEndMonth(3)}}),
        LoadHistory.countDocuments({created_by:user._id, updatedAt: {$gte: getDateStartMonth(2)},updatedAt: {$lte: getDateEndMonth(2)}}),
        LoadHistory.countDocuments({created_by:user._id, updatedAt: {$gte: getDateStartMonth(1)},updatedAt: {$lte: getDateEndMonth(1)}}),
        LoadHistory.countDocuments({created_by:user._id, updatedAt: {$gte: getDateStartMonth(0)}})
      ])
      user[getDateStartMonth(7).format('DD-MMMM')] = f;
      user[getDateStartMonth(6).format('DD-MMMM')] = g;
      user[getDateStartMonth(5).format('DD-MMMM')] = h;
      user[getDateStartMonth(4).format('DD-MMMM')] = a;
      user[getDateStartMonth(3).format('DD-MMMM')] = b;
      user[getDateStartMonth(2).format('DD-MMMM')] = c;
      user[getDateStartMonth(1).format('DD-MMMM')] = d;
      user[getDateStartMonth(0).format('DD-MMMM')] = e;
      user["load_reduction"] = getLoadReduction(c,d,e);
    }
    const json2csvParser = new Parser();
    const csv = json2csvParser.parse(users);
    res.set('Content-Disposition', ["attachment; filename=loadsCount.csv"].join(''));
		res.set('Content-Type', 'text/csv');
    return res.end(csv.toString());
  } catch(err){
    common.log('Error while getLoadsCountPeUsers :--',err)
    return res.status(400).send({success:false,message:err.message});
  }
}


module.exports.getPELoadsCountReport7DaysFromToday = async function(req,res){
  try{

    let data ={
      type: 'UpdateAt'
    }, data1={
      type: 'UpdateAt with ready date'
    },createdData={
      type: 'CreateAt'
    }, da=[];
    // for (let i = 0; i < users.length; i++) {
    //   const user = users[i];
      let [f,g,h,a,b,c,d,e] = await Promise.all([
        LoadHistory.countDocuments({source:'PostEverywhere', updatedAt: {$gte: getDateStartMonth(7)},updatedAt: {$lte: getDateEndMonth(7)}}),
        LoadHistory.countDocuments({source:'PostEverywhere', updatedAt: {$gte: getDateStartMonth(6)},updatedAt: {$lte: getDateEndMonth(6)}}),
        LoadHistory.countDocuments({source:'PostEverywhere', updatedAt: {$gte: getDateStartMonth(5)},updatedAt: {$lte: getDateEndMonth(5)}}),
        LoadHistory.countDocuments({source:'PostEverywhere', updatedAt: {$gte: getDateStartMonth(4)},updatedAt: {$lte: getDateEndMonth(4)}}),
        LoadHistory.countDocuments({source:'PostEverywhere', updatedAt: {$gte: getDateStartMonth(3)},updatedAt: {$lte: getDateEndMonth(3)}}),
        LoadHistory.countDocuments({source:'PostEverywhere', updatedAt: {$gte: getDateStartMonth(2)},updatedAt: {$lte: getDateEndMonth(2)}}),
        LoadHistory.countDocuments({source:'PostEverywhere', updatedAt: {$gte: getDateStartMonth(1)},updatedAt: {$lte: getDateEndMonth(1)}}),
        LoadHistory.countDocuments({source:'PostEverywhere', updatedAt: {$gte: getDateStartMonth(0)}})
    ])
      data[getDateStartMonth(7).format('DD-MMMM')] = f;
      data[getDateStartMonth(6).format('DD-MMMM')] = g;
      data[getDateStartMonth(5).format('DD-MMMM')] = h;
      data[getDateStartMonth(4).format('DD-MMMM')] = a;
      data[getDateStartMonth(3).format('DD-MMMM')] = b;
      data[getDateStartMonth(2).format('DD-MMMM')] = c;
      data[getDateStartMonth(1).format('DD-MMMM')] = d;
      data[getDateStartMonth(0).format('DD-MMMM')] = e;
      data["load_reduction"] = getLoadReduction(c,d,e);
      da.push(data);

      let [f1,g1,h1,a1,b1,c1,d1,e1] = await Promise.all([
        LoadHistory.countDocuments({source:'PostEverywhere', createdAt: {$gte: getDateStartMonth(7)},createdAt: {$lte: getDateEndMonth(7)}}),
        LoadHistory.countDocuments({source:'PostEverywhere', createdAt: {$gte: getDateStartMonth(6)},createdAt: {$lte: getDateEndMonth(6)}}),
        LoadHistory.countDocuments({source:'PostEverywhere', createdAt: {$gte: getDateStartMonth(5)},createdAt: {$lte: getDateEndMonth(5)}}),
        LoadHistory.countDocuments({source:'PostEverywhere', createdAt: {$gte: getDateStartMonth(4)},createdAt: {$lte: getDateEndMonth(4)}}),
        LoadHistory.countDocuments({source:'PostEverywhere', createdAt: {$gte: getDateStartMonth(3)},createdAt: {$lte: getDateEndMonth(3)}}),
        LoadHistory.countDocuments({source:'PostEverywhere', createdAt: {$gte: getDateStartMonth(2)},createdAt: {$lte: getDateEndMonth(2)}}),
        LoadHistory.countDocuments({source:'PostEverywhere', createdAt: {$gte: getDateStartMonth(1)},createdAt: {$lte: getDateEndMonth(1)}}),
        LoadHistory.countDocuments({source:'PostEverywhere', createdAt: {$gte: getDateStartMonth(0)}})
    ])
      createdData[getDateStartMonth(7).format('DD-MMMM')] = f1;
      createdData[getDateStartMonth(6).format('DD-MMMM')] = g1;
      createdData[getDateStartMonth(5).format('DD-MMMM')] = h1;
      createdData[getDateStartMonth(4).format('DD-MMMM')] = a1;
      createdData[getDateStartMonth(3).format('DD-MMMM')] = b1;
      createdData[getDateStartMonth(2).format('DD-MMMM')] = c1;
      createdData[getDateStartMonth(1).format('DD-MMMM')] = d1;
      createdData[getDateStartMonth(0).format('DD-MMMM')] = e1;
      createdData["load_reduction"] = getLoadReduction(c1,d1,e1);
      da.push(createdData);
      //ready date check
      let cd= new Date();
      cd.setHours(cd.getHours-96);

      let [s,t,u,v,w,x,y,z] = await Promise.all([
        LoadHistory.countDocuments({source:'PostEverywhere', ready_date:{$gte:cd}, updatedAt: {$gte: getDateStartMonth(7)},updatedAt: {$lte: getDateEndMonth(7)}}),
        LoadHistory.countDocuments({source:'PostEverywhere', ready_date:{$gte:cd}, updatedAt: {$gte: getDateStartMonth(6)},updatedAt: {$lte: getDateEndMonth(6)}}),
        LoadHistory.countDocuments({source:'PostEverywhere', ready_date:{$gte:cd}, updatedAt: {$gte: getDateStartMonth(5)},updatedAt: {$lte: getDateEndMonth(5)}}),
        LoadHistory.countDocuments({source:'PostEverywhere', ready_date:{$gte:cd}, updatedAt: {$gte: getDateStartMonth(4)},updatedAt: {$lte: getDateEndMonth(4)}}),
        LoadHistory.countDocuments({source:'PostEverywhere', ready_date:{$gte:cd}, updatedAt: {$gte: getDateStartMonth(3)},updatedAt: {$lte: getDateEndMonth(3)}}),
        LoadHistory.countDocuments({source:'PostEverywhere', ready_date:{$gte:cd}, updatedAt: {$gte: getDateStartMonth(2)},updatedAt: {$lte: getDateEndMonth(2)}}),
        LoadHistory.countDocuments({source:'PostEverywhere', ready_date:{$gte:cd}, updatedAt: {$gte: getDateStartMonth(1)},updatedAt: {$lte: getDateEndMonth(1)}}),
        LoadHistory.countDocuments({source:'PostEverywhere', ready_date:{$gte:cd}, updatedAt: {$gte: getDateStartMonth(0)}})
    ])
    data1[getDateStartMonth(7).format('DD-MMMM')] = s;
    data1[getDateStartMonth(6).format('DD-MMMM')] = t;
    data1[getDateStartMonth(5).format('DD-MMMM')] = u;
    data1[getDateStartMonth(4).format('DD-MMMM')] = v;
    data1[getDateStartMonth(3).format('DD-MMMM')] = w;
    data1[getDateStartMonth(2).format('DD-MMMM')] = x;
    data1[getDateStartMonth(1).format('DD-MMMM')] = y;
    data1[getDateStartMonth(0).format('DD-MMMM')] = z;
    data1["load_reduction"] = getLoadReduction(x,y,z);
    da.push(data1);
    //}
    const json2csvParser = new Parser();
    const csv = json2csvParser.parse(da);
    res.set('Content-Disposition', ["attachment; filename=PELoadReport.csv"].join(''));
		res.set('Content-Type', 'text/csv');
    return res.end(csv.toString());
  } catch(err){
    common.log('Error while getPELoadsCountReport7DaysFromToday :--',err)
    return res.status(400).send({success:false,message:err.message});
  }
}

const findCompanyTransCredit= async (company_id)=>{
try{
  let aggregate = [
    {
      $match: {_id: ObjectId(company_id)}
    },
    {
      $lookup: {
        from: "companytranscreditdetails", localField: "mc_number", foreignField: "ID", as: "transCreditDetail"
      }
    },
    { "$sort": {created_at:-1} },
    { "$limit": 1 }

  ];
  let details= await Company.aggregate(aggregate);
  if(details.length>0){
    return details[0].transCreditDetail;
  }else{
    return [];
  }
}catch(error){
  common.log("error in finding findCompanyTransCredit", error.message);
  return [];
}
}


/**
 * process for transcredit report of company
 */

 const processTranscreditReport= async (company_name)=>{
  try{

      let resp = await common.xml_request("POST",null,null,null,company_name);
      let _resp= await xmlreader(resp);
      if(_resp.ListOfCompany.length>0){
           console.log("report id is ",_resp.ListOfCompany[0].CompanyDetails[0].ReportID[0]);
           if(_resp.ListOfCompany[0].CompanyDetails[0].ReportID[0]){

           let xml_body=`<?xml version="1.0" encoding="utf-8"?>
           <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
           <soap:Body>
           <getCompleteCreditV2 xmlns="http://tempuri.org/">
           <API_key>${config.TRANSCREDIT_KEY}</API_key>
           <ReportID>${_resp.ListOfCompany[0].CompanyDetails[0].ReportID[0]}</ReportID>
           </getCompleteCreditV2>
           </soap:Body>
           </soap:Envelope>`;



          let fullreport= await common.xml_request('POST',xml_body,null,null,null);
          let _fullreport= await xmlreader(fullreport);
           return _fullreport;
      }
  }else{
    return;
  }

  }catch(error){
      common.log("Error while fetching report:____",error.message);
      return;
  }
}


const xmlreader= async (data)=>{
  try{
    let parseString = require('xml2js').parseString;
return new Promise((resolve,reject)=>{
  parseString(data, function (err, result) {
    // console.log("result",result);
    if(err)
     reject(err);

     //return reponse for v2 report
     if(result['soap:Envelope']['soap:Body'][0]['getReportSearchV2Response'])
     resolve(result['soap:Envelope']['soap:Body'][0]['getReportSearchV2Response'][0]['getReportSearchV2Result'][0]);


     resolve(result['soap:Envelope']['soap:Body'][0])
  });
})

}catch(error){
    console.log("error in xml reader :__", error.message);
   throw error.message;
  }
}
