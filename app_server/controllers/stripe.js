const stripeHelper = require('../helpers/stripe');
const common = require('../helpers/common');

module.exports.getPlans = async function(req,res){
    try{
        let plans = await stripeHelper.allPlans();
        // let plan_prods = await Promise.all(plans.map(plan=>stripeHelper.retrieveProduct(plan.product)));
        // plan_prods.forEach((result,index)=>{
        //     if(!(result instanceof Error)){
        //         plans[index].product=result;
        //     }
        // })
        return res.send({success:true,data:plans});
    } catch(err){
        common.log('Error while get plans :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}

module.exports.addPlan = async function(req,res){
    try{
        if(!req.body.price){
            return res.status(400).send({success:false,message:"Price is required"});
        }
        if(!req.body.name){
            return res.status(400).send({success:false,message:"Name is required"});
        }
        const plan = await stripeHelper.addPlan(req.body.name,req.body.price,req.body.trial_period_days);
        plan.product = await stripeHelper.retrieveProduct(plan.product);
        return res.send({success:true,data:plan})
    } catch(err){
        common.log('Error while add plan :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}

module.exports.editPlan = async function(req,res){
    try{
        const plan = await stripeHelper.updatePlan(req.params.id,req.body.trial_period_days);
        plan.product = await stripeHelper.retrieveProduct(plan.product);
        return res.send({success:true,data:plan})
    } catch(err){
        common.log('Error while add plan :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}

module.exports.deletePlan = async function(req,res){
    try{
        const deleted = await stripeHelper.deletePlan(req.params.id);
        return res.send({success:false,data:deleted});
    } catch(err){
        common.log('Error while add plan :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}

module.exports.getCoupons = async function(req,res){
    try{
        let coupons = await stripeHelper.getCoupons();
        return res.send({success:true,data:coupons});
    } catch(err){
        common.log('Error while getCoupons :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}

module.exports.getPromotions = async function(req,res){
    try{
        let promos= await stripeHelper.getPromotions();
        return res.send({success:true,data:promos});
    } catch(err){
        common.log('Error while getPromo :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}

module.exports.deleteCoupon = async function(req,res){
    try{
        let deleted = await stripeHelper.deleteCoupon(req.params.id);
        return res.send({success:true,data:"Coupon delete successfully"});
    } catch(err){
        common.log('Error while deleteCoupon :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}

module.exports.createCoupon = async function(req,res){
    try{
        if(!req.body.type || !(req.body.type == "percent" || req.body.type == "amount")){
            return res.status(400).send({success:false,message:"type should be either \'percent\' or \'amount\'"});
        }
        if(!req.body.value){
            return res.status(400).send({success:false,message:"value is required"});
        }
        let coupon = await stripeHelper.createCoupon(req.body);
        return res.send({success:true,data:coupon})
    } catch(err){
        common.log('Error while createCoupon :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}

module.exports.createPromotion = async function(req,res){
    try{
        if(!req.body.coupon){
            return res.status(400).send({success:false,message:"coupon is required"});
        }
        if(!req.body.code){
            return res.status(400).send({success:false,message:"code is required"});
        }
        let promo = await stripeHelper.createPromotionCode(req.body.coupon,req.body.code);
        return res.send({success:true,data:promo})
    } catch(err){
        common.log('Error while createPromotion :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}

module.exports.createPrice = async function(req,res){
    try{
        if(!req.body.name){
            return res.status(400).send({success:false,message:"name is required"});
        }
        if(!req.body.amount){
            return res.status(400).send({success:false,message:"amount is required"});
        }
        let price = await stripeHelper.createPrice(req.body.name,req.body.amount,req.body.type,req.body.recurring);
        price.product = await stripeHelper.retrieveProduct(price.product);
        return res.send({success:true,data:price})
    } catch(err){
        common.log('Error while createPromotion :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}

module.exports.getPrices = async function(req,res){
    try{
        let prices = await stripeHelper.getPriceList();
        // for (let i = 0; i < prices.length; i++) {
        //     const price = prices[i];
        //     price.product = await stripeHelper.retrieveProduct(price.product);
        //     await common.snooze(200);            
        // }
        // let price_prods = await Promise.all(prices.map(price=>stripeHelper.retrieveProduct(price.product)));
        // price_prods.forEach((result,index)=>{
        //     if(!(result instanceof Error)){
        //         prices[index].product=result;
        //     }
        // })
        return res.send({success:true,data:prices})
    } catch(err){
        common.log('Error while createPromotion :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}

module.exports.voidInvoice = async function(req,res){
    try{
        let invoice = await stripeHelper.voidInvoice(req.body.invoice_id);
        return res.send({success:true,data:invoice});
    } catch(err){
        common.log('Error while voidInvoice :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}

module.exports.payInvoice = async function(req,res){
    try{
        let invoice = await stripeHelper.payInvoice(req.body.invoice_id, req.body.payment_method);
        return res.send({success:true,data:invoice});
    } catch(err){
        common.log('Error while payInvoice :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}