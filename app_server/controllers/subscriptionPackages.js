const mongoose = require('mongoose');
const SiteModule = mongoose.model('SiteModule');
const stripeHelper = require('../helpers/stripe');
const common = require('../helpers/common');
const { ObjectID } = require('mongodb');
var User = mongoose.model('User');
const SubscriptionPackages = mongoose.model('SubscriptionPackages');


module.exports.addSiteModule = async function(req,res){
    try{
        if(!req.body.name){
            return res.status(400).send({success:false,message:"name field is required"});
        }
        if(!req.body.frontend_key){
            return res.status(400).send({success:false,message:"frontend_key field is required"});
        }
        let checkDuplicate = await SiteModule.find({name:req.body.name,is_delete:false}).lean();
        if(checkDuplicate && checkDuplicate.length>0){
            return res.status(400).send({success:false, message: 'Site module already exist'});
        }
        let siteModule = new SiteModule();
        siteModule.name = req.body.name;
        siteModule.frontend_key = req.body.frontend_key
        siteModule.roles = req.body.roles
        siteModule.company_admin = req.body.company_admin || false;
        siteModule.full_access = req.body.full_access || false;
        siteModule.is_delete = false;
        siteModule.order = await SiteModule.count();
        let sm = await siteModule.save();
        return res.send({success:true, data: sm});
    }
    catch(err){
        common.log("Error While add module----- ",err);
        return res.status(400).send({success:false, message: err.message});
    }
}

module.exports.editSiteModule = async function(req,res){
    try{
        if(req.body.name){
            let checkDuplicate = await SiteModule.findOne({name:req.body.name, is_delete:false, _id:{$ne:req.params.id}}).lean();
            if(checkDuplicate){
                return res.status(400).send({success:false, message: 'Site module with this name already exist'});
            }
        }
            let sm = await SiteModule.findById(req.params.id);
            if(!sm){
                return res.status(400).send({success:false, message: 'Site module not found'});
            }
            if(req.body.name){
                sm.name = req.body.name
            }
            if(req.body.frontend_key){
                sm.frontend_key = req.body.frontend_key
            }
            if(req.body.roles){
                sm.roles = req.body.roles
            }
            if(typeof(req.body.company_admin) == 'boolean'){
                sm.company_admin = req.body.company_admin
            }
            if(typeof(req.body.full_access) == 'boolean'){
                sm.full_access = req.body.full_access
            }
            if(req.body.order){
                sm.order = req.body.order
            }
            let upsertSM = sm.toObject();
            delete upsertSM._id;
            await SiteModule.updateOne({_id:req.params.id},upsertSM,{upsert:true});
            return res.send({success:true, data:sm});
        
    }
    catch(err){
        common.log("Error While edit module----- ",err);
        return res.status(400).send({success:false, message: err.message});
    }
}

module.exports.deleteSiteModule = async function(req,res){
    try{
        await SiteModule.updateOne({_id:req.params.id},{$set:{is_delete:true}},{upsert:false});
        return res.send({success:true, message:'Site module remove successfully'});
        
    }
    catch(err){
        common.log("Error While delete module----- ",err);
        return res.status(400).send({success:false, message: err.message});
    }
}

module.exports.siteModuleAddRole = async function(req,res){
    try {
        if(!req.body.role){
            return res.status(400).send({success:false, message:'role is required.'});
        }
        let sm = await SiteModule.findById(req.params.id).lean();
        if(!sm){
            return res.status(404).send({success:false, message:'Site module not found'});
        }else if(sm.roles.indexOf(req.body.role)!=-1){
            return res.status(400).send({success:false, message:'Role already exist'});
        }
        await SiteModule.updateOne({_id:req.params.id},{$push:{roles:req.body.role}})
        return res.send({success:true,message:"Role added successfully"})
    } catch (error) {
        common.log("Error while add role in site module",error)
        return res.status(400).send({success:false, message:error.message});
    }
}

module.exports.siteModuleRemoveRole = async function(req,res){
    try {
        if(!req.body.role){
            return res.status(400).send({success:false, message:'role is required.'});
        }
        let sm = await SiteModule.findById(req.params.id).lean();
        if(!sm){
            return res.status(404).send({success:false, message:'Site module not found'});
        }else if(sm.roles.indexOf(req.body.role)==-1){
            return res.status(400).send({success:false, message:'Role is not exist'});
        }
        await SiteModule.updateOne({_id:req.params.id},{$pull:{roles:req.body.role}})
        await SubscriptionPackages.updateOne({role:req.body.role},{$pull:{module_access:req.params.id}})
        return res.send({success:true,message:"Role remove successfully"})
    } catch (error) {
        common.log("Error while add role in site module",error)
        return res.status(400).send({success:false, message:error.message});
    }
}

module.exports.siteModuleUpdateSortOrder = async function(req,res){
    try {
        if(!req.body.data){
            return res.status(400).send({success:false, message:'data is required.'});
        }
        let bulk_list=[];
        for (let i = 0; i < req.body.data.length; i++) {
            const sm = req.body.data[i];
            bulk_list.push({
                "updateOne": {
                    "filter": { "_id": ObjectID(sm._id) },
                    "update": { "$set": { "order": i+1 } }
                }
            });
        }
        if(bulk_list.length>0){
            await SiteModule.bulkWrite(bulk_list)
            return res.send({success:true,message:"sort order update successfully"});
        }else{
            return res.send({success:false,message:"list empty"});
        }
    } catch (error) {
        common.log("Error while site Module Update Sort Order",error)
        return res.status(400).send({success:false, message:error.message});
    }
}

module.exports.subscriptionPackageUpdateSortOrder = async function(req,res){
    try {
        if(!req.body.data){
            return res.status(400).send({success:false, message:'data is required.'});
        }
        let bulk_list=[];
        for (let i = 0; i < req.body.data.length; i++) {
            const sp = req.body.data[i];
            bulk_list.push({
                "updateOne": {
                    "filter": { "_id": ObjectID(sp._id) },
                    "update": { "$set": { "order": i+1 } }
                }
            });
        }
        if(bulk_list.length>0){
            await SubscriptionPackages.bulkWrite(bulk_list)
            return res.send({success:true,message:"sort order update successfully"});
        }else{
            return res.send({success:false,message:"list empty"});
        }
    } catch (error) {
        common.log("Error while Update Sort Order SubscriptionPackages",error)
        return res.status(400).send({success:false, message:error.message});
    }
}

module.exports.getAllModules = async function(req,res){
    try{
        const list= await SiteModule.find({is_delete:false}).sort({order:1}).lean()
        return res.send({success:true, data:list});
    }
    catch(err){
        common.log("Error While get module----- ",err);
        return res.status(400).send({success:false, message: err.message});
    }
}

module.exports.getModule = async function(req,res){
    try{
        // let list = await SiteModule.find({is_delete:false}).lean();
        let list = await SiteModule.aggregate([{
            $lookup: {
                from: 'roles',
                localField: 'roles',
                foreignField: 'name',
                as: 'roles'
            }
        }, {
            $match: {
                is_delete: false,
                "roles.is_public": true
            }
        },
        {
            $sort:{
                order:1
            }
        } ,
        {
            $project: {
                _id: 1,
                name: 1,
                frontend_key: 1
            }
        }]);
        return res.send({success:true, data:list});
    }
    catch(err){
        common.log("Error While get module----- ",err);
        return res.status(400).send({success:false, message: err.message});
    }
}

module.exports.addSubscriptionPackages = async function(req,res){
    try{
        // if(!req.body.price && req.body.price<0){
        //     return res.status(400).send({success:false,message:"price field is required"});
        // }
        if(!req.body.role){
            return res.status(400).send({success:false,message:"role is required"});
        }

        let spexist = await SubscriptionPackages.findOne({stripe_plan_id:req.body.plan_id,role:req.body.role,is_delete:false});
        if(spexist){
            return res.status(400).send({success:false,message:"Subscription Package already exist with same Role and Stripe Plan"});
        }

        let subscriptionpackage = new SubscriptionPackages();
        subscriptionpackage.price = req.body.price;
        subscriptionpackage.subscription_name = req.body.subscription_name;
        if(req.body.module_access){
            subscriptionpackage.module_access = req.body.module_access;
        }
        subscriptionpackage.role = req.body.role;
        subscriptionpackage.with_carrier = (req.body.with_carrier == true);
        subscriptionpackage.is_delete= false;
        subscriptionpackage.active=true;
        subscriptionpackage.stripe_plan_id = req.body.plan_id;
        subscriptionpackage.stripe_price_id = req.body.price_id;
        subscriptionpackage.is_free = (req.body.is_free == true);
        subscriptionpackage.order = await SubscriptionPackages.count();
        let sp = await subscriptionpackage.save();
        // let product_name=""
        // if(req.body.subscription_name) {
        //     product_name = req.body.subscription_name;
        // }else{
        //     product_name = req.body.role+"_"+req.body.price;
        // }
        // const plan = await stripeHelper.addPlan(product_name,req.body.price);
        // sp.stripe_plan_id = plan.id;
        // await SubscriptionPackages.updateOne({_id:sp._id},{$set:{stripe_plan_id:plan.id}},{upsert:false});

        return res.send({success:true, data:sp});
    }
    catch(err){
        common.log("Error While add SubscriptionPackages----- ",err);
        return res.status(400).send({success:false, message: err.message});
    }
}

module.exports.editSubscriptionPackages = async function(req,res){
    try{
        let spexist = await SubscriptionPackages.findOne({_id:{$ne:ObjectID(req.params.id)},stripe_plan_id:req.body.plan_id,role:req.body.role,is_delete:false});
        if(spexist){
            return res.status(400).send({success:false,message:"Subscription Package already exist with same Role and Stripe Plan"});
        }
        let subscriptionpackage = await SubscriptionPackages.findById(req.params.id);
        if(!subscriptionpackage){
            return res.status(400).send({success:false, message:"Subscription package not found"});
        }
        if(typeof(req.body.price) == "number"){
            subscriptionpackage.price = req.body.price;
        }

        if(req.body.subscription_name){
            subscriptionpackage.subscription_name = req.body.subscription_name;
        }

        if(req.body.module_access){
            subscriptionpackage.module_access = req.body.module_access;
        }
        if(req.body.plan_id){
            subscriptionpackage.stripe_plan_id = req.body.plan_id;
        }
        if(req.body.order){
            subscriptionpackage.order = req.body.order;
        }
        subscriptionpackage.stripe_price_id = req.body.price_id || '';
        if(typeof(req.body.with_carrier)  == 'boolean'){
            subscriptionpackage.with_carrier = req.body.with_carrier;
        }
        if(typeof(req.body.is_free)  == 'boolean'){
            subscriptionpackage.is_free = req.body.is_free;
        }
        if(req.body.role){
            subscriptionpackage.role = req.body.role;
        }
        let upsertSP = subscriptionpackage.toObject();
        delete upsertSP._id;
        await SubscriptionPackages.updateOne({_id:req.params.id},upsertSP,{upsert:false});
        return res.send({success:true, data:subscriptionpackage});
    }
    catch(err){
        common.log("Error While edit SubscriptionPackages----- ",err);
        return res.status(400).send({success:false, message: err.message});
    }
}

module.exports.addModuleInSubscriptionPackages = async function(req,res){
    try{
        let sp = await SubscriptionPackages.findById(req.params.id).lean();
        if(!sp){
            return res.status(400).send({success:false,message:"Subscription package not found"});
        }
        else if(sp && sp.module_access.findIndex(function(id){return id==req.body.site_module_id})>=0){
            return res.status(400).send({success:false,message:"Site module already added in Subscription package"});
        }
        else{
            await SubscriptionPackages.updateOne({_id:req.params.id},{$push:{module_access:req.body.site_module_id}},{upsert:false});
            let usp = await SubscriptionPackages.findById(req.params.id).lean();
            return res.send({success:true,data:usp});
        }
    }
    catch(err){
        common.log("Error While add module id in SubscriptionPackages----- ",err);
        return res.status(400).send({success:false, message: err.message});
    }
}


module.exports.removeModuleInSubscriptionPackages = async function(req,res){
    try{
        let sp = await SubscriptionPackages.findById(req.params.id);
        if(!sp){
            return res.status(400).send({success:false,message:"Subscription package not found"});
        }
        else if(sp && sp.module_access.findIndex(function(id){return id==req.body.site_module_id})>=0){
            await SubscriptionPackages.updateOne({_id:req.params.id},{$pull:{module_access:req.body.site_module_id}},{upsert:false});
            let usp = await SubscriptionPackages.findById(req.params.id).lean();
            return res.send({success:true,data:usp});
        }else{
            return res.status(400).send({success:false,message:"Site module is not exist in subscription package"});
        }
    }
    catch(err){
        common.log("Error While remove module id in SubscriptionPackages----- ",err);
        return res.status(400).send({success:false, message: err.message});
    }
}

module.exports.deleteSubscriptionPackages = async function(req,res){
    try{
        let sp = await SubscriptionPackages.findById(req.params.id);
        if(!sp){
            return res.status(400).send({success:false,message:"Subscription package not found"});
        }
        else{
            await SubscriptionPackages.updateOne({_id:req.params.id},{$set:{is_delete:true}},{upsert:false});
            let usp = await SubscriptionPackages.findById(req.params.id).lean();
            return res.send({success:true,data:usp});
        }
    }
    catch(err){
        common.log("Error While remove module id in SubscriptionPackages----- ",err);
        return res.status(400).send({success:false, message: err.message});
    }
}


module.exports.activateSubscriptionPackage = async function(req,res){
    try{
        let sp = await SubscriptionPackages.findById(req.params.id);
        if(!sp){
            return res.status(400).send({success:false,message:"Subscription package not found"});
        }
        sp.active=true;
        let upsertSP = sp.toObject();
        delete upsertSP._id;
        await SubscriptionPackages.updateOne({_id:req.params.id},upsertSP,{upsert:false});
        let usp = await SubscriptionPackages.findById(req.params.id).lean();
        return res.send({success:true, data:usp});
        
    }
    catch(err){
        common.log("Error While active SubscriptionPackages----- ",err);
        return res.status(400).send({success:false, message: err.message});
    }
}

module.exports.deactivateSubscriptionPackages = async function(req,res){
    try{
        let sp = await SubscriptionPackages.findById(req.params.id);
        if(!sp){
            return res.send({success:false,message:"Subscription package not found"});
        }
        sp.active=false;
        let upsertSP = sp.toObject();
        delete upsertSP._id;
        await SubscriptionPackages.updateOne({_id:req.params.id},upsertSP,{upsert:false});
        let usp = await SubscriptionPackages.findById(req.params.id).lean();
        return res.send({success:true, data:usp});
        
    }
    catch(err){
        common.log("Error While deactive SubscriptionPackages----- ",err);
        return res.status(400).send({success:false, message: err.message});
    }
}

module.exports.getSubscription = async function(req,res){
    try{
        if(!req.params.role){
            return res.status(400).send({success:false,message:"role is required"});
        }
        let list = await SubscriptionPackages.find({is_delete:false,active:true,role:req.params.role}).sort({order:1}).lean();
        await stripeList(list,res)
    }
    catch(err){
        common.log("Error While get subscription packages----- ",err);
        return res.status(400).send({success:false, message: err.message});
    }
}

module.exports.getSubscriptionSignUpAndProfilePackage = async function(req,res){
    
    try{
        if(!req.params.role){
            return res.status(400).send({success:false,message:"role is required"});
        }
        // Just checking private is false 
        let list = await SubscriptionPackages.find({is_delete:false,active:true,is_private:false,role:req.params.role}).sort({order:1}).lean();
        await stripeList(list,res)
        
    }
    catch(err){
        common.log("Error While get subscription packages----- ",err);
        return res.status(400).send({success:false, message: err.message});
    }
}

async function stripeList(list,res){
    for (let i = 0; i < list.length; i++) {
        let pack = list[i];
        if(pack.stripe_plan_id){
            await common.snooze(100);
            pack['stripe_plan'] = await stripeHelper.retrievePlan(pack.stripe_plan_id)
        }
        if(pack.stripe_price_id){
            await common.snooze(100);
            pack['stripe_price'] = await stripeHelper.retrievePrice(pack.stripe_price_id)
        }
    }
    return res.send({success:true, data:list});
}
module.exports.getSubscriptionForAdmin = async function(req,res){
    try{
        // let limit = req.body.limit || 10;
        // let skip = (((req.body.skip || 1)-1)*limit);
        // let sort_by = req.body.sort_by || 'order';
        // let sort_direction = req.body.sort_direction == 'desc'?-1:1;
        // let sortObj = {};
        // sortObj[sort_by] = sort_direction;

        let list = await SubscriptionPackages.find({is_delete:false}).sort({order:1}).lean()
        
        return res.send({success:true, data:list});
    }
    catch(err){
        common.log("Error While get subscription packages for admin----- ",err);
        return res.status(400).send({success:false, message: err.message});
    }
}

module.exports.updateRoleSubscriptionPackageForAdmin = async function(req,res){
    let {_id,is_private}=req.body
    console.log(is_private)
    try{

        let list = await SubscriptionPackages.findByIdAndUpdate({_id:ObjectID(_id)},{ $set :{is_private:is_private}},{ new: true, upsert: false}).lean()
        return res.send({success:true, data:list});
    }
    catch(err){
        common.log("Error While get subscription packages for admin----- ",err);
        return res.status(400).send({success:false, message: err.message});
    }
}



module.exports.getSubscriptionForPricing = async function(req,res){
    try{
        let list = await SubscriptionPackages.find({$and:[{is_delete:false},{is_private:{$ne:true}}]}).sort({order:1}).lean();
        return res.send({success:true, data:list});
    }
    catch(err){
        common.log("Error While get subscription packages for admin----- ",err);
        return res.status(400).send({success:false, message: err.message});
    }
}

module.exports.getSubscriptionById = async function(req,res){
    try{

        let list = await SubscriptionPackages.findById(req.params.id).lean();
        return res.send({success:true, data:list});
    }
    catch(err){
        common.log("Error While get subscription packages for admin----- ",err);
        return res.status(400).send({success:false, message: err.message});
    }
}

module.exports.getSubscriptionPackagesFromStripePlanId = async function(plan_id,role,is_free=false){
    console.log("plan_id :___", plan_id,"\nrole:____", role);
    try{
        let query ={
            role:role
        };
        if(plan_id){
            query.stripe_plan_id=plan_id
        }
        if(is_free){
            query.is_free=is_free
        }
        let sp = await SubscriptionPackages.findOne(query).lean();
        return sp;
    }
    catch(err){
        throw err;
    }
}

module.exports.getSubscriptionPackagesFromPlanId = async function(plan_id,is_free=false,role=null){

    try{
        let query ={};
        if(role){
            query.role= role
        }
        if(plan_id){
            query.stripe_plan_id=plan_id
        }
        if(is_free){
            query.is_free=is_free
        }
        
        console.log("query:___",query);
        let sp = await SubscriptionPackages.findOne(query).lean();
        return sp;
    }
    catch(err){
        throw err;
    }
}

module.exports.getModulesWithRoles = async function(req,res){
    try{
       let accessableModules =  await SiteModule.aggregate([{
            $lookup: {
                from: 'subscriptionpackages',
                localField: '_id',
                foreignField: 'module_access',
                as: 'subscriptionPackages',
            }
        }, {
            $unwind: {
                path: '$subscriptionPackages'
            }
        }, {
            $match: {
                'subscriptionPackages.role': (req.params.role||'carrier'),
                'is_delete': false
            }
        }, {
            $group: {
                _id: {
                    id: '$_id',
                    name: '$name',
                    frontend_key: "$frontend_key",
                    createdAt: '$createdAt',
                    is_delete: '$is_delete'
                }
            }
        }, {
            $project: {
                '_id': "$_id.id",
                "name": "$_id.name",
                frontend_key: "$_id.frontend_key",
                createdAt: '$_id.createdAt',
                is_delete: '$_id.is_delete'
            }
        }, {
            $sort: {
                order: 1
            }
        }])
        return res.send({success:true,data:accessableModules})
    }
    catch(err){
        common.log("Error while get modules list with roles");
        return res.send({success:false,message:err.message})
    }
}