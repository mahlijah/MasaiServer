const mongoose = require('mongoose');
const Offer = mongoose.model('Offer');
const User = mongoose.model('User');
const UserOfferHistory = mongoose.model('UserOfferHistory');
const { ObjectId } = require('mongodb');
const StripeHelper = require('../helpers/stripe');
const common = require('../helpers/common');
const CtrlSubscriptionPackages = require('../services/subscriptionServices')
const RoleServices= require('../services/roleServices');
const UserStripeDetail = mongoose.model('UserStripeDetail');
const {adminStoreLogs}= require('../controllers/userActivityLog');

module.exports.getAllOffers = async function(req,res){
    try{
        let offers =  await Offer.find({is_delete:false}).populate('offer_on').lean();
        return res.send(offers);
    } catch(err){
        common.log('Error while getOffer :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}
module.exports.getOffers = async function(req,res){
    try{
        const current_date=new Date();
        let query = {is_delete:false, start_date:{$lte:current_date},end_date:{$gte:current_date}}
        let pRoles = await RoleServices.getPublicRoles();
        const user = await User.findById(req.payload._id,{subscription_package:1,role:1}).lean();
        if(pRoles.indexOf(user.role)>=0){
            query["offer_on"]={$in:user.subscription_package}
        }
        let offers =  await Offer.find(query).populate('offer_on').lean();
        return res.send(offers);
    } catch(err){
        common.log('Error while getOffer :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}
module.exports.getOffer = async function(req,res){
    try{
        let offer =  await Offer.findById(req.params.id).populate('offer_on').lean();
        if(offer.offer_type=="extra_features"){
            offer.value_subscription_package = await CtrlSubscriptionPackages.getSubscriptionPackageById(offer.offer_value)
        }
        return res.send(offer);
    } catch(err){
        common.log('Error while getOffer :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}
module.exports.addOffer = async function(req,res){
    let error_list=[];
    if(!req.body.start_date){
        error_list.push({error:'required', message:'Start date is required'});
    }
    if(!req.body.end_date){
        error_list.push({error:'required', message:'End date is required'});
    }
    if(!req.body.title){
        error_list.push({error:'required', message:'Title is required'});
    }
    if(!req.body.offer_on){
        error_list.push({error:'required', message:'offer_on is required'});    
    }
    if(!req.body.offer_type){
        error_list.push({error:'required', message:'offer_type is required'});
    }
    if(!req.body.offer_value){
        error_list.push({error:'required', message:'offer_value is required'});
    }
    if(!req.body.offer_upto){
        error_list.push({error:'required', message:'offer_upto is required'});
    }
    if(!req.body.offer_on_pay){
        error_list.push({error:'required', message:'offer_on_pay is required'});
    }
    if(error_list.length){
        return res.status(400).send({
            success: false,
            error: error_list
        });
    }
    try{
        let offer = new Offer();
        offer.start_date =req.body.start_date;
        offer.end_date =req.body.end_date;
        offer.title =req.body.title;
        offer.offer_on =req.body.offer_on;
        offer.offer_type =req.body.offer_type;
        offer.offer_value =req.body.offer_value;
        offer.offer_upto =req.body.offer_upto;
        offer.offer_on_pay =req.body.offer_on_pay;
        offer.save();
        return res.send({success:true,data:offer});
    } catch(err){
        common.log('Error while addOffer :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}

module.exports.editOffer = async function(req,res){
    let error_list=[];
    if(!req.params.id){
        error_list.push({error:'required', message:'id is required'});
    }
    if(!req.body.start_date){
        error_list.push({error:'required', message:'Start date is required'});
    }
    if(!req.body.end_date){
        error_list.push({error:'required', message:'End date is required'});
    }
    if(!req.body.title){
        error_list.push({error:'required', message:'Title is required'});
    }
    if(!req.body.offer_on){
        error_list.push({error:'required', message:'offer_on is required'});
    }
    if(!req.body.offer_type){
        error_list.push({error:'required', message:'offer_type is required'});
    }
    if(!req.body.offer_value){
        error_list.push({error:'required', message:'offer_value is required'});
    }
    if(!req.body.offer_upto){
        error_list.push({error:'required', message:'offer_upto is required'});
    }
    if(!req.body.offer_on_pay){
        error_list.push({error:'required', message:'offer_on_pay is required'});
    }
    if(error_list.length){
        return res.status(400).send({
            success: false,
            error: error_list
        });
    }
    try{
        let offer = await Offer.findById(req.params.id);
        if(!offer){
            return res.status(404).send({success:false,message:'Offer not found'});
        }
        offer.start_date =req.body.start_date;
        offer.end_date =req.body.end_date;
        offer.title =req.body.title;
        offer.offer_on =req.body.offer_on;
        offer.offer_type =req.body.offer_type;
        offer.offer_value =req.body.offer_value;
        offer.offer_upto =req.body.offer_upto;
        offer.offer_on_pay =req.body.offer_on_pay;
        let upsertOffer = offer.toObject();
        delete upsertOffer._id;
        await Offer.updateOne({_id:offer._id},{$set:upsertOffer});
        return res.send({success:true,data:offer});
    } catch(err){
        common.log('Error while addOffer :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}

module.exports.deleteOffer = async function(req, res){
    try{
        let offer = await Offer.findById(req.params.id).lean();
        if(!offer){
            return res.status(404).send({success:false,message:'Offer not found'});
        }
        if(offer.is_delete){
            return res.status(400).send({success:false,message:'Offer already deleted'});
        }
        await Offer.updateOne({
            _id:offer._id
        },{
            $set:{
                is_delete:true
            }
        })
        return res.send({success:true, message:'Offer delete success fully'});
    }catch(err){
        common.log('Error while deleteOffer :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}

module.exports.subscribe_offer = async function(req,res){
    try{
        const error_list = [];
        if(!req.body.offer_id){
            error_list.push({error:'required', message: 'offer_id is required'});
        }
        if(!req.body.subscription_package_id){
            error_list.push({error:'required', message: 'subscription_package_id is required'});
        }
        if(!req.body.user_id){
            error_list.push({error:'required', message: 'user_id is required'});
        }
        if(error_list.length){
            return res.status(400).send({success:false,message: error_list});
        }
        const offer = await Offer.findById(req.body.offer_id).lean();

        if(!offer){
            return res.status(404).send({success:false,message: "Offer not found"});
        }
        const current_date=new Date();
        if(offer.is_delete || !(offer.start_date<=current_date && offer.end_date>=current_date )){
            return res.status(400).send({success:false,message: "Offer is not available"});
        }
        if(offer.offer_on.findIndex(id=>id=req.body.subscription_package_id)==-1){
            return res.status(400).send({success:false,message: "Offer is not available for selected subscription package"});
        }
        const user = await User.findById(req.body.user_id).lean();
        if(!user){
            return res.status(404).send({success:false,message: "User not found"});
        }
        if(!user.stripe_id){
            return res.status(404).send({success:false,message: "User stripe account not found"});
        }
        const sp = await CtrlSubscriptionPackages.getSubscriptionPackageById(req.body.subscription_package_id);
        if(!sp){
            return res.status(404).send({success:false,message: "Subscription Package not found"});
        }
        const customer = await StripeHelper.getCustomer(user.stripe_id);
        if(!customer.invoice_settings || !customer.invoice_settings.default_payment_method){
            const pml = await StripeHelper.getPaymentMethods(user.stripe_id);
            if(!pml.data || pml.data.length==0) return res.status(400).send({success:false, message:'Payment method not found.'})
            await StripeHelper.updateCustomerDefaultPM(user.stripe_id,pml.data[0].id);
        }
        //let product =  await StripeHelper.findProductByName(offer.title);
        let upDate=new Date();
        let unit_amount = 0;
        let quantity =1;
        if(offer.offer_upto && offer.offer_type != "amount"){
            unit_amount = sp.stripe_plan.amount;
            quantity=parseInt(offer.offer_upto);
            upDate.setMonth(upDate.getMonth() + parseInt(offer.offer_upto));
        }else if(offer.offer_upto && offer.offer_type == "amount"){
            unit_amount = Math.round(parseFloat(offer.offer_value) * 100);
            quantity=parseInt(offer.offer_upto);
            upDate.setMonth(upDate.getMonth() + parseInt(offer.offer_upto));
        }
        if(offer.offer_type=="extra_months"){
            upDate.setMonth(upDate.getMonth() + parseFloat(offer.offer_value));
        }
        let coupon;
        if(offer.offer_type=="percent"){
            let code = offer.offer_type.toUpperCase()+offer.offer_value.toString();
            try{
                coupon =  await StripeHelper.findCouponByCode(code);
            }catch{
                coupon = await StripeHelper.createCoupon({
                    id:code,
                    type:offer.offer_type,
                    value:parseFloat(offer.offer_value)
                });
            }
        }

        let price_data = {
            currency:'usd',
            product:sp.stripe_plan.product,
            unit_amount: unit_amount
        }
        let trails_days = Math.round((upDate-current_date)/(1000*60*60*24));
        const subscription = await StripeHelper.createSubscription_restart(user.stripe_id,sp.stripe_plan_id,false,trails_days,null,(coupon?coupon.id:null),price_data,quantity);

        if (customer && customer.subscriptions && customer.subscriptions.data && customer.subscriptions.data.length > 0) {
            const invoice = customer.subscriptions.data[0].latest_invoice;
            if(invoice && (invoice.status =="open" || invoice.status =="uncollectible")){
                await StripeHelper.voidInvoice(invoice.id);
            }
            await StripeHelper.deleteSubscription(customer.subscriptions.data[0].id);
        }
        
        await User.updateOne({
            _id: user._id
          }, {
            $set: {
              role: sp.role,
              subscription_package: offer.offer_type=="extra_features" ? offer.offer_value :sp._id,
              subscription_type: sp.subscription_name,
              subscription: true,
              schedule: null,
              payment_status: subscription.status,
              free_subscription_package_after_cancel_paid: null,
              subscription_expire_on: new Date(subscription.current_period_end * 1000).toISOString(),
              status:'active'
            }
          })
          let userOfferHistory = new UserOfferHistory();
          userOfferHistory.user=user._id;
          userOfferHistory.offer=offer._id;
          userOfferHistory.created_by=req.payload._id;
          userOfferHistory.updated_by=req.payload._id;
          userOfferHistory.save();
        return res.send({ success: true, message: 'Offer avail success fully' })
    } catch(err){
        common.log('Error while subscript_offer :--',err)
        return res.status(400).send({success:false,message:err.message});
    }finally{
        await adminStoreLogs(req,null,"apply offer",'UserOfferHistory')
    }
}

module.exports.isOfferUsed = async function(req,res){
    try{
        const result = await UserOfferHistory.exists({user:req.payload._id});
        return res.send({success:true,data:result});
    } catch(err){
        common.log('Error while isOfferUsed :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}

module.exports.getUsedOfferDetail = async function(req,res){
    try{
        const result = await UserOfferHistory.findOne({user:req.body.user_id}).populate({
            path:'offer',
            populate:{
                path:'offer_on',
                model:'SubscriptionPackages'
            }
        });
        return res.send({success:true,data:result});
    } catch(err){
        common.log('Error while isOfferUsed :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}

module.exports.getUserOfferDetail = async function(req,res){
    try{
        let result = await UserOfferHistory.find({user:req.body.user_id}).populate({
            path:'offer',
            populate:{
                path:'offer_on',
                model:'SubscriptionPackages'
            }
        }).populate({
            path: "created_by",
            // populate:{path:"User"}
            select: 'email first_name last_name role'
        });

        //search for restart subscriptions if there is any
        let restart_sub_obj= await UserStripeDetail.find({user:req.body.user_id,created_by:{'$ne':null}}).populate({
            path:"created_by",
            select: 'email first_name last_name role'
        })
        if(restart_sub_obj.length>0){
            restart_sub_obj.map((res)=>{
                if(res?.subscription?.discount?.coupon){
                res._doc.created_at=new Date(res.subscription.trial_start * 1000).toISOString();
                res._doc.offer={
                 is_restart_subscription: true,
                 start_date: new Date(res.subscription.trial_start * 1000).toISOString(),
                 end_date :new Date(res.subscription.trial_start * 1000).toISOString(),
                 title : res.subscription?.discount?.coupon?.id,
                 offer_type :  res.subscription?.discount?.coupon?.object,
                 offer_value : res.subscription?.discount?.coupon?.percent_off,
                 createdAt :  new Date(res.subscription?.discount?.coupon?.created*1000).toISOString()
                }
            }  
            result.push(res);
            })
        }
        // console.log("restart object mapping:___", restart_sub_obj,
        // "\n______\n");
        // result =[...result,restart_obj]
        return res.send({success:true,data:result});
    } catch(err){
        common.log('Error while isOfferUsed :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}