
var passport = require('passport');
var crypto = require('crypto');
var randomstring = require("randomstring");
var mongoose = require('mongoose');
var config = require('../config/config');
var Load = mongoose.model('Load');
var Truck = mongoose.model('Truck');
var User = mongoose.model('User');
const Company = mongoose.model('Company');
var Truck = mongoose.model('Truck');
const UserStripeDetail = mongoose.model('UserStripeDetail');
var TemporaryUsers = mongoose.model('TemporaryUsers');
var Note = mongoose.model('Note');
var common = require('../helpers/common');
var jwt = require('jsonwebtoken');
var btoa = require('btoa');
var ResetPassword = mongoose.model('ResetPassword');
var moment = require('moment');
const { ObjectId } = require('bson');
const crmHelper = require('../helpers/crm');
const SubscriptionService = require('../services/subscriptionServices');
const stripeHelper = require('../helpers/stripe');
const emailTemplateService = require('../services/emailTemplateService');
const subscriptionPackages = mongoose.model('SubscriptionPackages');
var TempNotes = mongoose.model('TempNotes');
const _ = require('lodash')


module.exports.posteverywhereregister = function (req, res) {

  var accountRequest = req.body.accountRequest;
  common.log("account request: ", accountRequest);
  var data = common.splitRequestXMLData(accountRequest);

  common.log(data);

  if (!data.userName) {
    common.log("Error: userName required")
    res.status(400).send("Error: userName required");
    return;
  }
  if (!data.password) {
    common.log("Error: password required");
    res.status(200).send("Error: password required");
    return;
  }
  const url = process.env.PORTAL_URL || 'https://test.forkfreight.com';
  User.findOne({ username: data.userName }, function (err, docs) {
    if (docs !== null) {
      var hash = crypto.pbkdf2Sync(data.password, docs.salt, 1000, 64, 'sha512').toString('hex');

      if (hash == docs.hash) {
        common.log("Account already exists with same password");
        res.status(200).send("Account already exists with same password");
        return;
      } else {
        common.log("Account already exists with different password");
        res.status(200).send("Account already exists with different password");
        return;
      }
    } else {
      if (!common.validateEmail(data.email)) {
        common.log("email field is not a valid email");
        res.status(200).send("The email field is not a valid email address");
        return;
      }


      var user = new User();
      var addr = data.addr1;
      if (data.addr2 != null) addr = addr + ',' + data.addr2;
      addr = addr + ',' + data.city + ',' + data.state + ' ' + data.postalCode + ',' + data.country;
      user.username = data.userName;
      user.first_name = data.fName;
      user.last_name = data.lName;
      //prepend the username to email to make the email unique. 
      //posteverywhere creates multiple users for same email so we need to handle that on our side. 
      var email = data.userName + "." + data.email;
      user.email = email;
      user.company = data.compName;
      user.address = addr;
      user.phone = data.phone;
      user.mc_Number = data.MCNumber;
      user.role = "broker";
      user.status = "active";
      user.businessType = data.busnType;
     
      if( data.userName.startsWith('LBN'))
      {
        user.source_data='LoadBoardNetwork';
      }
      else
      {
        user.source_data='PostEverywhere';
      }
      
      user.setPassword(data.password);


      user.save(async function (err) {
        if (err) {
          common.log(err);
          res.status(200).send("Error: error saving user");
          return;
        }


        common.log("account created");
        // common.sendWelcomeEmailWithActivateLink(user,url);
        let activationToken = emailTemplateService.activationTokenGeneration(user);
        let activationLink = url + '/public/activateUser?token=' + activationToken + "&email=" + btoa(user.email);

        await emailTemplateService.sendEmail('welcome-email', { user }, user.email, activationLink)
        res.status(200).send("Account created");
      });
    }
  });

};

module.exports.updateEveryWhereUser=async function(req,res){
  try {
    let count = 0;
    var query = {};
    query["$and"] = []
    query["$and"].push(
      { $or:  [
        // {email:{ $regex: 'pels.*', $options: 'i' }},
        { email: { $regex: 'pel*', $options: 'i' } },
        { email: { $regex: '^([0-9]+)@(forkfreight\.com)$', $options: 'i' } }
      ],
      source_data:{$ne:"posteverywhere"}
    }
    );
    do {
      let [total,Users] = await Promise.all([
        User.countDocuments(query).lean(),
        User.find(query).sort({ updateAt: 1 }).limit(100).lean()
      ]);
     count = total; 
   let bulkUpdateQuery = Users.map(users => {
       let source_data="posteverywhere";
        return {
          "updateOne": {
            filter: { _id: users._id },
            update:{ 
              $set: {
                source_data:source_data
              }
            }
          }
        }
      })
     await User.bulkWrite(bulkUpdateQuery);
    } while (count>0);
    return res.send("Data Updated");
  } catch (err) {
    common.log('Error while changePostEveryWhereUser :--', err)
    return res.status(400).send({ success: false, message: err.message });
  } 
}


module.exports.posteverywherelogin = function (req, res) {
  // common.log(req)
  var accountRequest = req.body.accountRequest;

  var data = common.splitRequestXMLData(accountRequest);
  common.log(data);
  if (!data.userName) {
    res.status(200).send("Error: userName required");
    return;
  }

  if (!data.password) {
    res.status(200).send("Error: password required");
    return;
  }

  passport.authenticate('local', function (err, user, info) {
    var token;
    if (err) {
      res.status(400).send({ Error: err });
      return;
    }
    if (user) {
      token = user.generateJwt();
      var role = user.role.toLocaleLowerCase();
      res.status(200).send({ "id": user._id, "token": token, "role": role });
    } else {
      User.findOne({ username: data.userName }, function (err, user) {
        common.log(data)
        if (user === null) {
          return res.status(200).send('Account is confirmed invalid');
        }

        var hash = crypto.pbkdf2Sync(data.password, user.salt, 1000, 64, 'sha512').toString('hex');
        if (user.usename != data.userName && hash != user.hash) {

          return res.status(200).send("Username or Password is not correct");
        }
        var datausername = user.username.trim();
        var userusername = user.username.trim();
        if (datausername != userusername) {
          common.log("Username not found");
          return res.status(400).send("Username not found");
        }
        if (hash == user.hash) {
          common.log("Account is valid");
          if (user) res.status(200).send("Account is confirmed valid");
        } else {
          common.log("Incorrect password");
          return res.status(200).send("Incorrect password");
        }
      });
    }
  })(req, res);

}

module.exports.sendActivationEmail = async function (req, res) {

  try {

    const url = req.headers.origin || process.env.PORTAL_URL;
    if (!req.body.email) {
      res.status(400).send({ message: "email missing" });
      return;
    }

    let user = await User.findOne({ email: req.body.email });

    if (user == null) {
      common.log("user not found");
      res.status(400).send({ message: "user not found" });
      return;
    }

    let portalURL = url;
    //  Send notification
    let activationToken = emailTemplateService.activationTokenGeneration(user);

    let activationLink = portalURL + '/public/activateUser?token=' + activationToken;

    //common.sendActivationEmail(user, url);
    await emailTemplateService.sendEmail('account-activation', { user }, user.email, activationLink)

    res.status(200).json({ message: "activation success" });

  }
  catch (err) {

    common.log(err);
    res.status(400).send({ message: err.message });
    return;
  }

}

module.exports.sendVerifiationEmail = async function (req, res) {

  try {

    const url = req.headers.origin || process.env.PORTAL_URL;
    if (!req.payload._id) {
      res.status(400).send({ message: "email missing" });
      return;
    }

    let user = await User.findById(req.payload._id);

    if (user == null) {
      common.log("user not found");
      res.status(400).send({ message: "user not found" });
      return;
    }

    //  Send notification
    let portalURL = url;
    let activationToken = emailTemplateService.activationTokenGeneration(user);
    let activationLink = portalURL + '/public/activateUser?token=' + activationToken;


    //common.sendVerifiationEmail(user, url);
    await emailTemplateService.sendEmail('email-verification', { user }, user.email, activationLink)

    res.status(200).json({ message: "verification success" });

  }
  catch (err) {

    common.log(err);
    res.status(400).send({ message: err.message });
    return;
  }

}

module.exports.activateAccount = async function (req, res) {

  try {

    if (!req.body.token) {
      res.status(400).send({ message: "token missing" });
      return;
    }

    const url = req.headers.origin || process.env.PORTAL_URL;
    let decoded = jwt.verify(req.body.token, config.secretKey)

    if (decoded == null) {
      res.status(400).send({ message: "invalid token" });
      return;
    }
    else if (decoded != null && decoded.exp != null) {
      const dateNow = new Date();

      if (decoded.exp < dateNow.getTime() / 1000) {
        res.status(400).send({ message: "expired token" });
        return;
      }
    }

    let user = await User.findOne({ email: decoded.email }).exec();

    if (user == null) {
      common.log("user not found");
      res.status(400).send({ message: "user not found" });
      return;
    }

    if (user != null && user.status == 'active' && user.emailVerified == true) {
      common.log("user already activated. ");
      res.status(200).send({ message: "user already active" });
      return;
    }

    await User.updateOne({ _id: user._id }, { $set: { status: "active", emailVerified: true } }).exec();

    //common.sendAccountActivatedEmail(user,url);
    await emailTemplateService.sendEmail('email-verify-success', { user }, user.email, url)

    res.status(200).json({ message: "activation success" });

  }
  catch (err) {
    common.log(err);
    res.status(400).send({ message: err.message });
    return;
  }
}

module.exports.register = function (req, res) {
  var errorlist = [];
  if (!req.body.first_name) { errorlist.push("first_name"); }
  if (!req.body.last_name) { errorlist.push("last_name"); }
  if (!req.body.email) { errorlist.push("email"); }
  if (!req.body.password) { errorlist.push("password"); }
  if (!req.body.role) { errorlist.push("role"); }
  if (!req.body.company) { errorlist.push("company"); }
  if (!req.body.address) { errorlist.push("address"); }
  if (!req.body.phone) { errorlist.push("phone"); }
  if (errorlist.length > 0) {
    common.log("fname:" + req.body.first_name + "Lname: " + req.body.last_name + " email:" + req.body.email + " Password:" + req.body.password + " Role:" + req.body.role + " Company:" + req.body.company + " Address:" + req.body.address);
    res.status(400).send({ message: "missing some required fields", data: errorlist });
    return;
  }

  if (!common.validateEmail(req.body.email)) {
    res.status(400).send({ message: "The email field is not a valid email address" });
    return;
  }
  const url = req.headers.origin || process.env.PORTAL_URL;
  var user = new User();
  user.first_name = req.body.first_name;
  user.last_name = req.body.last_name;
  user.email = req.body.email;
  user.company = req.body.company;
  user.address = req.body.address;
  user.phone = req.body.phone;
  user.mc_Number = req.body.mc_Number;
  user.role = req.body.role;//"broker";
  user.setPassword(req.body.password);
  user.status = "inactive";

  user.save(async function (err) {
    if (err) {
      common.log(err);
      res.status(400).send({ message: err.message });
      return;
    }
    //common.sendWelcomeEmail(user,url);
    var registrationLink = url + "/public/signup?complete=" + btoa(user.email)
    await emailTemplateService.sendEmail('welcome-email', { user }, user.email, registrationLink)

    var token = user.generateJwt();
    res.status(200).json({ "id": user._id, "token": token, "role": user.role });
  });
};



module.exports.login = function (req, res) {

  if (!req.body.email || !req.body.password || !req.body.device_id) {
    res.status(400).send({ message: "All fields are required" });
    return;
  }
  _login(req, res, false);
};

module.exports.checkAndLogin = function (req, res) {
  if (!req.body.email || !req.body.password || !req.body.device_id) {
    res.status(400).send({ message: "All fields are required" });
    return;
  }
  _login(req, res, true)
};

module.exports.logout = function (req, res) {
  if (req.payload._id) {
    User.findById(req.payload._id).exec(function (err, user) {
      if (err) {
        common.log("Error while get user detail in logout", err);
        return res.status(404).send({ code: 404, message: 'Login detail not found' });
      }
      if (!user) return res.status(404).send({ code: 404, message: 'Login detail not found' });
      try {
        User.findOneAndUpdate({ _id: user._id }, { $set: { last_login_at: null, session_count: 0 } }, { new: true }).exec(function (err) {
          if (err) {
            common.log("Error while remove login detail", err);
            return res.send({ success: false, message: "logout failed" });
          }
          return res.send({ success: true, message: "logout success" });
        });
      }
      catch (err) {
        common.log("Error while remove login detail", err);
        return res.send({ success: false, message: "logout failed" });
      }
    })
  } else {
    return res.status(404).send({ code: 404, message: 'Login detail not found' });
  }
}

module.exports.forgotPassword = async function (req, res) {

  common.log(req.body);
  if (!req.body.email) {
    res.status(400).send({ message: "The email field is required" });
    return;
  }

  var random = randomstring.generate({ length: 5, charset: 'alphabetic' });
  User.findOne({ email: req.body.email }, function (err, user) {
    if (!err) {
      if (user) {
        user.setPassword(random);

        var upsertData = user.toObject();
        delete upsertData._id;

        User.update({ _id: user.id }, upsertData, { upsert: true }, async function (err) {
          if (!err) {

            // Send notification
            // var subject = 'Password Request';
            // var message = '<p>Hi ' + user.first_name + ',</p><p>Your password has been reset. New password is ' + random + '</p><p>If you have any questions or need support, please email us at ' + config.supportEmail + '</p><p>Best wishes,<br>Fork Freight Team</p>';

            //common.sendEmail(subject, message, config.supportEmail, user.email);
            await emailTemplateService.sendEmail('forgot-password', { user }, user.email)

            res.status(200).json({ message: "Password has been sent to registered email" });
            common.log("password ", random, "resent to :", user.email);
          } else {
            common.log(err)
            res.status(400).send({ message: err.message });
            return;
          }
        });
      } else {
        common.log("user not found");
        res.status(401).send({ message: 'UnauthorizedError: User not found' });
      }
    } else {
      common.log(err);
      res.status(404).json(err);
    }
  });
};

module.exports.sendResetPasswordLink = async function (req, res) {
  if (!req.body.email) {
    return res.status(400).send({ message: 'email is required' });
  }
  try {
    var user = await User.findOne({ email: req.body.email }).exec();
    if (user) {
      console.log(user)
      const url = req.headers.origin || process.env.PORTAL_URL;
      var date = new Date();
      date.setHours(date.getHours() + 2);
      var resetPassword = new ResetPassword();
      resetPassword.user_id = user._id;
      resetPassword.expired_at = date;
      resetPassword.generateResetToken();
      resetPassword.save();
      console.log(resetPassword)
      //common.sendResetPasswordLink(user,url,resetPassword.reset_token,resetPassword.expired_at);


      var resetPasswordLink = url + "/public/reset-password/" + resetPassword.reset_token
      await emailTemplateService.sendEmail('forgot-password', { user }, user.email, resetPasswordLink)

      return res.status(200).send({ message: 'Password link send on your email' });
    } else {
      return res.status(404).send({ message: 'User not found' });
    }
  }
  catch (err) {

  }
}

module.exports.verifyResetToken = async function (req, res) {
  var errorList = [];
  if (!req.body.reset_token) {
    errorList.push({ filed: 'reset_token', message: 'Reset token is required' })
  }
  try {
    ResetPassword.findOne({ reset_token: req.body.reset_token }).exec(function (err, resetPassword) {
      if (err || !resetPassword) {
        common.log("Error while get reset token detail", err);
        return res.status(200).send({ token_valid: false, message: 'Reset token not found' });
      }
      var currentDate = new Date();
      var token_valid = false;
      if (resetPassword.expired_at > currentDate) {
        token_valid = true;
      }
      return res.status(200).send({ token_valid: token_valid });
    })
  }
  catch (err) {
    common.log("Error verify reset token", err);
    return res.status(400).send({ token_valid: false, err: err });
  }
}

module.exports.resetPassword = async function (req, res) {
  var errorList = [];
  if (!req.body.password) {
    errorList.push({ filed: 'password', message: 'Password is required' })
  }
  if (!req.body.reset_token) {
    errorList.push({ filed: 'reset_token', message: 'Reset token is required' })
  }
  if (errorList.length > 0) {
    return res.status(400).send({ err: errorList });
  }
  try {
    ResetPassword.findOne({ reset_token: req.body.reset_token }).exec(function (err, resetPassword) {
      if (err || !resetPassword) {
        common.log("Error while get reset detail", err);
        return res.status(400).send({ message: 'Reset token not found' });
      }
      var currentDate = new Date();
      if (resetPassword.expired_at < currentDate) {
        return res.status(400).send({ message: 'Reset password token is expired.' });
      }
      User.findById(resetPassword.user_id).exec(function (err, user) {
        if (err || !user) {
          common.log("Error while get user detail", err);
          return res.status(400).send({ message: 'User not found' });
        }
        user.setPassword(req.body.password);
        var upsertData = user.toObject();
        delete upsertData._id;
        User.update({ _id: user.id }, upsertData, { upsert: true }, function (err) {
          if (err) {
            common.log("Error while update user", err);
            return res.status(400).send({ message: 'Unable to change password. try again later.' });
          }
          var rp = resetPassword.toObject();
          delete rp._id;
          rp.expired_at = currentDate;
          ResetPassword.update({ _id: resetPassword._id }, rp, { upsert: true }, async function (err) {
            if (err) {
              common.log("Error while update expire time", err)
              return res.status(400).send({ message: 'Unable to expire token.' });
            }
            //common.sendResetPasswordComplete(user);
            await emailTemplateService.sendEmail('password-changed', { user }, user.email)

            return res.status(200).send({ message: "Password update successfully" });
          })
        })
      })
    })
  }
  catch (err) {
    common.log("Error reset password", err);
    return res.status(400).send(err);
  }
}

module.exports.roleAuthorization = function (roles) {

  return function (req, res, next) {

    var user = req.user;

    User.findById(user._id, function (err, foundUser) {

      if (err) {
        res.status(401).send({ message: 'UnauthorizedError: User not found' });
        return next(err);
      }

      if (roles.indexOf(foundUser.role) > -1) {
        return next();
      }

      res.status(401).send({ message: 'UnauthorizedError: You are not authorized to view this content' });
      return next('Unauthorized');

    });

  }

};

module.exports.sendReminderToIncompleteUsers = function () {
  const url = process.env.PORTAL_URL || 'https://test.forkfreight.com';
  let date = new Date();
  date.setDate(date.getDate() - 1)
  User.find({
    businessType: null,
    status: 'inactive',
    created_at: {
      $gte: new Date(date).setHours(00, 00, 00),
      $lt: new Date(date).setHours(23, 59, 59),
    }
  }).exec(function (err, users) {
    if (err) {
      console.error("", err);
      return false;
    }
    users.forEach(async function (user) {

      //common.sendReminderEmail(user,url);
      var registrationLink = url + "/public/signup?complete=" + btoa(user.email)
      await emailTemplateService.sendEmail('reminder-to-verify-email-within-7-days-after-signup-if-not-verified', { user }, user.email, registrationLink)

    })
  })
}



async function findAndUpdateFreeUser(tu,saveUser){
  let query={$and:[{source:'free',created_by:ObjectId(tu._id)}]}
  let setValue={$set:{created_by:saveUser._id}}
  
  await Truck.updateMany(query,setValue)
}




module.exports.newRegisterStep1 = async function (req, res) {
  try {
    let returnData= await processSignup(req.body)
    return res.send(returnData);
  }
  catch (err) {
    common.log("Error While register step 1", err);
    return res.status(400).send({message:err.message})
  }
}


module.exports.newRegisterMobileStep1 = async function (req, res) {
  try {
    let {email,password,device_id}=req.body;
    var user = new User();
    user.email=email;
    if(password)
    user.setPassword(password);
    user.is_Mobile=true
    user.device_id=device_id
    let saved_user=await user.save();
    if(saved_user)
    return res.status(200).json({message:'Successfully saved User'});
  }
  catch (err) {
    common.log("Error While register step 1", err);
    return res.status(400).send({message:err.message})
  }
}







module.exports.newRegisterStep2 = async function (req, res) {
  try {
    var returnData;
    var tu = await TemporaryUsers.findById(req.body.id).exec();
    if (tu) {
      var upsertData = tu.toObject();
      delete upsertData._id;
    }
    let companyUserCount = await User.count({ company_id: req.body.company_id });
    upsertData.company_id = req.body.company_id;
    upsertData.is_company_admin = companyUserCount == 0 ? true : false;
    if (req.body.mc_Number) upsertData.mc_Number = req.body.mc_Number;
    returnData = await TemporaryUsers.findOneAndUpdate({ _id: req.body.id }, upsertData, { new: true, upsert: true, useFindAndModify: true }).exec();
    return res.send(returnData);
  }
  catch (err) {
    common.log("Error While register step 1", err);
    return res.status(400).send(err)
  }
}

module.exports.newRegisterStep3 = async function (req, res) {
  let timeZone;
  let options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric' }
  options.timeZone = 'America/New_York';
  options.timeZoneName = 'long'

  try {
    const url = req.headers.origin || process.env.PORTAL_URL;
    var tu = await TemporaryUsers.findById(req.body.id).exec();
    var tempnotes=await TempNotes.find({user:ObjectId(req.body.id)}).exec()
       

    if (tu) {

      var user = new User();
      if(tu.notes){
        user.notes=tu.notes
      }
      
      if(tu.equipment_type.length>0){
        user.equipment_type=tu.equipment_type;
      }
      user.first_name = tu.first_name;
      user.last_name = tu.last_name;
      user.company_id = tu.company_id;
      user.is_company_admin = tu.is_company_admin;
      user.address = tu.address;
      user.phone = tu.phone;
      user.mc_Number = tu.mc_Number;
      user.role = tu.role;
      user.source_data = "Website";
      user.email = tu.email;
      user.subscription_package = tu.subscription_package;
      user.subscription_type = tu.subscription_type;
      user.payment_status = tu.payment_status;

      user.registration_started= tu.created_at;
      
      if (tu.stripe_id) {
        let st_cus= await stripeHelper.getCustomer(tu.stripe_id);
        // console.log("signup_promotion:___",st_cus.subscriptions.data[0]?.latest_invoice?.discount);
        if(st_cus.subscriptions.data[0]?.latest_invoice?.discount?.promotion_code){
          let promo= await stripeHelper.getPromotion_by_Id(st_cus.subscriptions.data[0]?.latest_invoice?.discount?.promotion_code)
          // console.log("promo:__", promo)
          if(promo?.code)
          user.sign_up_promotion=promo?.code
        }
        timeZone = new Date(tu.subscription_expire_on).toLocaleString('en-US', options);
        user.stripe_id = tu.stripe_id;
        user.subscription = tu.subscription;
        user.subscription_expire_on = tu.subscription_expire_on

      } else {
        timeZone = user.subscription_expire_on
      }
      
     if(tu.password){
       //for password setting 
      // common.log("________:atob the password: __", Buffer.from(tu.password, 'base64').toString('binary'))
      user.setPassword(Buffer.from(tu.password, 'base64').toString('binary'));
     }
       //previous logic
      // if(req.body.password){
      // user.setPassword(req.body.password);
      // }
      if(req.body.ref_Id)
      user.ref_Id=req.body.ref_Id
      
      user.status = "active";
    
      var saveUser = await user.save();
 

      findAndUpdateFreeUser(tu,saveUser)

      /**
       * update the user in crm too 
       */
      // crmHelper.add_or_modify_crm_contact(saveUser);   

       if(tempnotes){
        tempnotes.map(async result=>{
          var note=new Note();
          note._id=result._id
          note.created_at=result.created_at
          note.comment=result.comment
          note.user=saveUser._id
          note.created_by=result.created_by
          note.save()
          await TempNotes.deleteOne({ user: ObjectId(req.body.id) })
        })
      }

      await TemporaryUsers.deleteOne({ _id: req.body.id })
      let subscription_package = await subscriptionPackages.findOne({ _id: tu.subscription_package })

      let activationToken = emailTemplateService.activationTokenGeneration(user);
      let activationLink = url + '/public/activateUser?token=' + activationToken + "&email=" + btoa(user.email);

      await emailTemplateService.sendEmail('welcome-to-your-trial-v2', { user, timeZone, sp: subscription_package }, user.email, activationLink)
      var token = user.generateJwt();
      _updateLastLogin({ id: saveUser._id, device_id: req.body.device_id }, function (err) {        
        if (err) {
          common.log('Error while last login');
          return res.status(400).send({ message: 'error while generate token' });
        }else{
          return res.status(200).json({ "id": saveUser._id, "token": token, "role": saveUser.role });
        } 
      });
    } else {
      common.log("User temporary data not found");
      return res.status(400).send({ message: 'Some thing went wrong! make sure you have enter full registration detail.' })
    }
  }
  catch (err) {
    common.log("Error While register step 1", err);
    return res.status(400).send(err)
  }
}

async function findAndUpdateFreeUser(tu,saveUser){
  let query={$and:[{source:'free',created_by:ObjectId(tu._id)}]}
  let setValue={$set:{created_by:saveUser._id}}
  await Load.updateMany(query,setValue)
  
}

module.exports.found_us = async function (req, res) {
    let found_us=req.body.found_us;
    let userId=req.body.userId;
   try {
      let latestUser=await User.findOne({_id:ObjectId(userId)})
      if(latestUser){
        let updatedUser= await User.updateOne({_id:ObjectId(latestUser._id)},{$set:{found_us:found_us}})
        return res.json({message:'found_us',updatedUser})
      }else{
        return res.status(400).send({success:false,messgae:"User not found"});
      }
    }catch(err){
      common.log("Error found Us", err);
    return res.status(400).send(err)
    }
  }

module.exports.setSelectedSubscriptionPackage = async function (req, res) {
  try {
    var returnData;
    var tu = await TemporaryUsers.findById(req.body.id).exec();
    if (tu) {
      const sp = await SubscriptionService.getSubscriptionPackageById(req.body.subscription_package);
      var upsertData = tu.toObject();
      delete upsertData._id;
      upsertData.subscription_package = req.body.subscription_package;
      upsertData.subscription_type = sp.subscription_name;
      returnData = await TemporaryUsers.findOneAndUpdate({ _id: req.body.id }, upsertData, { new: true, upsert: true, useFindAndModify: true }).exec();
      return res.send(returnData);
    } else {
      return res.status(400).send("User Details not found")
    }
  }
  catch (err) {
    common.log("Error While register step 1", err);
    return res.status(400).send(err)
  }
}
module.exports.findUserByEmail = async function (req, res) {
  try {
    let [user, tpUser] = await Promise.all([
      User.findOne({ email: req.body.email }).populate("subscription_package").lean(),
      TemporaryUsers.findOne({ email: req.body.email }).populate("subscription_package").lean()
    ]);
    if (user) { 
      if(user.subscription_package.stripe_plan_id){
        user.subscription_package['stripe_plan'] = await stripeHelper.retrievePlan(user.subscription_package.stripe_plan_id)
      }
      if(user.subscription_package.stripe_price_id){
        user.subscription_package['stripe_price'] = await stripeHelper.retrievePrice(user.subscription_package.stripe_price_id)
      }
      return res.send({ success: true, data: user }); 
    }
    else if (tpUser) { 
      if(tpUser.subscription_package && tpUser.subscription_package.stripe_plan_id){
        tpUser.subscription_package['stripe_plan'] = await stripeHelper.retrievePlan(tpUser.subscription_package.stripe_plan_id)
      }
      if(tpUser.subscription_package && tpUser.subscription_package.stripe_price_id){
        tpUser.subscription_package['stripe_price'] = await stripeHelper.retrievePrice(tpUser.subscription_package.stripe_price_id)
      }
      return res.send({ success: true, data: tpUser }); 
    }
    else { return res.send({ success: false, message: "No user found with this email" }); }
  }
  catch (err) {
    common.log("Error While find user by email", err);
    return res.status(400).send({ success: false, message: err.message });
  }
}

module.exports.addChildUser = async function (req, res) {
  try {
    var createdBy = await User.findById(req.payload._id).populate('company_id').lean();
    const url = req.headers.origin || process.env.PORTAL_URL;
    let obj = req.body;

    let user = new User();
    user.first_name = obj.first_name;
    user.last_name = obj.last_name;
    user.company_id = createdBy.company_id._id;
    user.address = obj.address;
    user.phone = obj.phone;
    user.role = obj.role || createdBy.role;
    const sp = await SubscriptionService.getSubscriptionPackageById(obj.subscription_package);
    if (sp) {
      user.subscription_package = obj.subscription_package;
      user.subscription_type = sp.subscription_name;
      user.subscription_expire_on = 30 * 24 * 60 * 60 * 1000;
    }
    user.email = obj.email;
    user.is_company_admin = obj.is_company_admin || false;
    user.parent_id = req.payload._id;
    user.status = "active";
    var saveUser = await user.save();
    var token = jwt.sign({
      _id: saveUser._id
    }, config.secretKey);
    var setPasswordLink = url + "/public/set-password/" + token
    await emailTemplateService.sendEmail('invitation-to-child-user-under-company-admin-set-password', { user, createdBy }, user.email, setPasswordLink)  //invitation to ste password 

    return res.send({ success: true, data: saveUser });
  }
  catch (err) {
    common.log("Error while add child user----- ", err);
    return res.status(400).send({ success: false, message: err.message });
  }
}

module.exports.editChildUser = async function (req, res) {
  try {
    let obj = req.body;
    let user = await User.findById(req.params.id);
    if (user) {
      user.first_name = obj.first_name;
      user.last_name = obj.last_name;
      user.address = obj.address;
      user.phone = obj.phone;
      user.is_company_admin = obj.is_company_admin;
      user.role = obj.role || createdBy.role;
      const sp = await SubscriptionService.getSubscriptionPackageById(obj.subscription_package);
      if (sp) {
        user.subscription_package = obj.subscription_package;
        user.subscription_type = sp.subscription_name;
      }

      let userUpsert = user.toObject();
      delete userUpsert._id;
      const u = await User.findOneAndUpdate({ _id: req.params.id }, userUpsert, { upsert: false });
      return res.send({ success: true, data: u });
    } else {
      return res.status(400).send({ success: false, message: 'User not found' });
    }
  }
  catch (err) {
    common.log("Error while add child user----- ", err);
    return res.status(400).send({ success: false, message: err.message });
  }
}

module.exports.getChildUsers = async function (req, res) {
  try {
    var limit = req.body.limit || 20;
    var skip = ((req.body.skip || 1) - 1) * limit;
    let sortBy = req.body.sort_by || "created_at";
    let sortObj = {};
    sortObj[sortBy] = req.body.sort_direction == 'asc' ? 1 : -1;
    let searchObj = {
      parent_id: req.payload._id
    };

    if (req.body.first_name) searchObj.first_name = { $regex: req.body.first_name, $options: 'i' };
    if (req.body.last_name) searchObj.last_name = { $regex: req.body.last_name, $options: 'i' };
    if (req.body.email) searchObj.email = { $regex: req.body.email, $options: 'i' };
    if (req.body.phone) searchObj.phone = { $regex: req.body.phone, $options: 'i' };


    let [count, users] = await Promise.all([User.count(searchObj), User.find(searchObj).populate('subscription_package').sort(sortObj).skip(skip).limit(limit).lean()]);
    return res.send({ success: true, data: users, count: count });
  }
  catch (err) {
    common.log("Error While get child users list");
    return res.status(400).send({ success: false, message: err.message });
  }
}
module.exports.getChildUser = async function (req, res) {
  try {
    let user = await User.findById(req.params.id).lean();
    return res.send({ success: true, data: user });
  }
  catch (err) {
    common.log("Error While get child users list");
    return res.status(400).send({ success: false, message: err.message });
  }
}

module.exports.setPassword = async function (req, res) {
  try {
    const token = req.body.token;
    jwt.verify(token, config.secretKey, async (err, userDetail) => {
      if (err) {
        common.log("Error while set password", err);
        return res.status(400).send({ success: false, message: err.message });
      }
      if (!userDetail) {
        return res.status(400).send({ success: false, message: "User details not found" });
      }
      try {
        var user = await User.findById(userDetail._id);
        user.setPassword(req.body.password);
        var upsertData = user.toObject();
        delete upsertData._id;
        await User.updateOne({ _id: userDetail._id }, upsertData, { upsert: true });
        return res.send({ success: true, message: "Password set successful" });
      }
      catch (err) {
        return res.status(400).send({ success: false, message: err.message });
      }
    })
  }
  catch (err) {
    common.log("Error while set password", err);
    return res.status(400).send({ success: false, message: err.message });
  }
}

module.exports.getChildUserDetailByTaken = async function (req, res) {
  try {
    const token = req.body.token;
    jwt.verify(token, config.secretKey, async (err, userDetail) => {
      if (err) {
        common.log("Error while set password", err);
        return res.status(400).send({ success: false, message: err.message });
      }
      if (!userDetail) {
        return res.status(400).send({ success: false, message: "User details not found" });
      }
      var user = await User.findById(userDetail._id);
      return res.send({ success: true, data: user });
    })
  }
  catch (err) {
    common.log("Error while set password", err);
    return res.status(400).send({ success: false, message: err.message });
  }
}

module.exports.checkChildUserEmail = async function (req, res) {
  try {
    let [user, tempUser] = await Promise.all([
      User.findOne({ email: req.body.email }).populate('company_id').lean(),
      TemporaryUsers.findOne({ email: req.body.email }).populate('company_id').lean()
    ]);
    if (user) {
      return res.send({ success: false, tempUser: false, data: user })
    } else if (tempUser) {
      return res.send({ success: false, tempUser: true, data: tempUser })
    }
    return res.send({ success: true, message: 'User not register yet' });
  }
  catch (err) {
    common.log("Error while check duplicate email for child user--- ", err);
    return res.send(400).send({ success: false, message: err.message })
  }
}

module.exports.checkPhoneNumber = async function (req, res) {
  try {
    let query = { phone: req.body.phone }
    if (req.body.temp_id) {
      query['_id'] = { $ne: ObjectId(req.body.temp_id) }
    }
    let [user, tempUser] = await Promise.all([
      User.findOne({ phone: req.body.phone }).populate('company_id').lean(),
      TemporaryUsers.findOne(query).populate('company_id').lean()
    ]);
    if (user) {
      return res.send({ success: false, tempUser: false, data: user })
    } else if (tempUser) {
      return res.send({ success: false, tempUser: true, data: tempUser })
    }
    return res.send({ success: true, message: 'User not register yet' });
  }
  catch (err) {
    common.log("Error while check duplicate email for child user--- ", err);
    return res.send(400).send({ success: false, message: err.message })
  }
}

module.exports.switchChildUserCompany = async function (req, res) {
  if (!req.body.parent_id) {
    return res.status(400).send({ success: false, message: 'parent_id is required' })
  }
  if (!req.body.company_id) {
    return res.status(400).send({ success: false, message: 'company_id is required' })
  }
  try {
    let [user, parentUser] = await Promise.all([
      User.findById(req.payload._id),
      User.findById(req.body.parent_id),
    ]);
    if (user) {
      user.company_id = req.body.company_id;
      user.parent_id = req.body.parent_id;
      let userUpsert = user.toObject();
      delete userUpsert._id;
      await User.updateOne({ email: req.body.email }, userUpsert, { upsert: true });
      await emailTemplateService.sendEmail('change-company-success', { user, user1: parentUser }, user.email);
      return res.send({ success: true, message: 'register user in your company' });
    } else {
      return res.send({ success: false, message: 'User not found' });
    }
  }
  catch (err) {
    common.log("Error while change child user company--- ", err);
    return res.send(400).send({ success: false, message: err.message })
  }
}
module.exports.sendInvitationToJoinCompany = async function (req, res) {
  try {
    let [user, my] = await Promise.all([
      User.findOne({ email: req.body.email }).lean(),
      User.findById(req.payload._id).populate('company_id').lean()
    ]);
    if(user && my){
      const url = req.headers.origin ||  process.env.PORTAL_URL || 'https://app.forkfreight.com';
      let token = await emailTemplateService.companyTokenGeneration(user,my);
      let link = url+'/join-company/'+token
      await emailTemplateService.sendEmail('invitation-to-join-company-if-user-already-registered',{user,my,company:my.company_id},user.email,link);
      return res.send({success:true, message:'Invitation sent'});
    }else{
      return res.send({success:false, message:'User not found'});
    }
  }
  catch (err) {
    common.log("Error while sending child user--- ", err);
    return res.send(400).send({ success: false, message: err.message })
  }
}
module.exports.getDetailsFromInvitationToken = async function (req, res) {
  try {
    const token = req.params.token;
    jwt.verify(token, config.secretKey, async (err, userDetail) => {
      if (err) {
        common.log("Error while set password", err);
        return res.status(400).send({ success: false, message: err.message });
      }
      if (!userDetail) {
        return res.status(400).send({ success: false, message: "User details not found" });
      }
      let [user, requestedBy] = await Promise.all([
        User.findOne({ email: userDetail.user_id }).lean(),
        User.findById(userDetail.requested_user_id).populate('company_id').lean()
      ]);
      return res.send({ success: true, data: { user, requestedBy } });
    })
  }
  catch (err) {
    common.log("Error while sending child user--- ", err);
    return res.send(400).send({ success: false, message: err.message })
  }
}

module.exports.markedAsCompanyAdmin = async function (req, res) {
  try {
    let user = await User.findById(req.params.id);
    if (user) {
      user.is_company_admin = req.body.is_company_admin;
      let userUpsert = user.toObject();
      delete userUpsert._id;
      let u = await User.findOneAndUpdate({ _id: ObjectId(req.params.id) }, userUpsert, { upsert: true }).lean();
      return res.send({ success: true, message: 'User marked as company admin' });
    } else {
      return res.send({ success: false, message: 'User not found' });
    }
  }
  catch (err) {
    common.log("Error while change child user company--- ", err);
    return res.send(400).send({ success: false, message: err.message })
  }
}

module.exports.getMyAccessList = async function (req, res) {
  try {
    const user = await User.findById(req.payload._id);
    let modules_list = []
    let siteModule_list = await SubscriptionService.getModuleAccessableList(user.role);
    let isRolePrivate = await SubscriptionService.isRolePrivate(user.role);
    if (isRolePrivate) {
      siteModule_list.forEach(siteModule => {
        siteModule.access = true;
      });
    } else if (user.subscription_package) {
      let sp = await SubscriptionService.getSubscriptionPackageById(user.subscription_package);
      if(sp){
        siteModule_list.forEach(function(obj){
          if(obj.full_access){
            obj.access=true;
          }else{
            let idx = sp.module_access.findIndex(function(_sp){ return _sp.toString() == obj._id; });
            obj.access= idx>=0 ? true : false;
            if(obj.company_admin){
              if(user.is_company_admin){
                obj.access=true;
              }else{
                obj.access=false;
              }
            } else{
              obj.access = true;
            }
          }
          // modules_list.push(obj);
        })
      }else{
        siteModule_list.forEach(siteModule => {
          if(siteModule.full_access){
            siteModule.access=true;
          }else if(siteModule.company_admin && user.is_company_admin){
            siteModule.access=true;
          }else{
            siteModule.access=false;
          }
        });
      }
    }else{
      siteModule_list.forEach(siteModule=>{
        if(siteModule.full_access){
          siteModule.access=true;
        }else if(siteModule.company_admin && user.is_company_admin){
          siteModule.access=true;
        }else{
          siteModule.access=false;
        }
      });
    }
    if(!user.company_admin){
      let idx = siteModule_list.findIndex(obj=>obj.company_admin);
      siteModule_list.splice(idx,1);
    }
    modules_list = siteModule_list;
    return res.send({ success: true, "role": user.role,"is_public":!isRolePrivate, "is_company_admin": user.is_company_admin, access_module_list: modules_list })
  }
  catch (err) {
    common.log("Error While get my access list", err);
    return res.send({ success: false, message: err.message });
  }
}


module.exports.signUpInOneStep = async function (req, res) {
  try {
    var user = new User();
    if(!req.body.company_id){
      let company_addition_response= await addCompany(
        {company_name,
        address,
        city,
        state,
        company_email,
        company_phone,
        mc_number
        }= req.body
        );
       //set the company id 
      req.body.company_id = company_addition_response._id;
    }
    user.first_name = req.body.first_name;
    user.last_name = req.body.last_name;
    user.company_id = req.body.company_id;
    user.is_company_admin = req.body.is_company_admin;
    user.address = req.body.address;
    user.phone = req.body.phone;
    user.source_data = "ForkFreightAdmin";
    user.mc_Number = req.body.mc_Number;
    user.role = req.body.role;
    user.email = req.body.email;
    user.created_by = req.payload._id;
    
    if(req.body.equipment_type.length>0){
      user.equipment_type=req.body.equipment_type;
    }
    user.setPassword(req.body.first_name + "_" + req.body.role);
    if(req.body.subscription_package){
      const sp = await SubscriptionService.getSubscriptionPackageById(req.body.subscription_package)
      user.subscription_package = req.body.subscription_package;
      user.subscription_type = sp.subscription_name;

      if (req.body.paymentMethod && req.body.paymentMethod.id) {
        const customer = await stripeHelper.createCustomer(req.body.email, req.body.first_name + ' ' + req.body.last_name, req.body.paymentMethod.id);
        //const paymentMethod = await stripeHelper.attachPaymentMethod(customer.id,req.body.paymentMethod.id)
        const subscription = await stripeHelper.createSubscription(customer.id, sp.stripe_plan_id, true, null, sp.stripe_price_id,req.body?.coupon);
        // console.log("subscription response:___", subscription,"Json stringify:___\n");   
        user.stripe_id = customer.id;
        user.subscription_expire_on=new Date(subscription.current_period_end * 1000);
        user.payment_status= subscription.status
        // throw "wait"    
        let usd = new UserStripeDetail();
        usd.created_by= req.payload._id;
        usd.updated_by = req.payload._id;
        usd.user = user._id;
        usd.customer = customer;
        usd.paymentMethod = req.body.paymentMethod
        usd.subscription = subscription;
        usd.save();
      }
    }
    if (req.body.temp_id) {
      await TemporaryUsers.deleteOne({ _id: ObjectId(req.body.temp_id) });
    }
    // console.log("user to save:__", user);
    // throw "call here"
    const saveUser = await user.save();   
    

    var token = jwt.sign({
      _id: saveUser._id
    }, config.secretKey);
    const url = req.headers.origin || process.env.PORTAL_URL;
    //common.sendSetPasswordLinkByMarketing(saveUser, url, token);
    var setPasswordLink = url + "/public/set-password/" + token
    await emailTemplateService.sendEmail('create-password', { user:saveUser }, user.email, setPasswordLink)

    /**
     * add user to telemarketing too 
     */
   // crmHelper.add_or_modify_crm_contact(saveUser);
    
    return res.send({ success: true })
  }
  catch (err) {
    common.log("Error while registering user in single sign up process", err);
    return res.status(400).send({ success: false, message: err.message });
  }
}

/**
 * validate email for presignup process
 */
module.exports.validateEmail= async(req,res)=>{
  if (!req.body.email) {
    return res.status(400).send({ filed: 'email', message: "email is required" });
}
try {
    var user = await User.findOne({ email: req.body.email }).exec();
    var errorMessage= "This email is already registered. Click 'Forgot' to reset your password";
    if (user) {
        // return res.status(400).send({ status: false, message: errorMessage });
        throw new Error(errorMessage);
    } else {
        var tempUser = await TemporaryUsers.findOne({ email: req.body.email }).populate('company_id').exec();
         if (tempUser) {
            return res.status(200).send({ status: true,userdata: tempUser });
        }
    }
    return res.status(200).send({ status: true,userdata:[]});
}
catch (err) {
    common.log('Error while check email validation', err);
    return res.status(400).send({ success: false, message: err.message })
}

}

// Private Functions

const _login = function (req, res, isCheckLastLogin = true) {
  passport.authenticate('local', async function (err, user, info) {
    var token;
    let date = new Date();
    date.setDate(date.getDate() - 1)
    if (err) {
      res.status(404).json(err);
      return;
    }
    if (user) {

      if (user.status === 'inactive') {
        res.status(200).json({ code: 424, message: "Your Account is Inactive, please contact the support team" });
        return;
      }

      if (isCheckLastLogin && user.last_login_at !== null && user.device_id != null && user.device_id != req.body.device_id) {
        res.status(400).json({ code: 423, message: "Your Account is login on another device please logout and try later" });
        return;
      }
      token = user.generateJwt();
      _updateLastLogin({ id: user.id, device_id: req.body.device_id }, async function (err) {
        if (err) {
          console.error("Error while update last login time --->", err)
          return res.status(400).send(err.message);
        }
        //get subscriptionDetails
        let modules_list = [];
        let list = await SubscriptionService.getModuleAccessableList(user.role);
        let isRolePrivate = await SubscriptionService.isRolePrivate(user.role);
        if (isRolePrivate) {
          list.forEach(obj => {
            obj.access = true;
          });
        } else if (user.subscription_package) {
          let sp = await SubscriptionService.getSubscriptionPackageById(user.subscription_package);
          if(sp){
            list.forEach(function(obj){
              if(obj.full_access){
                obj.access=true;
              }else {
                let idx = sp.module_access.findIndex(function(_sp){
                    return _sp.toString() == obj._id;
                })
                obj.access= idx>=0 ? true : false;
                if(obj.company_admin){
                  if(user.is_company_admin){
                    obj.access=true;
                  }else{
                    obj.access=false;
                  }
                }else{
                  obj.access = true;
                }
              }
              // modules_list.push(obj);
            })
          }else{
            siteModule_list.forEach(siteModule => {
              if(siteModule.full_access){
                siteModule.access=true;
              }else if(siteModule.company_admin && user.is_company_admin){
                siteModule.access=true;
              }else{
                siteModule.access=false;
              }
            });
          }
        }else{
          list.forEach(obj=>{
            if(obj.full_access){
              obj.access=true;
            }else if(obj.company_admin && user.is_company_admin){
              obj.access=true;
            }else{
              obj.access=false;
            }
          });
        }
        if(!user.company_admin){
          let idx = list.findIndex(obj=>obj.company_admin);
          list.splice(idx,1);
        }
        modules_list = list;
        res.status(200).json({ "id": user._id, "token": token, "role": user.role,"is_public":!isRolePrivate, "is_company_admin": user.is_company_admin, access_module_list: modules_list });
      })
    } else {
      res.status(200).json(info);
    }
  })(req, res);
}

const _updateLastLogin = function ({ id, device_id }, callback) {
  User.updateOne({ _id: id }, { $set: { device_id: device_id, last_login_at: parseInt((new Date()).getTime() / 1000) } }, { upsert: true }, callback)
}
/**
 * process signup 
 */
const processSignup= async(reqbody)=>{
   try{
      /**
       * if request body doesn't include company_id add new company_id
       */
      if(!reqbody.company_id){
        let company_addition_response= await addCompany(
          {company_name,
          address,
          city,
          state,
          company_email,
          company_phone,
          mc_number
          }= reqbody
          );
         //set the company id 
        reqbody.company_id = company_addition_response._id;
      }
      /**
       * setting company data
       */
      let companyUserCount = await User.count({ company_id: reqbody.company_id });
      reqbody.is_company_admin = companyUserCount == 0 ? true : false;
        //password should not be save in plan text in DB
        if(reqbody.password){
          reqbody.password= btoa(reqbody.password)
        }
        
      // insert or update temporary users
      let resp= await createTempUserIfMissing(reqbody);
       //for temp user indication to crm for incomplete users
       resp.signup_complete="false";

      // crmHelper.add_or_modify_crm_contact(resp);
      return resp;
   }catch(error){
     common.log("Error While register step ", error);
     throw new Error(error);
      // return res.status(400).send(err)
   }
}
/** 
 * add company if not found
 */
const addCompany = async function (company_obj) {
  try {
      let company = new Company();
      company.company_name = company_obj.company_name;
      company.address = company_obj.address;
      company.city = company_obj.city;
      company.state = company_obj.state;
      company.email = company_obj.company_email;
      company.phone = company_obj.company_phone;
      company.mc_number = company_obj.mc_number;
      company.save();
      // return res.send({ success: true, message: 'Company saved successful', data: company });
       return company;
  }
  catch (err) {
      common.log("Error while add company--- ", err);
      // return res.status(400).send({ success: false, message: err.message });
      throw new Error(err);
  }
}

/**
 * create user in tempUsers collection if missing else update user
 */

const createTempUserIfMissing= async(reqbody)=>{
  try {
    let temporaryUsers;
    let tu;
    let returnData;
    let isEdit = false;
    if (reqbody.id) {
       tu = await TemporaryUsers.findById(reqbody.id).exec();
    }
    if (tu) {
      isEdit = true;
      temporaryUsers = tu.toObject();
      delete temporaryUsers._id;
    }
    else {
      temporaryUsers = new TemporaryUsers();
    }
    
      let upsertData=(({id,...others})=>({...others}))(reqbody);
      // temporaryUsers= {...temporaryUsers, ...upsertData}
      console.log(temporaryUsers)
      Object.assign(temporaryUsers,upsertData);
      
    // if (reqbody.address) temporaryUsers.address = reqbody.address;
    if (isEdit) {
      returnData = await TemporaryUsers.findOneAndUpdate({ _id: reqbody.id }, temporaryUsers, { new: true, upsert: true }).exec();
    }
    else {
      returnData = await temporaryUsers.save();
    }
    return returnData;
  }
  catch (err) {
    common.log("Error in func(): createTempUserIfMissing:__", err);
    // return res.status(400).send(err)
    throw new Error(err.message);
  }
}