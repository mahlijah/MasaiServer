var mongoose = require('mongoose');
var User = mongoose.model('User');
var Load = mongoose.model('Load');
var Truck = mongoose.model('Truck');
var common = require('../helpers/common');
var crypto = require('crypto');
var config = require('../config/config');
var fs = require('fs');
var multer = require('multer')
var upload = multer();
var path = require('path');
var mammoth = require("mammoth");
const _ = require('lodash');
var async = require("async");


var distance = require('google-distance');
distance.apiKey  = config.googleGoecoder_key;

module.exports.revanueChart = function (req, res) {
    var today = new Date();
    var current_year = today.getFullYear();
    Load.find({}, function( error, responce){
        var data = {};
        data.load = {};
        data.load.jan = 0;
        data.load.fab = 0;
        data.load.mar = 0;
        data.load.apr = 0;
        data.load.may = 0;
        data.load.jun = 0;
        data.load.jly = 0;
        data.load.agu = 0;
        data.load.sep = 0;
        data.load.oct = 0;
        data.load.nov = 0;
        data.load.dec = 0;
        responce.forEach(element => {    
                
            if( element.distance === 0 ){
                if( element.delivery_addresses !== undefined && element.pickup_addresses !== undefined){
                    distance.get({
                        origin: element.delivery_addresses,
                        destination: element.pickup_addresses},
                    function(err, data) {
                        if(data !== undefined){
                            common.log(data.distanceValue)
                            var total_ditance_mile  = Math.floor(data.distanceValue*0.000621371);
                            Load.findOneAndUpdate({ _id: element._id }, { $set: { distance:  total_ditance_mile} }, { 'new': true })
                            .exec(function (err, load) {
                                if (err) {
                                    common.log("error")
                                }else{
                                    common.log("Updated !")
                                }
                            });
                        }
                    });
                }
            }          
            // var distance = calculateDistance(delivery_lat, delivery_lng, pickup_lat, pickup_long);
            var distance_in_mile = element.distance; 
            var permile = 2.31;
            var feulprice = distance_in_mile*permile;
            var revanue_price  =  element.target_rate - feulprice;         

            var load_create_date = new Date(element.createdAt);
            var load_create_year = load_create_date.getFullYear();
            var load_create_month = load_create_date.getMonth()+1; 
            var checkNun = Number.isNaN(revanue_price);
            if( checkNun !== true){
                if( load_create_year == current_year && load_create_month == 1 ){
                    data.load.jan+=revanue_price;
                }else if( load_create_year == current_year && load_create_month == 2 ){ 
                    load.fab+=revanue_price;
                }else if( load_create_year == current_year && load_create_month == 3 ){ 
                    data.load.mar+=revanue_price;
                }else if( load_create_year == current_year && load_create_month == 4 ){ 
                    data.load.apr+=revanue_price;
                }else if( load_create_year == current_year && load_create_month == 5 ){ 
                    data.load.may+=revanue_price;
                }else if( load_create_year == current_year && load_create_month == 6 ){ 
                    data.load.jun+=revanue_price;
                }else if( load_create_year == current_year && load_create_month == 7 ){ 
                    common.log(revanue_price)
                    data.load.jly+=revanue_price;
                }else if( load_create_year == current_year && load_create_month == 8 ){ 
                    data.load.agu+=revanue_price;
                }else if( load_create_year == current_year && load_create_month == 9 ){ 
                    data.load.sep+=revanue_price;
                }else if( load_create_year == current_year && load_create_month == 10 ){ 
                    data.load.oct+=revanue_price;
                }else if( load_create_year == current_year && load_create_month == 11 ){ 
                    data.load.nov+=revanue_price;
                }else if( load_create_year == current_year && load_create_month == 12 ){ 
                    data.load.dec+=revanue_price;
                }
            }
            
        });

        res.status(200).json({data});
        return;    
        
        
    });
    
    var calculateDistance = function(lat1, lon1, lat2, lon2) {
        var R = 6371; // km
        var dLat = (lat2-lat1).toRad();
        var dLon = (lon2-lon1).toRad();
        var lat1 = lat1.toRad();
        var lat2 = lat2.toRad();
    
        var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
        var d = R * c;
        return d;
    }
    Number.prototype.toRad = function() {
        return this * Math.PI / 180;
    }

    function distance2(element){
        distance.get({
            origin: element.delivery_addresses,
            destination: element.pickup_addresses},
          function(err, data) {
            //if (err) return common.log(err);
            if(data !== undefined){
                return JSON.stringify(data);
            }
        });   
    }
        
        
};
