
var mongoose = require('mongoose');
var Truck = mongoose.model('Truck');
var TruckWatchedUsers = mongoose.model('TruckWatchedUsers');
var TrucksByCsvUpload = mongoose.model('TrucksByCsvUpload');
var Load = mongoose.model('Load');
var User = mongoose.model('User');
var TemporaryUsers = mongoose.model('TemporaryUsers');
var config = require('../config/config');
var common = require('../helpers/common');
var async = require("async");
const response = require('http-status-codes');
var Transaction = mongoose.model('Transaction');
var moment = require('moment');
const { count } = require('../models/price');
const { ObjectId } = require('bson');
var s3FileDownload = require('../helpers/s3FileDownload').s3FileDownload;

var url = require('url');
const REDIS_URL = config.loadJobs.redisUri;
const redis_uri = url.parse(REDIS_URL);
const redisOptions = REDIS_URL.includes("rediss://")
  ? {
    port: Number(redis_uri.port),
    host: redis_uri.hostname,
    password: redis_uri.auth.split(":")[1],
    maxRetriesPerRequest: 0,
    tls: {
      rejectUnauthorized: false,
      servername: redis_uri.hostname
    },
    enableReadyCheck: false
  }
  : REDIS_URL;
var Queue = require('bull');
var newCSVTruckQueue = new Queue(config.truckJobs.newCsvTrucksQueue.name, { redis: redisOptions });

module.exports.trucksAndLoadsInState = function (req, res) {
  var truckList = [];
  async.waterfall([
    function (callback) {
      Truck.find({ origin_bystate_states: { $elemMatch: req.params.state } })
        .exec(function (err, trucks) {
          common.log("data of user is ", trucks);
          if (err) {
            return callback(err);
          } else {
            if (!trucks) {
              return callback('No user Found.')
            } else {
              callback(null, trucks);
            }
          }
        });
    },
    function (trucks, callback) {
      truckList = trucks;
      Load.find({ origin_bycity_state: req.params.state })
        .exec(function (err, loads) {
          if (err) {
            return callback(err);
          } else {
            if (!loads) {
              return callback('No load found.')
            } else {
              callback(null, loads);
            }
          }
        });
    },

  ],
    function (err, loads) {
      if (err) {
        res.json(err)
      }
      else {
        var data = {};
        data.trucks_count = truckList.length;
        data.loads_count = loads.length;
        data.state = req.params.state;
        res.json(data, 'Trucks and Loads count in a state');
      }
    }
  );
}

module.exports.findTrucks = function (search_object, socket, callback) {
  'use strict';
  common.log('search object', search_object);
  var search_options = {};
  var limitResults = 20;
  var skipResults = 0;

  // search_options["is_opened"] = true;
  // search_options["created_by"] = { $ne: socket.decoded._id };

  if (search_object !== undefined) {
    if (search_object.pickup_states) {
      if (search_object.pickup_states.length != 0) {
        // search_options['origin_bystate_states'] = {$in: search_object.pickup_states}
        search_options.$and = [{
          $or: [{
            'origin_bystate_states': {
              $in: search_object.pickup_states
            }
          }, {
            'origin_bycity_state': {
              $in: search_object.pickup_states
            }
          }]
        }]

      }
    }
    if (search_object.delivery_states) {
      if (search_object.delivery_states.length != 0) {
        // search_options['destination_bystate_states'] = {$in: search_object.delivery_states}
        search_options.$and = [{
          $or: [{
            'destination_bystate_states': {
              $in: search_object.delivery_states
            }
          }, {
            'destination_bycity_state': {
              $in: search_object.delivery_states
            }
          }]
        }]
      }
    }
    if (search_object.delivery_states && search_object.pickup_states) {
      if (search_object.delivery_states.length != 0 && search_object.pickup_states.length != 0) {
        search_options.$and = [
          {
            $or: [{
              'origin_bystate_states': {
                $in: search_object.pickup_states
              }
            }, {
              'origin_bycity_state': {
                $in: search_object.pickup_states
              }
            }]
          },
          {
            $or: [{
              'destination_bystate_states': {
                $in: search_object.delivery_states
              }
            }, {
              'destination_bycity_state': {
                $in: search_object.delivery_states
              }
            },
            {
              'destination_bystate_states': {
                $eq: []
              }
            }
            ]
          }
        ]
      }
    }

    if (search_object.equipment_types) {
      if (search_object.equipment_types.length != 0) {
        search_options.$and['equipment_type'] = { $in: search_object.equipment_types }
      }
    }


    if (search_object.start_date && search_object.start_date !== undefined) {
      search_options['ready_date'] = {
        $gte: new Date(search_object.start_date).toLocaleDateString(),
      }
    }

    if (search_object.start_date && search_object.end_date !== undefined) {
      search_options['ready_date'] = {
        $gte: new Date(search_object.start_date).toLocaleDateString(),
        $lte: new Date(search_object.end_date).toLocaleDateString()
      }
    }

    if (search_object.limit != null && search_object.limit != '') {
      limitResults = search_object.limit;
    }

    if (search_object.skip != null && search_object.skip != '') {
      skipResults = search_object.skip;
    }
    // if (search_object.date_available) {
    //   search_options['ready_start'] = { $gte: new Date(search_object.date_available) }
    // }

    // if (search_object.start_date && search_object.end_date) {
    //   search_options.$and['ready_start'] = {
    //     $gte: new Date(search_object.start_date),
    //     $lte: new Date(search_object.end_date)
    //   }
    // }
    // if (search_object.start_date && !search_object.end_date) {
    //     search_options.$and['ready_start'] = new Date(search_object.start_date);
    // }

    // if (search_object.end_date && !search_object.start_date) {
    //     search_options.$and['ready_end'] = new Date(search_object.end_date)
    // }
    // if(search_object.start_date){
    //   search_options['ready_start'] = {
    //     $gte: new Date(search_object.start_date),
    //     $lte: new Date(search_object.end_date)
    //   }
    // }
  }
  common.log("search options", search_options);
  Truck.find(search_options)
    .populate('created_by')
    .sort({ updatedAt: -1 })
    .limit(limitResults)
    .skip(skipResults)
    .exec(function (err, trucks) {
      callback(trucks);
    });

};

module.exports.findTruckshttp = async function (req, res) {
  'use strict';
  var search_object = req.body;
  common.log(search_object);
  try {

    var search_options = {};
    var limitResults = 20;
    var skipResults = 1;

    var sort_by = req.body.sort_by || 'ready_date';
    var sort_direction = req.body.sort_direction == 'asc' ? 1 : -1;
    var sortObj = {};
    var sort_split = sort_by.split(',');
    for (let index = 0; index < sort_split.length; index++) {
      sortObj[sort_split[index].trim()] = sort_direction;
    }

    search_options["$and"] = [];
    let isOpened = { "is_opened": true };
    search_options["$and"].push(isOpened);
    // let readDate = { "ready_end": { $gte: new Date().toLocaleDateString("af-ZA") } }
    // search_options["$and"].push(readDate);
    // search_options['ready_end'] = {     
    //   $gte: new Date(),
    // }
    // search_options["created_by"] = { $ne: socket.decoded._id };

    if (search_object !== undefined) {
      // search_options = await common.generateSearchObject(search_object);
      if (search_object.pickup_states) {
        if (search_object.pickup_states.length != 0) {
          // search_options['origin_bystate_states'] = {$in: search_object.pickup_states}
          search_options["$and"].push({
            $or: [{
              'origin_bystate_states': {
                $in: search_object.pickup_states
              }
            }, {
              'origin_bycity_state': {
                $in: search_object.pickup_states
              }
            }]
          })

        }
      }
      if (search_object.delivery_states) {
        if (search_object.delivery_states.length != 0) {
          // search_options['destination_bystate_states'] = {$in: search_object.delivery_states}
          search_options["$and"].push({
            $or: [{
              'destination_bystate_states': {
                $in: search_object.delivery_states
              }
            }, {
              'destination_bycity_state': {
                $in: search_object.delivery_states
              }
            }]
          })
        }
      }
      if (search_object.delivery_states && search_object.pickup_states) {
        if (search_object.delivery_states.length != 0 && search_object.pickup_states.length != 0) {
          search_options["$and"].push(
            {
              $or: [{
                'origin_bystate_states': {
                  $in: search_object.pickup_states
                }
              }, {
                'origin_bycity_state': {
                  $in: search_object.pickup_states
                }
              }]
            },
            {
              $or: [{
                'destination_bystate_states': {
                  $in: search_object.delivery_states
                }
              }, {
                'destination_bycity_state': {
                  $in: search_object.delivery_states
                }
              },
              {
                'destination_bystate_states': {
                  $eq: []
                }
              }
              ]
            }
          )
        }
      }

      if (search_object.equipment_types) {
        if (search_object.equipment_types.length != 0) {
          search_options["$and"].push(common.getEqupementTypeForSearch(search_object.equipment_types))
        }
      }


      if (search_object.start_date && !search_object.end_date ) {
        search_options["$and"].push({
          'ready_date': {
            $gte: search_object.start_date+'T00:00:00Z',
          }
        })
      }

      if (search_object.start_date && search_object.end_date) {
        search_options["$and"].push({
          'ready_date': {
            $gte: search_object.start_date+'T00:00:00Z',
            $lte: search_object.end_date+'T23:59:59Z'
          }
        })
      }

      if (!search_object.start_date && search_object.end_date) {
        search_options["$and"].push({
          'ready_date': {
            $lte: search_object.end_date+'T23:59:59Z'
          }
        })
      }

      if (search_object.date_available) {
        search_options["$and"].push({
          'ready_date': {
            $gte: new Date(search_object.date_available),
          }
        })
      }

      if(!search_object.start_date && !search_object.end_date && !search_object.date_available){
        var currentDate = new Date();
        currentDate.setHours(-96,0,0);
        search_options["$and"].push({
          'ready_date': {
            $gte: currentDate,
          }
        })
      }

      if (search_object.limit != null && search_object.limit != '') {
        limitResults = search_object.limit;
      }

      if (search_object.skip != null && search_object.skip != '') {
        skipResults = search_object.skip;
        if(skipResults <= 0 ) { skipResults = 1};
      }

    }
    common.log("search options", JSON.stringify(search_options));

    let totalcount = await Truck.find(search_options)
      .count();

    let trucks = await Truck.find(search_options,
      {
        _id:1,
        origin_bystate_states:1,
        destination_bystate_states:1,
        origin_bycity_address:1,
        origin_bycity_lat_long:1,
        origin_bycity_zip:1,
        origin_bycity_state:1,
        origin_bycity_city:1,
        origin_bycity_range:1,
        destination_bycity_address:1,
        destination_bycity_range:1,
        destination_bycity_lat_long:1,
        destination_bycity_zip:1,
        destination_bycity_state:1,
        destination_bycity_city:1,
        equipment_type:1,
        weight:1,
        length:1,
        rate_per_mile:1,
        description:1,
        ready_date: { $dateToString: { format: "%m/%d/%Y", date: "$ready_date" } },
        updatedAt:1,

      })
      .populate('created_by')
      .populate('company')
      .sort(sortObj)
      .limit(limitResults)
      .skip((skipResults - 1) * limitResults)
      .allowDiskUse(true)
      .lean()

    let result = {};
    result["count"] = totalcount;
    result["data"] = trucks;
    return res.send(result);


  } catch (err) {
    common.log("error occured in database:", err);
    return res.status(500).send("error occured.");
  }
};



module.exports.postTruck = async(req, res) => {
  'use strict';
  let data = req.body;
  let truck;

  if (!req.payload._id) {
    res.status(401).json({ success: false, message: "UnauthorizedError: Private action" })
  } else {
    if (data.address_type_city == true) {
      if (data.origin_bycity_state.length < 0 && !data.destination_bycity_city && data.equipment_type.length < 0 && !data.weight && !data.length && !data.ready_date
      ) {

        res.status(404).json({ success: false, message: "Origin, weight, length, and ready date  are required" })
        return;
      }
      truck=truckPost(data)
      truck.created_by = req.payload._id;
      addTruckFun(truck, data, res);
    } else {
      if (data.destination_bystate_states.length < 0 && !data.origin_bystate_states && !data.weight && !data.length && !data.ready_date && data.equipment_type.length < 0
      ) {
        res.status(404).json({ success: false, message: "Origin, weight, length, and ready date  are required" })
        return;
      }
      truck=truckPost(data)
      truck.created_by = req.payload._id;
      addTruckFun(truck, data, res);
    }
  }

};

function truckPost(data){
  var truck = new Truck();
  truck.is_opened = true;
  truck.origin_bycity_address = data.origin_bycity_address;
  truck.origin_bycity_city = data.origin_bycity_city;
  truck.origin_bycity_state = data.origin_bycity_state;
  truck.origin_bycity_zip = data.origin_bycity_zip;
  truck.origin_bycity_lat_long = { type: "Point", coordinates: data.origin_bycity_lat_long ? data.origin_bycity_lat_long : [0, 0] };
  truck.origin_bycity_range = data.origin_bycity_range;
  truck.destination_bycity_address = data.destination_bycity_address;
  truck.destination_bycity_city = data.destination_bycity_city;
  truck.destination_bycity_state = data.destination_bycity_state;
  truck.destination_bycity_zip = data.destination_bycity_zip;
  truck.destination_bycity_lat_long = { type: "Point", coordinates: (data.destination_bycity_lat_long && data.destination_bycity_lat_long.length == 2) ? data.destination_bycity_lat_long : [0, 0] };
  truck.destination_bycity_range = data.destination_bycity_range;
  truck.origin_bystate_states = data.origin_bystate_states;
  truck.destination_bystate_states = data.destination_bystate_states;
  truck.equipment_type = data.equipment_type;
  truck.weight = data.weight;
  truck.length = data.length;
  truck.rate_per_mile = data.rate_per_mile;
  truck.ready_date = data.ready_date;
  truck.ready_end = +truck.ready_date + (+config.truckCloseAfter) * 60 * 60 * 1000;
  truck.description = data.description;
  return truck
}

module.exports.postTruckFree =async  (req, res) => {
  'use strict';
  let { data, email, phone } = req.body;
  let tempUser = await TemporaryUsers.findOne({ email: email })
  let user =await User.findOne({ email: email })
  let truck;
  var saveUser;
  if(user){
    return res.status(200).json({code:400,message:'user is already signup'})
  }
  if (!tempUser) {
    let user = new TemporaryUsers({
      email,
      phone
    })
    saveUser = await user.save();
  }
  let user_id = tempUser ? tempUser._id : saveUser._id
  
  if (data.address_type_city == true) {
    if (data.origin_bycity_state.length < 0 && !data.destination_bycity_city && data.equipment_type.length < 0 && !data.weight && !data.length && !data.ready_date
    ) {

      res.status(404).json({ success: false, message: "Origin, weight, length, and ready date  are required" })
      return;
    }

     truck = truckPost(data)
    truck.created_by = user_id;
    truck.source='free'
    addTruckFun(truck, data, res);

  } else {
    if (data.destination_bystate_states.length < 0 && !data.origin_bystate_states && !data.weight && !data.length && !data.ready_date && data.equipment_type.length < 0
    ) {

      res.status(404).json({ success: false, message: "Origin, weight, length, and ready date  are required" })
      return;
    }
     truck = truckPost(data)
    truck.created_by = user_id;
    truck.source='free'
    addTruckFun(truck, data, res);

  }
};



function addTruckFun(truck, data, res) {
  truck.save(function (err) {
    if (err) {
      res.status(404).json({ success: false, message: err.message })
    } else {
      res.status(200).json({ message: "add_truck_response", truck: truck, data: data, success: true })
    }
  });
}

module.exports.activateTruck = function (truckItem, socket) {
  'use strict';
  if (socket.decoded._id) {
    if (truckItem.created_by._id != socket.decoded._id) {
      socket.emit('activate_truck_response', { success: false, message: "UnauthorizedError: Private action" });
    } else {
      if (!truckItem.is_opened) {
        Truck.findOneAndUpdate({ _id: truckItem._id }, { $set: { is_opened: true } }, { 'new': true })
          .populate('created_by')
          .exec(function (err, truck) {
            if (err) {
              socket.emit('activate_truck_response', { success: false, message: err.message });
            } else {
              socket.emit('activate_truck_response', { success: true, message: "Truck has been activated successfully" });
              common.updateTrucks(truck, socket);
            }
          });
      } else {
        socket.emit('activate_truck_response', { success: false, message: "Truck has already been activated" });
      }
    }
  }
};

module.exports.closeTruck = (req, res) => {
  'use strict';
  let truckItem = req.body;
  if (!req.payload._id) {

    res.status(401).json({ success: false, message: "UnauthorizedError: Private action" });
  } else {

    Truck.findOneAndUpdate({ _id: req.params.id }, { $set: { is_opened: false } }, { 'new': true })
      .populate('created_by')
      .exec(function (err, truck) {
        if (err) {
          res.status(404).json({ success: false, message: err.message })

        } else {
          res.status(200).json({ success: true, message: "Truck has been closed successfully", truck: truck });
        }
      });


  }
}

module.exports.editTruck = async (req, res) => {
  'use strict';
  let data = req.body;

  if (!req.payload._id) {
    common.log("UnauthorizedError: Private action")
    res.status(401).json({ success: false, message: "UnauthorizedError: Private action" })
  } else {
    if (Object.keys(data).length == 0) {
      common.log("Data is empty")
      return res.status(404).json({ message: 'Data is empty' })
    } else {
      console.log(req.params.id)
      var truck = await Truck.findById({ _id: ObjectId(req.params.id) }).lean()

      if (!truck) {
        res.status(400).json({ message: 'Truck Object is empty' })
      } else {
        truck.origin_bycity_address = data.origin_bycity_address ? data.origin_bycity_address : truck.origin_bycity_address
        truck.origin_bycity_address = data.origin_bycity_address ? data.origin_bycity_address : truck.origin_bycity_address;
        truck.origin_bycity_city = data.origin_bycity_city ? data.origin_bycity_city : truck.origin_bycity_city;
        truck.origin_bycity_state = data.origin_bycity_state ? data.origin_bycity_state : truck.origin_bycity_state;
        truck.origin_bycity_zip = data.origin_bycity_zip ? data.origin_bycity_zip : truck.origin_bycity_zip;
        truck.origin_bycity_lat_long = data.origin_bycity_lat_long ? data.origin_bycity_lat_long : truck.origin_bycity_lat_long;
        truck.origin_bycity_range = data.origin_bycity_range ? data.origin_bycity_range : truck.origin_bycity_range;
        truck.destination_bycity_address = data.destination_bycity_address ? data.destination_bycity_address : truck.destination_bycity_address;
        truck.destination_bycity_city = data.destination_bycity_city ? data.destination_bycity_city : truck.destination_bycity_city;
        truck.destination_bycity_state = data.destination_bycity_state ? data.destination_bycity_state : truck.destination_bycity_state;
        truck.destination_bycity_zip = data.destination_bycity_zip ? data.destination_bycity_zip : truck.destination_bycity_zip;
        truck.destination_bycity_lat_long = data.destination_bycity_lat_long ? data.destination_bycity_lat_long : truck.destination_bycity_lat_long;
        truck.destination_bycity_range = data.destination_bycity_range ? data.destination_bycity_range : truck.destination_bycity_range;
        truck.origin_bystate_states = data.origin_bystate_states ? data.origin_bystate_states : truck.origin_bystate_states;
        truck.destination_bystate_states = data.destination_bystate_states ? data.destination_bystate_states : truck.destination_bystate_states;
        truck.equipment_type = data.equipment_type ? data.equipment_type : truck.equipment_type;
        truck.weight = data.weight ? data.weight : truck.weight;
        truck.length = data.length ? data.length : truck.length;
        truck.rate_per_mile = data.rate_per_mile ? data.rate_per_mile : truck.rate_per_mile;
        truck.ready_date = data.ready_date ? data.ready_date : truck.ready_date;
        truck.ready_end = data.ready_date ? +(new Date(data.ready_date)) + (+config.truckCloseAfter) * 60 * 60 * 1000 : truck.ready_end;
        truck.description = data.description ? data.description : truck.description;
        truckUpdate(req.params.id, truck, res)

      }

    }
  }

};
const truckUpdate = (orignal_truck_id, data, res) => {
  data.ready_end = +(new Date(data.ready_date)) + (+config.truckCloseAfter) * 60 * 60 * 1000;
  data.updated_at = Date.now();
  console.log(orignal_truck_id)
  Truck.update({ _id: orignal_truck_id }, data, { upsert: true }, function (err) {

    if (err) {
      common.log("Error", err)
      res.json({ success: false, message: err.message });
    } else {
      data._id = orignal_truck_id;

      Truck.findOne({ _id: data._id })
        .populate('created_by')
        .exec(function (err, truck) {
          if (truck) {
            return res.status(200).json({ message: "update Truck", truck })
          } else {
            common.log("Error", err)
            return res.status(404).json({ message: err })
          }
        });
    }

  });
}

//solve merge conflicts
module.exports.detailTruck = async (req, res) => {
  if (!req.payload._id) {
    res.status(401).json({ success: false, message: "UnauthorizedError: Private action" });
  } else {
    try {
      let truck_details = await Truck.findOne({ _id: req.params.id })
        .populate('company')
        .populate({
          path: 'created_by',
          populate: 'company_id'
        })
        .lean();
        let twu = await TruckWatchedUsers.findOne({ watched_users: ObjectId(req.payload._id), truck:req.params.id  }).lean();
        if(twu){
          truck_details.addToWatchlist=true;
          return   res.status(200).json({ message: "detail send succesfully", data: truck_details })   
        }else{
          truck_details.addToWatchlist=false;
          return   res.status(200).json({ message: "detail send succesfully", data: truck_details }) 
        }
      } catch (error) {
      res.status(404).json({ error });
    }
  }

};


module.exports.setReadyEnd = async function (req, res) {
  try {
    var trucks = await Truck.find();
    trucks.forEach(async function (truck) {
      try {
        await Truck.updateOne({ _id: truck._id }, { $set: { ready_end: +truck.ready_date + 24 * 60 * 60 * 1000 } }, { upsert: true })
      }
      catch (err) {
        common.log(err);
        throw err;
      }
    })
    return res.send("ok");
  }
  catch (err) {
    common.log("Error while set Ready_end", err);
    return res.status(500).send(err);
  }
}



module.exports.truckRefreshAge = (req, res) => {

  'use strict';
  let data = req.body;
  if (!req.payload._id) {
    res.status(401).json({ success: false, message: "UnauthorizedError: Private action" });
    return;
  } else {

    var orignal_truck_id = req.params.id;
    // delete data._id;
    var refAge = {
      updated_at: Date.now()
    }

    Truck.updateOne({ _id: orignal_truck_id }, refAge, { upsert: true }, function (err) {

      if (err) {

        res.status(404).json({ success: false, message: err.message })
      } else {
        data.truck_id = orignal_truck_id;
        Truck.findOne({ _id: data.truck_id })
          .populate('created_by')
          .exec(function (err, truck) {
            if (!err) {
              res.status(200).json({ message: "truckRefreshAge", truck: truck })
            }
          });
      }

    });

  }

};

module.exports.watchlist = function (socket, callback) {
  'use strict';
  if (socket.decoded._id) {
    Truck.find({ watched_users: socket.decoded._id, is_opened: true })
      .populate('created_by')
      .exec(function (err, trucks) {
        callback(trucks);
      });
  }

};

module.exports.watchlistHttp = async function (req, res) {
  try {
    var limitResults = 20;
    var skipResults = 0;
    var search_object = req.body;
    var sort_by = req.body.sort_by || 'ready_date';
    var sort_direction = req.body.sort_direction == 'asc' ? 1 : -1;
    var sortObj = {};
    var sort_split = sort_by.split(',');
    for (let index = 0; index < sort_split.length; index++) {
      sortObj[sort_split[index].trim()] = sort_direction;
    }
    let twu = await TruckWatchedUsers.find({ watched_users: ObjectId(req.payload._id) }).lean();
    let truckIds = twu.map(function (obj) { return obj.truck });
    //convert search parameters to mongodb search criteria
    let search_options = {};
    search_options["$and"] = [];
    let isOpened = { "is_opened": true };
    search_options["$and"].push(isOpened);
    // search_options["$and"].push({ "watched_users": req.payload._id });
    search_options["$and"].push(
      {
        $or: [
          { "watched_users": ObjectId(req.payload._id) },
          { '_id': { $in: truckIds } }
        ]
      }
    );

    if (search_object.pickup_states) {
      if (search_object.pickup_states.length != 0) {
        // search_options['origin_bystate_states'] = {$in: search_object.pickup_states}
        search_options['$and'].push({
          $or: [{
            'origin_bystate_states': {
              $in: search_object.pickup_states
            }
          }, {
            'origin_bycity_state': {
              $in: search_object.pickup_states
            }
          }]
        })

      }
    }
    if (search_object.delivery_states) {
      if (search_object.delivery_states.length != 0) {
        // search_options['destination_bystate_states'] = {$in: search_object.delivery_states}
        search_options['$and'].push({
          $or: [{
            'destination_bystate_states': {
              $in: search_object.delivery_states
            }
          }, {
            'destination_bycity_state': {
              $in: search_object.delivery_states
            }
          }]
        })
      }
    }
    if (search_object.delivery_states && search_object.pickup_states) {
      if (search_object.delivery_states.length != 0 && search_object.pickup_states.length != 0) {
        search_options['$and'].push(
          {
            $or: [{
              'origin_bystate_states': {
                $in: search_object.pickup_states
              }
            }, {
              'origin_bycity_state': {
                $in: search_object.pickup_states
              }
            }]
          },
          {
            $or: [{
              'destination_bystate_states': {
                $in: search_object.delivery_states
              }
            }, {
              'destination_bycity_state': {
                $in: search_object.delivery_states
              }
            },
            {
              'destination_bystate_states': {
                $eq: []
              }
            }
            ]
          })
      }
    }

    if (search_object.equipment_types) {
      if (search_object.equipment_types.length != 0) {
        search_options["$and"].push(common.getEqupementTypeForSearch(search_object.equipment_types));
      }
    }


    if (search_object.start_date && !search_object.end_date) {
      search_options['$and'].push({
        'ready_date': {
          $gte: search_object.start_date+'T00:00:00Z'
        }
      })
    }

    if (search_object.start_date && search_object.end_date) {
      search_options['$and'].push({
        'ready_date': {
          $gte: search_object.start_date+'T00:00:00Z',
          $lte: search_object.end_date+'T23:59:59Z'
        }
      })
    }

    if (!search_object.start_date && search_object.end_date) {
      search_options['$and'].push({
        'ready_date': {
          $lte: search_object.end_date+'T23:59:59Z'
        }
      })
    }

    if (search_object.date_available) {
      search_options['$and'].push({
        'ready_date': {
          $gte: new Date(search_object.date_available),
        }
      })
    }

    if(!search_object.start_date && !search_object.end_date && !search_object.date_available){
      var currentDate = new Date();
      currentDate.setHours(-96,0,0);
      search_options["$and"].push({
        'ready_date': {
          $gte: currentDate,
        }
      })
    }


    if (search_object.limit != null && search_object.limit != '') {
      limitResults = search_object.limit;
    }

    if (search_object.skip != null && search_object.skip != '') {
      skipResults = (search_object.skip - 1) * limitResults;
    }

    console.log(JSON.stringify(search_options));

    // let truck = await Truck.aggregate([
    //   {
    //     "$lookup": {
    //       from: 'truckwatchedusers',
    //       localField: "_id",
    //       foreignField: "truck",
    //       as: "twu"
    //     }
    //   },
    //   {
    //     "$lookup": {
    //       from: 'users',
    //       localField: "created_by",
    //       foreignField: "_id",
    //       as: "created_by"
    //     }
    //   },
    //   { $unwind: { path: "$twu", preserveNullAndEmptyArrays: true } },
    //   {
    //     $match: search_options
    //   }, {
    //     $facet: {
    //       "trucks_count": [{ "$group": { _id: null, count: { $sum: 1 } } }],
    //       "trucks": [
    //         {
    //           $sort: sortObj
    //         },
    //         { "$skip": skipResults },
    //         { "$limit": limitResults }
    //       ]
    //     },
    //   },
    //   { $unwind: "$trucks_count" },
    //   {
    //     $project: {
    //       count: "$trucks_count.count",
    //       data: "$trucks"
    //     }
    //   }
    // ])


    // //var mongoQuery = Load.find(search_options);
    // let totalcount = await Truck.find(search_options)
    //   .count()
    //   .exec();

    // let loads = await Truck.find(search_options)
    //   .populate('created_by')
    //   .sort(sortObj)
    //   .limit(limitResults)
    //   .skip(skipResults)
    //   .exec();
    let [count, data] = await Promise.all([
      Truck.count(search_options),
      Truck.find(search_options,
        {
          _id:1,
          origin_bystate_states:1,
          destination_bystate_states:1,
          origin_bycity_address:1,
          origin_bycity_lat_long:1,
          origin_bycity_zip:1,
          origin_bycity_state:1,
          origin_bycity_city:1,
          origin_bycity_range:1,
          destination_bycity_address:1,
          destination_bycity_range:1,
          destination_bycity_lat_long:1,
          destination_bycity_zip:1,
          destination_bycity_state:1,
          destination_bycity_city:1,
          equipment_type:1,
          weight:1,
          length:1,
          rate_per_mile:1,
          description:1,
          ready_date: { $dateToString: { format: "%m/%d/%Y", date: "$ready_date" } },
          updatedAt:1,
  
        }).sort(sortObj).skip(skipResults).limit(limitResults).lean()
    ])
    let result = {
      count,
      data
    };
    return res.send(result);
  }
  catch (err) {
    common.log("Error while get watch list truck", err);
    return res.status(500).send(err);
  }

};

module.exports.addToWatchlist = async function (truck_id, socket) {
  'use strict';
  common.log(truck_id)
  if (socket.decoded._id) {
    try {
      let truckWatchedUsers = await TruckWatchedUsers.findOne({ truck: truck_id }).lean();
      if (truckWatchedUsers) {
        if (truckWatchedUsers.watched_users.map(function (id) { return id.toString() }).indexOf(socket.decoded._id) === -1) {
          await TruckWatchedUsers.findOneAndUpdate({ truck: truck_id }, { $push: { watched_users: socket.decoded._id } });
          socket.emit('add_to_watchlist_truck_response', { success: true, message: "Truck has been added to watchlist" });
        } else {
          socket.emit('add_to_watchlist_truck_response', { success: false, message: "You have already added this truck to watchlist" });
        }
      } else {
        let _truckWatchedUsers = new TruckWatchedUsers();
        _truckWatchedUsers.truck = truck_id,
          _truckWatchedUsers.watched_users = [socket.decoded._id];
        _truckWatchedUsers.save();
        socket.emit('add_to_watchlist_truck_response', { success: true, message: "Truck has been added to watchlist" });
      }
    }
    catch (err) {
      common.log("Error while add user into watchlist", err);
      socket.emit('add_to_watchlist_truck_response', { success: false, message: err.message });
    }
  }
}


module.exports.addToWatchlistHttp = async function (req, res) {
  'use strict';
  var addToWatchList = true ;
  if (req.payload._id) {
    try {
      let truckWatchedUsers = await TruckWatchedUsers.findOne({ truck: req.params.id }).lean();
      if (truckWatchedUsers) {
        if (truckWatchedUsers.watched_users.map(function (id) { return id.toString() }).indexOf(req.payload._id) === -1) {
         let result= await TruckWatchedUsers.findOneAndUpdate({ truck: req.params.id }, { $push: { watched_users: req.payload._id } });
         if(result){
           return res.status(200).json({ success: true, message: "Truck has been added to watchlist",addToWatchList });
         } 
        } else {
          return res.status(400).json({ success: false, message: "You have already added this truck to watchlist",addToWatchList });
        }
      } else {
        let _truckWatchedUsers = new TruckWatchedUsers();
        _truckWatchedUsers.truck = req.params.id,
          _truckWatchedUsers.watched_users = [req.payload._id];
        _truckWatchedUsers.save();
        res.status(200).json({ success: true, message: "Truck has been added to watchlist", addToWatchList });
      }
    }
    catch (err) {
      common.log("Error while add user into watchlist", err);
      res.status(400).json({ success: false, message: err.message });
    }
  }
}


module.exports.removeFromWatchlist = function (truck_id, socket) {
  'use strict';
  if (socket.decoded._id) {
    TruckWatchedUsers.findOneAndUpdate({ truck: truck_id }, { $pull: { watched_users: socket.decoded._id } }, { upsert: true },
      function (err, truck) {
        if (err) {
          socket.emit('remove_from_watchlist_truck_response', { success: false, message: err.message });
        } else {
          socket.emit('remove_from_watchlist_truck_response', { success: true, message: "Truck has been removed from watchlist" });
        }
      });
  }
}

module.exports.removeFromWatchlistHttp = function (req, res) {
  'use strict';
  if (req.payload._id) {
    TruckWatchedUsers.findOneAndUpdate({ truck: req.params.id }, { $pull: { watched_users: req.payload._id } }, { upsert: true },
      function (err, truck) {
        if (err) {
          res.status(400).json({ success: false, message: err.message });
        } else {
          res.status(200).json({ success: true, message: "Truck has been removed from watchlist" });
        }
      });
  }
}


module.exports.myTrucksHttp = async function (req, res) {
  'use strict';

  try {
    var limitResults = req.body.limit || 10;
    var skipResults = ((req.body.skip || 1) - 1) * limitResults;
    var sort_by = req.body.sort_by || 'updatedAt';
    var sort_direction = req.body.sort_direction == 'asc' ? 1 : -1;
    var sortObj = {};
    var sort_split = sort_by.split(',');
    for (let index = 0; index < sort_split.length; index++) {
      sortObj[sort_split[index].trim()] = sort_direction;
    }

    if (req.payload._id) {
      let [count, trucks] = await Promise.all([
        Truck.count({ created_by: req.payload._id, "is_opened": true }),
        Truck.find({ created_by: req.payload._id, "is_opened": true },
        {
          _id:1,
          origin_bystate_states:1,
          destination_bystate_states:1,
          origin_bycity_address:1,
          origin_bycity_lat_long:1,
          origin_bycity_zip:1,
          origin_bycity_state:1,
          origin_bycity_city:1,
          origin_bycity_range:1,
          destination_bycity_address:1,
          destination_bycity_range:1,
          destination_bycity_lat_long:1,
          destination_bycity_zip:1,
          destination_bycity_state:1,
          destination_bycity_city:1,
          equipment_type:1,
          weight:1,
          length:1,
          rate_per_mile:1,
          description:1,
          ready_date:{ $cond:

            {
            
            if : { $eq : [{$type:'$ready_date'},'date']},
            
            then: { $dateToString: { format: "%m/%d/%Y", date: "$ready_date" } },
            
            else: {
            
            $cond :{
            
            if : {$eq:['$ready_date',null]},
            
            then : '',
            
            else :"$ready_date"
            
            
            
            }
            
            }
            
            }
            
            },
          updatedAt:1,
        }).sort(sortObj).skip(skipResults).limit(limitResults).lean()
      ]);
      return res.status(200).json({ count: count, data: trucks });
    }
  }
  catch (err) {
    common.log("Error while get my trucks", err);
    return res.status(400).send({ message: err.messgae });
  }
}


module.exports.myTrucksWithUserId = async function (req, res) {
  'use strict';
  
  try {
    let {userId,limit,skip,sort_by,sort_direction}=req.body
   
    let limitResults = limit || 10;
    let skipResults = ((skip || 1) - 1) * limitResults;
    let sortBy = sort_by || 'updatedAt';
    let sortDirection = sort_direction == 'asc' ? 1 : -1;
    let sortObj = {};
    let sort_split = sortBy.split(',');
    if(!userId){
      return res.status(response.StatusCodes.BAD_REQUEST).send("UserId is required");
    }
    for (let index = 0; index < sort_split.length; index++) {
      sortObj[sort_split[index].trim()] = sortDirection;
    }
    let [truck_count,trucks ] = await Promise.all([
    Truck.find({ created_by: ObjectId(userId), "is_opened": true }).count(),
    Truck.find({ created_by: ObjectId(userId), "is_opened": true },{
      _id:1,
      origin_bystate_states:1,
      destination_bystate_states:1,
      origin_bycity_address:1,
      origin_bycity_lat_long:1,
      origin_bycity_zip:1,
      origin_bycity_state:1,
      origin_bycity_city:1,
      origin_bycity_range:1,
      destination_bycity_address:1,
      destination_bycity_range:1,
      destination_bycity_lat_long:1,
      destination_bycity_zip:1,
      destination_bycity_state:1,
      destination_bycity_city:1,
      equipment_type:1,
      weight:1,
      length:1,
      rate_per_mile:1,
      description:1,
      ready_date: { $dateToString: { format: "%m/%d/%Y", date: "$ready_date" } },
      updatedAt:1,
    })
          .populate('created_by').sort(sortObj).skip(skipResults).limit(limitResults).lean()
  ])
    
     
      return res.status(response.StatusCodes.OK).json({ count: truck_count, data: trucks });
    
  }
  catch (err) {
    common.log("Error while get my trucks", err);
    return res.status(response.StatusCodes.INTERNAL_SERVER_ERROR).send({ message: err.messgae });
  }
}



module.exports.uploadCSVTrucks = async function (req, res) {
  try {
    if (!req.file) {
      return res.status(400).send("file is required");
    }
    let trucksByCsvUpload = new TrucksByCsvUpload();
    trucksByCsvUpload.s3_key = req.file.key;
    trucksByCsvUpload.file_location = req.file.location;
    trucksByCsvUpload.original_name = req.file.originalname;
    trucksByCsvUpload.mimetype = req.file.mimetype;
    trucksByCsvUpload.user = req.payload._id;
    await trucksByCsvUpload.save();
    await newCSVTruckQueue.add({ id: trucksByCsvUpload._id })
    return res.send({ success: true, message: "uploading request in process" });
  } catch (err) {
    common.log('Error while upload csv loads :--', err)
    return res.status(400).send({ success: false, message: err.message });
  }
}

module.exports.getUploadCSVTrucks = async function (req, res) {
  try {
    let sort_direction = req.body.sort_direction == 'asc' ? 1 : -1;
    let sort_by = req.body.sort_by || 'createdAt';
    let sortObj = {};
    sortObj[sort_by] = sort_direction;
    let limit = req.body.limit || 10;
    let skip = ((req.body.skip || 1) - 1) * limit;
    let searchObject = { user: ObjectId(req.payload._id) };
    const [count, data] = await Promise.all([
      TrucksByCsvUpload.count(searchObject),
      TrucksByCsvUpload.find(searchObject).sort(sortObj).skip(skip).limit(limit).lean()
    ])
    return res.send({ success: true, count, data });
  } catch (err) {
    common.log('Error while get upload csv trucks :--', err)
    return res.status(400).send({ success: false, message: err.message });
  }
}

module.exports.downloadFile = async function (req, res) {
  if (!req.params.id) {
    return res.status(400).send({ param: 'id', message: 'Truck file id is required' });
  }
  try {
    let document = await TrucksByCsvUpload.findById(req.params.id)
    if (document) {
      s3FileDownload(document, req, res);
    } else {
      common.log("Truck csv file not found");
      return res.status(400).send({ message: "Truck csv file not found" });
    }
  }
  catch (err) {
    common.log("Error while get trucks csv document detail", err);
    return res.status(400).send(err);
  }
}

module.exports.datTruckUpdate=async function(req,res){
  let equipmentType
  var equipmentOptions = [];
   equipmentOptions.push("Flatbed/Step Deck","Flatbed", "Flatbed/Van", "Flatbed/Van/Reefer", "B-Train Flatbed", "Flatbed with sides",
        "Flatbed with Tarps", "Flatbed with Pallet Exchange", "Hazmat Flatbed", "Maxi Flatbed", "Team Flatbed", "Hotshot Flatbed",
        "Flatbed/Step Deck", "Hotshot Step Deck", "Removable Gooseneck","Step Deck",
        "Reefer with Pallet Exchange", "Hazmat Reefer", "Van/Reefer", "Flatbed/Van/Reefer","Reefer","Van",
        "Flatbed/Van/Reefer", "Flatbed/Van", "Air Ride Van", "Van with curtains", "Van with Pallet Exchange",
        "Hazmat Van", "Vented Van", "Team Van", "Walking Floor Van", "Van/Reefer", "Box Truck"

        );  
   
  await Truck.createIndexes({source:1,equipment_type:1})
 let truck= await Truck.find({source:'Dat'},{source:1,equipment_type:1}).sort({source:-1}).skip(0).limit(110923).lean()
    truck.map(async truckData=>{
      if(truckData.equipment_type[0]!=null){
        equipmentType = await common.getEquipmentType(truckData.equipment_type[0]);  
      }
      
      if(equipmentType!=null){
       await  Truck.updateOne({_id:truckData._id},{ $addToSet: {equipment_type : equipmentType } },(error)=>{
         if(error){
         //retur n res.json({error})
         } else{
           console.log('update successfully')
         }         
        })
      }else{
        common.log("Dat truck equipement not found")
      }
    })
    
}