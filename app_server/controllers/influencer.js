const mongoose = require('mongoose');
const Influencer =  mongoose.model('Influencer');
const common = require('../helpers/common');

exports.getAllInfluencers = async function(req,res){
    try{
        let Influencers = await Influencer.find().lean()
        return res.send({success:true,data:Influencers})
    } catch(err){
        common.log('Error while get all Influencers :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}

exports.getInfluencerByEmail = async function(req,res){
    try{
        let Influencers = await Influencer.find({email:req.body.email}).lean();
        return res.send({success:true,data:Influencers})
    } catch(err){
        common.log('Error while get all role :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}

exports.addInfluencer = async function(req,res){
    try{
        let influencer =  new Influencer();
          influencer.name=req.body.name;
          influencer.phone=req.body.phone;
          influencer.email= req.body.email;
          influencer.created_by=req.payload._id
          await influencer.save();
        return res.send({success:true,data:influencer})
    } catch(err){
        common.log('Error while add influencer :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}

exports.deleteInfluencer = async function(req,res){
    try{
        let influencer = await Influencer.findById(req.params.id).lean();
        if(!influencer){
            return res.status(404).send({success:false,message:'influencer not found.'});
        }
        influencer.is_deleted = true;
        delete influencer._id;
        await Role.updateOne({_id:req.params.id},influencer);
        return res.send({success:true,message:"influencer deleted successfully."})
    } catch(err){
        common.log('Error while delete influencer :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}