var mongoose = require('mongoose');
var datCompany = mongoose.model('DatCompany');
const { Parser } = require('json2csv');
const common = require('../helpers/common');

module.exports.getCompanyCSV = async function (req, res) {
    try {
        let role = req.params.role || 'broker';
        let companies = await await datCompany.aggregate([
            {
                $match: { role: role }
            },
            {
                $project: {
                    company: 1,
                    contactPhone: 1,
                    email: 1,
                    updatedAt: 1,
                    first_name: { $arrayElemAt: [{ $split: ['$contactName', ', '] }, 0] },
                    last_name: { $arrayElemAt: [{ $split: ['$contactName', ', '] }, 1] },
                    city: { $arrayElemAt: [{ $split: ['$officeLocation', ','] }, 0] },
                    state: { $arrayElemAt: [{ $split: ['$officeLocation', ','] }, 1] }
                }
            },
            {
                $facet: {
                    count: [{ "$group": { _id: null, count: { $sum: 1 } } }],
                    data: [
                        {
                            $project: {
                                company: 1,
                                contactPhone: 1,
                                email: 1,
                                updatedAt: 1,
                                first_name: 1,
                                last_name: 1,
                                city: 1,
                                state: 1
                            }
                        }

                    ]
                }
            },
        ])


        let filename = 'Companies.csv';
        let fields = [
            {
                label: 'Date', value: function (row) {
                    return row.updatedAt
                }
            },
            { label: 'Company', value: 'company' },
            {
                label: 'First Name', value: 'first_name'
            },
            {
                label: 'Last Name', value: 'last_name'
            },
            { label: 'Phone Number', value: 'contactPhone' },
            { label: 'Email', value: 'email' },
            {
                label: 'City', value: 'city'
            },
            {
                label: 'State', value: 'state'
            },
        ];
        let opts = {
            fields: fields
        }
        try {
            const parser = new Parser(opts);
            const csv = parser.parse(companies[0].data);
            //common.log(csv);
            res.set('Content-Disposition', ["attachment; filename=", filename].join(''));
            res.set('Content-Type', 'text/csv');
            return res.end(csv);
        } catch (err) {
            console.error("Error while generate csv file", err);
            throw err;
        }
    } catch (err) {
        common.log("Error while get company for export", err);
        return res.status(500).send(err);
    }
}

module.exports.getCompany = async function (req, res) {
    try {
        var sortObj = {};
        var role = req.body.role || 'broker';
        var limit = req.body.limit || 10;
        var skip = req.body.skip || 1;
        var sortBy = req.body.sort_by || 'company';
        var sortDirection = req.body.sort_direction == 'desc' ? -1 : 1;
        var sort_split = sortBy.split(',');
        for (let i = 0; i < sort_split.length; i++) {
            sortObj[sort_split[i]] = sortDirection;
        }
        var search_obj = {};
        search_obj = add_searchFiled(req.body, 'first_name', search_obj);
        search_obj = add_searchFiled(req.body, 'last_name', search_obj);
        search_obj = add_searchFiled(req.body, 'email', search_obj);
        search_obj = add_searchFiled(req.body, 'phone', search_obj);
        search_obj = add_searchFiled(req.body, 'company', search_obj);
        search_obj = add_searchFiled(req.body, 'city', search_obj);
        search_obj = add_searchFiled(req.body, 'state', search_obj);

        // var totalRecords = await datComapny.find({ role: role }).count();

        var companies = await datCompany.aggregate([
            {
                $match: { role: role }
            },
            {
                $project: {
                    company: 1,
                    contactPhone: 1,
                    email: 1,
                    updatedAt: 1,
                    first_name: { $arrayElemAt: [{ $split: ['$contactName', ', '] }, 0] },
                    last_name: { $arrayElemAt: [{ $split: ['$contactName', ', '] }, 1] },
                    city: { $arrayElemAt: [{ $split: ['$officeLocation', ','] }, 0] },
                    state: { $arrayElemAt: [{ $split: ['$officeLocation', ','] }, 1] }
                }
            },
            {
                $sort: sortObj
            },
            {
                $match: search_obj
            },
            {
                $facet: {
                    count: [{ "$group": { _id: null, count: { $sum: 1 } } }],
                    data: [
                        {
                            $project: {
                                company: 1,
                                contactPhone: 1,
                                email: 1,
                                updatedAt: 1,
                                first_name: 1,
                                last_name: 1,
                                city: 1,
                                state: 1
                            }
                        },
                        {
                            $skip: (skip - 1) * limit
                        },
                        {
                            $limit: limit
                        }
                    ]
                }
            },
        ]);
        var returnObj = {
            count: 0,
            data: []
        }
        if (companies && companies[0] && companies[0].count && companies[0].count[0] && companies[0].count[0].count) {
            returnObj.count = companies[0].count[0].count;
        }
        if (companies && companies[0].data) {
            returnObj.data = companies[0].data;
        }
        return res.send(returnObj);
    } catch (err) {
        common.log("Error while get companis", err);
        return res.status(500).send(err);
    }
}

function add_searchFiled(body, key, output) {
    if (body[key]) {
        output[key] = { $regex: body[key], $options: 'i' }
    }
    return output;
}