const mongoose = require('mongoose');
const SiteModule = mongoose.model('SiteModule');
const SubscriptionPackages = mongoose.model('SubscriptionPackages');
const stripeHelper = require('../helpers/stripe');
const common = require('../helpers/common');
const config = require('../config/config');

module.exports.CreateDefaultPackages = async function (req, res) {
    try {
        await addPackage({ price: 29.99, subscription_name: "Silver", role: "carrier" });
        await addPackage({ price: 69.99, subscription_name: "Gold", role: "carrier" });
        await addPackage({ price: 99.99, subscription_name: "Platinum", role: "carrier" });

        await addPackage({ price: 99.99, subscription_name: "Platinum", role: "shipper" });

        await addPackage({ price: 99.00, subscription_name: "Silver", role: "broker" });
        await addPackage({ price: 149.00, subscription_name: "Platinum", role: "broker" });

        await addPackage({ price: 99.99, subscription_name: "Platinum", role: "dispatcher" });

        let Module_names = [
            {
                name: "Post Load", frontend_key: "post-load",
                accessFor: [
                    { subscription_name: "Platinum", role: "shipper" },
                    { subscription_name: "Silver", role: "broker" },
                    { subscription_name: "Platinum", role: "broker" },
                ]
            },
            {
                name: "Find Load (24/7 Live Load Seach)", frontend_key: "find-load",
                accessFor: [
                    { subscription_name: "Silver", role: "carrier" },
                    { subscription_name: "Gold", role: "carrier" },
                    { subscription_name: "Platinum", role: "carrier" },
                    { subscription_name: "Platinum", role: "shipper" },
                    { subscription_name: "Silver", role: "broker" },
                    { subscription_name: "Platinum", role: "broker" },
                    { subscription_name: "Platinum", role: "dispatcher" }
                ]
            },
            {
                name: "Post Truck", frontend_key: "post-truck",
                accessFor: [
                    { subscription_name: "Silver", role: "carrier" },
                    { subscription_name: "Gold", role: "carrier" },
                    { subscription_name: "Platinum", role: "carrier" },
                    { subscription_name: "Platinum", role: "broker" },
                    { subscription_name: "Platinum", role: "dispatcher" }
                ]
            },
            {
                name: "Find Truck (24/7 Live Truck Seach)", frontend_key: "find-truck",
                accessFor: [
                    { subscription_name: "Platinum", role: "shipper" },
                    { subscription_name: "Silver", role: "broker" },
                    { subscription_name: "Platinum", role: "broker" },
                ]
            },
            {
                name: "Improved Load Filtering & Sorting", frontend_key: "load_filtering_sorting",
                accessFor: [
                    { subscription_name: "Silver", role: "carrier" },
                    { subscription_name: "Gold", role: "carrier" },
                    { subscription_name: "Platinum", role: "carrier" },
                    { subscription_name: "Platinum", role: "shipper" },
                    { subscription_name: "Silver", role: "broker" },
                    { subscription_name: "Platinum", role: "broker" },
                    { subscription_name: "Platinum", role: "dispatcher" }
                ]
            },
            {
                name: "Watch List (Save Loads & Trucks with Alert Option)", frontend_key: "load_Watchlist",
                accessFor: [
                    { subscription_name: "Gold", role: "carrier" },
                    { subscription_name: "Platinum", role: "carrier" },
                    { subscription_name: "Platinum", role: "shipper" },
                    { subscription_name: "Silver", role: "broker" },
                    { subscription_name: "Platinum", role: "broker" },
                    { subscription_name: "Platinum", role: "dispatcher" }
                ]
            },
            {
                name: "Forkstimate™ (Calculate Load Expenses)", frontend_key: "base_expenes_within_load",
                accessFor: [{ subscription_name: "Gold", role: "carrier" },
                { subscription_name: "Platinum", role: "carrier" },
                { subscription_name: "Platinum", role: "shipper" },
                { subscription_name: "Silver", role: "broker" },
                { subscription_name: "Platinum", role: "broker" },
                { subscription_name: "Platinum", role: "dispatcher" }
            ]
            },
            {
                name: "Maps & Mileage (Powered by Google)", frontend_key: "milega_and_maps",
                accessFor: [{ subscription_name: "Silver", role: "carrier" },
                { subscription_name: "Gold", role: "carrier" },
                { subscription_name: "Platinum", role: "carrier" },
                { subscription_name: "Platinum", role: "shipper" },
                { subscription_name: "Silver", role: "broker" },
                { subscription_name: "Platinum", role: "broker" },
                { subscription_name: "Platinum", role: "dispatcher" }
            ]
            },
            {
                name: "Dispatcher (Real-Time Updates)", frontend_key: "dispatcher",
                accessFor: [
                    { subscription_name: "Platinum", role: "broker" },
                    { subscription_name: "Platinum", role: "dispatcher" }
                ]
            },
            {
                name: "Expenses (Track Money & Get Important Insightsts) ", frontend_key: "expenses_report_export",
                accessFor: [
                    { subscription_name: "Platinum", role: "carrier" },
                    { subscription_name: "Platinum", role: "shipper" },
                    { subscription_name: "Silver", role: "broker" },
                    { subscription_name: "Platinum", role: "broker" },
                    { subscription_name: "Platinum", role: "dispatcher" }
                ]
            },
            {
                name: "Heatmap", frontend_key: "heatmap",
                accessFor: [
                    { subscription_name: "Gold", role: "carrier" },
                    { subscription_name: "Platinum", role: "carrier" },
                    { subscription_name: "Platinum", role: "shipper" },
                    { subscription_name: "Silver", role: "broker" },
                    { subscription_name: "Platinum", role: "broker" },
                    { subscription_name: "Platinum", role: "dispatcher" }
                ]
            },
            {
                name: "Enhanced Mileage, Maps & Tolls", frontend_key: "mileage_maps_tolls_and_rate_estimate",
                accessFor: [
                    { subscription_name: "Platinum", role: "carrier" },
                    { subscription_name: "Platinum", role: "shipper" },
                    { subscription_name: "Silver", role: "broker" },
                    { subscription_name: "Platinum", role: "broker" },
                    { subscription_name: "Platinum", role: "dispatcher" }
                ]
            },
            {
                name: "Company Management (Additional Users within Your Company)", frontend_key: "parent_child_user_relationship",
                accessFor: [
                    { subscription_name: "Silver", role: "carrier" },
                    { subscription_name: "Gold", role: "carrier" },
                    { subscription_name: "Platinum", role: "carrier" },
                    { subscription_name: "Platinum", role: "shipper" },
                    { subscription_name: "Silver", role: "broker" },
                    { subscription_name: "Platinum", role: "broker" },
                    { subscription_name: "Platinum", role: "dispatcher" }
                ]
            },
            {
                name: "Book It Now (Coming Soon!)", frontend_key: "book_now",
                accessFor: [
                    { subscription_name: "Gold", role: "carrier" },
                    { subscription_name: "Platinum", role: "carrier" },
                    { subscription_name: "Platinum", role: "shipper" },
                    { subscription_name: "Silver", role: "broker" },
                    { subscription_name: "Platinum", role: "broker" },
                    { subscription_name: "Platinum", role: "dispatcher" }
                ]
            },
            {
                name: "24/7 Live Support", frontend_key: "chat_option",
                accessFor: [
                    { subscription_name: "Silver", role: "carrier" },
                    { subscription_name: "Gold", role: "carrier" },
                    { subscription_name: "Platinum", role: "carrier" },
                    { subscription_name: "Platinum", role: "shipper" },
                    { subscription_name: "Silver", role: "broker" },
                    { subscription_name: "Platinum", role: "broker" },
                    { subscription_name: "Platinum", role: "dispatcher" }
                ]
            },
            {
                name: "Credit Scores & DaysTo Pay (Coming Soon!)", frontend_key: "credit_scores",
                accessFor: [
                    { subscription_name: "Platinum", role: "carrier" },
                    { subscription_name: "Platinum", role: "shipper" },
                    { subscription_name: "Silver", role: "broker" },
                    { subscription_name: "Platinum", role: "broker" },
                    { subscription_name: "Platinum", role: "dispatcher" }
                ]
            },
            {
                name: "Dispatch & Order Management", frontend_key: "dispatch_order_management",
                accessFor: [
                    { subscription_name: "Platinum", role: "broker" },
                    { subscription_name: "Platinum", role: "dispatcher" }
                ]
            },
            {
                name: "Fork Documents (Capture, Store, & Send Documents)", frontend_key: "document_inbox",
                accessFor: [
                    { subscription_name: "Gold", role: "carrier" },
                    { subscription_name: "Platinum", role: "carrier" },
                    { subscription_name: "Platinum", role: "shipper" },
                    { subscription_name: "Silver", role: "broker" },
                    { subscription_name: "Platinum", role: "broker" },
                    { subscription_name: "Platinum", role: "dispatcher" }
                ]
            },
            {
                name: "E-sign Bill of Lading (Coming Soon!)", frontend_key: "e-sign",
                accessFor: [
                    { subscription_name: "Platinum", role: "carrier" },
                    { subscription_name: "Platinum", role: "shipper" },
                    { subscription_name: "Silver", role: "broker" },
                    { subscription_name: "Platinum", role: "broker" },
                    { subscription_name: "Platinum", role: "dispatcher" }
                ]
            },
            {
                name: "Fax & Scan Documents (Coming Soon!)", frontend_key: "fax_scan",
                accessFor: [
                    { subscription_name: "Platinum", role: "carrier" },
                    { subscription_name: "Platinum", role: "shipper" },
                    { subscription_name: "Silver", role: "broker" },
                    { subscription_name: "Platinum", role: "broker" },
                    { subscription_name: "Platinum", role: "dispatcher" }
                ]
            },
            {
                name: "Freight Tracking & Location Management (Coming Soon!)", frontend_key: "freight_tracking",
                accessFor: [
                    { subscription_name: "Silver", role: "carrier" },
                    { subscription_name: "Gold", role: "carrier" },
                    { subscription_name: "Platinum", role: "carrier" },
                    { subscription_name: "Platinum", role: "shipper" },
                    { subscription_name: "Silver", role: "broker" },
                    { subscription_name: "Platinum", role: "broker" },
                    { subscription_name: "Platinum", role: "dispatcher" }
                ]
            },
            {
                name: "Fuel Card", frontend_key: "fuel_card",
                accessFor: [
                    { subscription_name: "Silver", role: "carrier" },
                    { subscription_name: "Gold", role: "carrier" },
                    { subscription_name: "Platinum", role: "carrier" },
                    { subscription_name: "Platinum", role: "shipper" },
                    { subscription_name: "Silver", role: "broker" },
                    { subscription_name: "Platinum", role: "broker" },
                    { subscription_name: "Platinum", role: "dispatcher" }
                ]
            },
            {
                name: "Load Management & Summary", frontend_key: "load_management_summary",
                accessFor: [
                    { subscription_name: "Gold", role: "carrier" },
                    { subscription_name: "Platinum", role: "carrier" },
                    { subscription_name: "Platinum", role: "shipper" },
                    { subscription_name: "Silver", role: "broker" },
                    { subscription_name: "Platinum", role: "broker" },
                    { subscription_name: "Platinum", role: "dispatcher" }
                ]
            },
            {
                name: "Manage Income & Transactions", frontend_key: "manage_income_expense",
                accessFor: [
                    { subscription_name: "Platinum", role: "carrier" },
                    { subscription_name: "Platinum", role: "shipper" },
                    { subscription_name: "Silver", role: "broker" },
                    { subscription_name: "Platinum", role: "broker" },
                    { subscription_name: "Platinum", role: "dispatcher" }
                ]
            },
            {
                name: "Real-Time Load Updates & Alerts", frontend_key: "real_time_load_alerts",
                accessFor: [
                    { subscription_name: "Platinum", role: "carrier" },
                    { subscription_name: "Silver", role: "broker" },
                    { subscription_name: "Platinum", role: "broker" },
                    { subscription_name: "Platinum", role: "dispatcher" }
                ]
            },
            {
                name: "Real-Time Truck Updates & Alerts", frontend_key: "real_time_truck_alerts",
                accessFor: [
                    { subscription_name: "Platinum", role: "shipper" },
                    { subscription_name: "Platinum", role: "broker" },
                ]
            },
            {
                name: "Reporting (Determine Profit & Loss, Net Income & Export Reports)", frontend_key: "reporting",
                accessFor: [
                    { subscription_name: "Platinum", role: "carrier" },
                    { subscription_name: "Platinum", role: "shipper" },
                    { subscription_name: "Silver", role: "broker" },
                    { subscription_name: "Platinum", role: "broker" },
                    { subscription_name: "Platinum", role: "dispatcher" }
                ]
            },
            {
                name: "Real-Time Dispatacher Alerts (Email & Text Options)", frontend_key: "text_email_alerts_dispatcher",
                accessFor: [
                    { subscription_name: "Platinum", role: "carrier" },
                    { subscription_name: "Platinum", role: "broker" },
                    { subscription_name: "Platinum", role: "dispatcher" }
                ]
            },
            {
                name: "Factoring", frontend_key: "factoring",
                accessFor: [
                    { subscription_name: "Silver", role: "carrier" },
                    { subscription_name: "Gold", role: "carrier" },
                    { subscription_name: "Platinum", role: "carrier" },
                    { subscription_name: "Platinum", role: "shipper" },
                    { subscription_name: "Silver", role: "broker" },
                    { subscription_name: "Platinum", role: "broker" },
                    { subscription_name: "Platinum", role: "dispatcher" }
                ]
            },
            {
                name: "Document Expiry Reminders", frontend_key: "insurance_expiry_report",
                accessFor: [
                    { subscription_name: "Gold", role: "carrier" },
                    { subscription_name: "Platinum", role: "carrier" },
                    { subscription_name: "Platinum", role: "shipper" },
                    { subscription_name: "Silver", role: "broker" },
                    { subscription_name: "Platinum", role: "broker" },
                    { subscription_name: "Platinum", role: "dispatcher" }
                ]
            },
        ]

        for (let index = 0; index < Module_names.length; index++) {
            const element = Module_names[index];
            await addSiteModule(element);
        }
        return res.send("Success");

    }
    catch (err) {
        common.log("Error while setup database", err);
        return res.status(400).send({ err });
    }
}

async function addPackage(data) {
    try {
        let subscriptionpackage = new SubscriptionPackages();
        subscriptionpackage.price = data.price;
        subscriptionpackage.subscription_name = data.subscription_name;
        subscriptionpackage.role = data.role;
        subscriptionpackage.is_delete = false;
        subscriptionpackage.active = true;
        let sp = await subscriptionpackage.save();
        const plan = await stripeHelper.addPlan(data.subscription_name, data.price, config.freeDaysTrail);
        sp = await SubscriptionPackages.findOneAndUpdate({ _id: sp._id }, { $set: { stripe_plan_id: plan.id } }, { upsert: false });
        return sp;
    }
    catch (err) {
        throw err;
    }
}
async function addSiteModule(data) {
    try {
        let siteModule = new SiteModule();
        siteModule.name = data.name;
        siteModule.frontend_key = data.frontend_key
        siteModule.is_delete = false;
        let sm = await siteModule.save();
        for (let index = 0; index < data.accessFor.length; index++) {
            const element = data.accessFor[index];
            await SubscriptionPackages.updateOne(element,{$push:{module_access: sm._id}})
        }
        return sm
    }
    catch (err) {
        throw err;
    }
}
