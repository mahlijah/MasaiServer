const { ObjectID } = require('mongodb');
const mongoose = require('mongoose');
const EmailTemplate = mongoose.model('EmailTemplate');
const EmailType = mongoose.model('EmailTypes');
const User = mongoose.model('User');
const common = require('../helpers/common');
const EmailTemplateService = require('../services/emailTemplateService');


module.exports.getEmailTypes = async function(req,res){
    try{
        let findEmailTypes=await EmailType.find({},{email_type:1,_id:0}).lean();
        let newEmailType=findEmailTypes.map(result=>result.email_type);
        return res.send({success:true,data:newEmailType});
    } catch(err){
        common.log('Error while get email templates  :--',err);
        return res.status(400).send({success:false,message:err.message});
    }
}

module.exports.getEmailTypesById = async function(req,res){
    let {id}=req.params;
    
    try{
        let findEmailType=await EmailType.findOne({_id:ObjectID(id)}).lean();
        return res.send({success:true,data:findEmailType.email_type});
    } catch(err){
        common.log('Error while get email templates  :--',err);
        return res.status(400).send({success:false,message:err.message});
    }
}

module.exports.postTypeEmail=async function(req,res){
    let {email_type}=req.body;
    if (!email_type) {
        common.log("Error  email type--- ");
        return res.status(400).send({
          field: 'email_type',
          message: 'email type is required'
        })
    }
    if (!req.payload._id) {
        common.log("Error  user not found--- ");
        return res.status(400).send({
          message: 'user not found'
        })
    }
    let findEmailType=await EmailType.findOne({email_type:email_type})
    if(!findEmailType){
        var emailType = new EmailType();
        emailType.email_type=email_type;
        let emailTypeSave=await emailType.save(); 
        if(!emailTypeSave){
            common.log('Email Type not save successfully');
            return res.status(400).json({message:"Email Type not save successfully"})
        }else{
            return res.status(200).json({message:emailTypeSave})
        }
        
    }else{
        return res.status(400).json({message:"Email Type exist"})
    }
                  
}



module.exports.updateTypeEmail=async function(req,res){
    let {id}=req.params;
    let {email_type}=req.body;
    if (!id) {
        common.log("Error  id is not available--- ");
        return res.status(400).send({
           message: 'id is required'
        })
    }
    let emailType=await EmailType.findById({_id:ObjectID(id)}) 
     if(emailType && email_type!=''){
       let updateEmail= await  EmailType.updateOne({_id:ObjectID(id)},{$set:{email_type:email_type}})
       if(updateEmail){
           return res.status(200).json({message:'email type updated'})
       }else{
        common.log('email type is not updated');
        return res.status(400).json({message:'email type is not updated'})
       }
     }else if(emailType && email_type==''){
        let updateWithExitingEmailType= await  EmailType.updateOne({_id:ObjectID(id)},{$set:{email_type:emailType.email_type}})
        if(updateWithExitingEmailType){
            return res.status(200).json({message:'email type updated with existing one'})
        }else{
         common.log('email type is not updated');
         return res.status(400).json({message:'email type is not updated'})
        }   
     }
     else{
        common.log('email type is not found');
        return res.status(400).send({
            message: 'email_type is not found'
         })
     }       

}

module.exports.deleteTypeEmail=async function(req,res){
    let {id}=req.params;
   
    if (!id) {
        common.log("Error  id is not available--- ");
        return res.status(400).send({
           message: 'id is required'
        })
    }
    let deleteEmail= await  EmailType.deleteOne({_id:id})
    if(deleteEmail){
           return res.status(200).json({message:"email type deleted succesfully"})
    }else{
        common.log('email type is not deleted');
        return res.status(400).json({message:'email type is not deleted'})
    }
}
module.exports.getEmailTemplateList = async function(req,res){
    try{
        let limit=req.body.limit||10;
        let skip= ((req.body.skip||1)-1)*limit;
        let sortBy= req.body.sortBy || 'updateAt';
        let sortDirection= req.body.sortDirection ==asc?1:-1;
        let sort = {};
        sort[sortBy]=sortDirection;
        let [count,data]= await Promise.all([
            EmailTemplate.count(),
            EmailTemplate.find({}).sort(sort).skip(skip).limit(limit).lean()
        ]);
        return res.send({success:true,count,data});
    } catch(err){
        common.log('Error while get email templates  :--',err);
        return res.status(400).send({success:false,message:err.message});
    }
}
module.exports.getEmailTemplateByType = async function(req,res){
    try{
        const data = await EmailTemplateService.getTemplateByType(req.params.type);
        return res.send({success:true,data});
    } catch(err){
        common.log('Error while get email templates  :--',err);
        return res.status(400).send({success:false,message:err.message});
    }
}

module.exports.addEmailTemplate = async function(req,res){
    try{
        if(!req.body.subject || !req.body.body || !req.body.type) {
            return res.status(400).send({success:false,message:"Email subject,body and type is required"});
        }
        let et = new EmailTemplate();
        et.subject = req.body.subject;
        et.body = req.body.body;
        et.type = req.body.type;
        let sd = await et.save();
        return res.send({success:true,data:sd});
    } catch(err){
        common.log('Error while add email template :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}

module.exports.editEmailTemplate = async function(req,res){
    try{
        if(!req.params.id) {
            return res.status(400).send({success:false,message:"id is required for edit email template"});
        }
        let et = await EmailTemplate.findById(req.params.id);
        if(req.body.subject) et.subject = req.body.subject;
        if(req.body.body) et.body = req.body.body;
        if(req.body.type) et.type = req.body.type;
        let upsert = et.toObject();
        delete upsert._id;
        await EmailTemplate.updateOne({_id:ObjectID(req.params.id)},upsert);
        return res.send({success:true,data:null});
    } catch(err){
        common.log('Error while add email template :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}

module.exports.testEmail = async function(req,res){
    try{
        await EmailTemplateService.sendEmail(req.body.type,{},req.body.email);
        return res.send({success:true,message:"Email sent"});
    } catch(err){
        common.log('Error while test :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}