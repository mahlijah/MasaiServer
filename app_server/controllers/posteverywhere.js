
var mongoose = require('mongoose');
var config = require('../config/config');
var Load   = mongoose.model('Load');
var User   = mongoose.model('User');
var Truck  = mongoose.model('Truck');
var LoadHistory = mongoose.model('LoadHistory');
var removeableloads= mongoose.model('removeableloads');
var NodeGeocoder = require('node-geocoder');
var City = mongoose.model('City');
var Queue = require('bull');
const _common = require('../helpers/common');

var url = require('url');
const REDIS_URL = config.notification.redisUri;
const redis_uri = url.parse(REDIS_URL);
const redisOptions = REDIS_URL.includes("rediss://")
    ? {
        port: Number(redis_uri.port),
        host: redis_uri.hostname,
        password: redis_uri.auth.split(":")[1],
        maxRetriesPerRequest:0,
        tls: {
            rejectUnauthorized: false,
            servername: redis_uri.hostname
        },
        enableReadyCheck: false
    }
    : REDIS_URL;    

var newLoadQueue = new Queue('New Loads', {redis:redisOptions});
var deleteLoadQueue = new Queue('Deleted Loads', {redis:redisOptions});

var options = {
	provider: 'google',
	httpAdapter: 'https', 
	apiKey: config.googleGoecoder_key, 
	formatter: null,
};
var geocoder = NodeGeocoder(options);

class CommonFunctions
{
	async  getCityStateCoordinates(city, state)
	{
		
		try
		{
			let coordinate = await City.findOne({"State": state, "City" : city});
			if(coordinate != null && coordinate.Latitude != null && coordinate.Longitude != null)
			{    
				//_common.log("coordinates: " + coordinate);
				//_common.log(city + ", " + state + " : "+ coordinate.Longitude + " " + coordinate.Latitude);         
				let lat_long = {type: "MultiPoint", coordinates:[[Number(coordinate.Longitude),Number(coordinate.Latitude)]]};
				//_common.log("coordinate: " + lat_long);
				return lat_long;
			}
			else if(coordinate != null && (coordinate.Latitude == null || coordinate.Longitude == null)){
				//city exists in db without geo information
				return null;
			}
			else
			{
				//city not in database , create a place holder record.
				_common.log("saving new City and State"  + city + " " + state);
				let newCity = new City();
				newCity.City = city;
				newCity.State = state;
				await newCity.save();
				return null;
			}
		}        
        catch(err) {
            _common.log(err);
            return;
        }
	}

	async  getCoordinates(city, state)
	{
		let pickup_coordinates = [];
                
        if (city && city !== 'undefined' )
        { 
          pickup_coordinates.push(city)
        }
        
        if (state && state !== 'undefined' )
        { 
          pickup_coordinates.push(state)
        }
    
        
        //convert address to coordinates , city and state
        let pickup_address_full = pickup_coordinates.join();
        let geocode_long;
        let geocode_lat;
        let geocode = {};
		
		try
		{
			var geoInfo = await  geocoder.geocode(pickup_address_full);
			if(geoInfo != null){
				geocode_lat = (geoInfo && geoInfo.length >0)? geoInfo[0].latitude : " ";
				geocode_long = (geoInfo && geoInfo.length >0)? geoInfo[0].longitude : " ";
				geocode["geocode_lat"] = geocode_lat;
				geocode["geocode_long"] = geocode_long;
				geocode["state_code"] = (geoInfo[0].administrativeLevels)? geoInfo[0].administrativeLevels.level1short : "";
				geocode["country_code"] = (geoInfo[0].countryCode)? geoInfo[0].countryCode : "US";
				geocode["city"] = (geoInfo[0].city)? geoInfo[0].city : "";
				geocode["state"] = (geoInfo[0].administrativeLevels)? geoInfo[0].administrativeLevels.level1long : "";            
				//_common.log(geoInfo);
				return geocode;
			}
			else {
				return null;
			}
		}        
        catch(err) {
            _common.log(err);
            return;
        }
		
		
        
	}

	async addLoads(loads, user)
	{			
		let postedCount = 0;
		let failedCount = 0;
		_common.log(loads);

		if (!_common.isIterable(loads)) {
			return true;
		}
		
		for(let loaddata of loads)
		{
			try
			{
				// loaddata = ld;
				//_common.log("loadData" ,loaddata);
				/*  loaddata data section start */
				var load 	 = new Load();
				var trackingn 		=	loaddata['tracking-number'][0];
				
				var origin 	  		= 	loaddata.origin[0];
				var origincity 		= 	origin.city[0];
				var originstate 	= 	origin.state[0];
				var originpostcode 	= 	origin.postcode[0];
				var origincounty 	= 	origin.county[0];
				var origincountry 	= 	origin.country[0];
				var pickup_addresses= 	"";

				if(origincity != null) pickup_addresses = origincity; 
				if(originstate != null & originstate != "" ) pickup_addresses= pickup_addresses.length > 0 ? pickup_addresses  + ','+originstate : originstate;
				if(originpostcode != null & originpostcode != "" )pickup_addresses = pickup_addresses.length > 0 ? pickup_addresses  + ','+originpostcode : originpostcode;
				if(origincountry != null & origincountry != "")pickup_addresses = pickup_addresses.length > 0 ? pickup_addresses  + ','+origincountry : origincountry;

		
				var originlatitude 	= "" ;//	origin.latitude[0]; //not used by posteverywhere
				var originlongitude = "";	//origin.longitude[0];//not used by posteverywhere
		
				var origindatestart = 	origin['date-start'][0];
				var origin_year 	= 	origindatestart.year[0];
				var origin_month 	= 	origindatestart.month[0];
				var origin_day 		= 	origindatestart.day[0];
				var pickup_date 	= 	origin_month+'/'+origin_day+'/'+origin_year;
				if(pickup_date == "//")pickup_date = "";

		
	
				var origindateend 	= 	origin['date-end'][0];
				var originend_year 	= 	origindateend.year[0];
				var originend_month = 	origindateend.month[0];
				var originend_day 	= 	origindateend.day[0];
				var pickupend_date 	= 	originend_month+'/'+originend_day+ '/' + originend_year;
				if(pickupend_date == "//")pickupend_date = "";
		
				var destination 			= 	loaddata.destination[0];
				var destinationcity 		= 	destination.city[0];
				var destinationstate 		= 	destination.state[0];
				var destinationpostcode 	= 	destination.postcode[0];
				var destinationcounty 		= 	destination.county[0];
				var destinationcountry 		= 	destination.country[0];
				var destination_addresses	= 	"" ; 
				if(destinationcity != null & destinationcity != "") destination_addresses = destinationcity; 
				if(destinationstate != null & destinationstate != "" ) destination_addresses= destination_addresses.length > 0 ? destination_addresses  + ','+destinationstate : destinationstate;
				if(destinationpostcode != null & destinationpostcode != "" )destination_addresses = destination_addresses.length > 0 ? destination_addresses  + ','+destinationpostcode : destinationpostcode;
				if(destinationcountry != null & destinationcountry != "" )destination_addresses = destination_addresses.length > 0 ? destination_addresses  + ','+destinationcountry : destinationcountry;

		
				var destinationlatitude 	= 	""; //destination.latitude[0];//not used by posteverywhere
				var destinationlongitude 	= 	""; //destination.longitude[0]; //not used by posteverywhere
				var destinationdatestart 	= 	destination['date-start'][0];
				var destinationdateend 		= 	destination['date-end'][0];
		
				var destination_year 		= 	destinationdatestart.year[0];
				var destination_month 		= 	destinationdatestart.month[0];
				var destination_day 		= 	destinationdatestart.day[0];
		
				var delivery_date 	= destination_month+'/'+destination_day+'/'+destination_year;
				if(delivery_date == "//")delivery_date = "";

				var equipment = loaddata.equipment[0]
				var equipmentType =await this.getEquipmentTypes(equipment);
		
				var loadsize 		= loaddata.loadsize[0];
				var full_or_partia 	= loadsize['$'].fullload;
				var length 		= loadsize.length[0];
				var width 		= loadsize.width[0];
				var height 		= loadsize.height[0];
				var weight 		= loadsize.weight[0];
				//_common.log('weight: ', weight);
				var loadcount 	= loaddata['load-count'][0];
				var stops 		= loaddata.stops[0];
				var distance 	= loaddata.distance[0];
				var rateDetail 		= loaddata.rate[0];
				//_common.log('rate: ', rateDetail);
				var rate		="0.00";
				if(isNaN(rateDetail) == true)
				{				
					 rate = rateDetail.$ != null && rateDetail.$.note != null? rateDetail.$.note : rateDetail._;
				}
				else
				{
					rate = rateDetail;
				}			

				var comment 	= loaddata.comment[0];				

				let geocoordinates = await this.getCityStateCoordinates(origincity, originstate); 
				if(geocoordinates!= null)
				{
					load.pickup_lat_long 	= 	geocoordinates;
				}

				var destinationcoordinates = await this.getCityStateCoordinates(destinationcity, destinationstate); 
				if(destinationcoordinates!= null)
				{
					load.delivery_lat_long  =   destinationcoordinates;
				}								
							
				var createdby =  user._id;

				load.pickup_addresses 	= 	pickup_addresses;	
				
				load.pickup_cities 		= 	origincity;
				load.pickup_states		=	originstate;
				load.delivery_addresses = 	destination_addresses;	
				load.delivery_cities	= 	destinationcity;
				load.delivery_states	= 	destinationstate;
				
				load.equipment_type 	= 	equipmentType;
				load.no_of_count 		= 	loadcount;
				load.target_rate 		= 	rate;
				load.load_weight 		= 	weight*1000;
				load.load_length 		= 	length;
				load.tracking_number	=	trackingn;
				load.ready_date 		= 	pickup_date;
				load.pickup_date 		= 	pickupend_date;
				load.delivery_date 		= 	delivery_date;
				if(stops > 0)
				{
					load.description 		= stops + " additional stops. " +	comment;
				}
				else
				{
					load.description 		= 	comment;	
				}
				load.created_by 		= 	createdby;
				load.internal = false;
				load.source = user.source_data;
				//_common.log("load before save: " + load);
				try
				{
							
					var existingLoad = await Load.findOne({tracking_number:Number(trackingn), created_by : createdby});
					if(existingLoad == null){						
						var doc = await load.save();
						if(doc){
							//queue it for history collection and broadcast to connected clients
							newLoadQueue.add({ id : doc._id});
						}
					}
					else
					{						
							var result = await	Load.updateOne({_id:existingLoad._id},{$set :{
								pickup_addresses 		:	load.pickup_addresses								
								,pickup_cities 			:	load.pickup_cities 		
								,pickup_states			:	load.pickup_states		
								,delivery_addresses 	:	load.delivery_addresses  	
								,delivery_cities		:	load.delivery_cities	 	
								,delivery_states		:	load.delivery_states	 	    
								,equipment_type 		:	load.equipment_type 	 	
								,no_of_count 			:	load.no_of_count 		
								,target_rate 			:	load.target_rate 		
								,load_weight 			:	load.load_weight 		
								,load_length 			:	load.load_length 		
								,tracking_number		:	load.tracking_number		
								,ready_date 			:	load.ready_date 		 	
								,pickup_date 			:	load.pickup_date 		
								,delivery_date 			:	load.delivery_date 		
								,description 			:	load.description 		
								,created_by 			:	load.created_by
							}}); 
							//_common.log('load ', trackingn, ' updated.');		
							//update history record
							//  await	LoadHistory.updateOne({loadId:existingLoad._id},{$set :{
							// 	pickup_addresses 		:	load.pickup_addresses								
							// 	,pickup_cities 			:	load.pickup_cities 		
							// 	,pickup_states			:	load.pickup_states		
							// 	,delivery_addresses 	:	load.delivery_addresses  	
							// 	,delivery_cities		:	load.delivery_cities	 	
							// 	,delivery_states		:	load.delivery_states	 	    
							// 	,equipment_type 		:	load.equipment_type 	 	
							// 	,no_of_count 			:	load.no_of_count 		
							// 	,target_rate 			:	load.target_rate 		
							// 	,load_weight 			:	load.load_weight 		
							// 	,load_length 			:	load.load_length 		
							// 	,tracking_number		:	load.tracking_number		
							// 	,ready_date 			:	load.ready_date 		 	
							// 	,pickup_date 			:	load.pickup_date 		
							// 	,delivery_date 			:	load.delivery_date 		
							// 	,description 			:	load.description 		
							// 	,created_by 			:	load.created_by
							// }}); 
					}
					postedCount++;
				}
				catch(ex)
				{
					_common.log("error occured:", ex);
					failedCount++;
					continue;
				}
			}
			catch(ex)
			{
				_common.log("error occured:", ex);
				failedCount++;
				continue;
			}			
		
		}
		_common.log(postedCount + " loads posted out of " + loads.length);
		_common.log(failedCount + " loads failed out of " + loads.length);
		return true;
	}
    
	async getEquipmentTypes(equipment) {
		var equipmentType = "";
		var equipmentKey = Object.keys(equipment);
		var equipmentKeys = "";
		if(equipmentKey.length > 1)
		{
			equipmentKeys = equipmentKey.join("");
		}
		else
		{
			equipmentKeys = equipmentKey[0];
		}
		//_common.log("equipment keys", equipmentKeys);

		switch (equipmentKeys) {
			case "ac":
				equipmentType = 'Auto Carrier (AC)';
				break;
			case "dd":
				equipmentType = 'Drop & Hook';
				break;
			case "dt":
				equipmentType = 'Dump Trailer';
				break;
			case "f":
				equipmentType = 'Flatbed';
				if(equipment[equipmentKey][0] != null)
				{
					let accessoryName = await this.getAccessoryName(equipment, equipmentKey);
					if(accessoryName != null){
						switch(accessoryName){
							case "b-train":
								equipmentType = "B-Train Flatbed";
								break;
							case "sides":
								equipmentType = "Flatbed with sides";
								break;
							case "tarps":
								equipmentType = "Flatbed with Tarps";
								break;
							case "palletexchange":
								equipmentType = "Flatbed with Pallet Exchange";
								break;
							case "hazmat":
								equipmentType = "Hazmat Flatbed";
								break;
							case "maxi":
								equipmentType = "Maxi Flatbed";
								break;
							case "team":
								equipmentType = "Team Flatbed";
								break;
							case "hotshot":
								equipmentType = "Hotshot Flatbed";
								break;
						}
					}
				}
				break;
			case "hb":
				equipmentType = 'Hopper Bottom';
				break;
			case "lb":
				equipmentType = 'Low Boy';
				break;
			case "po":
				equipmentType = 'Power Only';
				break;
			case "r":
				equipmentType = 'Reefer';
				if(equipment[equipmentKey] != null)
				{
					let accessoryName = await this.getAccessoryName(equipment, equipmentKey);
					if(accessoryName != null){
						switch(accessoryName){
							case "palletexchange": 
								equipmentType = "Reefer with Pallet Exchange";
								break;
							case "hazmat" : 
								equipmentType = "Hazmat Reefer";
								break;
						}
					}
				}
				break;
			case "sd":
				equipmentType = 'Step Deck';
				if(equipment[equipmentKey] != null)
				{
					let accessoryName = await this.getAccessoryName(equipment, equipmentKey);
					if(accessoryName != null){
						switch(accessoryName){
							case "hotshot" :
								equipmentType = "Hotshot Step Deck";
								break;
							case "removablegooseneck" : 
								equipmentType = "Removable Gooseneck";
								break;
						}
					}
				}
				break;
			case "t":
				equipmentType = 'Tanker';
				break;
			case "rv":
				equipmentType = 'Van/Reefer';
				break;
			case "vr":
				equipmentType = 'Van/Reefer';
				break;
			case "fv":
				equipmentType = 'Flatbed/Van';
				break;
			case "vf":
				equipmentType = 'Flatbed/Van';
				break;
			case "fsd":
				equipmentType = 'Flatbed/Step Deck';
				break;
			case "fvr":
				equipmentType = 'Flatbed/Van/Reefer';
				break;
			case "rfv":
				equipmentType = 'Flatbed/Van/Reefer';
				break;
			case "vfr":
				equipmentType = 'Flatbed/Van/Reefer';
				break;
			case "v":
				equipmentType = 'Van';
				if(equipment[equipmentKey] != null)
				{
					let accessoryName = await this.getAccessoryName(equipment, equipmentKey);
					if(accessoryName != null){
						switch(accessoryName){
							case "airride":
								equipmentType = "Air Ride Van";
								break;
							case "curtains":
								equipmentType = "Van with Curtains";
								break;
							case "palletexchange": 
								equipmentType = "Van with Pallet Exchange";
								break;
							case "hazmat" : 
								equipmentType = "Hazmat Van";
								break;
							case "vented":
								equipmentType = "Vented Van";
								break;
							case "team": 
								equipmentType = "Team Van";
								break;
							case "walkingfloor" :
								equipmentType = "Walking Floor Van";
								break;
						}
					}
				}
				break;
			default : 
				equipmentType = equipmentKey[0];
			break;
		}
		return equipmentType;
	}

	async getAccessoryName(equipment, equipmentKey) 
	{
		//_common.log("equipment", equipment[equipmentKey][0]);
		if (equipment[equipmentKey][0]["$"] != null && typeof equipment[equipmentKey][0]["$"] === 'object') {
			let accessory = equipment[equipmentKey][0]["$"];
			//_common.log("object accessory:", accessory);
			let accessoryNames = Object.getOwnPropertyNames(accessory);
			if (accessoryNames != null && accessoryNames.length > 0) {
				//_common.log("accessory Name:", accessoryNames[0]);
				return accessoryNames[0];
				}
		}
	}
  
	// Initially I tried to work with the data here, but it wasn't all present yet.	  
	async removeLoads(loads, user) {
		let response = true;
		_common.log(loads);

		if (!_common.isIterable(loads)) {
			return true;
		}

		for (let loaddata of loads) {
			try {
				//let loaddata = ld.load[0];
				//_common.log("loadData" ,JSON.stringify(loaddata));
				/*  loaddata data section start */

				let trackingn = loaddata['tracking-number'][0];
				let existingLoad = await Load.findOne({ tracking_number: Number(trackingn), created_by: user._id }, { is_opened: 1, _id: 1 });
				if (existingLoad == null) {
					_common.log('load: ', trackingn, ' not found.');
				}
				else {
					// await	Load.updateMany({tracking_number:Number(trackingn), created_by : user._id}, {$set:{is_opened : false}}) ;		 
					//  //_common.log('load ', trackingn, ' removed');	
					//  deleteLoadQueue.add({tracking_number:Number(trackingn), created_by : user._id});	 
					// await 	
					let rm_Loads = new removeableloads();
					rm_Loads.tracking_number = Number(trackingn);
					rm_Loads.created_by = user._id;
					await rm_Loads.save();
				}
			}
			catch (ex) {
				_common.log("error occured:", ex);
				response = false
			}

		}
		return response;
	}


	async addTrucks(trucks,user)
	{
		for(let ld of trucks)
		{
			try
			{
				let trucksdata = ld.truck[0];
				//_common.log("trucksdata:" ,trucksdata);
				var trackingn 		=	trucksdata['tracking-number'][0];
				var origin 	  		= 	trucksdata.origin[0];
				var origincity 		= 	origin.city[0];
				var originstate 	= 	origin.state[0];
				var originpostcode 	= 	origin.postcode[0];
				var origincounty 	= 	origin.county[0];
				var origincountry 	= 	origin.country[0];
				var pickup_addresses= 	"";
				if(origincity != null) pickup_addresses = origincity; 
				if(originstate != null & originstate != "" ) pickup_addresses= pickup_addresses.length > 0 ? pickup_addresses  + ','+originstate : originstate;
				if(originpostcode != null & originpostcode != "" )pickup_addresses = pickup_addresses.length > 0 ? pickup_addresses  + ','+originpostcode : originpostcode;
				if(origincountry != null & origincountry != "")pickup_addresses = pickup_addresses.length > 0 ? pickup_addresses  + ','+origincountry : origincountry;


				var originlatitude 	= "";	//origin.latitude[0]; //not used by posteverywhere
				var originlongitude = ""; //origin.longitude[0]; //not used by posteverywhere

				var origindatestart = 	origin['date-start'][0];
				var origin_year 	= 	origindatestart.year[0];
				var origin_month 	= 	origindatestart.month[0];
				var origin_day 		= 	origindatestart.day[0];

				var pickupdate 	= 	origin_month+'/'+origin_day+'/'+origin_year;
				if(pickupdate == "//")pickupdate = "";

				var origindateend 	= 	origin['date-end'][0];
				var originend_year 	= 	origindateend.year[0];
				var originend_month = 	origindateend.month[0];
				var originend_day 	= 	origindateend.day[0];
				var pickendupdate 	= 		originend_month+'/'+originend_day+'/'+originend_year;
				if(pickendupdate == "//")pickendupdate = "";
				var destination 			= 	trucksdata.destination[0];
				var destinationcity 		= 	destination.city[0];
				var destinationstate 		= 	destination.state[0];
				var destinationpostcode 	= 	destination.postcode[0];
				var destinationcounty 		= 	destination.county[0];
				var destinationcountry 		= 	destination.country[0];
				var destination_addresses	= "";
				if(destinationcity != null & destinationcity != "") destination_addresses = destinationcity; 
				if(destinationstate != null & destinationstate != "" ) destination_addresses= destination_addresses.length > 0 ? destination_addresses  + ','+destinationstate : destinationstate;
				if(destinationpostcode != null & destinationpostcode != "" )destination_addresses = destination_addresses.length > 0 ? destination_addresses  + ','+destinationpostcode : destinationpostcode;
				if(destinationcountry != null & destinationcountry != "" )destination_addresses = destination_addresses.length > 0 ? destination_addresses  + ','+destinationcountry : destinationcountry;

				var destinationlatitude 	= ""; //	destination.latitude[0]; //not used by posteverywhere
				var destinationlongitude 	= ""; //	destination.longitude[0]; //not used by posteverywhere

				var equipment 		= trucksdata.equipment[0];
				var equipmentType 	= this.getEquipmentTypes(equipment);
				var trucksize 		= trucksdata.trucksize[0];
				var full_or_partia 	= trucksize['$'].fullload;
				var length 		 	= trucksize.length[0];
				var height 		 	= trucksize.height[0];
				var width 		 	= trucksize.width[0];
				//var weight 		 	= trucksize.weight[0];
				var loadcount 		= trucksdata['tracking-number'][0];
				var comment 		= trucksdata.comment[0];
				/*  trucksdata data section end */
					

				if(originlatitude !== '' && originlongitude !== '' ){
					var pickup 	= Number(originlatitude);
					var pickup2 = Number(originlongitude);
					var pickup_lat_long = {type: "Point", coordinates:[pickup,pickup2]};

				} 
				
								//removing because we are incurring a huge bill from google for this api. 
				// maybe we can reactivate this feature when we have need for it and funds available
				// else{
					
				// 	var geocoordinates = await this.getCoordinates(origincity, originstate); 
				// 	if(geocoordinates!= null)
				// 	{
				// 	var pickup_lat_long = {type: "MultiPoint", coordinates:[ [ geocoordinates["geocode_long"],geocoordinates["geocode_lat"]]]};
				// 	}
				// }


				if(destinationlatitude !== '' && destinationlongitude !== '' ){
					var delivery = Number(destinationlatitude);
					var delivery2 = Number(destinationlongitude);
					var delev_lat_long = {type: "Point", coordinates: [delivery,delivery2] };
				} 
				
				//removing because we are incurring a huge bill from google for this api. 
				// maybe we can reactivate this feature when we have need for it and funds available
				// else{
				// 	var geocoordinates = await this.getCoordinates(destinationcity, destinationstate); 
				// 	if(geocoordinates!= null)
				// 	{
				// 	var delev_lat_long = {type: "MultiPoint", coordinates:[ [ geocoordinates["geocode_long"],geocoordinates["geocode_lat"]]]};
				// 	}
				// }

				var createby =  user._id;
				var truck 	 = new Truck();			
				truck.origin_bycity_address		  =	  pickup_addresses;
				truck.origin_bycity_city		  =   origincity;
				truck.origin_bycity_state		  =   originstate;
				truck.origin_bycity_zip			  =   originpostcode;
				truck.origin_bycity_lat_long	  =   pickup_lat_long;
				truck.destination_bycity_address  =   destination_addresses;
				truck.destination_bycity_city	  =   destinationcity;
				truck.destination_bycity_state	  =   destinationstate;
				truck.destination_bycity_zip	  =   destinationpostcode;
				truck.destination_bycity_lat_long =   delev_lat_long;
				truck.equipment_type			  =   equipmentType;
				truck.width			 		 	  =   width;
				truck.length			 		  =   length;
				truck.tracking_number			  =   trackingn;
				truck.ready_start			 	  =   pickupdate;
				truck.ready_end			 		  =   pickendupdate;
				truck.description			 	  =   comment;
				truck.created_by			 	  =   createby;
				truck.internal 					  = false;
				truck.source 					  = "PostEverywhere";
				try
				{
							
					var existingTruck = await Truck.findOne({tracking_number:trackingn, created_by : createdby});
					if(existingTruck == null){						
						var doc = await truck.save();
						if(doc){
							_common.log("truck ", trackingn, " not found, new truck added");
						}
					}
					else
					{						
							var result = await	Truck.updateOne({_id:existingTruck._id},{$set : 
								{
								origin_bycity_address		 					: truck.origin_bycity_address		  
								,origin_bycity_city		 						: truck.origin_bycity_city		     
								,origin_bycity_state		 					: truck.origin_bycity_state		     
								,origin_bycity_zip			 					: truck.origin_bycity_zip			  
								,origin_bycity_lat_long	 						: truck.origin_bycity_lat_long	     
								,destination_bycity_address 					: truck.destination_bycity_address   
								,destination_bycity_city	 					: truck.destination_bycity_city	     
								,destination_bycity_state	 					: truck.destination_bycity_state	  
								,destination_bycity_zip	 						: truck.destination_bycity_zip	     
								,destination_bycity_lat_long					: truck.destination_bycity_lat_long  
								,equipment_type			 						: truck.equipment_type			     
								,width			 		 	 					: truck.width			 		 	  
								,length			 		 						: truck.length			 		     
								,tracking_number			 					: truck.tracking_number			     
								,ready_start			 	 					: truck.ready_start			 	     
								,ready_end			 		 					: truck.ready_end			 		  
								,description			 	 					: truck.description			 	     
								,created_by			 	 						: truck.created_by			 	     
							}}); 
							//_common.log('truck ', trackingn, ' updated.');					 
					}
				}
				catch(ex)
				{
					_common.log("error occured:", ex);
					
				}				
			}
			catch(ex)
			{
				_common.log("error occured:", ex);
				return false
			}			
		
		}
		return true;
	}

	async removeTrucks(trucks, user)
	{
		var response = true;
		for(let ld of trucks)
		{
			try
			{
				let truckData = ld.truck[0];
				//_common.log("truckData" ,truckData);
				var trackingn 		=	truckData['tracking-number'][0];				
				var existingTruck = await Truck.findOne({tracking_number:trackingn, created_by : user._id});
				if(existingTruck == null){
					_common.log('truck: ', trackingn, ' not found.');
				}
				else
				{											
					 	var result = await	Truck.updateMany({tracking_number:trackingn, created_by : user._id}, {$set:{is_opened : false}}) ; 
					 	_common.log('truck ', trackingn, ' removed');				 	
				}
			}
			catch(ex)
			{
				_common.log("error occured:", ex);
				response = false
			}			
		
		}
		return response;
	}	
}
exports.CommonFunctions = CommonFunctions;

module.exports.postload = async function(req, res ) {

		/*  loaduser data section start */	
		var userLoad  		= req.body.peloadpostings.postingaccount[0];		
		//_common.log(JSON.stringify(userLoad));		
		var	userName 			= userLoad.username[0];
	
		_common.log("finding user details: ", userName);
		var user = await User.findOne({username:userName}); 
		
		if(user == null){
			
				_common.log("user with this userName: " + userName + " not found");
				res.status(200).send("Error: Please enter valid user name");
				return;	
		}
		else{
			if(user.status == "inactive"){
				_common.log("Account Inactive");
				res.status(200).send("Account inactive");
				return;
			}
		}

		/*  loaduser data section end */

		var common = new CommonFunctions();
		var postloadresult = true;
		var removeloadResult = true;
		
			
		var loads  = req.body.peloadpostings != null &&  req.body.peloadpostings.postloads != null ? req.body.peloadpostings.postloads : null ;
		//_common.log("loads: ",JSON.stringify(loads));

		if(typeof req.body.peloadpostings.postloads !== "undefined" ){
			_common.log('processing loads additions');
			var loads  = req.body.peloadpostings.postloads[0].load;
			postloadresult = await common.addLoads(loads, user);
		}

		if(typeof req.body.peloadpostings.removeloads !== "undefined" ){
			var loadsToRemove  = req.body.peloadpostings.removeloads;
			_common.log('processing loads removals');
			//_common.log(loadsToRemove[0]["load"]);
			removeloadResult = await common.removeLoads(loadsToRemove[0]["load"], user);
		}
		
		if(postloadresult && removeloadResult)
		{
			_common.log("Successfully posted");
			res.status(200).send("Successfully posted");
		}
		else{

			_common.log("Successfully posted");
			res.status(200).send("Successfully posted");
		}	
}

module.exports.validateAddress=async function(address){
	let geocode_long;
	let geocode_lat;
	let geocode = {};
	try
		{
			var geoInfo = await  geocoder.geocode(address);
			if(geoInfo != null){
				geocode_lat = (geoInfo && geoInfo.length >0)? geoInfo[0].latitude : " ";
				geocode_long = (geoInfo && geoInfo.length >0)? geoInfo[0].longitude : " ";
				geocode["geocode_lat"] = geocode_lat;
				geocode["geocode_long"] = geocode_long;
				geocode["state_code"] = (geoInfo[0].administrativeLevels)? geoInfo[0].administrativeLevels.level1short : "";
				geocode["country_code"] = (geoInfo[0].countryCode)? geoInfo[0].countryCode : "US";
				geocode["city"] = (geoInfo[0].city)? geoInfo[0].city : geoInfo[0].administrativeLevels.level2short.split(' ')[0] ;
				geocode["state"] = (geoInfo[0].administrativeLevels)? geoInfo[0].administrativeLevels.level1long : "";            
				_common.log(geoInfo);
				return geocode;
			}
			else {
				return null;
			}
		}        
        catch(err) {
            _common.log(err);
            return;
        }
	}

module.exports.removeload = async function(req, res) {


	/*  loaduser data section start */	
	var userLoad  		= req.body.peloadpostings.postingaccount[0];		
	//_common.log(JSON.stringify(userLoad));		
	var	contactemail 	= userLoad.contactemail[0];
	var	userName 			= userLoad.username[0];
	
	_common.log("finding user details");
	var user = await User.findOne({username:userName}); 
	
	if(user == null){
				
			_common.log("user with this userName: " + userName + " not found");
			res.status(200).send("Error: Please enter valid user name");
			return;	
	}
	else{
		if(user.status == "inactive"){
			_common.log("Account Inactive");
			res.status(200).send("Account inactive");
			return;
		}
	}

	/*  loaduser data section end */

	var common = new CommonFunctions();
	var removeloadResult = true;
	//_common.log(req.body.peloadpostings);

	if(typeof req.body.peloadpostings.removeloads !== "undefined" ){
		
		let loadsToRemove  = req.body.peloadpostings.removeloads;
		_common.log('processing loads removals');		
		removeloadResult = await common.removeLoads(loadsToRemove[0]["load"], user);
	}
	
	if(removeloadResult)
	{
		_common.log("Successfully posted");
		res.status(200).send("Successfully posted");
	}
	else{

		_common.log("Data Invalid");
		res.status(200).send("Data Invalid");
	}	
}



module.exports.posttruck = async function(req, res) {
		

   /*  user data section start */
	var userLoad  		= req.body.peloadpostings.postingaccount[0];		
	//_common.log(JSON.stringify(userLoad));		
	var	userName 			= userLoad.username[0];

	_common.log("finding user details");
	var user = await User.findOne({username:userName}); 
	
	if(user == null){
		
		if(err){
			_common.log("Error:" , err);
		}
		if(user == null){
			_common.log("user with this userName not found");
			res.status(200).send("Error: Please enter valid user name");
			return;
			
		}
	}
	else{
		if(user.status == "inactive"){
			_common.log("Account Inactive");
			res.status(200).send("Account inactive");
			return;
		}
	}

   /*  loaduser data section end */

   	var common = new CommonFunctions(); 
   	var posttruckresult = true;
	var removetruckResult = true;
		
			
	var trucks  = req.body.petruckpostings != null &&  req.body.petruckpostings.posttrucks != null ? req.body.petruckpostings.posttrucks : null ;
	//_common.log("trucks: ",JSON.stringify(trucks));

	if(typeof req.body.peloadpostings.posttrucks !== "undefined" ){
		_common.log('processing truck additions');
		var trucks  = req.body.peloadpostings.posttrucks[0].truck;
		posttruckresult = await common.addTrucks(trucks, user);
	}

	if(typeof req.body.peloadpostings.removetrucks !== "undefined" ){
		_common.log('processing truck removals');
		var trucksToRemove  = req.body.peloadpostings.removetrucks;
		removetruckResult = await common.removeTrucks(trucksToRemove, user);
	}

	if(posttruckresult && removetruckResult)
	{
		_common.log("Successfully posted");
		res.status(200).send("Successfully posted");
	}
	else{

		_common.log("Successfully posted");
		res.status(200).send("Successfully posted");
	}	
}


module.exports.removetruck = async function(req, res) {

	/*  user data section start */
	var userLoad  		= req.body.peloadpostings.postingaccount[0];		
	//_common.log(JSON.stringify(userLoad));		
	var	userName 			= userLoad.username[0];

	_common.log("finding user details");
	var user = await User.findOne({username:userName}); 
	
	if(user == null){
		
		if(err){
			_common.log("Error:" , err);
		}
		if(user == null){
			_common.log("user with this userName not found");
			res.status(200).send("Error: Please enter valid user name");
			return;
			
		}
	}
	else{
		if(user.status == "inactive"){
			_common.log("Account Inactive");
			res.status(200).send("Account inactive");
			return;
		}
	}

   /*  loaduser data section end */
   	var common = new CommonFunctions(); 
	var removetruckResult = true;

	if(typeof req.body.peloadpostings.removetrucks !== "undefined" ){
		_common.log('processing truck removals');
		var trucksToRemove  = req.body.peloadpostings.removetrucks;
		//_common.log(trucksToRemove);
		removetruckResult = await common.removeTrucks(trucksToRemove, user);
	}

	if(removetruckResult)
	{
		_common.log("Successfully posted");
		res.status(200).send("Successfully posted");
	}
	else{

		_common.log("Successfully posted");
		res.status(200).send("Successfully posted");
	}	
}


function parse_place(place)
{
    var location = [];
    var address = place.split(',');
    _common.log(address.address_components)
    return location;
}
