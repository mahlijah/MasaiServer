const twillioSMS = require('../helpers/twilioSMS');
var mongoose = require('mongoose');
var User = mongoose.model('User');
const common = require('../helpers/common');

module.exports.sendVerificationCode = function (req, res) {
    var errorList=[];
    if(!req.body.id) errorList.push({field:"id",message:"id is required"})
    if(!req.body.phone) errorList.push({field:"phone",message:"phone is required"})
    if(errorList.length>0){
        return res.status(400).send({message:'field missing',err: errorList});
    }

    var code = Math.floor((Math.random() * 999999) + 111111);
    User.findById(req.body.id).exec(function (geterr, user) {
        if (geterr) {
            return res.status(400).send({ message: 'User not found' })
        }
        if (!user) {
            return res.status(400).send({ message: 'User not found' })
        }
        var updateModel = {
            phoneVerificationCode: code
        }
        if (user.phone != req.body.phone) {
            updateModel.phone = req.body.phone
        }
        twillioSMS.sendSMS(req.body.phone, 'Your verification code is: ' + code, function (err, message) {
            if (err) {
                common.log('Error while send twilio sms', err)
                return res.status(400).send(err)
            }
            User.update({ _id: user._id }, { $set: updateModel }, { useFindAndModify: true }).exec(function (err) {
                if (err) {
                    return res.status(400).send({ message: 'User not found' })
                }
                return res.send({success:true, message:'SMS sent on you phone number'});
            })
        })

    });
}

module.exports.verifyPhoneCode = function(req,res){
    var errorList=[];
    if(!req.body.id) errorList.push({field:"id",message:"id is required"})
    if(!req.body.verifyCode) errorList.push({field:"verifyCode",message:"Verify code is required"})
    if(errorList.length>0){
        return res.status(400).send({message:'field missing',err: errorList});
    }

    User.findById(req.body.id).exec(function(err,user){
        if(err){
            return res.status(400).send({message:"User not found"});
        }
        if(!user){
            return res.status(400).send({message:"User not found"});
        }
        if(user.phoneVerificationCode == req.body.verifyCode){
            User.update({_id:user._id},{$set:{phoneVerified:true}},{useFindAndModify:true}).exec(function(err){
                if(err){
                    common.log("Error while update phone verify",err);
                    return res.status(400).send({message:"Unable to update user."})
                }
                return res.send({success:true,message:"phone verify"});
            })
        }
    })
}