const mongoose = require("mongoose");
const Influencer = mongoose.model("Influencer");
const User = mongoose.model("User");
const Referal = mongoose.model("referal");
const stripeHelper = require("../helpers/stripe");
const common = require("../helpers/common");

/**
 * if promo is code true
 * create promotion and coupon for that promotion
 *
 */
exports.createReferalLink = async (req, res) => {
  try {
    const url = req.headers.origin || process.env.PORTAL_URL;
    console.log("url:__", url, "\t:____", req.headers)
    let _promo_details;
    let _referal = new Referal();
    _referal.referralLink = await alphaNumeric("uppercase", 5, 4);
    if (req.body.is_promo && req.body.discount_details) {
      //   _promo_details = await processPromotionCode(req.body.discount_details);
      _referal.promo_details = req.body.discount_details
    }
    if (req.body.userId) _referal.userId = req.body.userId;

    if (req.body.influencerId) _referal.influencerId = req.body.influencerId;

    if (req.body.is_promo) _referal.is_promo = true;

    if (req.body.created_by) _referal.created_by = req.body.created_by;

    await _referal.save();

    let Link = url + "/public/signup?ref=" + _referal.referralLink;
    _referal.referralLink = Link;
    return res.send({ success: true, data: _referal });
  } catch (error) {
    return res.status(400).send({ success: false, message: error.message });
  }
};
/**
 * get influencer referal link by id
 */
exports.getInlfuencerReferalLinks = async (req, res) => {
  try {
    let _referals;
    if (req.body.referralLink) {
      _referals = await Referal.findOne({ referralLink: req.body.referralLink }).populate("created_by")
        .populate('influencerId').populate("userId");
    } else {
      _referals = await Referal.find().populate("created_by")
        .populate('influencerId').populate("userId").sort({ createdAt: -1 });;
    }
    return res.status(200).send({ success: true, data: _referals });
  } catch (error) {
    common.log("influenecer referal link:____", error.message);
    return res.status(400).send({ success: false, message: err.message });
  }
};

/**
 * get referal link by id
 */
exports.getReferalLinksById = async (req, res) => {
  try {
    let _referals;
    if (req.query.user) {
      _referals = await Referal.find({ userId: req.query._id })
        .populate("created_by").populate("userId").populate("influencerId")
        .sort({ createdAt: -1 });
    } else {
      _referals = await Referal.find({ influencerId: req.query._id })
        .populate("created_by").populate("userId").populate("influencerId")
        .sort({ createdAt: -1 });
    }
    return res.status(200).send({ success: true, data: _referals });
  } catch (error) {
    common.log("influenecer referal link:____", error.message);
    return res.status(400).send({ success: false, message: err.message });
  }
};

/*
 * Remove Commas
 */
async function removeCommas(word) {
  return word.replace(/,/g, "");
}

/*
 * Generate alpha-numeric referral code
 */
async function alphaNumeric(type, wordlength, numlength) {
  // english alphabets
  let alphabets = [
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",
  ];

  // english numbers
  let numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

  // check for word case type
  if (type == "uppercase") {
    // alphabets and numbers collection array
    let picks = [];
    // loop through alphabets array and pick an alphabet with the generated index
    for (let i = 0; i < wordlength; i++) {
      let key = Math.floor(Math.random() * alphabets.length);
      picks.push(alphabets[key].toUpperCase());

      // loop through numbers array and pick a number with the generated index
      for (let k = 0; k < numlength; k++) {
        let pin = Math.floor(Math.random() * numbers.length);
        picks.push(numbers[pin]);
      }
    }

    // convert selected alphabets array to string and remove seperating commas
    let letters = picks.toString();
    let data = await removeCommas(letters);

    // return letters in wordcase format and numbers
    return data;
  } else {
    // alphabets and numbers collection array
    let picks = [];

    // loop through alphabets array and pick an alphabet with the generated index
    for (let i = 0; i < wordlength; i++) {
      let key = Math.floor(Math.random() * alphabets.length);
      picks.push(alphabets[key].toLowerCase());

      // loop through numbers array and pick a number with the generated index
      for (let k = 0; k < numlength; k++) {
        let pin = Math.floor(Math.random() * numbers.length);
        picks.push(numbers[pin]);
      }
    }

    // convert selected alphabets array to string and remove seperating commas
    let letters = picks.toString();
    let data = removeCommas(letters);

    // return letters in wordcase format and numbers
    return data;
  }
}

/**
 * create promotion code for the referal if the promotion code is not already exist
 * if promotion code already exist add the existing promo code to the referal link
 */
async function processPromotionCode(reqdetails) {
  try {
    return true;
  } catch (error) {
    throw error;
  }
}


exports.testxml = async (req, res) => {
  try {
    let resp = await common.xml_call('POST');
    return res.status(200).send({ data: resp });
  } catch (error) {
    console.log("ERROR in test xml\n", error);
    return res.status(400).send({ success: false, error: error.message })
  }
}

exports.xmlreader = async (req, res) => {
  try {
    let parseString = require('xml2js').parseString;

    let d = `<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
 <soap:Body>
 <getFullReportV3Response xmlns="http://tempuri.org/">
 <getFullReportV3Result>
 <NoOfRecords>int</NoOfRecords>
 <Info>string</Info>
 <ID>string</ID>
 <CompanyName>string</CompanyName>
 <DOT>string</DOT>
 <Address>string</Address>
 <City>string</City>
 <State>string</State>
 <Zip>string</Zip>
 <County>string</County>
 <Headquarters>string</Headquarters>
 <Phone>string</Phone>
 <t800>string</t800>
 <Fax>string</Fax>
 <FileDate>string</FileDate>
 <BusOrgnz>string</BusOrgnz>
 <FID>string</FID>
 <BusEstab>string</BusEstab>
 <YrsInBus>string</YrsInBus>
 <CorpOfficerOwner>string</CorpOfficerOwner>
 <Title>string</Title>
 <TypeofBusiness>string</TypeofBusiness>
 <BillingDocuments>string</BillingDocuments>
 <Email>string</Email>
 <BillingCity>string</BillingCity>
 <BillingState>string</BillingState>
 <BillingZip>string</BillingZip>
 <SendFB>string</SendFB>
 <AnnualSales>string</AnnualSales>
 <Emply>string</Emply>
 <Collections>string</Collections>
 <Days_To_Pay>string</Days_To_Pay>
 <AvgRefYrs>string</AvgRefYrs>
 <AvgCredXtension>string</AvgCredXtension>
 <CreditScore>string</CreditScore>
 <Remarks>string</Remarks>
 <Bank>string</Bank>
 <BankCity>string</BankCity>
 <BankState>string</BankState>
 <BankAcct>string</BankAcct>
 <BrokAuth>string</BrokAuth>
 <ContAuth>string</ContAuth>
 <CommAuth>string</CommAuth>
 <PendBrokAuth>string</PendBrokAuth>
 <PendContAuth>string</PendContAuth>
 <PendCommAuth>string</PendCommAuth>
 <BrokAuthRevoc>string</BrokAuthRevoc>
 <ContAuthRevoc>string</ContAuthRevoc>
 <CommAuthRevoc>string</CommAuthRevoc>
 <BondSurReqYn>string</BondSurReqYn>
 <CargoReqYn>string</CargoReqYn>
 <BiPdReq>string</BiPdReq>
 <BondOnFile>string</BondOnFile>
 <CargoOnFile>string</CargoOnFile>
 <BiPdOnFile>string</BiPdOnFile>
 <BondCo>string</BondCo>
 <BondNo>string</BondNo>
 <BondDate>string</BondDate>
 <BondAttn>string</BondAttn>
 <BondCity>string</BondCity>
 <BondSt>string</BondSt>
 <BondPhn>string</BondPhn>
 <FundCo>string</FundCo>
 <FundNo>string</FundNo>
 <FundDate>string</FundDate>
 <FundCity>string</FundCity>
 <FundSt>string</FundSt>
 <FundPhn>string</FundPhn>
 <CargoCo>string</CargoCo>
 <CargoNo>string</CargoNo>
 <CargoDate>string</CargoDate>
 <CargoAttn>string</CargoAttn>
 <CargoCity>string</CargoCity>
 <CargoSt>string</CargoSt>
 <CargoPhn>string</CargoPhn>
 <LiabCo>string</LiabCo>
 <LiabNo>string</LiabNo>
 <LiabDate>string</LiabDate>
 <LiabAttn>string</LiabAttn>
 <LiabCity>string</LiabCity>
 <LiabSt>string</LiabSt>
 <LiabPhn>string</LiabPhn>
 <DBA>string</DBA>
 <Website>string</Website>
 <Other_Offices>string</Other_Offices>
 <Filings>string</Filings>
 <MemberOf>string</MemberOf>
 <CorporateInfoLastUpdate>string</CorporateInfoLastUpdate>
 <ReferencesLastUpdated>string</ReferencesLastUpdated>
 <CreditSummary>
 <ReferencesDetailsFR>
 <Date>string</Date>
 <Providers>int</Providers>
 <Monthly_Credit>string</Monthly_Credit>
 <Current>string</Current>
 <Current_Per>string</Current_Per>
 <Term1_30>string</Term1_30>
 <Term1_30_Per>string</Term1_30_Per>
 <Term31_60>string</Term31_60>
 <Term31_60_Per>string</Term31_60_Per>
 <Term61_90>string</Term61_90>
 <Term61_90_Per>string</Term61_90_Per>
 <Term91>string</Term91>
 <Term91_Per>string</Term91_Per>
 <Balance>string</Balance>
.
.
 </ReferencesDetailsFR>
 </CreditSummary>
 <IndustrySummary>
 <IndustryDetailsFR>
 <Industry_Name>string</Industry_Name>
 <Providers>string</Providers>
 <DaysToPay>string</DaysToPay>
 <Monthly_Credit>string</Monthly_Credit>
 <Current>string</Current>
 <Current_Per>string</Current_Per>
 <Term1_30>string</Term1_30>
 <Term1_30_Per>string</Term1_30_Per>
 <Term31_60>string</Term31_60>
 <Term31_60_Per>string</Term31_60_Per>
 <Term61_90>string</Term61_90>
 <Term61_90_Per>string</Term61_90_Per>
 <Term91>string</Term91>
 <Term91_Per>string</Term91_Per>
 <Total_Balance>string</Total_Balance>
 </IndustryDetailsFR>
 .
.
 </IndustrySummary>
 <HistoryCSDTP>
 <CreditScorePayTrendFR>
 <ReportID>int</ReportID>
 <CreditScore>string</CreditScore>
 <AverageDaysToPay>string</AverageDaysToPay>
 <ScoreDate>dateTime</ScoreDate>
 </CreditScorePayTrendFR>
 .
.
 </HistoryCSDTP>
 <ReferenceDetails>
 <RefDetailsFR>
 <Name>string</Name>
 <NoofInvoices>string</NoofInvoices>
 <YearsOpen>string</YearsOpen>
 <DaysToPay>string</DaysToPay>
 <Terms>string</Terms>
 <amountCurrent>string</amountCurrent>
 <amountThirty>string</amountThirty>
 <amountSixty>string</amountSixty>
 <amountNinety>string</amountNinety>
 <amountNinetyplus>string</amountNinetyplus>
 <TotalBalance>string</TotalBalance>
 <Credit>string</Credit>
 <lastSaleDate>dateTime</lastSaleDate>
 <Collections>string</Collections>
 <Industry>string</Industry>
 </RefDetailsFR>
 .
.
 </ReferenceDetails>
 </getFullReportV3Result>
 </getFullReportV3Response>
 </soap:Body>
</soap:Envelope>`

    parseString(d, function (err, result) {
      return res.status(200).send({ status: true, result });
    });

  } catch (error) {
    console.log("error in xml reader :__", error.message);
    return res.status(400).send({ status: false, error: error.message });
  }
}