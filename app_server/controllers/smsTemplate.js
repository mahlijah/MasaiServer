const { ObjectID } = require('mongodb');
const mongoose = require('mongoose');
const SmsTemplate = mongoose.model('SmsTemplate');
const SmsType = mongoose.model('SmsTypes');
const User = mongoose.model('User');
const config = require('../config/config');
const common = require('../helpers/common');
const jwt = require('jsonwebtoken');
const SmsTemplateService = require('../services/smsTemplateService');


module.exports.getSmsTypes = async function(req,res){
    try{
        let findSmsTypes=await SmsType.find({},{sms_type:1,_id:0}).lean();
        
        if(findSmsTypes.length>0){
            let newSmsType=findSmsTypes.map(result=>result.sms_type);
          
        return res.send({success:true,data:newSmsType});
        }else{
            common.log('Error while get sms types  :--');
        
            return res.status(400).status({success:false,message:'sms list not found'});
        }
        
    } catch(err){
        common.log('Error while get sms types  :--',err);
        return res.status(400).send({success:false,message:err.message});
    }
}

module.exports.getSmsTypesById = async function(req,res){
    let {id}=req.params;
    try{
        let findSmsType=await SmsType.findOne({_id:ObjectID(id)}).lean();
        return res.send({success:true,data:findSmsType.sms_type});
    } catch(err){
        common.log('Error while get sms types  :--',err);
        return res.status(400).send({success:false,message:err.message});
    }
}

module.exports.postTypeSms=async function(req,res){
    let {sms_type}=req.body;
    if (!sms_type) {
        common.log("Error  email type--- ");
        return res.status(400).send({
          field: 'sms_type',
          message: 'sms type is required'
        })
    }
    if (!req.payload._id) {
        common.log("Error  user not found--- ");
        return res.status(400).send({
          message: 'user not found'
        })
    }
    let findSmsType=await SmsType.findOne({sms_type:sms_type})
    if(!findSmsType){
        var smsType = new SmsType();
        smsType.sms_type=sms_type;
        let smsTypeSave=await smsType.save(); 
        if(!smsTypeSave){
            common.log('Sms Type not save successfully');
            return res.status(400).json({message:"Sms Type not save successfully"})
        }else{
            return res.status(200).json({message:smsTypeSave})
        }
        
    }else{
        return res.status(400).json({message:"Sms Type exist"})
    }
                  
}



module.exports.updateTypeSms=async function(req,res){
    let {id}=req.params;
    let {sms_type}=req.body;
    if (!id) {
        common.log("Error  id is not available--- ");
        return res.status(400).send({
           message: 'id is required'
        })
    }
    let smsType=await SmsType.findById({_id:ObjectID(id)}) 
     if(smsType &&  sms_type!=''){
       let updateSMS= await  SmsType.updateOne({_id:ObjectID(id)},{$set:{sms_type:sms_type}})
       if(updateSMS){
           return res.status(200).json({message:'sms type updated'})
       }else{
        common.log('sms type is not updated');
        return res.status(400).json({message:'sms type is not updated'})
       }
     }else if(smsType && sms_type==''){
        let updateWithExitingSmsType= await  SmsType.updateOne({_id:ObjectID(id)},{$set:{sms_type:smsType.sms_type}})
        if(updateWithExitingSmsType){
            return res.status(200).json({message:'sms type updated with existing one'})
        }else{
         common.log('sms type is not updated');
         return res.status(400).json({message:'sms type is not updated'})
        }   
     }
     else{
        common.log('sms type is not found');
        return res.status(400).send({
            message: 'sms_type is not found'
         })
     }       

}

module.exports.deleteTypeSms=async function(req,res){
    let {id}=req.params;
   
    if (!id) {
        common.log("Error  id is not available--- ");
        return res.status(400).send({
           message: 'id is required'
        })
    }
    let deleteSMS= await  SmsType.deleteOne({_id:id})
    if(deleteSMS){
           return res.status(200).json({message:"sms type deleted succesfully"})
    }else{
        common.log('sms type is not deleted');
        return res.status(400).json({message:'sms type is not deleted'})
    }
}

module.exports.getSmsTemplateList = async function(req,res){
    try{
        let limit=req.body.limit||10;
        let skip= ((req.body.skip||1)-1)*limit;
        let sortBy= req.body.sortBy || 'updateAt';
        let sortDirection= req.body.sortDirection ==asc?1:-1;
        let sort = {};
        sort[sortBy]=sortDirection;
        let [count,data]= await Promise.all([
            SmsTemplate.count(),
            SmsTemplate.find({}).sort(sort).skip(skip).limit(limit).lean()
        ]);
        return res.send({success:true,count,data});
    } catch(err){
        common.log('Error while get email templates  :--',err);
        return res.status(400).send({success:false,message:err.message});
    }
}
module.exports.getSmsTemplateByType = async function(req,res){
    try{
        const data = await SmsTemplateService.getSmsTemplateByType(req.params.type);
        return res.send({success:true,data});
    } catch(err){
        common.log('Error while get sms templates  :--',err);
        return res.status(400).send({success:false,message:err.message});
    }
}

module.exports.addSmsTemplate = async function(req,res){
    try{
        if( !req.body.body || !req.body.type) {
            return res.status(400).send({success:false,message:"Sms body and type is required"});
        }
        let smsTemplate = new SmsTemplate();
        smsTemplate.body = req.body.body;
        smsTemplate.type = req.body.type;
        let saveSmsTemplate = await smsTemplate.save();
        return res.send({success:true,data:saveSmsTemplate});
    } catch(err){
        common.log('Error while add sms template :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}

module.exports.editSmsTemplate = async function(req,res){
    try{
        if(!req.params.id) {
            return res.status(400).send({success:false,message:"id is required for edit email template"});
        }
        let smsTemplate = await SmsTemplate.findById(req.params.id);
        if(req.body.body) smsTemplate.body = req.body.body;
        if(req.body.type) smsTemplate.type = req.body.type;
        let upsert = smsTemplate.toObject();
        delete upsert._id;
        await SmsTemplate.updateOne({_id:ObjectID(req.params.id)},upsert);
        return res.send({success:true,data:null});
    } catch(err){
        common.log('Error while add sms template :--',err)
        return res.status(400).send({success:false,message:err.message});
    }
}

