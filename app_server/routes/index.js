var express = require('express');
var multer = require('multer');
var router = express.Router();
const {fileUpload,csvFileUpload, fileUploadImageOnly} = require('../middleware/fileuploadMiddleware');
var jwtMiddleware = require('../middleware/jwtMiddleware');
var {duplicateEmailValidation, duplicateNewRegisterEmailValidation, duplicateNewRegisterPhoneValidation, duplicateNewRegisterEmailForMarketingUserValidation} = require('../middleware/validation/duplicateEmailValidation');
var {registerStep1Validation,registerStep2Validation,registerStep3Validation,registerStep1MobileValidation} = require('../middleware/validation/registerValidation');
var {duplicateRoleValidation} = require('../middleware/validation/roleValidation');
var auth = jwtMiddleware.AuthMiddleWare;
var authAdmin = jwtMiddleware.AuthAdminMiddleWare;
var authCompanyAdmin = jwtMiddleware.AuthCompanyAdminMiddleWare;
var authTelemarketingAndAdminMiddleWare = jwtMiddleware.AuthTelemarketingAndAdminMiddleWare;
var upload = multer();
var ctrlRevenuchart = require('../controllers/revenuchart');
var ctrleverwhere = require('../controllers/posteverywhere');
var ctrlProfile = require('../controllers/profile');
//var customuser = require('../controllers/customuser');
var ctrlPrice = require('../controllers/price');
var ctrlTruck = require('../controllers/trucks');
var ctrlState = require('../controllers/state');
var ctrLoads = require('../controllers/loads');
var ctrAccount = require('../controllers/accountinfo');
var ctrlAuth = require('../controllers/authentication');
var ctrlNotification = require('../controllers/notification');
var ctrlUser = require('../controllers/users');
var ctrlNote = require('../controllers/notes');
var ctrlPhoneVerification= require('../controllers/phoneverification');
var ctrlTransaction= require('../controllers/transaction');
var ctrlDatComapny = require('../controllers/datCompany');
var ctrlSubscriptionPackages = require('../controllers/subscriptionPackages');
var ctrlCompany = require('../controllers/company');
var ctrlSetDefaultData = require('../controllers/setDefaultData');
var ctrlSmsAlert = require('../controllers/smsAlert');
var ctrlAdvertisement = require('../controllers/advertisement');
var ctrlEmailTemplate = require('../controllers/emailTemplate');
var ctrlSmsTemplate = require('../controllers/smsTemplate');
var ctrlRole = require('../controllers/role');
var ctrlDemo = require('../controllers/demo');
var ctrlOffer = require('../controllers/offer');
const ctrlActivityLogs= require('../controllers/userActivityLog');
const ctrlreports = require('../controllers/reports');
const ctrlInfluencer= require('../controllers/influencer');
const ctrlreferal= require('../controllers/referalcontroller');

// profile
router.get('/profile', auth ,  ctrlProfile.profileRead);
router.get('/getAuthuser', auth ,ctrlProfile.authuser);
//router.get('/userres', auth,ctrlActivityLogs.storeLogs, customuser.register);
//router.get('/profile', auth,ctrlActivityLogs.storeLogs, ctrlProfile.profileRead);
router.get('/profile/:userID', auth,ctrlActivityLogs.storeLogs,  ctrlProfile.userProfileRead);
router.put('/update_account', auth,ctrlActivityLogs.storeLogs,ctrlProfile.updateProfile);
router.post('/updateAccount',  ctrlProfile.updateProfile2);
// router.get('/state', auth,ctrlActivityLogs.storeLogs, ctrlProfile.stateRecord);
// router.post('/update_subscription', auth,ctrlActivityLogs.storeLogs, ctrlProfile.updateSubscription);
router.post('/update_subscription', auth,ctrlActivityLogs.storeLogs, ctrlProfile.updateStripeSubscription);
router.post('/cancel_subscription', auth,ctrlActivityLogs.storeLogs, ctrlProfile.cancelSubscription);
router.post('/initiatePayment',ctrlProfile.createStripeToken);
router.post('/cancel_subscription_schedule',auth, ctrlProfile.cancelUpdateScheduleSubscriptionRequest);
//stripe webhook route on payment success
router.post('/stripePaymentComplete',ctrlProfile.StripePaymentComplete);
router.post('/Userdocs',auth,ctrlActivityLogs.storeLogs ,ctrlProfile.updateDcuments);
router.post('/upload_profile_pic',auth,ctrlActivityLogs.storeLogs,fileUploadImageOnly, ctrlProfile.updateAvatar);
router.post('/get_profile_uploads', auth,ctrlActivityLogs.storeLogs, ctrlProfile.getProfileUploads);
router.post('/upload_document', auth,ctrlActivityLogs.storeLogs, fileUpload, ctrlProfile.uploadDocument);
router.get('/download_user_document/:id',ctrlProfile.downloadFile);
router.delete('/upload_document/:id',auth,ctrlActivityLogs.storeLogs,ctrlProfile.deleteUploadDocument);
router.post('/updatePaymentMethod',auth,ctrlActivityLogs.storeLogs,ctrlProfile.updatePaymentMethod)
router.post('/updatePaymentMethod/:id',auth,ctrlActivityLogs.storeLogs,ctrlProfile.updatePaymentMethodAdmin)
//states trucks and loads
router.get('/state/:state', auth,ctrlActivityLogs.storeLogs, ctrlTruck.trucksAndLoadsInState);
router.post('/watchListTrucks',auth,ctrlActivityLogs.storeLogs,  ctrlTruck.watchlistHttp);
router.post('/watchListLoads',auth,ctrlActivityLogs.storeLogs,  ctrLoads.watchlistHttp);
router.post('/findTrucks',  ctrlTruck.findTruckshttp);
router.post('/state/create', ctrlState.create);
router.get('/state/state/list/type/:type', ctrlState.list);
//price
router.get('/subscription_price',ctrlPrice.subscriptionPrice);
// authentication
router.post('/create_account', ctrlAuth.register); 
router.post('/createAccount', ctrlAuth.posteverywhereregister);
router.post('/verifyAccount', ctrlAuth.posteverywherelogin);
router.post('/login',ctrlActivityLogs.storeLogs ,ctrlAuth.login);
router.post('/checkAndLogin', ctrlActivityLogs.storeLogs,ctrlAuth.checkAndLogin);
router.post('/logout',auth,ctrlActivityLogs.storeLogs,ctrlActivityLogs.storeLogs,ctrlAuth.logout);
// router.post('/forgot_password', ctrlAuth.forgotPassword);
router.post('/send_reset_password_link', ctrlAuth.sendResetPasswordLink); //forgot password
router.post('/verify_reset_token', ctrlAuth.verifyResetToken);
router.post('/reset_password', ctrlAuth.resetPassword);
router.post('/activateUser', ctrlAuth.activateAccount);
router.post('/sendActivationEmail',ctrlAuth.sendActivationEmail);
router.post('/sendVerifiationEmail',auth,ctrlAuth.sendVerifiationEmail);
router.get('/my-access-list',auth,ctrlAuth.getMyAccessList);
//Post everyWhere Api 
router.post('/PostLoads', ctrleverwhere.postload);
router.post('/removeLoads', ctrleverwhere.removeload);
router.post('/PostTrucks', ctrleverwhere.posttruck);
router.post('/removeTrucks', ctrleverwhere.removetruck);
//load
router.get('/loginLoadsfind', ctrLoads.loginLoadsfind);
router.post('/findLoads', ctrLoads.findLoadshttp);
router.post('/realTimeFindLoads', auth,ctrlActivityLogs.storeLogs, ctrLoads.realTimeFindLoads);
router.get('/findLoadDetail/:id', auth,ctrlActivityLogs.storeLogs, ctrLoads.findLoadDetail);
router.post('/deleteSearch', auth,ctrlActivityLogs.storeLogs,ctrLoads.removeSearchItem);
router.post('/getSearchList', auth,  ctrLoads.getSearchList);
router.post('/getSearchListV2',auth, ctrLoads.getSearchListV2);
router.post('/getSearchListV2_count', auth, ctrLoads.getSearchListV2_count);
router.post('/getHeatMapData', auth,ctrlActivityLogs.storeLogs,ctrLoads.heatMapData);
router.post('/findLoadsForLocation', ctrLoads.loginLocationLoads);
// chart
router.get('/revanuChart', ctrlRevenuchart.revanueChart);
//notification
router.post('/createNotification', ctrlNotification.createNotification);
router.post('/getNotifications', ctrlNotification.myNotifications);
router.post('/deleteNotification', ctrlNotification.deleteNotification);
router.post('/suspendNotification', ctrlNotification.suspendNotification);
router.post('/resumeNotification', ctrlNotification.resumeNotification);
router.post('/updateNotification', ctrlNotification.updateNotification);
// account
router.post('/factoringRequest', ctrAccount.factoringRequest);
router.post('/contactMessage',ctrAccount.contactMessage );
// user
//router.get('/users',authAdmin, ctrlUser.getAllUsersWithoutFilter);
router.post('/getUsers',authTelemarketingAndAdminMiddleWare, ctrlUser.getAllUsers);
router.post('/getAllInternalUsers',authTelemarketingAndAdminMiddleWare, ctrlUser.getAllInternalUsers);
router.post('/getInCompleteUsers',authTelemarketingAndAdminMiddleWare, ctrlUser.getAllInCompleteUsers);
router.post('/getPostEveryWhereUsers',authTelemarketingAndAdminMiddleWare, ctrlUser.getPostEveryWhereUsers);
router.get('/exportUserToCSV',authTelemarketingAndAdminMiddleWare, ctrlUser.exportUserToCSV);
router.get('/exportUserToCSVByStripeStatus/:status',authTelemarketingAndAdminMiddleWare,ctrlUser.exportUserToCSVByStripeStatus);
router.get('/exportToCSVOnTab/:tabStatus',authTelemarketingAndAdminMiddleWare, ctrlUser.exportToCSVOnTab);
router.get('/user/:id',authTelemarketingAndAdminMiddleWare, ctrlUser.getUser);
router.put('/user/:id',authTelemarketingAndAdminMiddleWare,duplicateEmailValidation, ctrlUser.updateUser);
router.put('/user/:id/activate',authTelemarketingAndAdminMiddleWare, ctrlUser.userActivate);
router.put('/user/:id/deactivate',authTelemarketingAndAdminMiddleWare, ctrlUser.userInactive);
router.put('/user/:id/cancel_subscription', authTelemarketingAndAdminMiddleWare, ctrlUser.cancelSubscription); //admin cancel subscription
router.put('/user/:id/regenerate_password', authTelemarketingAndAdminMiddleWare, ctrlUser.changePassword);
router.put('/user/:id/set_as_internal', authTelemarketingAndAdminMiddleWare, ctrlUser.setAsInternal);
router.put('/user/:id/unset_as_internal', authTelemarketingAndAdminMiddleWare, ctrlUser.unSetAsInternal);
// note
router.post('/note',authTelemarketingAndAdminMiddleWare, ctrlNote.saveNote);

// new registration process end point (25 Dec 2020)
router.post('/registerStepFirst', registerStep1Validation, ctrlAuth.newRegisterStep1);
router.post('/registerStepSecond',registerStep2Validation,ctrlAuth.newRegisterStep2);
router.post('/registerStepThird',registerStep3Validation,ctrlAuth.newRegisterStep3);
router.post('/found_us',ctrlAuth.found_us);
router.post('/findUserByEmail',ctrlAuth.findUserByEmail);
router.post('/setSelectedSubscriptionPackage',ctrlAuth.setSelectedSubscriptionPackage);
router.post('/validateEmail',ctrlAuth.validateEmail);
// verification phone sms
router.post('/sendVerificationCode', ctrlPhoneVerification.sendVerificationCode);
router.post('/verifyPhoneCode', ctrlPhoneVerification.verifyPhoneCode);
//router.get('/sendSubscriptionRenewalemail',ctrlAuth.sendSubscriptionRenewalemail);
// router.get('/updateReadyDate',ctrlTruck.updateReadyDate);
router.post('/getSearchListGroupBy',auth,  ctrLoads.getSearchListGroupBy);
// Transaction
router.get('/defaultExpensesTransactions', auth,ctrlActivityLogs.storeLogs, ctrlTransaction.defaultExpensesTransactionsHttp);
router.get('/transactionCategories', auth,ctrlActivityLogs.storeLogs, ctrlTransaction.getTransactionCategoriesHttp);
router.post('/getTransactions', auth,ctrlActivityLogs.storeLogs, ctrlTransaction.getTransactionHttp);
router.post('/transaction', auth,ctrlActivityLogs.storeLogs, ctrlTransaction.addTransactionHttp);
router.put('/transaction/:id', auth, ctrlTransaction.editTransactionHttp);
router.delete('/transaction/:id', auth,ctrlActivityLogs.storeLogs, ctrlTransaction.deleteTransactionHttp);
router.post('/getGraph', auth,ctrlActivityLogs.storeLogs, ctrlTransaction.getGraphDataHttp);
router.post('/getPieChart', auth,ctrlActivityLogs.storeLogs, ctrlTransaction.getPieChartDataHttp);
router.post('/getIncomeChart', auth,ctrlActivityLogs.storeLogs, ctrlTransaction.incomeGraphDataHttp);
router.post('/generatePDFReport', auth,ctrlActivityLogs.storeLogs, ctrlTransaction.generatePDFReport);
router.post('/exportTransactionsToCSV', auth,ctrlActivityLogs.storeLogs, ctrlTransaction.exportTransactionsToCSV);
router.post('/upload_transaction_document/:transactionid', auth,ctrlActivityLogs.storeLogs, fileUpload, ctrlTransaction.uploadTransactionDocument);
router.delete('/upload_transaction_document/:documentid', auth, fileUpload, ctrlTransaction.deleteTransactionDocument);
router.post('/get_transaction_uploads',auth, ctrlTransaction.getTransactionUploads)
router.get('/get_transactions_document_zip',auth,ctrlActivityLogs.storeLogs, ctrlTransaction.getTransactionDocumentsZip)
router.get('/exportCompaniesToCSV/:role',  ctrlDatComapny.getCompanyCSV);
router.post('/companies', authTelemarketingAndAdminMiddleWare, ctrlDatComapny.getCompany);
router.get('/setReadyEnd',  ctrlTruck.setReadyEnd);
//Company Endpoints
router.post('/company', ctrlCompany.addCompany);
router.put('/company/:id', ctrlCompany.editCompany);
router.get('/company/:id', ctrlCompany.getCompanyById);
router.post('/findcompanies', ctrlCompany.getCompanies);
router.post('/get_company_uploads', auth,ctrlActivityLogs.storeLogs, ctrlCompany.getCompanyUploads);
router.post('/upload_company_document', authCompanyAdmin, fileUpload, ctrlCompany.uploadCompanyDocument);
router.delete('/upload_company_document/:id', authCompanyAdmin, ctrlCompany.deleteCompanyDocument);
//convert 
router.post('/get_recipt_uploads',auth,ctrlActivityLogs.storeLogs, ctrlCompany.getReceiptDetails)

//Child Parent User Endpoints
router.post('/addChildUser', auth,ctrlActivityLogs.storeLogs, ctrlAuth.addChildUser); // 
router.put('/editChildUser/:id', auth,ctrlActivityLogs.storeLogs, ctrlAuth.editChildUser);
router.post('/getChildUsers', auth,ctrlActivityLogs.storeLogs, ctrlAuth.getChildUsers);
router.get('/getChildUser/:id', auth,ctrlActivityLogs.storeLogs, ctrlAuth.getChildUser);
router.post('/setPassword', ctrlAuth.setPassword);
router.post('/getChildUserDetailByTaken', ctrlAuth.getChildUserDetailByTaken);
router.post('/checkChildUserEmail',auth, ctrlAuth.checkChildUserEmail)
router.post('/sendInvitationToJoinCompany',auth,ctrlActivityLogs.storeLogs, ctrlAuth.sendInvitationToJoinCompany)
router.get('/getDetailsFromInvitationToken/:token',auth,ctrlActivityLogs.storeLogs, ctrlAuth.getDetailsFromInvitationToken)
router.post('/switchChildUserCompany',auth, ctrlAuth.switchChildUserCompany)
router.post('/markedAsCompanyAdmin/:id',auth,ctrlActivityLogs.storeLogs, ctrlAuth.markedAsCompanyAdmin)
router.post('/signUpInOneStep',auth,ctrlActivityLogs.storeLogs, duplicateNewRegisterEmailForMarketingUserValidation, ctrlAuth.signUpInOneStep)
router.post('/checkPhoneNumber', auth,ctrlActivityLogs.storeLogs, ctrlAuth.checkPhoneNumber)
//Post Truck Convert To Endpoints
router.post("/add_truck", auth,ctrlActivityLogs.storeLogs,ctrlTruck.postTruck)
router.put("/refresh_age/:id", auth,ctrlActivityLogs.storeLogs,ctrlTruck.truckRefreshAge)
router.put("/close_truck/:id", auth,ctrlActivityLogs.storeLogs,ctrlTruck.closeTruck)
router.put("/edit_truck/:id", auth,ctrlActivityLogs.storeLogs,ctrlTruck.editTruck)
router.get("/detail_truck/:id", auth,ctrlActivityLogs.storeLogs,ctrlTruck.detailTruck)
router.post("/my_truck", auth,ctrlActivityLogs.storeLogs,ctrlTruck.myTrucksHttp)
router.post("/upload_trucks", auth,ctrlActivityLogs.storeLogs, csvFileUpload, ctrlTruck.uploadCSVTrucks);
router.post("/get_upload_trucks", auth,ctrlActivityLogs.storeLogs, ctrlTruck.getUploadCSVTrucks);
router.get('/download_trucks_file/:id',auth,ctrlActivityLogs.storeLogs ,ctrlTruck.downloadFile)
//Post Load Convert to Endpoints
router.post("/add_load", auth,ctrlActivityLogs.storeLogs,ctrLoads.postLoad)
router.put("/load_refresh_age/:id", auth,ctrlActivityLogs.storeLogs,ctrLoads.LoadRefreshAge)
router.put("/close_load/:id", auth,ctrlActivityLogs.storeLogs,ctrLoads.closeLoad)
router.put("/edit_load/:id", auth,ctrlActivityLogs.storeLogs,ctrLoads.editLoad)
router.get("/detail_load/:id", auth,ctrlActivityLogs.storeLogs,ctrLoads.detailLoad)
router.post("/my_load", auth,ctrlActivityLogs.storeLogs,ctrLoads.myLoads);
router.post("/upload_loads", auth,ctrlActivityLogs.storeLogs, csvFileUpload, ctrLoads.uploadCSVLoads);
router.post("/get_upload_loads", auth,ctrlActivityLogs.storeLogs, ctrLoads.getUploadCSVLoads);
router.get('/download_loads_file/:id',auth,ctrlActivityLogs.storeLogs ,ctrLoads.downloadFile)
//load save to watchList
router.get('/addToLoadWatchList/:id',auth,ctrlActivityLogs.storeLogs,ctrLoads.addToWatchlistHttp);
router.delete('/removeToLoadWatchList/:id',auth,ctrlActivityLogs.storeLogs,ctrLoads.removeFromWatchlistHttp);
// truck save to truckList
router.get('/addToTruckWatchList/:id',auth,ctrlActivityLogs.storeLogs,ctrlTruck.addToWatchlistHttp);
router.get('/removeToTruckWatchList/:id',auth,ctrlActivityLogs.storeLogs,ctrlTruck.removeFromWatchlistHttp);
//siteModule and subscription packages
router.post('/add_site_module', authAdmin, ctrlSubscriptionPackages.addSiteModule);
router.put('/edit_site_module/:id', authAdmin, ctrlSubscriptionPackages.editSiteModule);
router.delete('/delete_site_module/:id', authAdmin, ctrlSubscriptionPackages.deleteSiteModule);
router.get('/get_site_module', ctrlSubscriptionPackages.getModule);
router.get('/get_site_module_for_admin', authAdmin, ctrlSubscriptionPackages.getAllModules);
router.put('/site_module_add_role/:id',auth,ctrlActivityLogs.storeLogs,ctrlSubscriptionPackages.siteModuleAddRole);
router.put('/site_module_remove_role/:id',auth,ctrlActivityLogs.storeLogs,ctrlSubscriptionPackages.siteModuleRemoveRole);
router.post('/site_module_update_sort_order',authAdmin,ctrlSubscriptionPackages.siteModuleUpdateSortOrder);
router.post('/add_subscription_package', authAdmin, ctrlSubscriptionPackages.addSubscriptionPackages);
router.put('/edit_subscription_package/:id', authAdmin, ctrlSubscriptionPackages.editSubscriptionPackages);
router.put('/add_module_in_subscription_package/:id', authAdmin, ctrlSubscriptionPackages.addModuleInSubscriptionPackages);
router.put('/remove_module_in_subscription_package/:id', authAdmin, ctrlSubscriptionPackages.removeModuleInSubscriptionPackages);
router.delete('/delete_subscription_package/:id', authAdmin, ctrlSubscriptionPackages.deleteSubscriptionPackages);
router.put('/activate_subscription_package/:id', authAdmin, ctrlSubscriptionPackages.activateSubscriptionPackage);
router.put('/deactivate_subscription_package/:id', authAdmin, ctrlSubscriptionPackages.deactivateSubscriptionPackages);
router.post('/subscription_package_update_sort_order', authAdmin, ctrlSubscriptionPackages.subscriptionPackageUpdateSortOrder);
router.post('/get_subscription_packages_for_admin', ctrlSubscriptionPackages.getSubscriptionForAdmin);
router.get('/get_subscription_packages_for_pricing', ctrlSubscriptionPackages.getSubscriptionForPricing);
router.get('/get_subscription_packages_by_id/:id',auth ,ctrlSubscriptionPackages.getSubscriptionById);
router.get('/get_subscription_packages/:role', ctrlSubscriptionPackages.getSubscription);
//for signup and profile only public package used
router.get('/get_subscription_packages_signUpProfilePackages/:role', ctrlSubscriptionPackages.getSubscriptionSignUpAndProfilePackage);
router.get('/getModulesWithRoles/:role', ctrlSubscriptionPackages.getModulesWithRoles);
router.get('/CreateDefaultPackages', ctrlSetDefaultData.CreateDefaultPackages);
// SMS Alert Endpoint
router.post('/dispatcherloadsSmsAlert',auth,ctrlActivityLogs.storeLogs,ctrlSmsAlert.dispatcherloadsSmsAlert);
//advertisement
router.post('/getAllAdvertisements', authTelemarketingAndAdminMiddleWare, ctrlAdvertisement.getAllAdvertisements);
router.post('/addAdvertisement', authTelemarketingAndAdminMiddleWare, fileUpload, ctrlAdvertisement.addAdvertisement);
router.put('/editAdvertisement/:id', authTelemarketingAndAdminMiddleWare, fileUpload, ctrlAdvertisement.editAdvertisement);
router.delete('/deleteAdvertisement/:id', authTelemarketingAndAdminMiddleWare, ctrlAdvertisement.deleteAdvertisement);
router.get('/getAdvertisements', auth ,  ctrlAdvertisement.getActiveAdvertisements);
router.get('/on_advertisement_click/:id', auth ,  ctrlAdvertisement.onAdvertisementClick);
//Email Template 
router.get('/emailTypes', authTelemarketingAndAdminMiddleWare, ctrlEmailTemplate.getEmailTypes);
router.get('/getEmailTemplate/:type', authTelemarketingAndAdminMiddleWare, ctrlEmailTemplate.getEmailTemplateByType);
router.post('/getEmailTemplateList', authTelemarketingAndAdminMiddleWare, ctrlEmailTemplate.getEmailTemplateList);
router.post('/addEmailTemplate', authTelemarketingAndAdminMiddleWare, ctrlEmailTemplate.addEmailTemplate);
router.put('/editEmailTemplate/:id', authTelemarketingAndAdminMiddleWare, ctrlEmailTemplate.editEmailTemplate);
router.post('/sendTestEmail',ctrlEmailTemplate.testEmail);
// Email Templates Collection
router.post('/emailTypes',authTelemarketingAndAdminMiddleWare,ctrlEmailTemplate.postTypeEmail)
router.put('/emailTypes/:id',authTelemarketingAndAdminMiddleWare,ctrlEmailTemplate.updateTypeEmail);
router.delete('/emailTypes/:id',authTelemarketingAndAdminMiddleWare,ctrlEmailTemplate.deleteTypeEmail);
router.get('/emailTypes/:id',authTelemarketingAndAdminMiddleWare,ctrlEmailTemplate.getEmailTypesById);
// Sms Types 
router.post('/smsTypes',authTelemarketingAndAdminMiddleWare,ctrlSmsTemplate.postTypeSms)
router.put('/smsTypes/:id',authTelemarketingAndAdminMiddleWare,ctrlSmsTemplate.updateTypeSms);
router.delete('/smsTypes/:id',authTelemarketingAndAdminMiddleWare,ctrlSmsTemplate.deleteTypeSms);
router.get('/smsTypes/:id',authTelemarketingAndAdminMiddleWare,ctrlSmsTemplate.getSmsTypesById);
router.get('/smsTypes', authTelemarketingAndAdminMiddleWare, ctrlSmsTemplate.getSmsTypes);
// sms Templates 
router.get('/smsTemplate/:type', authTelemarketingAndAdminMiddleWare, ctrlSmsTemplate.getSmsTemplateByType);
router.post('/getSmsTemplateList', authTelemarketingAndAdminMiddleWare, ctrlSmsTemplate.getSmsTemplateList);
router.post('/addSmsTemplate', authTelemarketingAndAdminMiddleWare, ctrlSmsTemplate.addSmsTemplate);
router.put('/editSmsTemplate/:id', authTelemarketingAndAdminMiddleWare, ctrlSmsTemplate.editSmsTemplate);
// Role
router.get('/roles', ctrlRole.getPublicRoles);
router.get('/get_roles_for_admin', authTelemarketingAndAdminMiddleWare, ctrlRole.getAllRoles);
router.post('/role', authAdmin, duplicateRoleValidation, ctrlRole.addRole);
router.put('/role/:id', authAdmin, duplicateRoleValidation, ctrlRole.editRole);
router.put('/role/:id/make_as_public/:status', authAdmin, ctrlRole.markAsPublicRole);
router.delete('/role/:id', authAdmin, ctrlRole.deleteRole);
router.post('/filter_upload_document', auth,ctrlActivityLogs.storeLogs,  ctrlProfile.uploadFilterDocument);
router.post('/zip_Document',auth,ctrlActivityLogs.storeLogs,ctrlProfile.downloadZipFile)
// Dat truck issue
router.get('/dat_truck_update', ctrlTruck.datTruckUpdate);
router.get('/update_payment_status',authTelemarketingAndAdminMiddleWare ,ctrlUser.updatePaymentStatus)
router.get('/posteverywhereuser',ctrlAuth.updateEveryWhereUser)
router.put('/addPrivateFieldSubscriptionPackage',ctrlSubscriptionPackages.updateRoleSubscriptionPackageForAdmin)
// demo api
router.post('/addDemoVideoRecord',authAdmin,ctrlDemo.addVideoDemo);
router.post('/getAllDemoVideoRecords',authAdmin,ctrlDemo.getVideoAllDemoRecords)
router.get('/getOneDemoVideoRecords/:page_name',auth,ctrlDemo.getOneVideoDemoRecords)
router.put('/updateDemoVideoRecord/:_id',authAdmin,ctrlDemo.updateOneVideoDemoRecords)
/**
 * user logs apis
 */
router.get('/exportUserActivity',authTelemarketingAndAdminMiddleWare ,ctrlActivityLogs.userCsvLogDateEvents);
router.get('/userlogs', ctrlActivityLogs.userLogs1);
router.post('/getle', ctrlActivityLogs.userLogDateEvents);

// router.get('/updateOldUsersPaymentStatus',ctrlProfile.updateOldUsersSubscriptionStatus)
router.get('/getIncompleteUserById/:_id',authTelemarketingAndAdminMiddleWare, ctrlUser.getIncompeleteUserById)
router.post('/addNotes',authTelemarketingAndAdminMiddleWare , ctrlNote.saveNoteForTemporaryUser);


router.post("/getLoadByUserId", auth,ctrlActivityLogs.storeLogs,ctrLoads.myLoadsWithId);
router.post("/getTruckByUserId", auth,ctrlActivityLogs.storeLogs,ctrlTruck.myTrucksWithUserId);


router.get('/getReciptsWithTransactionId',auth,ctrlProfile.getDocumentWithTransactionId);

router.get('/updatePaymentStatus',ctrlUser.updatePaymentStatus);
router.get('/updateScheduleSubscriptionStatus',ctrlUser.updateScheduleSubscriptionStatus);

// router.get('/userloadcount',ctrLoads.getLoadsCountPeUsers);
// router.get('/getPELoadsCountReport7DaysFromToday',ctrLoads.getPELoadsCountReport7DaysFromToday);

router.post('/restartSubscription/:id', authTelemarketingAndAdminMiddleWare, ctrlProfile.restartSubscription);

router.get('/userpayments/:userID', authTelemarketingAndAdminMiddleWare,ctrlProfile.customerPaymentHistory);

router.get('/all_offers', authTelemarketingAndAdminMiddleWare , ctrlOffer.getAllOffers);
router.get('/offers', auth , ctrlOffer.getOffers);
router.get('/offer/:id', auth , ctrlOffer.getOffer);
router.post('/offer', authTelemarketingAndAdminMiddleWare, ctrlOffer.addOffer);
router.put('/offer/:id', authTelemarketingAndAdminMiddleWare, ctrlOffer.editOffer);
router.delete('/offer/:id', authTelemarketingAndAdminMiddleWare, ctrlOffer.deleteOffer);

router.post('/apply_offer', auth, ctrlOffer.subscribe_offer);
router.get('/is_offer_used', auth, ctrlOffer.isOfferUsed);
router.post('/get_used_offer_detail', auth, ctrlOffer.getUsedOfferDetail);

router.post('/get_user_offer_details', auth, ctrlOffer.getUserOfferDetail);

//csv for usersigned up
router.get('/signedupuserscsv',authTelemarketingAndAdminMiddleWare,ctrlUser.getSignedUpUsersCsv);

//stripe routers
const stripeRouter = require('./stripe');
router.use('/stripe',authTelemarketingAndAdminMiddleWare ,stripeRouter);

router.post('/mobile_signup_step1',registerStep1MobileValidation, ctrlAuth.newRegisterMobileStep1)
// post load with new user

router.post('/postFreeLoad',ctrLoads.freePostLoad)




router.post("/postFreeTruck",ctrlTruck.postTruckFree)
//reports 
router.get("/usercsv/:status",authTelemarketingAndAdminMiddleWare,ctrlreports.exportUserCSV);
router.get("/incompletetocompletecsv/:hours", authTelemarketingAndAdminMiddleWare,ctrlreports.incompleteToComplete);
router.get('/referalcsv',authTelemarketingAndAdminMiddleWare,ctrlreports.referal_signed_up);
router.get("/salesreportcsv", authTelemarketingAndAdminMiddleWare,ctrlreports.sales_report);

/**
 * influencer and referal system routes 
 */
router.post("/influencer",authTelemarketingAndAdminMiddleWare,ctrlInfluencer.addInfluencer);
router.post("/referallink",authTelemarketingAndAdminMiddleWare,ctrlreferal.createReferalLink);
router.get("/influencer",authTelemarketingAndAdminMiddleWare,ctrlInfluencer.getAllInfluencers);
router.get("/referallink",authTelemarketingAndAdminMiddleWare,ctrlreferal.getInlfuencerReferalLinks);
router.get("/referralLinksById",authTelemarketingAndAdminMiddleWare,ctrlreferal.getReferalLinksById);

router.post("/getreferal",ctrlreferal.getInlfuencerReferalLinks);

router.get('/adminlogs',ctrlActivityLogs.userLogs);
router.post('/adminlogsdetails', ctrlActivityLogs.userLogDateEventsAdmin)

router.post('/signedupbySales',authTelemarketingAndAdminMiddleWare,ctrlUser.getUserRefSignedUp);

//get sales persons 
router.get("/getSalesPerson",authTelemarketingAndAdminMiddleWare,ctrlUser.get_all_sales_person)
//update temp user
router.put('/tempuser/:id',authTelemarketingAndAdminMiddleWare,duplicateEmailValidation, ctrlUser.updateTempUser);

module.exports = router;
