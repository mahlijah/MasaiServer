var express = require('express');
var router = express.Router({mergeParams:true});
const ctrlStripe = require('../controllers/stripe');

//Plans
router.get("/plans", ctrlStripe.getPlans );
router.post("/plan", ctrlStripe.addPlan );
router.put("/plan/:id", ctrlStripe.editPlan );
router.delete("/plan/:id", ctrlStripe.deletePlan );

//coupons
router.get("/coupons",ctrlStripe.getCoupons);
router.post("/coupon",ctrlStripe.createCoupon);
router.delete("/coupon/:id",ctrlStripe.deleteCoupon);

//promotion
router.get("/promotions",ctrlStripe.getPromotions);
router.post("/promotion",ctrlStripe.createPromotion);

//Price
router.get("/prices",ctrlStripe.getPrices);
router.post("/price",ctrlStripe.createPrice);

//invoice
router.post("/invoice/pay",ctrlStripe.payInvoice)
router.post("/invoice/void",ctrlStripe.voidInvoice)

module.exports = router;