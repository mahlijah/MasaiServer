const config = require('../config/config');
const AWS = require('aws-sdk');
const multer = require("multer");
const multerS3 = require("multer-s3");
AWS.config.update({
    accessKeyId: config.AWSAccessKeyId,
    secretAccessKey: config.AWSSecretKey,
    region: "us-east-2",
});
const s3 = new AWS.S3();

var _multerS3 = multerS3({
    acl: "public-read",
    s3,
    bucket: config.AWSBucketName,
    //contentType: multerS3.AUTO_CONTENT_TYPE,
    metadata: function (req, file, cb) {
        cb(null, { fieldName: "TESTING_METADATA", mimetype: file.mimetype });
    },
    key: function (req, file, cb) {
        cb(null, Date.now().toString());
    },
})

const fileFilter = (req, file, cb) => {
    //if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
        cb(null, true);
    // } else {
    //     cb(new Error("Invalid file type, only JPEG and PNG is allowed!"), false);
    // }
};

const fileFilterImageOnly = (req, file, cb) => {
    if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
        cb(null, true);
     } else {
         cb(new Error("Invalid file type, only JPEG and PNG is allowed!"), false);
     }
};

const upload = multer({
    fileFilter,
    storage: _multerS3,
});

const uploadImageOnly = multer({
    fileFilter:fileFilterImageOnly,
    storage: _multerS3,
});
const csvMimeType = [
    "text/csv",
    "application/csv",
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    "application/vnd.ms-excel.sheet.binary.macroEnabled.12",
    "application/vnd.ms-excel",
    "application/vnd.ms-excel.sheet.macroEnabled.12"
]

const csvFileFilter = (req, file, cb) => {
    console.log(file.mimetype, file);
    if (csvMimeType.includes(file.mimetype)) {
        cb(null, true);
    } else {
        cb(new Error("Invalid file type, only CSV is allowed!"), false);
    }
};

const csvUpload = multer({
    fileFilter:csvFileFilter,
    storage: _multerS3,
});


module.exports = {upload,csvUpload, uploadImageOnly};