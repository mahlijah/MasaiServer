var config = require('../config/config');
var url = require('url');
var Queue = require('bull');
var Redis = require('ioredis');
const common = require('./common');

var io;
module.exports = {
    init: function (_io) {
        io = _io;
        const REDIS_URL = config.loadJobs.redisUri;
        const redis_uri = url.parse(REDIS_URL);
        const redisOptions = REDIS_URL.includes("rediss://")
            ? {
                port: Number(redis_uri.port),
                host: redis_uri.hostname,
                password: redis_uri.auth.split(":")[1],
                maxRetriesPerRequest: 0,
                tls: {
                    rejectUnauthorized: false,
                    servername: redis_uri.hostname
                },
                enableReadyCheck: false
            }
            : REDIS_URL;

        broadcastNewloadsQueue = new Queue(config.loadJobs.broadcastNewloads.name, { redis: redisOptions }, config.loadJobs.queueOptions);
        broadcastNewloadsQueue.process(config.loadJobs.broadcastNewloads.concurrency, this.broadcastNewloads);
        setInterval(async () =>  {
            common.log("cleaning up queues");
            try{
               await broadcastNewloadsQueue.clean(config.loadJobs.batchSize)
            }
            catch(err){
                common.log("error while cleaning up queues",err);
            }
        }, config.loadJobs.cleanupInterval); 
        broadcastNewloadsQueue.on('error',function(error){
            common.log("Error while connect ", error)
        })
    },
    broadcastNewloads: async function (job, done) {
        try {
            var searchlist = job.data.data;
            var load = job.data.load;
            searchlist.forEach(obj => {
                io.to(obj._id).emit('newLoadForSearch', { data: load, searchId: obj._id });
            });
            done();
        }
        catch (err) {
            common.log("Error while broadcastNewloads", err);
            done(err);
        }
    }
}