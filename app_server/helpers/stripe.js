const config =  require('../config/config');
const stripe = require("stripe")(config.stripe_key);
const common = require('./common');

module.exports.addPlan= async function(product_name,price,trial_period_days){
    try{
        const plan = await stripe.plans.create({
            amount: Math.ceil(price*100),
            currency: 'usd',
            interval: 'month',
            product: {name:product_name},
            trial_period_days: trial_period_days
          });
        return plan;
    }
    catch (err){
        common.log("Error while creating plan---", err);
        throw err;
    }
}

module.exports.updatePlan= async function(plan_id,trial_period_days){
    try{
        const plan = await stripe.plans.update(plan_id,{
            trial_period_days: trial_period_days
          });
        return plan;
    }
    catch (err){
        common.log("Error while creating plan---", err);
        throw err;
    }
}

module.exports.allPlans = async function(){
    try{
        const plans = [];
        for await(const plan of stripe.plans.list({active:true,limit:100,expand:['data.product']})){
            plans.push(plan);
        }
        return plans;
    } catch(err){
        common.log('Error while allPlans :--',err)
        throw err;
    }
}

module.exports.getSubscriptionsByStatus = async function(status){
    try{
        const subscriptions = [];
        for await(const subscription of stripe.subscriptions.list({status:status,limit:10})){
            subscriptions.push(subscription);
        }
        return subscriptions;
    } catch(err){
        common.log('Error while all '+status+' Subscriptions :--',err)
        throw err;
    }
}

module.exports.retrievePlan = async function(plan_id){
    try{
        const plan = await stripe.plans.retrieve(plan_id);
        return plan;
    } catch(err){
        common.log('Error while retrievePlan :--',err)
        throw err;
    }
}

module.exports.deletePlan = async function(plan_id){
    try{
        const deleted = await stripe.plans.del(plan_id);
        return deleted;
    } catch(err){
        common.log('Error while delete plan :--',err)
        throw err;
    }
}

module.exports.createCheckoutSession = async function(plan_id,price_id,email,success_url,cancel_url, trial_from_plan = false, trial_period_days = null,promo_id=null){
    try{
        let subscription_data = {};
        if(trial_period_days){
            subscription_data['trial_period_days']=trial_period_days;
        }else{
            subscription_data['trial_from_plan']=trial_from_plan;
        }
        items = [
            {
                price: plan_id,
                quantity: 1,
            }
        ];
        if(price_id){
            items.unshift({
                price: price_id,
                quantity: 1,
            })
        }

     //create session object
     let options={
        payment_method_types: ['card'],
        customer_email: email,
        subscription_data: subscription_data,
        line_items: items,
        mode: 'subscription',
        success_url: success_url,
        cancel_url: cancel_url,
      }

      //if promo than by defaul discount will be select else allow promotion
        if(promo_id)
         options.discounts=[{promotion_code:promo_id}]
         else
         options.allow_promotion_codes= true

        const session = await stripe.checkout.sessions.create(options);
        return session;
    }
    catch(err){
        common.log("Error while creating checkout session:- ",err);
        throw err;
    }
} 

module.exports.createCheckoutSessionWithCustomerId = async function(plan_id, price_id, cus_id, success_url, cancel_url){
    try{
        let items = [];
        if(price_id){
            items.push({
                price: price_id,
                quantity: 1,
            })
        }
        if(plan_id){
            items.push({
                price: plan_id,
                quantity: 1,
            })
        }
        const session = await stripe.checkout.sessions.create({
            payment_method_types: ['card'],
            customer: cus_id,
            subscription_data: {
              trial_from_plan: false,
            },
            line_items: items,
            mode: 'subscription',
            allow_promotion_codes: true,
            success_url: success_url,
            cancel_url: cancel_url,
          });
        return session;
    }
    catch(err){
        common.log("Error while creating checkout session:- ",err);
        throw err;
    }
} 

module.exports.createSubscription = async function(customer_id, plan_id,trial_from_plan = false, trial_period_days = null,price_id=null, coupon=null, price_data=null, quantity=null){
    try{
        let subscription_data = {};
        if(trial_period_days){
            subscription_data['trial_period_days']=trial_period_days;
        }else{
            subscription_data['trial_from_plan']=trial_from_plan;
        }
        if(coupon){
            subscription_data['coupon']=coupon;
        }


        invoice_items = [];
        if(price_id){
            invoice_items.push({price:price_id});
        }
        if(price_data){
            invoice_items.push({
                price_data:price_data,
                quantity:quantity||1
            });
        }

        const subscription = await stripe.subscriptions.create({
            customer: customer_id,
            ...subscription_data,
            items: [
                {price:plan_id}
            ],
            add_invoice_items:invoice_items,
            payment_behavior:'allow_incomplete'
          });
          return subscription;
    }
    catch(err){
        common.log("Error while creating subscription on stripe:-  ", err);
        throw err;
    }
}

module.exports.changeSubscription = async function(sub_id,plan_id){
    try{
        const subscription = await this.getSubscription(sub_id);
        const subscription_update = await stripe.subscriptions.update(sub_id,{
            cancel_at_period_end: false,
            proration_behavior: subscription.status =='trialing'?'create_prorations':'always_invoice',
            payment_behavior:'error_if_incomplete',
            items: [{
              id: subscription.items.data[0].id,
              price: plan_id,
            }]
          });
          return subscription_update;
    }
    catch(err){
        common.log("Error while creating subscription on stripe:-  ", err);
        throw err;
    }
}

module.exports.scheduleChangeSubscription = async function(sub_id,plan_id){
    try{
        const subscription = await this.getSubscription(sub_id);
        const schedule = await stripe.subscriptionSchedules.create({
            from_subscription : sub_id
        });
        const scheduleUpdate = await stripe.subscriptionSchedules.update(
            schedule.id,
            {
              phases: [
                {
                    items: [{ price: subscription.items.data[0].price.id }],
                    start_date: subscription.current_period_start,
                    end_date: subscription.current_period_end,
                },
                {
                  items: [{ price: plan_id}],
                  start_date: subscription.current_period_end,
                },
              ],
            },
          );
        return { schedule, execute_on: new Date(subscription.current_period_end * 1000)};
    }
    catch(err){
        common.log("Error while creating subscription on stripe:-  ", err);
        throw err;
    }
}

module.exports.retrieveScheduleSubscription =async function(sub_sched_id){
    try{    
        const subscriptionSchedule = await stripe.subscriptionSchedules.retrieve(sub_sched_id);
        return subscriptionSchedule;
    } catch(err){
        common.log('Error while retriveScheduleSubscription :--',err)
        throw err;
    }
}

module.exports.cancelScheduleSubscription = async function(sub_sched_id){
    try{
        const subscriptionSchedule = await stripe.subscriptionSchedules.release(sub_sched_id);
        return subscriptionSchedule;
    }
    catch(error){
        common.log("Error while creating subscription on stripe:-  ", err);
        throw err;
    }
}

module.exports.cancelSubscription = async function(sub_id){
    try{
        const subscription = await stripe.subscriptions.update(
            sub_id,
            {cancel_at_period_end: true}
          );
        return subscription;
    }
    catch(err){
        common.log("Error while cancel subscription on stripe:-  ", err);
        throw err;
    }
}

module.exports.getSubscription = async function(sub_id){
    try{
        const subscription =  await stripe.subscriptions.retrieve(sub_id);
        return subscription;
    }
    catch(err){
        common.log("Error while getting subscription Detail:-- ", err);
        throw err;
    }
} 

module.exports.deleteSubscription = async function(sub_id){
    try{
        const subscription =  await stripe.subscriptions.del(sub_id);
        return subscription;
    }
    catch(err){
        common.log("Error while getting subscription Detail:-- ", err);
        throw err;
    }
}

module.exports.findCustomerByEmail =async function(email){
    try{
        const customers = await stripe.customers.list({
            email:email,
            limit: 1,
          });
        return customers;
    } catch(err){
        common.log('Error while findCustomerByEmail :--',err)
        throw err;
    }
}

module.exports.createCustomer = async function(email,username,pm_id){
    try{
        const customer = await stripe.customers.create({
            email: email,
            name: username,
            payment_method:pm_id,
            invoice_settings:{
                default_payment_method:pm_id
            }
          });
        return customer;
    }
    catch(err){
        common.log("Error while creating customer:-- ",err);
        throw err
    }
}

module.exports.updateCustomerDefaultPM =async function(cus_id, pm_id){
    try{
        const customer = await stripe.customers.update(
            cus_id,
            {invoice_settings:{
                default_payment_method:pm_id
            }}
          );
    } catch(err){
        common.log('Error while updateCustomerDefaultPM :--',err)
        throw err;
    }
}

module.exports.attachPaymentMethod = async function(cus_id,pm_id){
    try{
        const paymentMethod = await stripe.paymentMethods.attach(
            pm_id,
            {customer: cus_id}
          );
          return paymentMethod;
    }
    catch(err){
        throw err;
    }
}

module.exports.getCustomer = async function(customer_id){
    try{
        const customer = await stripe.customers.retrieve(customer_id,{
            expand:['subscriptions','subscriptions.data.latest_invoice']
        });
        return customer;
    }
    catch(err){
        common.log("Error while getting custome detail:--",err);
        throw err;
    }
} 

module.exports.deleteCustomer = async function(customer_id){
    try{
        const deleted = await stripe.customers.del(customer_id);
        return deleted;
    }
    catch(err){
        common.log("Error while deleting custome:--",err);
        throw err;
    }
}

module.exports.getPaymentMethods = async function(cus_id){
    try{
        const paymentMethods = await stripe.paymentMethods.list({
            customer: cus_id,
            type: 'card',
          });
        return paymentMethods;
    } catch(err){
        common.log('Error while get Payment Methods :--',err)
        throw err;
    }
}

module.exports.removePaymentMethod = async function(pm_id){
    try{
        const paymentMethod = await stripe.paymentMethods.detach(pm_id);
        return paymentMethod;
    } catch(err){
        common.log('Error while removePaymentMethod :--',err)
        throw err;
    }
}

module.exports.retrieveProduct= async function(prod_id){
    try{
        const product = await stripe.products.retrieve(prod_id);
        return product;
    }
    catch(err){
        return err;
    }
}

module.exports.getCoupons =async function(){
    try{
        let coupons=[];
        for await(let coupon of stripe.coupons.list({limit:100})){
            coupons.push(coupon)
        }
        return coupons;
    } catch(err){
        common.log('Error while getCoupons :--',err)
        throw err;
    }
}

module.exports.createCoupon =async function(_data){
    try{
        let data = {};
        if(_data.type=="percent"){
            data.percent_off = _data.value
        }
        else if(_data.type=="amount"){
            data.amount_off = Math.ceil(_data.value*100);
            data.currency = 'USD'
        }
        data.duration = _data.duration || 'once' 
        if(data.duration == "repeating"){
            data.duration_in_months=_data.duration_in_months||3;
        }
        if(_data.id){
            data.id = _data.id;
        }
        if(_data.name){
            data.id = _data.name;
        }
        const coupon = await stripe.coupons.create(data);
        return coupon;

    } catch(err){
        common.log('Error while createCoupon :--',err)
        throw err;
    }
}

module.exports.deleteCoupon =async function(co_id){
    try{
        const deleted = await stripe.coupons.del(co_id);
        return deleted;
    } catch(err){
        common.log('Error while deleteCoupon :--',err)
        throw err;
    }
}

module.exports.createPromotionCode =async function(coupon,code){
    try{
        const promotionCode = await stripe.promotionCodes.create({
            coupon: coupon,
            code:code
          });
        return promotionCode;
    } catch(err){
        common.log('Error while createPromotionCode :--',err)
        throw err;
    }
}

module.exports.getPromotions =async function(){
    try{
        const promotions=[];
        for await(let promo of stripe.promotionCodes.list({limit:100})){
            promotions.push(promo);
        }
        return promotions;
    } catch(err){
        common.log('Error while getPromotion :--',err)
        throw err;
    }
}

module.exports.getPriceList = async function(){
    try{
        let prices = [];
        for await(const price of stripe.prices.list({
            active:true,
            type:'one_time',
            limit:100,
            expand:['data.product']
        })){
            prices.push(price);
        }
        return prices;
    }
    catch(err){
        throw err
    }
}   

module.exports.createPrice = async function(name,amount,type,recurring){
    try{
        let data = {
            unit_amount: Math.ceil(amount*100),
            currency: 'usd',
            product_data: {
                name:name
            },
          }
          if(type == "recurring"){
              data.recurring= {interval: recurring};
          }
        const price = await stripe.prices.create(data);
        return price;
    }
    catch(err){
        throw err
    }
}

module.exports.retrievePrice = async function(price_id){
    try{
        const price = await stripe.prices.retrieve(price_id);
        return price;
    } catch(err){
        common.log('Error while retrievePrice :--',err)
        throw err;
    }
}

module.exports.voidInvoice =async function(in_id){
    try{
        const invoice = await stripe.invoices.voidInvoice(in_id);
    } catch(err){
        common.log('Error while voidInvoice :--',err)
        throw err;
    }
}

module.exports.retrieveInvoice =async function(in_id){
    try{
        const invoice = await stripe.invoices.retrieve(in_id);
    } catch(err){
        common.log('Error while voidInvoice :--',err)
        throw err;
    }
}

async function createProduct(product_name){
    try{
        const product = await stripe.products.create({
            name: product_name,
          });
        return product;
    }
    catch (err){
        common.log("Error While Create Product---", err);
        throw err;
    }
}

module.exports.findProductByName = async function(name){
    try{
        let product;
        for await(const _product of stripe.products.list({active:true,limit:10})){
            if(_product.name == name){
                product = _product;
                break;
            }
        }
        if(!product){
            product = await createProduct(name)
        }
        return product;
    }
    catch(err){
        throw err;
    }
}


module.exports.findCouponByCode = async function(code){
    try{
        const coupon = await stripe.coupons.retrieve(code);
        return coupon;
    }
    catch(err){
        throw err;
    }
}

module.exports.updateCustomerEmail =async function(cus_id, user_email){
    try{
        const customer = await stripe.customers.update(
            cus_id,
            {email:user_email}
          );
    return customer;
    } catch(err){
        common.log('Error while updateCustomerDefaultPM :--',err)
        throw err;
    }
}

module.exports.payInvoice =async function(in_id, payment_method){
    try{
        let data = {};
        if(payment_method){
            data["payment_method"]=payment_method;
        }
        const invoice = await stripe.invoices.pay(in_id,data);
        return invoice;
    } catch(err){
        common.log('Error while invoice_pay :--',err)
        throw err;
    }
}

module.exports.customerInvoices= async(cus_id)=>{
    try{
        const invoices = await stripe.invoices.list({
            customer:cus_id
        });
        return invoices;
    } catch(err){
        common.log('Error while updateCustomerDefaultPM :--',err)
        throw err;
    }
}

module.exports.customerOpenInvoices= async(cus_id)=>{
    try{
        const invoices = await stripe.invoices.list({
            customer:cus_id,
            status:'open'
        });
        return invoices;
    } catch(err){
        common.log('Error while updateCustomerDefaultPM :--',err)
        throw err;
    }
}

//user subscriptions
module.exports.getCustomerSubscriptions= async(cus_id)=>{
    try{
        const subscriptions = await stripe.subscriptions.list({
            customer:cus_id,
          });
        return subscriptions;
    } catch(err){
        common.log('Error while updateCustomerDefaultPM :--',err)
        throw err;
    }
}
//user cancel subscriptions
module.exports.getCustomerCancelSubscriptions= async(cus_id)=>{
    try{
        const subscriptions = await stripe.subscriptions.list({
            customer:cus_id,
            status:'canceled',
            expand:['data.latest_invoice']
          });
        return subscriptions;
    } catch(err){
        common.log('Error while updateCustomerDefaultPM :--',err)
        throw err;
    }
}


//stripe events 
module.exports.getcustomerEvents= async(cus_id)=>{
    try{
      const events = await stripe.events.list({limit:1000, related_object:cus_id});
        return events;
    } catch(err){
        common.log('Error while updateCustomerDefaultPM :--',err)
        throw err;
    }
}

module.exports.getCustomerList= async(opt)=>{
    try{
        let options={
            limit:100
           }
     if(opt.starting_after)
     options.starting_after=opt.starting_after;
     
        const customers = await stripe.customers.list(options);
        return customers;
    }
    catch(err){
        common.log("Error while getting custome detail:--",err);
        throw err;
    }
}

/**
 *  case handling only for restart subscription where incomplete payment is not allowed 
 */
 module.exports.createSubscription_restart = async function(customer_id, plan_id,trial_from_plan = false, trial_period_days = null,price_id=null, coupon=null, price_data=null, quantity=null){
    try{
        let subscription_data = {};
        if(trial_period_days){
            subscription_data['trial_period_days']=trial_period_days;
        }else{
            subscription_data['trial_from_plan']=trial_from_plan;
        }
        if(coupon){
            subscription_data['coupon']=coupon;
        }


        invoice_items = [];
        if(price_id){
            invoice_items.push({price:price_id});
        }
        if(price_data){
            invoice_items.push({
                price_data:price_data,
                quantity:quantity||1
            });
        }

        const subscription = await stripe.subscriptions.create({
            customer: customer_id,
            ...subscription_data,
            items: [
                {price:plan_id}
            ],
            add_invoice_items:invoice_items,
            payment_behavior:'error_if_incomplete'
          });
          return subscription;
    }
    catch(err){
        common.log("Error while creating subscription on stripe:-  ", err);
        throw err;
    }
}

//list customer charges 
module.exports.getCustomerChargesList= async(opt)=>{
    try{
        let options={
            limit:100
           }
     if(opt.starting_after)
     options.starting_after=opt.starting_after;

     if(opt.customer_id)
      options.customer= opt.customer_id

     const charges = await stripe.charges.list(options);
     return charges;
    }
    catch(err){
        common.log("Error while getting customer charges detail:--",err);
        throw err;
    }
}


//get promotion code
module.exports.getPromotion_by_Id=async(promo_id)=>{
    try{
        const promotionCode = await stripe.promotionCodes.retrieve(
            promo_id
          );
        return promotionCode;
    } catch(err){
        common.log('Error while getPromotion :--',err)
        throw err;
    }
}