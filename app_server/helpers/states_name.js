var config = require('../config/config');
var nodemailer  = require('nodemailer');
var azure = require('azure-storage'); //api for azure storage
var blobSvc = azure.createBlobService(config.AZURE_STORAGE_ACCOUNT, config.AZURE_STORAGE_ACCESS_KEY);
var NodeGeocoder = require('node-geocoder');
var _ = require('util');

var options = {
  provider: 'google',
 
  // Optional depending on the providers
  httpAdapter: 'https', // Default
  apiKey: config.googleGoecoder_key, // for Mapquest, OpenCage, Google Premier
  //formatter: 'string',         // 'gpx', 'string', ...
  //formatterPattern: '%p%t%z'
};
var geocoder = NodeGeocoder(options);

var self = module.exports = {
      
   usAllStates(){
      var states =  [
        {
            "name": "Alabama",
            "abbreviation": "AL",
            "value":1, // value is ratio of load and truck.
            "truck":0,
            "Loads":0,
        },
        {
            "name": "Alaska",
            "abbreviation": "AK",
            "value":1,
            "truck":0,
            "Loads":0
        },
        {
            "name": "American Samoa",
            "abbreviation": "AS","value":1,
            "truck":0,
            "Loads":0
        },
        {
            "name": "Arizona",
            "abbreviation": "AZ","value":1,
            "truck":0,
            "Loads":0
        },
        {
            "name": "Arkansas",
            "abbreviation": "AR","value":1,
            "truck":0,
            "Loads":0
        },
        {
            "name": "California",
            "abbreviation": "CA","value":1,
            "truck":0,
            "Loads":0
        },
        {
            "name": "Colorado","value":1,
            "abbreviation": "CO",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Connecticut","value":1,
            "abbreviation": "CT",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Delaware","value":1,
            "abbreviation": "DE",
            "truck":0,
            "Loads":0
        },
        {
            "name": "District Of Columbia","value":1,
            "abbreviation": "DC",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Federated States Of Micronesia","value":1,
            "abbreviation": "FM",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Florida","value":1,
            "abbreviation": "FL",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Georgia","value":1,
            "abbreviation": "GA",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Guam","value":1,
            "abbreviation": "GU",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Hawaii","value":1,
            "abbreviation": "HI",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Idaho","value":1,
            "abbreviation": "ID",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Illinois","value":1,
            "abbreviation": "IL",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Indiana","value":1,
            "abbreviation": "IN",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Iowa","value":1,
            "abbreviation": "IA",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Kansas","value":1,
            "abbreviation": "KS",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Kentucky","value":1,
            "abbreviation": "KY",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Louisiana","value":1,
            "abbreviation": "LA",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Maine","value":1,
            "abbreviation": "ME",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Marshall Islands","value":1,
            "abbreviation": "MH",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Maryland","value":1,
            "abbreviation": "MD",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Massachusetts","value":1,
            "abbreviation": "MA",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Michigan","value":1,
            "abbreviation": "MI",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Minnesota","value":1,
            "abbreviation": "MN",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Mississippi","value":1,
            "abbreviation": "MS",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Missouri","value":1,
            "abbreviation": "MO",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Montana","value":1,
            "abbreviation": "MT",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Nebraska","value":1,
            "abbreviation": "NE",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Nevada","value":1,
            "abbreviation": "NV",
            "truck":0,
            "Loads":0
        },
        {
            "name": "New Hampshire","value":1,
            "abbreviation": "NH",
            "truck":0,
            "Loads":0
        },
        {
            "name": "New Jersey","value":1,
            "abbreviation": "NJ",
            "truck":0,
            "Loads":0
        },
        {
            "name": "New Mexico","value":1,
            "abbreviation": "NM",
            "truck":0,
            "Loads":0
        },
        {
            "name": "New York","value":1,
            "abbreviation": "NY",
            "truck":0,
            "Loads":0
        },
        {
            "name": "North Carolina","value":1,
            "abbreviation": "NC",
            "truck":0,
            "Loads":0
        },
        {
            "name": "North Dakota","value":1,
            "abbreviation": "ND",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Northern Mariana Islands","value":1,
            "abbreviation": "MP",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Ohio","value":1,
            "abbreviation": "OH",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Oklahoma","value":1,
            "abbreviation": "OK",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Oregon","value":1,
            "abbreviation": "OR",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Palau","value":1,
            "abbreviation": "PW",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Pennsylvania","value":1,
            "abbreviation": "PA",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Puerto Rico","value":1,
            "abbreviation": "PR",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Rhode Island","value":1,
            "abbreviation": "RI",
            "truck":0,
            "Loads":0
        },
        {
            "name": "South Carolina","value":1,
            "abbreviation": "SC",
            "truck":0,
            "Loads":0
        },
        {
            "name": "South Dakota","value":1,
            "abbreviation": "SD",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Tennessee","value":1,
            "abbreviation": "TN",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Texas","value":1,
            "abbreviation": "TX",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Utah","value":1,
            "abbreviation": "UT",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Vermont","value":1,
            "abbreviation": "VT",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Virgin Islands","value":1,
            "abbreviation": "VI",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Virginia","value":1,
            "abbreviation": "VA",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Washington","value":1,
            "abbreviation": "WA",
            "truck":0,
            "Loads":0
        },
        {
            "name": "West Virginia","value":1,
            "abbreviation": "WV",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Wisconsin","value":1,
            "abbreviation": "WI",
            "truck":0,
            "Loads":0
        },
        {
            "name": "Wyoming","value":1,
            "abbreviation": "WY",
            "truck":0,
            "Loads":0
        }
    ];
      return states;
    },   
    
    getStates(delivery_addresses){
        geocoder.geocode(delivery_addresses, function(err, res) {          
          array = [];
          var level1long  = res[0].administrativeLevels.level1long
          var level1short = res[0].administrativeLevels.level1short;
          array.shortaddress = level1short;
          array.longaddress = level1long; 
          resolve(array);
        });
    }
}
