
const {accountSid, authToken,twilioNumber} = require('../config/config');
const client = require('twilio')(accountSid, authToken);

module.exports.sendSMS = function(to,body,callback){
    client.messages
    .create({
       body: body,
       from: twilioNumber,
       to: to
     })
    .then(message => callback(null,message))
    .catch(err=>callback(err));
}