const config = require('../config/config');
const fs = require('fs')
const join = require('path').join
const AWS = require('aws-sdk');
const s3Zip = require('s3-zip')
AWS.config.update({
  accessKeyId: config.AWSAccessKeyId,
  secretAccessKey: config.AWSSecretKey,
  region: "us-east-2",
});
const s3 = new AWS.S3();

module.exports.s3FileDownload = function(document,req,res){
      var bucketParams = {
        Bucket: config.AWSBucketName,  
        Key: document.s3_key
      };

      s3.getObject(bucketParams, function(err, data) {
        if (err) {
          // cannot get file, err = AWS error response, 
          // return json to client
          return res.json({
            success: false,
            error: err
          });
        } else {
          res.attachment(document.s3_key); //sets correct header (fixes your issue ) 
          //if all is fine, bucket and file exist, it will return file to client
          s3.getObject(bucketParams)
            .createReadStream()
            .pipe(res.set('Content-Type', document.mimetype).set('Content-Disposition', 'attachment; filename="'+(document.originalname || document.original_name) +'"'));
        }
      });
}

module.exports.s3GetFileStream = function(files,archiveFiles,res){
  const archive = s3Zip
  .archive({ s3:s3, bucket: config.AWSBucketName}, '', files,archiveFiles);
  archive.on('end',()=>{
    //res.end()
  });
  archive.on('error', (e) => {
    //res.status(500).send(e.message);
 });
  archive.pipe(res);
}

