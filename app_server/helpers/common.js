var config = require('../config/config');
var nodemailer = require('nodemailer');
var azure = require('azure-storage'); //api for azure storage
var blobSvc = azure.createBlobService(config.AZURE_STORAGE_ACCOUNT, config.AZURE_STORAGE_ACCESS_KEY);
var NodeGeocoder = require('node-geocoder');
var _ = require('util');
var mo = require('moment');
var isodate = require('isodate');
var jwt = require('jsonwebtoken');
var btoa = require('btoa');
const { lte } = require('lodash');
const moment = require("moment");;
const axios= require('axios');

var options = {
  provider: 'google',

  // Optional depending on the providers
  httpAdapter: 'https', // Default
  apiKey: config.googleGoecoder_key, // for Mapquest, OpenCage, Google Premier
  //formatter: 'string',         // 'gpx', 'string', ...
  //formatterPattern: '%p%t%z'
};

var geocoder = NodeGeocoder(options);
const http=require('http')


var self = module.exports = {
  posteveryWhereRegx: [
    // {email:{ $regex: 'pels.*', $options: 'i' }},
    { email: { $regex: 'pel*', $options: 'i' } },
    { email: { $regex: '^([0-9]+)@(forkfreight\.com)$', $options: 'i' } }
  ],
  posteveryWhereRegxForNot: [
    // {email: {$not:{ $regex: 'pels.*', $options: 'i' }}},
    { email: { $not: { $regex: 'pel*', $options: 'i' } } },
    { email: { $not: { $regex: '^([0-9]+)@(forkfreight\.com)$', $options: 'i' } } }
  ],
  splitRequestXMLData(accountRequest) {
    var data = {};
    //accountRequest = accountRequest.replace('<'+type+'>','').replace('</'+type+'>','').trim();
    if (accountRequest != '') {
      accountRequest = accountRequest.split("\n");
      //if( accountRequest.length == 2 ){
      keys = accountRequest[1].split("\t");
      values = accountRequest[2].split("\t");
      var keys = keys.filter(function (el) { return el != null; });
      var values = values.filter(function (el) { return el != null; });
      var idx = 0;
      keys.forEach(function (key) {
        data[key.trim()] = values[idx] ? values[idx].trim() : '';
        idx++;
      });
      // }
    }
    return data;
  },

  splitRequestXMLupdate(accountRequest, type) {
    var data = {};
    accountRequest = accountRequest.replace('<' + type + '>', '').replace('</' + type + '>', '').trim();
    if (accountRequest != '') {
      accountRequest = accountRequest.split('\n');
      if (accountRequest.length == 2) {
        keys = accountRequest[0].replace('\r', '').trim().split('|');
        values = accountRequest[1].replace('\r', '').trim().split('|');
        var idx = 0;
        keys.forEach(function (key) {
          data[key.trim()] = values[idx] ? values[idx].trim() : '';
          idx++;
        });
      }
    }
    return data;
  },


  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  },

  sendEmail(subject, message, from, to) {

    let transporter = nodemailer.createTransport({
      host: config.mailHost,
      port: config.mailPort,
      secure: config.mailSecure,
      auth: {
        user: config.mailUser,
        pass: config.mailPass
      }
    });

    let mailOptions = {
      from: '"Fork Freight  " <' + from + '>', // sender address
      to: to,
      subject: subject,
      html: message
    };

    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        return this.log(error);
      }
    });

  },

  async getEquipmentType(equipementType) {
    let equipmentType;
    let defaultValue = "";
    let equipementType_split = equipementType.split(":")
    let code = equipementType_split[0];
    defaultValue = equipementType_split[equipementType_split.length - 1];
    switch (code.toLocaleLowerCase()) {
      case "ac":
        equipmentType = 'Auto Carrier'
        break;
      case "b":
        equipmentType = 'Dry Bulk';
        break;
      case "bt":
        equipmentType = 'B-Train';
        break;
      case "c":
        equipmentType = 'Container';
        break;
      case "cn":
        equipmentType = 'Conestoga';
        break;
      case "ci":
        equipmentType = 'Container Insulated';
        break;
      case "cr":
        equipmentType = 'Container Refrigerated';
        break;
      case "cv":
        equipmentType = ' Conveyor';
        break;
      case "d":
        equipmentType = 'Decks, Standard';
        break;
      case "dd":
        equipmentType = 'Double Drop';
        break;
      case "dt":
        equipmentType = 'Dump Trailer';
        break;
      case "f":
        equipmentType = 'Flatbed';
        break;
      case "fv":
        equipmentType = 'Flatbed/Van';
        break;
      case "fsd":
        equipmentType = 'Flatbed/Step Deck';
        break;
      case "fvr":
        equipmentType = 'Flatbed/Van/Reefer';
        break;
      case "fd":
        equipmentType = 'Flatbed or Step Deck';
        break;
      case "fs":
        equipmentType = 'Flatbed w/Sides';
        break;
      case "ft":
        equipmentType = 'Flatbed w/Tarps';
        break;
      case "fm":
        equipmentType = 'Flatbed w/Team';
        break;
      case "fo":
        equipmentType = 'Flatbed, Over Dimension';
        break;
      case "fc":
        equipmentType = 'Flatbed, w/Chains';
        break;
      case "fr":
        equipmentType = 'Flatbed/Van/Reefer';
        break;
      case "fa":
        equipmentType = 'Flatbed Air-Ride';
        break;
      case "fn":
        equipmentType = 'Flatbed Conestoga';
        break;
      case "f2":
        equipmentType = 'Flatbed Double';
        break;
      case "fz":
        equipmentType = 'Flatbed Hazmat';
        break;
      case "fh":
        equipmentType = 'Flatbed Hotshot';
        break;
      case "hb":
        equipmentType = 'Hopper Bottom';
        break;
      case "ir":
        equipmentType = 'Insulated Van or Reefer';
        break;
      case "k":
        equipmentType = 'Decks, Specialized';
        break;
      case "lb":
        equipmentType = 'Low Boy';
        break;
      case "lr":
        equipmentType = 'Lowboy or RGN';
        break;
      case "lo":
        equipmentType = 'Lowboy, Over Dimension';
        break;
      case "la":
        equipmentType = 'Drop Deck Landoll';
        break;
      case "mv":
        equipmentType = 'Moving Van';
        break;
      case "mx":
        equipmentType = 'Flatbed Maxi';
        break;
      case "n":
        equipmentType = 'Conestoga';
        break;
      case "nu":
        equipmentType = 'Pneumatic';
      case "o":
        equipmentType = 'Other Equipment';
        break;
      case "po":
        equipmentType = 'Power Only';
        break;
      case "r":
        equipmentType = 'Reefer';
        break;
      case "rv":
        equipmentType = 'Van/Reefer';
        break;
      case "rfv":
        equipmentType = 'Flatbed/Van/Reefer';
        break;
      case "r2":
        equipmentType = 'Reefer Double';
        break;
      case "rz":
        equipmentType = 'Reefer Hazmat';
        break;
      case "rn":
        equipmentType = 'Reefer Intermodal';
        break;
      case "rl":
        equipmentType = 'Reefer Logistics';
        break;
      case "rm":
        equipmentType = 'Reefer w/Team';
        break;
      case "rp":
        equipmentType = 'Reefer, w/Pallet Exchange';
        break;
      case "rg":
        equipmentType = 'Removable Gooseneck';
        break;
      case "ra":
        equipmentType = 'Reefer Air-Ride';
        break;
      case "sd":
        equipmentType = 'Step Deck';
        break;
      case "s":
        equipmentType = 'Vans, Specialized';
        break;
      case "sr":
        equipmentType = 'Step Deck or RGN';
        break;
      case "sn":
        equipmentType = 'Stepdeck Conestoga';
        break;
      case "sb":
        equipmentType = 'Straight Box Truck';
        break;
      case "st":
        equipmentType = 'Stretch Trailer';
        break;
      case "t":
        equipmentType = 'Tanker';
        break;
      case "ta":
        equipmentType = 'Tanker Aluminum';
        break;
      case "tn":
        equipmentType = 'Tanker Intermodal';
        break;
      case "ts":
        equipmentType = 'Tanker Steel';
        break;
      case "tt":
        equipmentType = 'Truck and Trailer';
        break;
      case "v":
        equipmentType = 'Vans, Standard';
        break;
      case "vr":
        equipmentType = 'Van/Reefer';
        break;
      case "vf":
        equipmentType = 'Flatbed/Van';
        break;
      case "vfr":
        equipmentType = 'Flatbed/Van/Reefer';
        break;

      case "va":
        equipmentType = 'Van Air-Ride';
        break;
      case "vs":
        equipmentType = 'Van Conestoga';
        break;
      case "v2":
        equipmentType = 'Van Double';
        break;
      case "vz":
        equipmentType = 'Van Hazmat';
        break;
      case "vh":
        equipmentType = 'Van Hotshot';
        break;
      case "vi":
        equipmentType = 'Van Insulated';
        break;
      case "vn":
        equipmentType = 'Van Intermodal';
        break;
      case "vg":
        equipmentType = 'Van Lift-Gate';
        break;
      case "vl":
        equipmentType = 'Van Logistics';
        break;
      case "vt":
        equipmentType = 'Van Open-Top';
        break;
      case "vb":
        equipmentType = 'Van Roller Bed';
        break;
      case "v3":
        equipmentType = 'Van Triple';
        break;
      case "vc":
        equipmentType = 'Van w/Curtains';
        break;
      case "vm":
        equipmentType = 'Van w/Team';
        break;
      case "vp":
        equipmentType = 'Van, w/Pallet Exchange';
        break;
      case "w":
        equipmentType = 'Van Vented';
        break;
      case "ww":
        equipmentType = 'Van, w/Blanket Wrap';
        break;
      case "z":
        equipmentType = 'Hazardous Materials';
        break;

      default:
        equipmentType = defaultValue;
        break;
    }
    return equipmentType;
  },






  sendWelcomeEmail(user, url) {

    var subject = 'Welcome ' + user.first_name;
    var registrationLink = url + "/public/signup?complete=" + btoa(user.email)
    var message = '<p>Hi ' + user.first_name + ',</p><p>Welcome to Fork Freight!<br>You have completed the first step of the registration process.</p><p><br> To start your FREE' + config.freeDaysTrail + '-Day Trial, please click <a href="' + registrationLink + '">here</a> or log back in using your username and password.</p><p>If you have any questions or need support, send us an email! </p><p>Best wishes,<br>The Fork Freight Team<br>' + config.supportEmail + '<br><a href="https://forkfreight.com/">https://forkfreight.com/</a></p>';

    this.sendEmail(subject, message, config.supportEmail, user.email);
  },
  sendWelcomeEmailWithActivateLink(user, url) {

    var subject = 'Welcome ' + user.first_name;

    var expiry = new Date();
    expiry.setDate(expiry.getDate() + 1);

    const activationToken = jwt.sign({
      email: user.email,
      role: user.role,
      exp: parseInt(expiry.getTime() / 1000),
    }, config.secretKey);
    this.log(url)
    //  Send notification
    let activationLink = url + '/public/activateUser?token=' + activationToken + "&email=" + btoa(user.email);
    var message = '<p>Hi ' + user.first_name + ',</p><p>Welcome to Fork Freight!<br>You have completed the registration process.</p><p><br> Your FREE ' + config.freeDaysTrail + '-Day Trial has now started.<br/> Please click <a href="' + activationLink + '">here</a> to activate your account.</p><p>If you have any questions or need support, send us an email! </p><p>Best wishes,<br>The Fork Freight Team<br>' + config.supportEmail + '<br><a href="https://forkfreight.com/">https://forkfreight.com/</a></p>';
    this.sendEmail(subject, message, config.supportEmail, user.email);
  },
  sendReminderEmail(user, url) {

    var subject = 'Time to Finish Your Registration';
    var registrationLink = url + "/public/signup?complete=" + btoa(user.email)
    var message = '<p>Hi ' + user.first_name + ',</p><p>You\'re so close!<br>Thank you for signing up.<br>There are just a few more steps to start your <b>FREE</b> ' + config.freeDaysTrail + '-Day Trial!<br>Click <a href="' + registrationLink + '">here</a> or log back in using email and password.</p><p>If you have any questions or need support, send us an email! </p><p>Best wishes,<br>The Fork Freight Team<br>' + config.supportEmail + '<br><a href="https://forkfreight.com/">https://forkfreight.com/</a></p>';
    this.sendEmail(subject, message, config.supportEmail, user.email);
  },
  sendSubscriptionRenewalEmail(user, url) {

    var subject = 'Your Free Trial is Ending…';
    var registrationLink = url + "/public/subscription-check";
    var message = '<p>Hi ' + user.first_name + ',</p><p>Your Free ' + config.freeDaysTrail + '-Day Trial is coming to an end. <br>To continue your subscription, click <a href="' + registrationLink + '">here</a>!</p><p>If you have any questions or need support, send us an email! </p><p>Best wishes,<br>The Fork Freight Team<br>' + config.supportEmail + '<br><a href="https://forkfreight.com/">https://forkfreight.com/</a></p>';
    this.sendEmail(subject, message, config.supportEmail, user.email);
  },
  sendActivationEmail(user, url) {
    var expiry = new Date();
    expiry.setDate(expiry.getDate() + 1);

    const activationToken = jwt.sign({
      email: user.email,
      role: user.role,
      exp: parseInt(expiry.getTime() / 1000),
    }, config.secretKey);

    //  Send notification
    var subject = 'Account Activation!';
    let portalURL = url; //process.env.PORTAL_URL || "http://localhost:8080";
    let activationLink = portalURL + '/public/activateUser?token=' + activationToken;

    var message = '<p>Hi ' + user.first_name + ',</p><p>One More Step!</p><p>Click <a href="' + activationLink + '">here</a> to activate your account with Fork Freight!</p><p>If you have any questions or need support, send us an email! </p><p>Best wishes,<br>The Fork Freight Team<br>' + config.supportEmail + '<br><a href="https://forkfreight.com/">https://forkfreight.com/</a></p>';

    this.sendEmail(subject, message, config.supportEmail, user.email);
  },

  sendVerifiationEmail(user, url) {
    var expiry = new Date();
    expiry.setDate(expiry.getDate() + 1);

    const activationToken = jwt.sign({
      email: user.email,
      role: user.role,
      exp: parseInt(expiry.getTime() / 1000),
    }, config.secretKey);

    //  Send notification
    var subject = 'Account Verification!';
    let portalURL = url; //process.env.PORTAL_URL || "http://localhost:8080";
    let activationLink = portalURL + '/public/activateUser?token=' + activationToken;
    var message = '<p>Hi ' + user.first_name + ',</p><p>Click <a href="' + activationLink + '">here</a> to verify your your email with Fork Freight!</p><p>If you have any questions or need support, send us an email! </p><p>Best wishes,<br>The Fork Freight Team<br>' + config.supportEmail + '<br><a href="https://forkfreight.com/">https://forkfreight.com/</a></p>';

    this.sendEmail(subject, message, config.supportEmail, user.email);
  },

  sendSelfCancelSubscriptionEmail(user) {
    //  Send notification
    var subject = 'Your Subscription has been Canceled';
    var message = `<p>Hi ${user.first_name},</p>
    <p>
      We are sad to see you go!<br>
      As per your request, your subscription has been canceled. The good news is that your account will be active until ${user.subscription_expire_on}.<br>
      <br>
      We would like to learn the reason behind your cancellation so we can better serve our customers. If you could take a few minutes and respond back to this email, with any feedback, it would be greatly appreciated. <br>
      <br>
    </p>
    <p>If you have any questions or need support, send us an email! </p>
    <p>
      Best wishes,<br>
      The Fork Freight Team<br>${config.supportEmail}<br>
      <a href="https://forkfreight.com/">https://forkfreight.com/</a>
    </p>`;

    this.sendEmail(subject, message, config.supportEmail, user.email);
  },


  sendSelfCancelFreeSubscriptionEmail(user) {
    //  Send notification
    var subject = 'Your trial Subscription has been Canceled';
    var message = `<p>Hi ${user.first_name},</p>
    <p>
      We are sad to see you go!<br>
      As per your request, your ${config.freeDaysTrail}-Days free trial subscription has been canceled. You can Login any time for continue your subscription.<br>
      <br>
      We would like to learn the reason behind your cancellation so we can better serve our customers. If you could take a few minutes and respond back to this email, with any feedback, it would be greatly appreciated. <br>
      <br>
    </p>
    <p>If you have any questions or need support, send us an email! </p>
    <p>
      Best wishes,<br>
      The Fork Freight Team<br>${config.supportEmail}<br>
      <a href="https://forkfreight.com/">https://forkfreight.com/</a>
    </p>`;

    this.sendEmail(subject, message, config.supportEmail, user.email);
  },
  sendAccountActivatedEmail(user, url) {
    var subject = 'Your Account is Active!';
    var message = '<p>Hi ' + user.first_name + ',</p><p>Thank you!<br/>You have successfully activated your account with Fork Freight!</p><p>Click <a href="' + url + '">here</a> to get started or log back in using your username and password.</p><br><p>If you have any questions or need support, send us an email! </p><p>Best wishes,<br>The Fork Freight Team<br>' + config.supportEmail + '<br><a href="https://forkfreight.com/">https://forkfreight.com/</a></p>';
    this.sendEmail(subject, message, config.supportEmail, user.email);
  },
  sendResetPasswordLink(user, url, token, expiredate) {
    var subject = 'Reset Password ';
    var resetPasswordLink = url + "/public/reset-password/" + token
    var message = '<p>Hi ' + user.first_name + ',</p><p><br> Please click <a href="' + resetPasswordLink + '">here</a> to reset your Fork Freight password.</p><p>If you have any questions or need support, send us an email! </p><p>Best wishes,<br>The Fork Freight Team<br>' + config.supportEmail + '<br><a href="https://forkfreight.com/">https://forkfreight.com/</a></p>';

    this.sendEmail(subject, message, config.supportEmail, user.email);
  },
  sendResetPasswordComplete(user) {
    var subject = 'Password Changed';
    var message = '<p>Hi ' + user.first_name + ',</p><p>Your password has been changed successfully.</p><p>If you have any questions or need support, send us an email! </p><p>Best wishes,<br>The Fork Freight Team<br>' + config.supportEmail + '<br><a href="https://forkfreight.com/">https://forkfreight.com/</a></p>';

    this.sendEmail(subject, message, config.supportEmail, user.email);
  },
  sendSetPasswordLink(user, url, token, user1) {
    var subject = 'Create your Password';
    var setPasswordLink = url + "/public/set-password/" + token
    var message = '<p>Hi ' + user.first_name + ',</p><p><br>' + user1.first_name + ' ' + user1.last_name + ' added you to the company <b>' + user1.company_id.company_name + '</b>.</p><p><br> Please click <a href="' + setPasswordLink + '">here</a> to set your Fork Freight password.</p><p>If you have any questions or need support, send us an email! </p><p>Best wishes,<br>The Fork Freight Team<br>' + config.supportEmail + '<br><a href="https://forkfreight.com/">https://forkfreight.com/</a></p>';
    this.sendEmail(subject, message, config.supportEmail, user.email);
  },
  sendSetPasswordLinkByMarketing(user, url, token) {
    var subject = 'Create Your Fork Freight Password';
    var setPasswordLink = url + "/public/set-password/" + token
    var message = '<p>Hi ' + user.first_name + ',</p><p><br> Thank you for registering .</p><p><br> Please <a href="' + setPasswordLink + '">click here</a> to set your Fork Freight password.</p><p>If you have any questions or need support, send us an email! </p><p>Best wishes,<br>The Fork Freight Team<br>' + config.supportEmail + '<br><a href="https://forkfreight.com/">https://forkfreight.com/</a></p>';
    this.sendEmail(subject, message, config.supportEmail, user.email);
  },
  sendSwitchChildUserCompanyEmail(user, user1) {
    var subject = 'Company Changed';
    var message = '<p>Hi ' + user.first_name + ',</p><p><br>' + user1.first_name + ' ' + user1.last_name + ' added you to the company <b>' + user1.company_id.company_name + '</b>.</p><p><br/>If you have any questions or need support, send us an email! </p><p>Best wishes,<br>The Fork Freight Team<br>' + config.supportEmail + '<br><a href="https://forkfreight.com/">https://forkfreight.com/</a></p>';
    this.sendEmail(subject, message, config.supportEmail, user.email);
  },
  updateTrucks(truck, socket) {
    socket.broadcast.emit('my_trucks_response', { type: 'new', data: truck, success: true });
    socket.broadcast.emit('trucks_response', { type: 'old', data: truck, success: true });
    socket.broadcast.emit('watchlist_truck_response', { type: 'old', data: truck });
  },

  truckRefreshAge(truck, socket) {
    socket.broadcast.emit('my_trucks_response', { type: 'new', data: truck, success: true });
    socket.broadcast.emit('trucks_response', { type: 'old', data: truck, success: true });
  },

  updateLoads(load, socket) {
    socket.broadcast.emit('my_loads_response', { type: 'new', data: load });
    socket.broadcast.emit('bidswon_response', { type: 'new', data: load });
    socket.broadcast.emit('loads_response', { type: 'old', data: load });
    socket.broadcast.emit('watchlist_response', { type: 'old', data: load });
  },

  // //  Search Load
  // searchLoad (load, socket) {
  //     socket.emit('loads_response', {type: 'Search', data:load});
  //     // socket.broadcast.emit('bidswon_response', {type: 'new', data:load});
  //     // socket.broadcast.emit('loads_response', {type: 'old', data:load});  
  //     // socket.broadcast.emit('watchlist_response', {type: 'old', data:load});  
  // },

  loadRefreshAge(load, socket) {
    socket.broadcast.emit('my_loads_response', { type: 'new', data: load });
    socket.broadcast.emit('bidswon_response', { type: 'new', data: load });
    socket.broadcast.emit('loads_response', { type: 'old', data: load });
    socket.broadcast.emit('watchlist_response', { type: 'old', data: load });
  },

  uploadtoAzure(imageBuffer, callback) {

    var _id = self.generateUUID();
    var file = _id + '.jpg';

    var result = blobSvc.createBlockBlobFromText(config.AZURE_STORAGE_CONTAINER, file, imageBuffer, function (error, result, response) {

      if (!error) {
        callback(config.AZURE_STORAGE_URL + config.AZURE_STORAGE_CONTAINER + '/' + file);
      };

    });

    return;
  },

  generateUUID() { // Public Domain/MIT
    var d = new Date().getTime();
    if (typeof performance !== 'undefined' && typeof performance.now === 'function') {
      d += performance.now(); //use high-precision timer if available
    }
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
      var r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
  },

  // Decoding base-64 image
  // Source: http://stackoverflow.com/questions/20267939/nodejs-write-base64-image-file
  decodeBase64Image(dataString) {

    var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
    var response = {};

    if (matches != null) {
      if (matches.length !== 3) {
        return new Error('Invalid input string');
      }
    } else {
      return new Error('Invalid input string');
    }

    response.type = matches[1];
    response.data = new Buffer(matches[2], 'base64');

    return response;
  },

  SaveImageToDisk(base64Data, get_azure_image) {

    try {

      var imageBuffer = self.decodeBase64Image(base64Data);
      self.uploadtoAzure(imageBuffer.data, function (file_path) {
        get_azure_image(file_path);
      });

    } catch (error) {
      this.log('ERROR:', error);
    }


  },
  validateSearchFilter(search_object) {
    var search_options = {};
    if (search_object !== undefined) {

      // Search by Load Type
      // if (search_object.load_type == "1") {
      //   search_options['user_type'] = { $eq: "broker" }
      // } else
      //   if (search_object.load_type == "2") {
      //     search_options['user_type'] = { $eq: "shipper" }
      //   }

      // // Search by City 
      //this.log(search_object, 'testing1234');
      if (search_object.pickup_city) {
        if (search_object.pickup_city.length != 0) {
          search_options["pickup_cities"] = { $in: [search_object.pickup_city] };
        }
      }
      if (search_object.delivery_city) {
        if (search_object.delivery_city.length != 0) {
          search_options["delivery_cities"] = { $in: [search_object.delivery_city] };
        }
      }

      // // Search by State
      if (search_object.pickup_state) {
        if (search_object.pickup_state.length != 0) {
          search_options["pickup_states"] = { $in: [search_object.pickup_state] };
        }
      }
      if (search_object.delivery_state) {
        if (search_object.delivery_state.length != 0) {
          search_options["delivery_states"] = { $in: [search_object.delivery_state] };
        }
      }
      if (search_object.delivery_addresses && !search_object.delivery_long) {
        if (search_object.delivery_addresses.length != 0) {
          search_object.delivery_states = search_object.delivery_addresses;
          search_options["delivery_states"] = { $in: [search_object.delivery_state] };
          search_options.delivery_addresses = null;
        }
      }
      if (search_object.pickup_address && !search_object.pickup_long) {
        if (search_object.pickup_address.length != 0) {
          search_object.pickup_states = search_object.pickup_address;
          search_options["pickup_states"] = { $in: [search_object.pickup_state] };
          search_object.pickup_address = null;
        }
      }

      if (search_object.equipment_type) {
        if (search_object.equipment_type.length != 0) {
          search_options["equipment_type"] = { $in: search_object.equipment_type };
        }
      }
      if (search_object.load_weight) {
        if (search_object.load_weight != 0 && search_object.load_weight != undefined) {
          search_options["load_weight"] = { $lte: parseInt(search_object.load_weight) };
        }
      }

      if (search_object.load_length) {
        if (search_object.load_length == "1") {
          search_options["load_length"] = { $gte: 40 };
        } else
          if (search_object.load_length == "2") {
            search_options["load_length"] = { $lte: 40 };
          }
      }
      if (search_object.start_date && search_object.end_date) {
        search_options["ready_date"] = {
          "$gte": new Date(search_object.start_date),
          "$lte": new Date(search_object.end_date)
        }
      } else if (search_object.start_date && !search_object.end_date) {
        search_options["ready_date"] = {
          $gte: new Date(search_object.start_date)
        }
      }

      if (search_object.pickup_long && search_object.pickup_city_radius && search_object.pickup_city_radius != '0') {
        search_options["pickup_lat_long"] = {
          $near: {
            $geometry: {
              type: "MultiPoint",
              coordinates: [search_object.pickup_long, search_object.pickup_lat]
            },
            $maxDistance: parseInt(search_object.pickup_city_radius * 1609),
            //$minDistance: -10
            //$maxDistance:parseInt(search_object.delivery_city_radius),

          }

        }

      }

      if (!(search_object.pickup_long && search_object.pickup_city_radius && search_object.pickup_city_radius != '0') && search_object.delivery_long && search_object.delivery_city_radius && search_object.delivery_city_radius != '0') {
        search_options["delivery_lat_long"] = {
          $near: {
            $geometry: {
              type: "MultiPoint",
              coordinates: [search_object.delivery_long, search_object.delivery_lat]
            },
            $maxDistance: parseInt(search_object.delivery_city_radius * 1609),
            // $minDistance: -10
            //$maxDistance:parseInt(search_object.delivery_city_radius),                        
          }
        }

      }

    }
    return search_options;
  },
  getCoordinates(address, city, state) {
    return new Promise(function (resolve, reject) {

      let pickup_coordinates = [];
      if (address && address !== 'undefined') {
        pickup_coordinates.push(address)
      }

      if (city && city !== 'undefined') {
        pickup_coordinates.push(city)
      }

      if (state && state !== 'undefined') {
        pickup_coordinates.push(state)
      }


      //convert address to coordinates , city and state
      let pickup_address_full = pickup_coordinates.join();
      let geocode_long;
      let geocode_lat;
      let geocode = {};

      geocoder.geocode(pickup_address_full, function (err, res) {
        if (err) {
          this.log(err);
          reject(err);
        }
        else {
          geocode_lat = (res && res.length > 0) ? res[0].latitude : " ";
          geocode_long = (res && res.length > 0) ? res[0].longitude : " ";
          geocode["geocode_lat"] = geocode_lat;
          geocode["geocode_long"] = geocode_long;
          geocode["state_code"] = (res[0].administrativeLevels) ? res[0].administrativeLevels.level1short : "";
          geocode["country_code"] = (res[0].countryCode) ? res[0].countryCode : "US";
          geocode["city"] = (res[0].city) ? res[0].city : "";
          geocode["state"] = (res[0].administrativeLevels) ? res[0].administrativeLevels.level1long : "";
          this.log(res);
          resolve(geocode);
        }
      })
    })
  },
  async getCoordinatesAsync(address, city, state) {
    let pickup_coordinates = [];
    if (address && address !== 'undefined') {
      pickup_coordinates.push(address)
    }

    if (city && city !== 'undefined') {
      pickup_coordinates.push(city)
    }

    if (state && state !== 'undefined') {
      pickup_coordinates.push(state)
    }


    //convert address to coordinates , city and state
    let pickup_address_full = pickup_coordinates.join();
    let geocode_long;
    let geocode_lat;
    let geocode = {};

    try {
      var geoInfo = await geocoder.geocode(pickup_address_full);
      if (geoInfo != null) {


        geocode_lat = (geoInfo && geoInfo.length > 0) ? geoInfo[0].latitude : " ";
        geocode_long = (geoInfo && geoInfo.length > 0) ? geoInfo[0].longitude : " ";
        geocode["geocode_lat"] = geocode_lat;
        geocode["geocode_long"] = geocode_long;
        geocode["state_code"] = (geoInfo[0].administrativeLevels) ? geoInfo[0].administrativeLevels.level1short : "";
        geocode["country_code"] = (geoInfo[0].countryCode) ? geoInfo[0].countryCode : "US";
        geocode["city"] = (geoInfo[0].city) ? geoInfo[0].city : "";
        geocode["state"] = (geoInfo[0].administrativeLevels) ? geoInfo[0].administrativeLevels.level1long : "";
        this.log(geoInfo);
        return geocode;
      }
      else {
        return null;
      }
    }
    catch (err) {
      this.log(err);
      return;
    }
  },
  getFormattedDate(_date) {
    let date = new Date(_date);
    return (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear();
  },
  snooze(ms) {
    return new Promise((resolve) => {
      setTimeout(resolve, ms);
    });
  },
  async generateSearchObjectForFindLoad(search_object) {
    var search_options = {};
    search_options["$and"] = [];
    let isOpened = { "is_opened": true };
    search_options["$and"].push(isOpened);

    if (search_object !== undefined) {

      let pickupItems = {};
      pickupItems["$or"] = [];
      var originCity = {};
      var originState = {};
      let cityState = {};
      cityState["$and"] = [];
      // if (search_object.pickup_address && search_object.pickup_address != '') {      
      //   var pickupAddressArray = search_object.pickup_address.split(",");
      //   if (pickupAddressArray != null && pickupAddressArray.length > 0) {
      //     originCity["pickup_cities"] = { $in: [search_object?.pickup_city ? search_object?.pickup_city : pickupAddressArray[0].trim()] };
      //     cityState["$and"].push(originCity);
      //     pickupItems["$or"].push(cityState);
      //   }
      // }
      if (search_object.pickup_states && search_object.pickup_states != '') {
        originState["pickup_states"] = { $in: search_object.pickup_states };
        cityState["$and"].push(originState);
        pickupItems["$or"].push(cityState);
      }

      if (pickupItems["$or"].length > 0) { search_options["$and"].push(pickupItems); }


      // Delivery options
      let deliveryItems = {};
      deliveryItems["$or"] = [];
      var deliveryCity = {};
      var deliveryState = {};
      var deliverycityState = {};
      deliverycityState["$and"] = [];
      // if (search_object.delivery_addresses && search_object.delivery_addresses != '') {
      //   var deliveryAddressArray = search_object.delivery_addresses.split(",");
      //   if (deliveryAddressArray != null && deliveryAddressArray.length > 0) {
      //     deliveryCity["delivery_cities"] = { $in: [search_object?.delivery_city ? search_object?.delivery_city : deliveryAddressArray[0].trim()] };
      //     deliverycityState["$and"].push(deliveryCity);
      //     deliveryItems["$or"].push(deliverycityState);
      //   }
      // }
      if (search_object.delivery_states && search_object.delivery_states != '') {
        deliveryState["delivery_states"] = { $in: search_object.delivery_states };
        deliverycityState["$and"].push(deliveryState);
        deliveryItems["$or"].push(deliverycityState);
      }

      if (deliveryItems["$or"].length > 0) { search_options["$and"].push(deliveryItems); }

      if (search_object.delivery_addresses) {
        search_options["$and"].push({
          $or: [
            {
              delivery_addresses: {
                '$regex': search_object.delivery_city,
                '$options': 'i'
              }
            },
            {
              delivery_cities: {
                //'$regex': `^${search_object.delivery_addresses}.*`,
                '$regex': search_object.delivery_city,
                '$options': 'i'
              }
            }
          ]
        })
      }

      //date range
      let dateRange = {};
      if (search_object.start_date != null && search_object.end_date != null) {
        let st_dt = search_object.start_date.split(',')[0];
        let tim = "00:00";
        let s_datetime = moment.utc(st_dt + ' ' + tim);

        let start_date = new Date(s_datetime);
        start_date.setUTCHours(00, 00, 00);

        //for end date
        let _end_date = search_object.end_date.split(',')[0];
        let _time = "23:00";
        let end_datetime = moment.utc(_end_date + ' ' + _time);

        let end_date = new Date(end_datetime);
        end_date.setUTCHours(23, 59, 59);

        search_options["$and"].push({
          ready_date: {
            $gte: start_date,
            $lte: end_date
          }
        });
      } else if (search_object.start_date != null && search_object.end_date == null) {

        let st_dt = search_object.start_date.split(',')[0];
        let tim = "00:00";
        let s_datetime = moment.utc(st_dt + ' ' + tim);

        let start_date = new Date(s_datetime);
        start_date.setUTCHours(00, 00, 00);

        search_options["$and"].push({
          ready_date: {
            $gte: start_date
          }
        });
      } else if (search_object.end_date != null && search_object.start_date == null) {

        //for end date
        let _end_date = search_object.end_date.split(',')[0];
        let _time = "23:00";
        let end_datetime = moment.utc(_end_date + ' ' + _time);

        let end_date = new Date(end_datetime);
        end_date.setUTCHours(23, 59, 59);

        search_options["$and"].push({
          ready_date: {
            $lte: end_date
          }
        });

      } else if (search_object.end_date == null && search_object.start_date == null) {
        var currentDate = new Date();
        currentDate.setHours(-96, 0, 0);
        search_options["$and"].push({
          ready_date: {
            $gte: currentDate
          }
        });
      } else {
        console.log('lets do nothing');
      }

      // Equipment type, Load weight and Load length

      if (search_object.equipment_type && search_object.equipment_type != '') {
        var equipmentOptions = [];
        var equipmentTypes = search_object.equipment_type;
        //add list from search criteria
        for (const i of equipmentTypes) {
          equipmentOptions.push(new RegExp(`${i}`));
        }
        let equip = {};
        equip["equipment_type"] = { $in: equipmentOptions };
        search_options["$and"].push(equip);
      }
      if (search_object.load_weight && search_object.load_weight != '') {
        let load_weight = {};
        load_weight["load_weight"] = { $lte: Number(search_object.load_weight) };
        search_options["$and"].push(load_weight);
      }
      if (search_object.load_length && search_object.load_length != '') {
        let load_length = {};
        if (search_object.load_length == 1) {
          load_length["load_length"] = { $gte: 40 };
        } else if (search_object.load_length == 2) {
          load_length["load_length"] = { $lte: 40 };
        }
        search_options["$and"].push(load_length);
      }
    }

    //this.log("search_options")
    //this.log(JSON.stringify(search_options));
    return search_options;
  },
  async generateSearchObject(search_object) {
    var search_options = {};
    search_options["$and"] = [];
    let isOpened = { "is_opened": true };
    search_options["$and"].push(isOpened);
    search_options["$and"].push({ distance: { $nin: [0] }});
    // search_options["created_by"] = { $ne: socket.decoded._id };

    if (search_object !== undefined) {

      //  Pickup options
      let pickupItems = {};
      pickupItems["$or"] = [];
      var originCity = {};
      var originState = {};
      let cityState = {};
      cityState["$and"] = [];
      if (search_object.pickup_address && search_object.pickup_address != '') {
        var pickupAddressArray = search_object.pickup_address.split(",");
        //var geocoordinates = await common.getCoordinatesAsync(search_object.pickup_address);
        if (pickupAddressArray != null && pickupAddressArray.length > 0) {
          originCity["pickup_cities"] = { $in: [search_object?.pickup_city ? search_object?.pickup_city.toUpperCase() : pickupAddressArray[0].trim().toUpperCase()] };
          originState["pickup_states"] = { $in: [pickupAddressArray[1].trim().split(" ")[0].trim()] };

          cityState["$and"].push(originCity);
          cityState["$and"].push(originState);
          pickupItems["$or"].push(cityState);
        }
      }
      else if (search_object.pickup_states && search_object.pickup_states != '') {
        originState["pickup_states"] = { $in: search_object.pickup_states };
        cityState["$and"].push(originState);
        pickupItems["$or"].push(cityState);
      }

      if (search_object.pickup_city_radius != null && search_object.pickup_city_radius != "0" && search_object.pickup_city_radius != "" && search_object.pickup_lat != null
      && search_object.pickup_lat != "0" && search_object.pickup_long != null && search_object.pickup_long != "0") {
        let pickup_lat_long = {};
        pickup_lat_long["pickup_lat_long"] =
        {
          $geoWithin:
            { $centerSphere: [[Number(search_object.pickup_long), Number(search_object.pickup_lat)], Number(search_object.pickup_city_radius) / 3963.2] }
        }

        pickupItems["$or"].push(pickup_lat_long);
      }


      if ((search_object.pickup_city_radius == null || search_object.pickup_city_radius == "0" || search_object.pickup_city_radius == "") && search_object.pickup_lat != null
      && search_object.pickup_lat != "0" && search_object.pickup_long != null && search_object.pickup_long != "0") {
      let pickup_lat_long = {};
      pickup_lat_long["pickup_lat_long"] =
      {
        $geoWithin:
          { $centerSphere: [[Number(search_object.pickup_long), Number(search_object.pickup_lat)], config.defaultLoadRadius / 3963.2] }
      }

      pickupItems["$or"].push(pickup_lat_long);
    }

      if (pickupItems["$or"].length > 0) { search_options["$and"].push(pickupItems); }


      // Delivery options
      let deliveryItems = {};
      deliveryItems["$or"] = [];
      var deliveryCity = {};
      var deliveryState = {};
      var deliverycityState = {};
      deliverycityState["$and"] = [];
      if (search_object.delivery_addresses && search_object.delivery_addresses != '') {
        var deliveryAddressArray = search_object.delivery_addresses.split(",");
        //var geocoordinates = await common.getCoordinatesAsync(search_object.delivery_addresses);
        if (deliveryAddressArray != null && deliveryAddressArray.length > 0) {
          deliveryCity["delivery_cities"] = { $in: [search_object?.delivery_city ? search_object?.delivery_city.toUpperCase() : deliveryAddressArray[0].trim().toUpperCase()] };
          // deliveryState["delivery_states"] = { $in: [deliveryAddressArray[1].trim().split(" ")[0].trim()] };
          deliverycityState["$and"].push(deliveryCity);
          // deliverycityState["$and"].push(deliveryState);
          //deliveryItems["$or"].push(deliverycityState);
          deliveryState["delivery_states"] = { $in: search_object.delivery_states? search_object.delivery_states: [deliveryAddressArray[1].substr(1,3).trim()]};
          deliverycityState["$and"].push(deliveryState);
          deliveryItems["$or"].push(deliverycityState);
        }
      }
      else if (search_object.delivery_states && search_object.delivery_states != '') {
        deliveryState["delivery_states"] = { $in: search_object.delivery_states };
        deliverycityState["$and"].push(deliveryState);
        deliveryItems["$or"].push(deliverycityState);
      }


      if (search_object.delivery_city_radius != null && search_object.delivery_city_radius != "0"  && search_object.delivery_city_radius != "" && search_object.delivery_lat != null
      && search_object.delivery_lat != '0' && search_object.delivery_long != null  && search_object.delivery_long != "0") {
        let delivery_lat_long = {};
        delivery_lat_long["delivery_lat_long"] =
        {
          $geoWithin:
            { $centerSphere: [[Number(search_object.delivery_long), Number(search_object.delivery_lat)], Number(search_object.delivery_city_radius) / 3963.2] }
        }

        deliveryItems["$or"].push(delivery_lat_long);
      }
      //if radius is empty but destinations are given then use defaul radius
      if ((search_object.delivery_city_radius == null || search_object.delivery_city_radius == '0'  || search_object.delivery_city_radius == "") && search_object.delivery_lat != null
      && search_object.delivery_lat != '0' && search_object.delivery_long != null  && search_object.delivery_long != "0") {
        let delivery_lat_long = {};
        delivery_lat_long["delivery_lat_long"] =
        {
          $geoWithin:
            { $centerSphere: [[Number(search_object.delivery_long), Number(search_object.delivery_lat)], config.defaultLoadRadius / 3963.2] }
        }

        deliveryItems["$or"].push(delivery_lat_long);
      }

      if (deliveryItems["$or"].length > 0) { search_options["$and"].push(deliveryItems); }

      //date range
      let dateRange = {};
      if (search_object.start_date != null
        && search_object.end_date != null
      ) {
        
        let st_dt= search_object.start_date.split(',')[0];
        let tim = "00:00";
        let s_datetime= moment.utc(st_dt + ' '+ tim);
        // moment(search_object.start_date).utcOffset(0).startOf('day').format()

        let start_date = new Date(s_datetime);
        start_date.setUTCHours(00, 00, 00);

        //for end date
        let _end_date= search_object.end_date.split(',')[0];
        let _time = "23:00";
        let end_datetime= moment.utc(_end_date + ' '+ _time);

        let end_date = new Date(end_datetime);
        end_date.setUTCHours(23, 59, 59);

        search_options["$and"].push({
          ready_date: {
            $gte: start_date,
            $lte: end_date
          }
        });
       

      }
      else if (search_object.start_date != null
        && search_object.end_date == null) {
          let st_dt= search_object.start_date.split(',')[0];
        let tim = "00:00";
          let s_datetime= moment.utc(st_dt + ' '+ tim);
        // moment(search_object.start_date).utcOffset(0).startOf('day').format()

        let start_date = new Date(s_datetime);
        start_date.setUTCHours(00, 00, 00);


        search_options["$and"].push({
            ready_date:{
              $gte: start_date
            }
          });
        
      }
      else if (search_object.end_date != null
        && search_object.start_date == null) {
         
                //for end date
                let _end_date= search_object.end_date.split(',')[0];
        let _time = "23:00";
                let end_datetime= moment.utc(_end_date + ' '+ _time);

        let end_date = new Date(end_datetime);
        end_date.setUTCHours(23, 59, 59);

        // search_object.end_date.setHours(23,59,59);
        search_options["$and"].push({
          ready_date: {
            $lte: end_date
          }
        });
      
      } else if (search_object.end_date == null
        && search_object.start_date == null) {
        var currentDate = new Date();
        currentDate.setHours(-96, 0, 0);
        search_options["$and"].push({
          ready_date: {
            $gte: currentDate
          }
        });
      }else{
        console.log('lets do nothing');
      }


      // Equipment type, Load weight and Load length

      if (search_object.equipment_type && search_object.equipment_type != '') {
        var equipmentOptions = [];
        var equipmentTypes = search_object.equipment_type;
        
        //add list from search criteria
        for (const i of equipmentTypes) {
          equipmentOptions.push(new RegExp(`${i}`));
        }
        let equip = {};
        equip["equipment_type"] = { $in: equipmentOptions };
        search_options["$and"].push(equip);
      }


      if (search_object.load_weight && search_object.load_weight != '') {
        let load_weight = {};
        load_weight["load_weight"] = {$lte: Number(search_object.load_weight)};
        search_options["$and"].push(load_weight);
      }

      if (search_object.load_length && search_object.load_length != '') {

        let load_length = {};
        //load_length["load_length"] = Number(search_object.load_length);
        if (search_object.load_length == 1) {
          load_length["load_length"] = { $gte: 40 };
        } else if (search_object.load_length == 2) {
          load_length["load_length"] = { $lte: 40 };
        }
        search_options["$and"].push(load_length);
      }
    }

    //this.log("search_options")
    //this.log(JSON.stringify(search_options));

    return search_options;
  },
  async getDateRangeFilter(start_date, end_date) {

    let dateRange = {};
    if (start_date != null
      && end_date != null
    ) {
      dateRange["$expr"] =
      {
        "$and": [
          { $gte: [{ "$dateFromString": { "dateString": "$ready_date" } }, isodate(start_date)] },
          { $lte: [{ "$dateFromString": { "dateString": "$ready_date" } }, isodate(end_date)] }
        ]
      }
    }
    else if (start_date != null
      && end_date == null) {

      dateRange["$expr"] =
      {
        $gte: [{ "$dateFromString": { "dateString": "$ready_date" } }, isodate(start_date)]
      }

    }
    else if (end_date != null
      && start_date == null) {

      dateRange["$expr"] =
      {
        $lte: [{ "$dateFromString": { "dateString": "$ready_date" } }, isodate(end_date)]
      }

    }
    return dateRange;
  },
  async getDateRangeFilter(start_date, end_date) {

    let dateRange = {};
    if (start_date != null
      && end_date != null
    ) {
      dateRange["$expr"] =
      {
        "$and": [
          { $gte: [{ "$dateFromString": { "dateString": "$ready_date" } }, isodate(start_date)] },
          { $lte: [{ "$dateFromString": { "dateString": "$ready_date" } }, isodate(end_date)] }
        ]
      }
    }
    else if (start_date != null
      && end_date == null) {

      dateRange["$expr"] =
      {
        $gte: [{ "$dateFromString": { "dateString": "$ready_date" } }, isodate(start_date)]
      }

    }
    else if (end_date != null
      && start_date == null) {

      dateRange["$expr"] =
      {
        $lte: [{ "$dateFromString": { "dateString": "$ready_date" } }, isodate(end_date)]
      }

    }
    return dateRange;
  },
  async getUserSearchObject(search_object, isPostEveryWhere = false, is_internal = false, freeSubscriptionId = [],publicRoles = []) {
    var search_options = {};
    search_options["$and"] = []
    if(!is_internal){
      search_options['$and'].push({ $or: [{ "is_internal": is_internal }, { "is_internal": null }] })
      if(publicRoles.length){
        search_options['$and'].push({role:{$in:publicRoles} })
      }
    }else{
      search_options['$and'].push({ $or: [{ "is_internal": is_internal }, { "role": {$in:publicRoles} }] })
    }
    if (isPostEveryWhere) {
      search_options["$and"].push(
        { "source_data": { $eq: "posteverywhere" } }
      );
    } else {
      search_options["$and"].push(
        { "source_data": { $ne: "posteverywhere" } }
      );
    }
    /** now we r not using regex anymore as we implement source_data for user filter */
    // else {
    //   search_options["$and"].push(
    //     { $or: this.posteveryWhereRegxForNot }
    //   );
    // }

    function addInsearch(val) {
      if (!search_options["$and"]) search_options["$and"] = []
      search_options["$and"].push(val);
    }

    if (search_object.status && (search_object.status === 'active' || search_object.status === 'inactive')) {
      // let status_option = {}
      // status_option["status"] = search_object.status;
      // addInsearch(status_option);
      /**
      * for activeUsers filter 
      * subscription must be true and 
      * source_data should not posteverywhere and 
      * status should be active 
      */
      let today = new Date();
      today.setHours(0, 0, 0);
      if (search_object.status === 'active') {
        search_options['$and'].push({         
          status: "active",
          // $or:[
          //     {"subscription_package":{$in:freeSubscriptionId}},
          //     {"subscription_expire_on": { $gte: today }}
          // ],
          //"payment_status": {$nin:["past_due"]}          // {}
        });

      } else if (search_object.status === 'inactive') {
        //filter user with status "inactive and subcription=false or expiry less than today"
        search_options['$and'].push({
          $or: [
            // {subscription:false},
            // {$and:
            // [
            // {"subscription_package":{$nin:freeSubscriptionId}},
            // { subscription_expire_on: { $lt: today } }
            //  ]},
           // { "payment_status": { $in: ["past_due"] } },
            { status: "inactive" },            
            // {"subscription_package.is_free":false}
          ],
        })
      }
    }

    if (search_object.role && search_object.role !== '') {
      let role_option = {}
      // if(typeof search_object.role == "string"){
      role_option["role"] = search_object.role;
      // }else{
      //   role_option["role"] = { $in : search_object.roles};
      // }
      addInsearch(role_option);
    }
    if (search_object.first_name && search_object.first_name !== '') {
      let option = {}
      option["first_name"] = { $regex: '.*' + search_object.first_name + '.*', $options: 'i' };
      addInsearch(option);
    }
    if (search_object.username && search_object.username !== '') {
      let option = {}
      option["username"] = { $regex: '.*' + search_object.username + '.*', $options: 'i' };
      addInsearch(option);
    }
    if (search_object.last_name && search_object.last_name !== '') {
      let option = {}
      option["last_name"] = { $regex: '.*' + search_object.last_name + '.*', $options: 'i' };
      addInsearch(option);
    }
    if (search_object.email && search_object.email !== '') {
      let option = {}
      option["email"] = { $regex: '.*' + search_object.email + '.*', $options: 'i' };
      addInsearch(option);
    }
    if (search_object.company && search_object.company !== '') {
      let option = {}
      option["company"] = { $regex: '.*' + search_object.company + '.*', $options: 'i' };
      addInsearch(option);
    }
    if (search_object.phone && search_object.phone !== '') {
      let option = {}
      option["phone"] = { $regex: '.*' + search_object.phone + '.*', $options: 'i' };
      addInsearch(option);
    }
    if (search_object.mc_Number && search_object.mc_Number !== '') {
      let option = {}
      option["mc_Number"] = { $regex: '.*' + search_object.mc_Number + '.*', $options: 'i' };
      addInsearch(option);
    }
    if (search_object.businessType && search_object.businessType !== '') {
      let option = {}
      option["businessType"] = { $regex: '.*' + search_object.businessType + '.*', $options: 'i' };
      addInsearch(option);
    }
    if (search_object.subscription_type && search_object.subscription_type !== '') {
      let option = {}
      option["subscription_type"] = { $regex: '.*' + search_object.subscription_type + '.*', $options: 'i' };
      addInsearch(option);
    }
    if (search_object.payment_status) {
      let option = {}
      option["payment_status"] = search_object.payment_status;
      addInsearch(option);
    }
    if (search_object.payment_status) {
      let option = {}
      option["payment_status"] = search_object.payment_status;
      addInsearch(option);
    }

    return search_options;
  },
  sendCancelSubscriptionEmail(user) {
    var subject = 'Subsciption Cancel';
    var message = '<p>Hi ' + user.first_name + ',</p><p>Your subscription plan has been canceled by administartor.</p><p>If you have any questions or need support, please email us at ' + config.supportEmail + '</p><p>Best wishes,<br>Fork Freight Team</p>';
    this.sendEmail(subject, message, config.supportEmail, user.email);
  },
  getEqupementTypeForSearch(equipmentTypes) {
    var equipmentOptions = [];
    if (equipmentTypes.includes("Flatbed")) {
      equipmentOptions.push("Flatbed/Step Deck", "Flatbed/Van", "Flatbed/Van/Reefer", "B-Train Flatbed", "Flatbed with sides",
        "Flatbed with Tarps", "Flatbed with Pallet Exchange", "Hazmat Flatbed", "Maxi Flatbed", "Team Flatbed", "Hotshot Flatbed")
    }

    if (equipmentTypes.includes("Step Deck")) {
      equipmentOptions.push("Flatbed/Step Deck", "Hotshot Step Deck", "Removable Gooseneck")
    }

    if (equipmentTypes.includes("Reefer")) {
      equipmentOptions.push("Reefer with Pallet Exchange", "Hazmat Reefer", "Van/Reefer", "Flatbed/Van/Reefer")
    }

    if (equipmentTypes.includes("Van")) {
      equipmentOptions.push("Flatbed/Van/Reefer", "Flatbed/Van", "Air Ride Van", "Van with curtains", "Van with Pallet Exchange",
        "Hazmat Van", "Vented Van", "Team Van", "Walking Floor Van", "Van/Reefer", "Box Truck")
    }
    for (const i of equipmentTypes) {
      equipmentOptions.push(i);
    }
    let equip = {};
    equip["equipment_type"] = { $in: equipmentOptions };
    return equip;
  },
  async log(message, message1 = null) {
    if (config.DEBUG == true) {
      message1 ? console.log(message, message1) : console.log(message);
    }
  },
  /**
   * Determine whether the given `input` is iterable.
   *
   * @returns {Boolean}
   */
  isIterable(input) {  
    if (input === null || input === undefined) {
      return false
    }

    return typeof input[Symbol.iterator] === 'function'
  },

  /**
   * xml request generator
   */
   async xml_call(req_method,data,path_url=null,params=null){
    return new Promise((resolve,reject)=>{
      let resp_data = '';
      let body = `<?xml version="1.0" encoding="utf-8"?>
        <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
        xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
        xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
         <soap:Body>
         <getReportSearchV2 xmlns="http://tempuri.org/">
         <API_key>FO6d23Gm9rpQTest</API_key>
         <Company_Name>Abercrombie & Fitch</Company_Name>
         <City>New Albany</City>
         <State>Ohio</State>
         </getReportSearchV2>
         </soap:Body>
        </soap:Envelope>`;

      let postRequest = {
        host: "apitest.rcktechiees.com",
        path: "/Service1.asmxHTTP/1.1",
        //  port: 80,
        method: req_method,
        headers: {
          //  'Cookie': "cookie",
          'Content-Type': 'text/xml',
             'charset':'utf-8',
          'Content-Length': Buffer.byteLength(body),
          'SOAPAction': "http://tempuri.org/getReportSearchV2"
        }
      };

      var buffer = "";

var req = http.request( postRequest, function( res )    {

console.log("status code",res.statusCode );
        var buffer = "";
res.on( "data", function( data ) { buffer = buffer + data; } );
res.on( "end", function( data ) { console.log("response :____", buffer ,"\n_____________________"); return resolve(buffer) } );

      });

req.on('error', function(e) {
        console.log('problem with request: ' + e.message);
        return reject(e.message)
      });

req.write( body );
      req.end();
    })

  },

  /**
   * xml post using axios
   */
async xml_request(req_method='POST',data=null,path_url=null,params=null,company_name=null){
  let xmls=`<?xml version="1.0" encoding="utf-8"?>
  <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
  <getReportSearchV2 xmlns="http://tempuri.org/">
  <API_key>${config.TRANSCREDIT_KEY}</API_key>
  <Company_Name>${company_name}</Company_Name>
  <City></City>
  <State></State>
  <MC_FF></MC_FF>
  <Phone_Number></Phone_Number>
  <Toll_Free_Number></Toll_Free_Number>
  </getReportSearchV2>
  </soap:Body>
  </soap:Envelope>`;

  if(data)
  xmls= data;

return new Promise(async(resolve,reject)=>{
      await axios.post('http://api.transcredit.com/Service1.asmx',
        xmls,
{headers:
{'Content-Type': 'text/xml'}
}).then(res=>{
          // console.log("response for request",res);
let {status,data}= res;
status==200?resolve(data):reject(res);
          // resolve(res);
}).catch(err=>{
  console.log("error in xml request",err)
          reject(err);
        });

    });

  },

  /**
   * map call for distance calculations
   */
async mapMatrixGoogleRequest(origin,destination){
  try{
  let base_url='https://maps.googleapis.com/maps/api/distancematrix/json';
  let apiKey='AIzaSyC6WZGzkjs5t3wi64t-1fIkDdmRjm0wdjs';
const res= await axios.get(base_url,{params:{origins:origin,destinations:destination,key:apiKey}})
console.log("args :___",res.data.args,"__\n");
console.log("response of map matric api:___",res,"\n_________________________________");

      return JSON.stringify(res);
}catch(error){
      throw error;
    }
  }


}
