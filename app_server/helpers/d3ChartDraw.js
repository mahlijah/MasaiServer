const D3Node = require('d3-node');
const common = require('./common');

module.exports.GroupBarChart = function (content, groupData) {
  const options = { selector: '#groupBarChart', container: content }
  const d3n = new D3Node(options) // initializes D3 with container element
  const d3 = d3n.d3



  var margin = { top: 15, right: 20, bottom: 40, left: 70 },
    width = 750 - margin.left - margin.right,
    height = 230 - margin.top - margin.bottom;



  var x0 = d3.scaleBand().rangeRound([0, width]).paddingInner(0.1);
  var x1 = d3.scaleBand().padding(0.05);
  var y = d3.scaleLinear().rangeRound([height, 0]);

  var xAxis = d3.axisBottom().scale(x0)
    .tickValues(groupData.map(d => d.key));

  var yAxis = d3.axisLeft().scale(y);

  const color = d3.scaleOrdinal(["#abb6d8", "#fc9495"]);

  var svg = d3n.createSVG()
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  var categoriesNames = groupData.map(function (d) { return d.key; });
  var rateNames = groupData[0].values.map(function (d) { return d.grpName; });

  x0.domain(categoriesNames);
  x1.domain(rateNames).rangeRound([0, x0.bandwidth()]);
  y.domain([0, d3.max(groupData, function (key) { return d3.max(key.values, function (d) { return d.grpValue; }); })]);

  svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis).selectAll("text")
    .attr("y", 0)
    .attr("x", 9)
    .attr("dy", ".35em")
    .attr("transform", "rotate(45)")
    .style("text-anchor", "start");


  svg.append("g")
    .attr("class", "y axis")
    .style('opacity', '1')
    .call(yAxis)
    .append("text")
    .attr("transform", "rotate(-90)")
    .attr("y", 6)
    .attr("dy", ".71em")
    .style("text-anchor", "end")
    .style('font-weight', 'bold')
    .text("Amount");

  svg.select('.y').transition().duration(500).delay(1300).style('opacity', '1');
  var slice = svg.selectAll(".slice")
    .data(groupData)
    .enter().append("g")
    .attr("class", "g")
    .attr("transform", function (d) { return "translate(" + x0(d.key) + ",0)"; })

  slice.selectAll("rect")
    .data(function (d) { return d.values; })
    .enter().append("rect")
    .attr("width", x1.bandwidth())
    .attr("x", function (d) { return x1(d.grpName); })
    .style("fill", function (d) { return color(d.grpName) })
    .attr("y", function (d) { return y(d.grpValue); })
    .attr("height", function (d) { return height - y(d.grpValue); })
    .on("mouseover", function (d) {
      d3.select(this).style("fill", d3.rgb(color(d.grpName)).darker(2));
    })
    .on("mouseout", function (d) {
      d3.select(this).style("fill", color(d.grpName));
    });


  slice.selectAll("rect")
    .transition()
    .delay(function (d) { return Math.random() * 1000; })
    .duration(1000)
    .attr("y", function (d) { return y(d.grpValue); })
    .attr("height", function (d) { return height - y(d.grpValue); });

  //Legend
  var legend = svg.selectAll(".legend")
    .data(groupData[0].values.map(function (d) { return d.grpName; }).reverse())
    .enter().append("g")
    .attr("class", "legend")
    .attr("transform", function (d, i) { return "translate(0," + i * 20 + ")"; })
    .style("opacity", "1");

  legend.append("rect")
    .attr("x", width - 18)
    .attr("width", 18)
    .attr("height", 18)
    .style("fill", function (d) { return color(d); });

  legend.append("text")
    .attr("x", width - 24)
    .attr("y", 9)
    .attr("dy", ".35em")
    .style("text-anchor", "end")
    .text(function (d) { return d; });

  return d3n.html();
}



module.exports.BarChart = function (content, data) {
  const options = { selector: '#BarChart', container: content }
  const d3n = new D3Node(options) // initializes D3 with container element
  const d3 = d3n.d3
  // set the dimensions and margins of the graph
  var margin = { top: 20, right: 20, bottom: 30, left: 40 },
    width = 460 - margin.left - margin.right,
    height = 300 - margin.top - margin.bottom;

  // set the ranges
  var x = d3.scaleBand()
    .range([0, width])
    .padding(0.1);
  var y = d3.scaleLinear()
    .range([height, 0]);

  // append the svg object to the body of the page
  // append a 'group' element to 'svg'
  // moves the 'group' element to the top left margin
  var svg = d3n.createSVG()
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform",
      "translate(" + margin.left + "," + margin.top + ")");

  x.domain(data.map((d) => { return d.label; }));
  y.domain([0, d3.max(data, (d) => { return d.income; })]);
  svg.selectAll(".bar")
    .data(data)
    .enter().append("rect")
    .attr("class", "bar")
    .attr("x", function (d) { return x(d.label); })
    .attr("width", x.bandwidth())
    .attr("y", function (d) { return y(d.income); })
    .attr("height", function (d) { return height - y(d.income); });

  // add the x Axis
  svg.append("g")
    .attr("transform", "translate(0," + height + ")")
    .call(d3.axisBottom(x));

  // add the y Axis
  svg.append("g")
    .call(d3.axisLeft(y));

  return d3n.html();
}

module.exports.PieChart = function (content, data, selector) {
  const options = { selector: selector, container: content }
  const d3n = new D3Node(options) // initializes D3 with container element
  const d3 = d3n.d3;
  const width = 160;
  const height = 160;
  const radius = Math.min(width, height) / 2;

  const svg = d3n.createSVG()
    .attr("width", width)
    .attr("height", height);

  var g = svg.append("g")
    .attr("transform", `translate(${width / 2}, ${height / 2})`);

  const color = d3.scaleOrdinal(["#a6cee3", "#1f78b4", "#b2df8a", "#33a02c", "#fb9a99", "#e31a1c", "#fdbf6f", "#ff7f00", "#cab2d6", "#6a3d9a", "#ffff99", "#b15928"]);

  const pie = d3.pie().value(d => d.value);

  const arc = d3.arc()
    .innerRadius(radius - 40)
    .outerRadius(radius);
  //Generate groups
  var arcs = g.selectAll("arc")
    .data(pie(data))
    .enter()
    .append("g")
    .attr("class", "arc")

  //Draw arc paths
  arcs.append("path")
    .attr("fill", function (d, i) {
      return color(d.data.label);
    })
    .attr("d", arc);


  const l = svg.append('g')
    .attr('transform', `translate(0,${height - 100})`);

  const xl = d3.scaleBand()
    .range([0, width])
    .padding(0.1)
    .domain(data.map(d => d.label))
    ;

  // const legend = l.selectAll('.chart-legend')
  //   .data(color.domain())
  //   .enter()
  //   .append('g')
  //   .attr('class', 'chart-legend')
  //   .attr('transform', (d, i) => `translate(0,${20 * i})`)
  //   ;

  // legend.append('rect')
  //   .attr('width', 12)
  //   .attr('height', 12)
  //   .style('fill', color)
  //   ;

  // legend.append('text')
  //   .attr('x', 20)
  //   .attr('y', 10)
  //   .text(d => d)
  //   ;


  return d3n.html();
}

module.exports.HorizontalStackedBar = async function (content, data1, selector) {
  const options = { selector: selector, container: content }
  const d3n = new D3Node(options) // initializes D3 with container element
  const d3 = d3n.d3;
  var data = [];
  //var data1 = [{ "data": 3500, "label": "Unpaid" }, { "data": 5200, "label": "Over due" }, { "data": 3200, "label": "Paid" }];
  var groups = Object.keys(data1[0]);
  groups = groups.filter(function (d) {
    return d != "label";
  });
  for (let i = 0; i < groups.length; i++) {
    const element = groups[i];
    data.push({
      group: element
    });
  };
  for (let i = 0; i < data1.length; i++) {
    const d = data1[i];
    const keys = Object.keys(data1[i]);
    for (let j = 0; j < keys.length; j++) {
      const key = keys[j];
      var dataIndex = data.findIndex(function (d) {
        return d.group == key;
      });
      if (dataIndex >= 0) {
        var cunData = data[dataIndex];
        cunData[d.label] = d[key];
      }
    }

  }

  var subgroups = Object.keys(data[0]).slice(1);
  var legendWidth =
    d3.max(subgroups, function (d) {
      return d.length;
    }) * 16;

  var stackedData = d3.stack().keys(subgroups)(data);
  var subGroupsObject = subgroups.map(function (d) {
    return {
      name: d,
      disable: true
    };
  });
  var width = 350;
  var margin = {
    top: 10,
    right: 30,
    bottom: 20,
    left: 50
  };
  margin.bottom =
    30 + Math.ceil(subgroups.length / parseInt(width / legendWidth)) * 33;
  width = width - margin.left - margin.right;
  var height = 200 - margin.top - margin.bottom,
    legendh = 50, //determines the height of the legend below the chart
    padding = 40;

  var color = d3.scaleOrdinal(['orange', 'red', 'green']);

  var svg = d3n.createSVG()
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  var y = d3.scaleBand().domain(groups).range([height, 0]).padding([0.4]);
  svg
    .append("g")
    .call(d3.axisLeft(y).tickSizeOuter(2))
    .attr("class", "yAxisClass y-axis");

  var rect = null;

  subgroups = subGroupsObject
    .filter(function (d) {
      return d.disable;
    })
    .map(function (d) {
      return d.name;
    });

  groups = d3
    .map(data, function (d) {
      return d.group;
    })
    .keys();

  // Add X axis
  stackedData = d3.stack().keys(subgroups)(data);

  //Calculate MAx val
  mayValue = [];

  for (let i = 0; i < data.length; i++) {
    const val = data[i];
    var sum = 0;
    var keys = Object.keys(val);
    for (let j = 0; j < keys.length; j++) {
      const d = keys[j];
      if (d != "group" && subgroups.includes(d)) {
        sum += val[d];
      }
    }
    mayValue.push(sum);
    sum = 0;
  }
  common.log(mayValue);
  var x = d3
    .scaleLinear()
    .domain([0, Math.round(d3.max(mayValue) / 10) * 10])
    .range([0, width]);
  svg
    .append("g")
    .attr("transform", "translate(0," + height + ")")
    .call(d3.axisBottom(x).ticks(6).tickSize(-height))
    .attr("class", "xAxisClass x-axis");

  // Show the bars

  rect = svg
    .append("g")
    .attr("class", "rectArea")
    .selectAll("g")
    // Enter in the stack data = loop key per key = group per group
    .data(stackedData)
    .enter()
    .append("g")
    .attr("class", function (d, i) {
      return "rectBar " + d.key.replace(/ /g, ""); //+ '_' + i
    })
    .attr("fill", function (d, i) {
      var index = subGroupsObject.findIndex(function (a) {
        return a.name == d.key;
      });
      return color(index);
    });

  rect
    .selectAll("rect")
    .data(function (d) {
      return d;
    })
    .enter()
    .append("rect")
    .attr("y", function (d) {
      common.log(y(d.data.group));
      return y(d.data.group);
    })
    .attr("x", function (d) {
      return x(d[0]);
    })
    .attr("width", function (d) {
      return x(d[1]) - x(d[0]);
    })
    .attr("height", y.bandwidth());

  var legendHAdd = 20;
  var legendXAxisVal = 0;
  var legendYAxisVal = 27 + legendHAdd;
  var legend = svg
    .append("g")
    .attr("transform", "translate(" + (padding + 12) + "," + height + ")")
    .selectAll("g")
    .data(stackedData)
    .enter()
    .append("g")
    .attr("class", function (d, i) {
      return "legendClick name_" + d.key + "_" + i;
    })
    .attr("data-id", function (d, i) {
      return d.key.replace(/ /g, ""); //+ '_' + i
    })
    .style("cursor", "pointer");

  legend
    .append("circle")
    .attr("cx", function (d, i) {
      if (i == 0) legendXAxisVal = 0;

      if ((i - legendXAxisVal + 1) * legendWidth > width) {
        legendXAxisVal = i;
      }
      var val = (i - legendXAxisVal) * legendWidth - 10;
      return val;
    })
    .attr("cy", function (d, i) {
      if (i == 0) legendXAxisVal = 0;

      if ((i - legendXAxisVal + 1) * legendWidth > width) {
        legendXAxisVal = i;
        legendYAxisVal += 25;
      }
      common.log(legendYAxisVal);
      return legendYAxisVal;
    })
    .attr("r", 6)
    //.attr('height', 12)
    .attr("fill", function (d, i) {
      return color(i);
    });

  legendYAxisVal = 21 + legendHAdd;

  legend
    .append("text")
    .text(function (d) {
      return d.key;
    })
    .attr("x", function (d, i) {
      if (i == 0) legendXAxisVal = 0;

      if ((i - legendXAxisVal + 1) * legendWidth > width) {
        legendXAxisVal = i;
      }
      var val = (i - legendXAxisVal) * legendWidth;
      return val;
    })
    .attr("y", function (d, i) {
      if (i == 0) legendXAxisVal = 0;

      if ((i - legendXAxisVal + 1) * legendWidth > width) {
        legendXAxisVal = i;
        legendYAxisVal += 25;
      }
      common.log(legendYAxisVal);
      return legendYAxisVal;
    })
    .attr("text-anchor", "start")
    .attr("font-weight", "bold")
    .attr("class", "text")
    .attr("alignment-baseline", "hanging");

  legend
    .append("text")
    //.attr("transform", "rotate(-90)")
    .attr("text-anchor", "start")
    .attr("class", "text")
    .attr("alignment-baseline", "hanging")
    .attr("y", legendYAxisVal - legendHAdd - 10)
    .attr("x", width / 2)
    .attr("dy", "1em")
    .attr("class", "xAxisText")
    .style("text-anchor", "middle")
    .text("");

  return d3n.html();
}

module.exports.MixedBarAndLineChart = async function(content,data,selector){
  const options = { selector: selector, container: content }
  const d3n = new D3Node(options) // initializes D3 with container element
  const d3 = d3n.d3;
  
  var margin = { top: 5, right: 20, bottom: 40, left: 40 };
  var width = 550 - margin.left - margin.right;
  var height = 230 - margin.top - margin.bottom;

  var x = d3.scaleBand()
      .range([0, width]).padding(0.5).paddingInner(0.5);

  var y = d3.scaleLinear()
      .range([height, margin.top]);

  var center = d3.scaleLinear()
      .range([0, width]);

  var color = d3.scaleOrdinal()
      .range(["#abb6d8", "#fc9495"]);

  var labels = ["Income", "Expenses"];

  var xAxis = d3.axisBottom(x).ticks(10);
  var yAxis = d3.axisLeft(y).ticks(10);

  var centerLine = d3.axisTop(center).ticks(0);
  var keys = d3.keys(data[0]);

  var labels = keys.filter(function (key) {
      if (key !== "key") {
          return key;
      }
  });

  data.forEach(function (d) {
      var y0_positive = 0;
      var y0_negative = 0;
      d.components = labels.map(function (key) {
          if (d[key] >= 0) {
              return { key: key, y1: y0_positive, y0: y0_positive += d[key] };
          } else if (d[key] < 0) {
              return { key: key, y0: y0_negative, y1: y0_negative += d[key] };
          }
      })
  })

  var y_min = d3.min(data, function (d) { return d.Expenses });
  var y_max = d3.max(data, function (d) { return d.Income });

  x.domain(data.map((obj)=>obj.key));
  y.domain([y_min, y_max]);
  color.domain(labels);

  var cfnai_ma3 = d3.line()
      .x(function (d) { return (x.bandwidth()/2 + x(d.key)); })
      .y(function (d) { return y(d.Income + d.Expenses); })
      .curve(d3.curveMonotoneX);

  var svg = d3n.createSVG()
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis).selectAll("text")
      .attr("y", 0)
      .attr("x", 9)
      .attr("dy", ".35em")
      .attr("transform", "rotate(35)")
      .style("text-anchor", "start");

  svg.append("g")
      .attr("class", "y axis")
      .call(yAxis);

  svg.append("g")
      .attr("class", "centerline")
      .attr("transform", "translate(0," + y(0) + ")")
      .call(centerLine);

  var entry = svg.selectAll(".entry")
      .data(data)
      .enter().append("g")
      .attr("class", "g")
      .attr("transform", (d, i)=> { return "translate(" + x(d.key) + ", 0)"; });

  entry.selectAll("rect")
      .data(function (d) { return d.components; })
      .enter().append("rect")
      .attr("width", x.bandwidth())
      .attr("y", function (d) { return y(d.y0); })
      .attr("height", function (d) { return Math.abs(y(d.y0) - y(d.y1)); })
      .style("fill", function (d) { return color(d.key); });

  var cfnai_ma3_line = svg.append("path")
      .datum(data)
      .attr("class", "line")
      .attr("d", cfnai_ma3);

  svg.selectAll(".dot")
      .data(data)
  .enter().append("circle") // Uses the enter().append() method
      .attr("class", "dot") // Assign a class for styling
      .attr("cx", function(d, i) { return (x.bandwidth()/2 + x(d.key)); })
      .attr("cy", function(d) { return y(d.Income + d.Expenses); })
      .attr("r", 5);

  var legend = svg.selectAll(".legend")
      .data(color.domain())
      .enter().append("g")
      .attr("class", "legend");
  //.attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

  legend.append("rect")
      .attr("x", 675)
      .attr("y", function (d, i) { return i * 25 + 300 })
      .attr("width", 18)
      .attr("height", 18)
      .style("fill", color);

  legend.append("text")
      .attr("x", 700)
      .attr("y", function (d, i) { return i * 25 + 309; })
      .attr("dy", ".35em")
      .style("text-anchor", "start")
      .text(function (d, i) { return labels[i]; });

  return d3n.html();
}

