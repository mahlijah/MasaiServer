var pdf = require('html-pdf');

module.exports.PDFtoBuffer = function(html,obj,callback){
    var options = { 
        "format": "A4",        // allowed units: A3, A4, A5, Legal, Letter, Tabloid
        "orientation": "landscape", // portrait or landscape
        paginationOffset: 1,       // Override the initial pagination number
        "header":{
            "height":"10px",
            "contents":''
        },
        "footer": {
            "height": "28px",
            "contents": {
                //first: 'Cover page',
                //2: 'Second page', // Any page number is working. 1-based index
                default: '<div style="text-align: center; float: left; width: 90%;"><span>© 2018-2021 Fork Freight. All Rights Reserved.</span></div>', // fallback value
                last: '<div style="font-size:10px; "><span style="float:left"></span> <span style="float: right;"> <span style="color: #444;">Page {{page}}</span> of <span>{{pages}}</span></span></div>'
            }
        },
    };
    pdf.create(html,options).toBuffer(callback);
}