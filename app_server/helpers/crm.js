const config =  require('../config/config');
const common = require('./common');
const https = require('https');
const querystring = require('querystring'); //built in module for parsing query params 

/**
 * request config host and port and method
 */
 let options = {
    host: process.env.CRM_URL || "baclus.myfreshworks.com",
    path: '',
    method: '',
    headers: {
    //   'Accept': 'application/json',
      'Authorization': `Token token=${config.SALES_CRM_ACCESS_KEY}`,
      'Content-Type': 'application/json',
    //   'Access-Control-Allow-Origin': '*'
    }
  };

  /**
   * predefined paths (CRM_endpoints)
   */
  const CRM_ENDPOINTS={
      create_contact: '/crm/sales/api/contacts',
      search_contact: '/crm/sales/api/filtered_search/contact',
      update_contact: '/crm/sales/api/contacts/'
  }

/**
 * add contacts to CRM
 */
const add_or_modify_crm_contact=async(contact_info)=>{
    try {
    
        let {first_name,last_name,email,phone,
          address,role,subscription_type,
          subscription,created_at,payment_status,signup_complete="true"}= contact_info;
        if(payment_status)
        crm_model["contact"]["custom_field"]["cf_payment_status"]= payment_status

        let crm_model={
            "contact":{
            first_name,
            last_name,
            email,
            "job_title":role,
            "mobile_number": phone,
            address,
            created_at,
            "custom_field": {
                "cf_subscription_type": subscription_type,
                "cf_is_active": `${subscription}`,
                "cf_signup_complete": signup_complete
              }
        }};

      // console.log("updated_crm model:__", crm_model);
        /**
         * let search the user first if not found in CRM add else update
         */
        let {status, data:contact_found}= await find_contact(email);
        console.log("search response in add contact:_____", contact_found,"\n______________________***");
        if(status && contact_found.data?.contacts.length>0){
            // console.log("user founded:___")
            let {id:contact_id}= contact_found.data.contacts[0];
            // let contact_updated= 
            await edit_contact(crm_model,contact_id)
            // console.log("contact_updated :_______",contact_updated,"\n___________________");

        }else{

        // let res=
         await http_call("POST",crm_model,CRM_ENDPOINTS.create_contact);
        //  console.log("response:__________", res)
    }

        return {
            status: true
        };
    } catch (error) {
        console.error("add_or_modify_crm_contact ERROR:_____", error.message);
        return {status: false, err:error.message};
    }
}

/**
 * delete from CRM
 */
const delete_contact= async(user_id)=>{
    try {
        /**
         * for now we don't delete it from crm
         * will be implemented when there is need to delete from CRM
         */
    } catch (error) {
        console.error("Delete_Contact ERROR:____", error.message);
    }
}

/**
 * update user info for user in CRM
 */
const edit_contact= async(contact_info,contact_id)=>{
    try {
       let upd_resp= await http_call('PUT',contact_info,CRM_ENDPOINTS.update_contact + contact_id);
       return {status: true , message: upd_resp}
    } catch (error) {
        console.error("Update Contact ERROR:_____", error.message)
        return {status: false, message: error.message}
    }
}

/**
 * view contacts list
 */
const crm_contacts= async()=>{
    try{

    }catch(error){
        console.log("ERROR while viewing CRM CONTACTS:____", error.message);
      return {status: false , error}
    }
}


/**
 * 
**/
const find_contact= async(email)=>{
    try{ 
        let filter_query={
             "filter_rule" : [{
                 "attribute" : "contact_email.email",
                  "operator":"is_in",
                   "value": email
                }] 
            }
     let resp= await http_call("POST",filter_query,CRM_ENDPOINTS.search_contact);
     return {status: true, data:resp}
    }catch(error){
      console.log("Error in search contact:____",error);
      return {status: false, err: error.message}
    }
}

/** 
 * common method for request either post/get/put or delete
 */
const http_call = async (req_method,data,path_url=null,params=null)=>{
    return new Promise((resolve,reject)=>{
        let resp_data = '';
        options.method= req_method;
        //set path if not null
        if(path_url)
         options.path= path_url;

        //  console.log("options send to request",options)
        const request = https.request(options, async(res) => {
        if (res.statusCode !== 200) {
          console.error(`Did not get an OK from the server. Code: ${res.statusCode}`);
          res.resume();
          reject({status: false,message:`request failed with statusCode ${res.statusCode}`});
        }
      
       res.on('data', (chunk) => {
          resp_data += chunk;
        });
      
        res.on('close', () => {
          console.log('Updated data');
        //   console.log(JSON.parse(resp_data));
          resolve({status:true,data:JSON.parse(resp_data)})
        });
      });
      
     //sending body in request
      if(["POST","PUT"].includes(req_method)){
         request.write(JSON.stringify(data));
      }

      request.end();
      request.on('error', (err) => {
        console.error(`Encountered an error trying to make a request: ${err.message}`);
        reject({status: false,message:err.message});
      });
})

}

module.exports={
    add_or_modify_crm_contact,
    find_contact  
}