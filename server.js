var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cors = require('cors');
var url = require('url');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
const xmlparser = require("express-xml-bodyparser");
const zip = require("express-easy-zip");
var passport = require('passport');
var config = require('./app_server/config/config');
var jwt = require('jsonwebtoken');
require('./app_server/models/database');
require('./app_server/config/passport');
const common = require('./app_server/helpers/common');



// Bring in the routes for the API (delete the default routes)
var routesApi = require('./app_server/routes/index');
var ctrlLoads = require('./app_server/controllers/loads');
var ctrlTrucks = require('./app_server/controllers/trucks');
var ctrlDATLoads = require('./app_server/controllers/datload');
var ctrlAccount = require('./app_server/controllers/accountinfo');
var ctrlTransactions = require('./app_server/controllers/transaction');
var ctrlAuth = require('./app_server/controllers/authentication');
var redisQueueHelper = require('./app_server/helpers/redisQueues');
var NotificationService = require('./app_server/services/notificationService');
const ctrlActivityLogs = require('./app_server/controllers/userActivityLog');


var app = express();

const expressSwagger = require('express-swagger-generator')(app);
var https = require('https');
// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use("/public",express.static(path.join(__dirname, '/public')));
app.use(express.static(path.join(__dirname, '/uploads')));

app.use(cors({ credentials: true, "origin": "*", "methods": "GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS", "preflightContinue": false, "optionsSuccessStatus": 200 }));
app.use(function (req, res, next) {

    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Origin", "*");

    next();
});
app.use(zip());
app.use(logger('dev'));
app.use(bodyParser.json({
    // Because Stripe needs the raw body, we compute it but only when hitting the Stripe callback URL.
    verify: function (req, res, buf) {
        var url = req.originalUrl;
        if (url.startsWith('/api/stripePaymentComplete')) {
            req.rawBody = buf.toString()
        }
    },
    limit: '50mb'
})
);

app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
app.use(xmlparser());
app.use(cookieParser());

// Initialise Passport before using the route middleware
app.use(passport.initialize());

app.set('etag', false); // turn off

let options = {
    swaggerDefinition: {
        info: {
            description: 'This is a sample server',
            title: 'Swagger',
            version: '1.0.0',
        },

        basePath: '/',
        produces: [
            "application/json",
            "application/xml"
        ],
        schemes: ['http', 'https'],
        securityDefinitions: {
            JWT: {
                type: 'apiKey',
                in: 'header',
                name: 'Authorization',
                description: "",
            }
        }
    },
    basedir: __dirname, //app absolute path
    files: ['./swagger/**/*.js'] //Path to the API handle folder
};
expressSwagger(options)
app.use('/api', routesApi);
// Catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// // Error handlers
// // Catch unauthorised errors
// app.use(function (err, req, res, next) {
//     if (err.name === 'UnauthorizedError') {
//         res.status(401);
//         res.json({ "message": err.name + ": " + err.message });
//     }
// });

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.json({
            message: err.message,
            error: err
        });
    });
}

// production error handler 
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.json({
        message: err.message,
        error: {}
    });
});

 
// process.on('uncaughtException', (error)  => {
   
//     common.log('Error on application crash: ',  error);

//     process.exit(1); // exit application 

// })

// process.on('unhandledRejection', (error, promise) => {
//     common.log('handle a promise rejection: ', promise);
//     common.log('The error was: ', error );
//   });

var server = app.listen(config.port, () => {
    common.log('server running on port ' + config.port);
});
//initialize notification service
NotificationService.init();

//var server = require('http').Server(app);
var io = require('socket.io')(server, {
    cors: {
        origin: "*"
    },
    pingInterval: 60000
});
const redis = require('socket.io-redis');
const REDIS_URL = config.notification.redisUri;
const redis_uri = url.parse(REDIS_URL);
const redisOptions = REDIS_URL.includes("rediss://")
    ? {
        port: Number(redis_uri.port),
        host: redis_uri.hostname,
        password: redis_uri.auth.split(":")[1],
        maxRetriesPerRequest: 0,
        tls: {
            rejectUnauthorized: false,
            servername: redis_uri.hostname
        },
        enableReadyCheck: false
    }
    : REDIS_URL;


io.adapter(redis(redisOptions));
//     ,{
//     tls: {
//         rejectUnauthorized: false,
//       }
// }
//io.listen(config.port);

io.use(function (socket, next) {
    if (socket.handshake.query != null && socket.handshake.query.token != null && socket.handshake.query.token != 'undefined') {
        //common.log("token : ",socket.handshake.query.token);
        jwt.verify(socket.handshake.query.token, config.secretKey, function (err, decoded) {

            if (err) {

                common.log("auth error: ", err);
                return next(new Error('UnauthorizedError'));
            }
            socket.decoded = decoded;
            next();
        });
    } else {
        next(new Error('UnauthorizedError'));
    }
}).on('connection', (socket) => {
    common.log('socket connected...');
    socket.on('disconnect', (reason) => {
     common.log('socket disconnected...' + reason);
    });
    //server is listening for add_transaction events
    socket.on('add_transaction', function (data) {
        ctrlTransactions.AddTransaction(data, socket); //save transaction
    });

    //server is listening for get transaction events
    //by default returning only transactions for the logged in user
    //todo: accept search criteria
    socket.on('get_transaction', function (data) {
        ctrlTransactions.getTransaction(data, socket);
    });

    // socket.on('edit_truck', function (data) {
    //     ctrlTrucks.editTruck(data, socket);
    // });

    // socket.on('refresh_truck_age', function (data) {
    //     ctrlTrucks.truckRefreshAge(data, socket);
    // })

    // socket.on('close_truck', function (data) {
    //     ctrlTrucks.closeTruck(data, socket);
    // });

    socket.on('activate_truck', function (data) {
        ctrlTrucks.activateTruck(data, socket);
    });

    socket.on('my_trucks', function () {
        ctrlTrucks.myTrucks(socket, function (trucks) {
            if (trucks.length > 0) {
                trucks.forEach(function (truck) {
                    //common.log(JSON.stringify(truck));
                    socket.emit('my_trucks_response', { type: 'new', data: truck }); // RT 
                });
            } else {
                socket.emit('my_trucks_response', { success: false, message: "No truck found" }); // RT 
            }

        });

    });

    socket.on('trucks', function (data) {
        common.log('find trucks via socket');
        ctrlTrucks.findTrucks(data, socket, function (trucks) {
            if (trucks && trucks !== 'null' && trucks !== 'undefined' && trucks.length > 0) {

                trucks.forEach(function (truck) {
                    socket.emit('trucks_response', { type: 'new', data: truck }); // RT 
                });

            }
            /** else
                {
                    ctrlDATTrucks.findtrucks(data, socket,io,function(trucks){
                        
                        if( trucks && trucks !== 'null' && trucks !== 'undefined') {
    
                            trucks.forEach(function(truck) {
                                common.log(truck);
                                //socket.emit('loads_response', {type: 'new', data:load}); // RT 
                            });
    
                        }
                        
                    });
                } **/

        });

    });


    // socket.on('add_load', function (data) {

    //     ctrlLoads.postLoad(data, socket);
    // });
    socket.on('account_info', function (data) {
        ctrlAccount.accountInfo(data, socket);
    });
    // socket.on('edit_load', function (data) {

    //     ctrlLoads.editLoad(data, socket);
    // });

    // socket.on('refresh_load_age', function (data) {
    //     ctrlLoads.LoadRefreshAge(data, socket);
    // })



    socket.on('loads', function (data) {
        // common.log('load search via socket', data);
        
        ctrlLoads.findLoads(data, socket, function (searchItem) {
            //common.log("load returned");
            if (searchItem != null && searchItem._id != null) {
                let room = searchItem._id.toString();
                socket.join(room, () => {
                    let rooms = Object.keys(socket.rooms);
                    common.log(rooms); // [ <socket.id>, 'room 237' ]
                    io.to(room).emit('a new user has joined the room'); // broadcast to everyone in the room
                });
            }
        });

    });

    socket.on('dispatcherloadslimited', function (data) {
        
        ctrlLoads.findLoadsLimitedList(data, socket, function (searchItem) {
            common.log("loads returned");
        });

    });



  

    socket.on('accept_bid', function (load_id, bid_user_id) {
        ctrlLoads.acceptBid(load_id, bid_user_id, socket);
    });

    socket.on('post_bid', function (load_id, bid) {
        ctrlLoads.postBid(load_id, bid, socket);
    });

    socket.on('add_to_watchlist', function (data) {
        
        ctrlLoads.addToWatchlist(data, socket);
    });

    socket.on('remove_from_watchlist', function (data) {
        ctrlLoads.removeFromWatchlist(data, socket);
    });


    socket.on('factor_load', function (data) {
        ctrlLoads.factorLoad(data, socket);
    });

    socket.on('accept_agreement', function (data) {
        ctrlLoads.acceptAgreement(data, socket);
    });

    socket.on('watchlist', function () {

        ctrlLoads.watchlist(socket, function (loads) {

            loads.forEach(function (load) {
                socket.emit('watchlist_response', { type: 'new', data: load }); // RT 
            });

        });

    });

    socket.on('bidswon', function () {

        ctrlLoads.bidsWon(socket, function (loads) {

            loads.forEach(function (load) {
                socket.emit('bidswon_response', { type: 'new', data: load }); // RT 
            });

        });

    });
    // Truck Watch LIst
    socket.on('add_to_watchlist_truck', function (data) {
        ctrlTrucks.addToWatchlist(data, socket);
    });

    socket.on('remove_from_watchlist_truck', function (data) {
        ctrlTrucks.removeFromWatchlist(data, socket);
    });
    socket.on('watchlist_truck', function () {

        ctrlTrucks.watchlist(socket, function (trucks) {

            trucks.forEach(function (truck) {
                socket.emit('watchlist_truck_response', { type: 'new', data: truck }); // RT 
            });
        });
    });


    socket.on('hit_map_data', async (type) => {
        common.log(type)
        let heatmap = await ctrlDATLoads.Heatmapdata(type);
        common.log('heatmap data:', heatmap);
        socket.emit('hit_map_data_response', { data: heatmap }); // RT             
    });

    socket.on('broadcastNewLoad', async (loaddata) => {
        common.log("broadcast new load to rooms");
        //broadcast to room 
        //socket.emit('newLoadForSearch', {data: loaddata.load, searchId : loaddata.room});
        //let room =loaddata.room.toString();
        //io.to(loaddata.room).emit('newLoadForSearch', { data: loaddata.load, searchId: loaddata.room });
        loaddata.room.forEach((singleroom) => {
            socket.to(singleroom).emit('newLoadForSearch', { data: loaddata.load, searchId: singleroom });
        })
        //socket.disconnect(true);           
    });


    socket.on('searchList', function (data) {
        //common.log('get searchlist', data);

        ctrlLoads.getSearchListws(data, socket, function (searchList) {
            common.log("load returned");
            if (searchList != null && searchList.length > 0) {
                for (const searchItem of searchList) {
                    const room = searchItem._id.toString();

                    socket.join(room, () => {
                        let rooms = Object.keys(socket.rooms);
                        common.log(rooms);
                        io.to(room).emit('a new user has joined the room'); // broadcast to everyone in the room
                    });
                }
            }
        });

    });

    socket.on('deleteSearch', function (data) {
        //common.log('delete search', data);

        ctrlLoads.removeSearchItemws(data, socket, function (searchList) {

            if (data != null) {
                const room = data._id.toString();

                socket.leave(room, () => {
                    let rooms = Object.keys(socket.rooms);
                    common.log(rooms);
                    io.to(room).emit('a user has left the room'); // broadcast to everyone in the room
                });
            }
        });

    });

    /**
     * socket for activity logs
     * 
     */
    socket.on('activity_logs',async(data)=>{
    let resp= await ctrlActivityLogs.userLogDateEvents_socket(data,socket);
      console.log("socket function response :___", resp);
      socket.emit('activity_logs_response', resp);
      socket.disconnect();
    })

    socket.on('rejoin_rooms', (data) => {
        if (data.length > 0) {
            for (let index = 0; index < data.length; index++) {
                const roomId = data[index];
                socket.join(roomId);
                socket.to(roomId).emit('a user has rejoin the room');
            }
        }
    })


});

redisQueueHelper.init(io);

//   //sticky.listen() will return false if Master  
//   if (!sticky.listen(server, config.port)) { 
//     // Master code
//     server.once('listening', function() {
//       common.log('server started on port: ' + config.port);
//     });

//     if (cluster.isMaster) {
//         common.log('Master server started on port '+ config.port);
//       } 

//   } else {
//     common.log('- Child server started on port '+ config.port+' case worker id='+ cluster.worker.id);
// }
setInterval(() => io.emit('time', new Date().toTimeString()), 1000);

module.exports = app;
