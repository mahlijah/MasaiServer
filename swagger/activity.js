
/**
 * @typedef UserActivityModel
 * @property {string} user_id 
 * @property {string} log_date  
 * @property {number} limit.required
 * @property {number} offset.required 
*/



/**
 * @route GET /api/userlogs
 * @group userActivity
 * @param {string} user_id.path.required
 * @param {string} start_date.path.required
 * @param {string} end_date.path.required
 * @param {number} limit.path.required
 * @param {number} offset.path.required
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */





/**
 * @route POST /api/logevents
 * @group userActivity
 * @param {UserActivityModel.model} UserActivityModel.body.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

