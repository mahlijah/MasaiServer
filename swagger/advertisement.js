/**
 * @typedef advertisementFilterModel
 * @property {number} skip.required 
 * @property {number} limit.required 
 * @property {string} sort_by.required 
 * @property {string} sort_direction.required 
 */

/**
 * @route POST /api/getAllAdvertisements
 * @group Advertisement
 * @param {advertisementFilterModel.model} advertisementFilterModel.body.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @typedef advertisementModel
 * @property {string} title.required 
 * @property {file} file.required 
 * @property {boolean} is_active 
 * @property {expire_at} expire_at 
 */

/**
 * @route POST /api/addAdvertisement
 * @group Advertisement 
 * @param {file} file.formData.required
 * @param {string} title.formData.required
 * @param {string} value.formData
 * @param {string} type.formData
 * @param {string} description.formData
 * @param {string} action.formData
 * @param {string} external_link.formData
 * @param {Date} start_date.formData
 * @param {Date} end_date.formData
 * @param {boolean} is_active.formData
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route PUT /api/editAdvertisement/{id}
 * @group Advertisement
 * @param {string} id.path.required
 * @param {file} file.formData
 * @param {string} title.formData
 * @param {string} value.formData
 * @param {string} type.formData
 * @param {string} description.formData
 * @param {string} action.formData
 * @param {string} external_link.formData
 * @param {Date} start_date.formData
 * @param {Date} end_date.formData
 * @param {boolean} is_active.formData
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route DELETE /api/deleteAdvertisement/{id}
 * @group Advertisement
 * @param {string} id.path.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */


/**
 * @route GET /api/getAdvertisements
 * @group Advertisement
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */