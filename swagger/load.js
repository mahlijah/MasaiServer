
/**
 * @typedef findLoadModel
 * @property {number} limit.required
 * @property {number} skip.required 
 * @property {string} sort_by.required 
 * @property {string} sort_direction.required 
 * @property {string} pickup_address 
 * @property {string} pickup_states
 * @property {number} pickup_city_radius
 * @property {decimal} pickup_lat   
 * @property {decimal} pickup_long   
 * @property {string} delivery_address   
 * @property {string} delivery_states
 * @property {number} delivery_city_radius
 * @property {decimal} delivery_lat   
 * @property {decimal} delivery_long   
 * @property {string} start_date   
 * @property {string} end_date    
 * @property {string} equipment_type  
 * @property {number} load_weight  
 * @property {number} load_length  
 */

/**
 * @typedef PostLoadModel
 * @property {array.<string>} delivery_addresses.required 
 * @property {array.<string>} delivery_cities.required
 * @property {string} delivery_date.required
 * @property {array.<string>} delivery_lat_long   
 * @property {array.<string>} delivery_states.required
 * @property {array.<string>} delivery_zips
 * @property {string} description
 * @property {string} equipment_type.required
 * @property {number} load_weight.true  
 * @property {number} load_length.true  
 * @property {number} no_of_count.true  
 * @property {array.<string>} pickup_addresses.required  
 * @property {array.<string>} pickup_cities.required
 * @property {string} pickup_date   
 * @property {array.<string>} pickup_lat_long    
 * @property {array.<string>} pickup_states.required
 * @property {array.<string>} pickup_zips
 * @property {string} post_to
 * @property {string} ready_date
 * @property {string} target_rate       
 */




 /**
 * @typedef GetLoadModelByUserId
 * @property {string} userId.required
 * @property {number} limit
 * @property {number} skip
 * @property {string} sort_by  
 * @property {string} sort_direction 
 * @property {boolean} reset  
*/


/**
 * @typedef CloseLoadModel
 * @property {Boolean} is_opened
        
 */

/**
 * @typedef deleteSearchModel
 * @property {string} _id
 */

/**
 * @typedef getHeatMapDataModel
 * @property {any} equipment
 */


/**
 * @typedef findLoadsForLocationModel
 * @property {decimal} lat
 * @property {decimal} long
 * @property {number} radius
 */

/**
 * @route GET /api/loginLoadsfind
 * @group Load
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */

 /**
 * @route POST /api/findLoads
 * @group Load
 * @param {findLoadModel.model} findLoadModel.body.required
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

 /**
 * @route POST /api/realTimeFindLoads
 * @group Load
 * @param {findLoadModel.model} findLoadModel.body.required
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

 /**
 * @route GET /api/findLoadDetail/{id}
 * @group Load
 * @param {string} id.path.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

 /**
 * @route POST /api/deleteSearch
 * @group Load
 * @param {deleteSearchModel.model} deleteSearchModel.body.required
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

 /**
 * @route POST /api/getSearchList
 * @group Load
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

 /**
 * @route POST /api/watchListLoads
 * @group Load
 * @param {findLoadModel.model} findLoadModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

 /**
 * @route POST /api/getHeatMapData
 * @group Load
 * @param {getHeatMapDataModel.model} getHeatMapDataModel.body.required
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

 /**
 * @route POST /api/findLoadsForLocation
 * @group Load
 * @param {findLoadsForLocationModel.model} findLoadsForLocationModel.body.required
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */


  /**
 * @route POST /api/add_load
 * @group  Load
 * @param {PostLoadModel.model} PostTrucksModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

 


 /**
 * @route PUT /api/edit_load/{id}
 * @group Load
 * @param {string} id.path.required - id
 * @param {PostLoadModel.model} PostTrucksModel.body.required
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */


 /**
 * @route PUT /api/load_refresh_age/{id}
 * @group Load
 * @param {string} id.path.required - id
 * @param {PostLoadModel.model} PostTrucksModel.body.required
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route PUT /api/close_load/{id}
 * @group Load
 * @param {string} id.path.required - id
 * @param {CloseLoadModel.model} CloseLoadModel.body.required
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */




/**
 * @route GET /api/detail_load/{id}
 * @group Load
 * @param {string} id.path.required - id
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */


/**
 * @route GET /api/addToLoadWatchList/{id}
 * @group Load
 * @param {string} id.path.required - id
 * @returns {object} 200 - An object of addWatchList info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route DELETE /api/removeToLoadWatchList/{id}
 * @group Load
 * @param {string} id.path.required - id
 * @returns {object} 200 - An object of addWatchList info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route POST /api/upload_loads
 * @group Load
 * @param {file} file.formData.required
 * @returns {object} 200 - An object of addWatchList info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @typedef PaginationModel
 * @property {number} limit.required
 * @property {number} skip.required 
 * @property {string} sort_by.required 
 * @property {string} sort_direction.required 
 */

/**
 * @route POST /api/get_upload_loads
 * @group Load
 * @param {PaginationModel.model} PaginationModel.body.required
 * @returns {object} 200 - An object of addWatchList info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route POST /api/my_load
 * @group Load
 * @param {PaginationModel.model} PaginationModel.body.required
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */


/**
 * @route GET /api/download_loads_file/{id}
 * @group Load
 * @param {string} id.path.required - id
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */


/**
 * @route POST /api/getLoadByUserId
 * @group  LoadByUserId
 * @param {GetLoadModelByUserId.model} GetLoadModelByUserId.body.required
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

