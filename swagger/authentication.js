
/**
 * @typedef loginModel
 * @property {string} email.required 
 * @property {string} device_id.required 
 * @property {string} password.required 
 */

/**
* @typedef registerModel
* @property {string} first_name.required 
* @property {string} last_name.required 
* @property {string} email.required 
* @property {string} password.required 
* @property {string} role.required 
* @property {string} company.required 
* @property {string} address.required 
* @property {string} phone.required 
* @property {string} mc_Number.required 
*/

/**
* @typedef newRegisterStepFirstModel
* @property {string} first_name.required 
* @property {string} last_name.required 
* @property {string} email.required 
* @property {string} role.required 
* @property {string} phone.required
* @property {string} company_id -if company_id is provided company other details mentioned in doc are optional else required
* @property {string} company_name
* @property {string} address
* @property {string} city
* @property {string} state
* @property {string} company_email
* @property {string} company_phone
* @property {string} mc_Number 
*/

/**
* @typedef newRegisterStepSecondModel
* @property {string} id.required 
* @property {string} mc_Number 
* @property {string} company.required 
*/

/**
* @typedef newRegisterStepThirdModel
* @property {string} id.required 
* @property {string} password.required 
*/

/**
* @typedef sendVerificationCodeModel
* @property {string} id.required 
* @property {string} phone.required 
*/

/**
* @typedef verifyPhoneCodeModel
* @property {string} id.required 
* @property {string} verifyCode.required 
*/

/**
 * @typedef forgotModel
 * @property {string} email.required 
 */

/**
 * @typedef verifyResetTokenModel
 * @property {string} reset_token.required  
 */

/**
 * @typedef resetPasswordModel
 * @property {string} reset_token.required  
 * @property {string} password.required  
 */

/**
 * @typedef activateEmailModel
 * @property {string} email.required 
 */
/**
 * @typedef activateUserModel
 * @property {string} token.required 
 */

/**
* @typedef addChildUserModel
* @property {string} first_name.required 
* @property {string} last_name.required 
* @property {string} email.required
* @property {string} phone.required
* @property {string} address.required 
* @property {boolean} is_company_admin.required 
*/

/**
* @typedef getChildUsersModel
* @property {string} first_name 
* @property {string} last_name 
* @property {string} email 
* @property {string} phone 
* @property {string} sort_by.required --i.e. first_name
* @property {string} sort_direction.required --i.e. asc 
* @property {string} skip.required  --i.e. 1
* @property {string} limit.required --i.e. 20 
*/

/**
* @typedef setPasswordModel
* @property {string} token 
* @property {string} password 
*/

/**
* @typedef checkChildUserEmailModel
* @property {string} email 
*/

/**
* @typedef checkPhoneModel
* @property {string} phone 
*/

/**
* @typedef switchChildUserCompanyModel
* @property {string} email 
* @property {string} company_id 
*/


/**
 * @route POST /api/login
 * @group Authentication
 * @param {loginModel.model} loginModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */

/**
 * @route POST /api/checkAndLogin
 * @group Authentication
 * @param {loginModel.model} loginModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */
/**
 * @route POST /api/logout
 * @group Authentication
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route POST /api/create_account
 * @group Authentication
 * @param {registerModel.model} registerModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */

/**
 * @route POST /api/registerStepFirst
 * @group Authentication
 * @param {newRegisterStepFirstModel.model} newRegisterStepFirstModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */

/**
 * @route POST /api/registerStepSecond
 * @group Authentication
 * @param {newRegisterStepSecondModel.model} newRegisterStepSecondModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */

/**
 * @route POST /api/registerStepThird
 * @group Authentication
 * @param {newRegisterStepThirdModel.model} newRegisterStepThirdModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */

/**
 * @route POST /api/forgot_password
 * @group Authentication
 * @param {forgotModel.model} forgotModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */

/**
 * @route POST /api/send_reset_password_link
 * @group Authentication
 * @param {forgotModel.model} forgotModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */

/**
 * @route POST /api/verify_reset_token
 * @group Authentication
 * @param {verifyResetTokenModel.model} verifyResetTokenModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */

/**
 * @route POST /api/reset_password
 * @group Authentication
 * @param {resetPasswordModel.model} resetPasswordModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */

/**
 * @route POST /api/activateUser
 * @group Authentication
 * @param {activateUserModel.model} activateUserModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */

/**
 * @route POST /api/sendActivationEmail
 * @group Authentication
 * @param {activateEmailModel.model} activateEmailModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */

/**
 * @route POST /api/sendVerifiationEmail
 * @group Authentication
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route POST /api/sendVerificationCode
 * @group Authentication
 * @param {sendVerificationCodeModel.model} sendVerificationCodeModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */

/**
 * @route POST /api/verifyPhoneCode
 * @group Authentication
 * @param {verifyPhoneCodeModel.model} verifyPhoneCodeModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */

/**
 * @route POST /api/addChildUser
 * @group Authentication
 * @param {addChildUserModel.model} addChildUserModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route PUT /api/editChildUser/{id}
 * @group Authentication
 * @param {string} id.params.required
 * @param {addChildUserModel.model} addChildUserModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route POST /api/getChildUsers
 * @group Authentication
 * @param {getChildUsersModel.model} getChildUsersModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route get /api/getChildUser/{id}
 * @group Authentication
 * @param {string} id.path.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route POST /api/setPassword
 * @group Authentication
 * @param {setPasswordModel.model} setPasswordModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */

/**
 * @route POST /api/getChildUserDetailByTaken
 * @group Authentication
 * @param {activateUserModel.model} activateUserModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */

/**
 * @route POST /api/checkChildUserEmail
 * @group Authentication
 * @param {checkChildUserEmailModel.model} checkChildUserEmailModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route POST /api/checkPhoneNumber
 * @group Authentication
 * @param {checkPhoneModel.model} checkPhoneModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route POST /api/switchChildUserCompany
 * @group Authentication
 * @param {switchChildUserCompanyModel.model} switchChildUserCompanyModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */


/**
* @typedef markedAsCompanyAdminModel
* @property {boolean} is_company_admin 
*/

/**
 * @route POST /api/markedAsCompanyAdmin/{id}
 * @group Authentication
 * @param {string} id.params.required
 * @param {markedAsCompanyAdminModel.model} markedAsCompanyAdminModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
/**
 * @route GET /api/my-access-list
 * @group Authentication
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
