/**
 * @typedef noteModel
 * @property {string} comment.required 
 * @property {string} user_id.required 
 */

/**
 * @route POST /api/note
 * @group Note
 * @param {noteModel.model} noteModel.body.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */