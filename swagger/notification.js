
/**
 * @typedef getHeatMapDataModel
 * @property {findLoadModel.model} searchCriteria
 */

/**
 * @typedef getNotificationsModel
 * @property {string} created_by
 */


/**
 * @route POST /api/createNotification
 * @group Notification
 * @param {getHeatMapDataModel.model} getHeatMapDataModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */

/**
 * @route POST /api/getNotifications
 * @group Notification
 * @param {getNotificationsModel.model} getNotificationsModel.body.required
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */

/**
 * @route POST /api/deleteNotification
 * @group Notification
 * @param {string} id.query.required
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */

/**
 * @route POST /api/suspendNotification
 * @group Notification
 * @param {string} id.query.required
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */

/**
 * @route POST /api/resumeNotification
 * @group Notification
 * @param {string} id.query.required
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */

/**
 * @route POST /api/updateNotification
 * @group Notification
 * @param {string} id.query.required
 * @param {findLoadModel.model} body.body.required
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */