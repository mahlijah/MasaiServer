
/**
 * @typedef foundUsFilterModel
 * @property {string} userId
 * @property {string} found_us
 */


/**
 * @route POST /api/found_us
 * @group found_us
 * @param {foundUsFilterModel.model} body.body 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */


