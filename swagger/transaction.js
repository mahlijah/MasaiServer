 /**
 * @typedef transactionFilterModel
 * @property {string} description
 * @property {number} amount
 * @property {string} transaction_category_id
 * @property {string} transactionType
 * @property {string} start_date
 * @property {string} end_date
 * @property {string} due_start_date
 * @property {string} due_end_date
 * @property {boolean} isPaid 
 * @property {number} limit
 * @property {number} skip 
 * @property {string} sort_by.required
 * @property {string} sort_direction.required
 */

 /**
 * @typedef transactionPostModel
 * @property {string} transactionType.required
 * @property {string} amount.required
 * @property {string} transactionDate
 * @property {string} transactionCategory.required
 * @property {string} transactionCategoryId
 * @property {string} description.required 
 * @property {string} dueDate
 * @property {boolean} isPaid.required 
 */

 /**
 * @typedef graphModel
 * @property {string} start_date.required
 * @property {string} end_date.required
 */

 /**
 * @typedef pdfGraphsModel
 * {
 * @property {string} showBarGraph
 * @property {string} showProfitAndLossGraph
 * @property {string} showIncomeAndExpensesGraph
 * @property {string} reportExecuteDateTime
 * @property {string} start_date.required
 * @property {string} end_date.required
 */

 /**
 * @typedef transactionCSVExportModel
 * @property {string} description
 * @property {number} amount
 * @property {string} transaction_category_id
 * @property {string} transactionType
 * @property {string} start_date
 * @property {string} end_date
 * @property {boolean} is_default
 */

/**
 * @route GET /api/defaultExpensesTransactions
 * @group Transaction
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route GET /api/transactionCategories
 * @group Transaction
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route POST /api/getTransactions
 * @group Transaction
 * @param {transactionFilterModel.model} body.body 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
/**
 * @route POST /api/transaction
 * @group Transaction
 * @param {transactionPostModel.model} body.body 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
/**
 * @route PUT /api/transaction/{id}
 * @group Transaction
 * @param {string} id.path.required 
 * @param {transactionPostModel.model} body.body 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
/**
 * @route DELETE /api/transaction/{id}
 * @group Transaction
 * @param {string} id.path.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
/**
 * @route POST /api/getGraph
 * @group Transaction
 * @param {graphModel.model} body.body 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
/**
 * @route POST /api/getPieChart
 * @group Transaction
 * @param {graphModel.model} body.body 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
/**
 * @route POST /api/getIncomeChart
 * @group Transaction
 * @param {graphModel.model} body.body 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
/**
 * @route POST /api/generatePDFReport
 * @group Transaction
 * @param {pdfGraphsModel.model} body.body 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
/**
 * @route POST /api/exportTransactionsToCSV
 * @group Transaction
 * @param {transactionCSVExportModel.model} body.body 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route POST /api/upload_transaction_document/{transactionid}
 * @group Transaction
 * @param {string} transactionid.path.required
 * @param {file} file.formData.required
 * @param {string} document_type.formData.required
 * @param {string} expire_at.formData
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route DELETE /api/upload_transaction_document/{documentid}
 * @group Transaction
 * @param {string} documentid.path.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

 /**
 * @typedef transactionDocumentFilterModel
 * @property {number} limit
 * @property {number} skip 
 * @property {string} sort_by.required
 * @property {string} sort_direction.required
 */

/**
 * @route POST /api/get_transaction_uploads
 * @group Transaction
 * @param {transactionDocumentFilterModel.model} transactionDocumentFilterModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route GET /api/get_transactions_document_zip
 * @group Transaction
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */