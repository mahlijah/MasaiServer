/**
 * @typedef factoringRequestModel
 * @property {string} email.required
 * @property {string} phone.required
 * @property {string} name
 * @property {string} mc_Number
 * @property {string} company
 * @property {string} message
 */

 /**
 * @typedef contactMessageModel
 * @property {string} email.required
 * @property {string} phone.required
 * @property {string} first_name
 * @property {string} last_name
 * @property {string} content
 */

/**
 * @route POST /api/factoringRequest
 * @group Account
 * @param {factoringRequestModel.model} factoringRequestModel.body.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */


/**
 * @route POST /api/contactMessage
 * @group Account
 * @param {contactMessageModel.model} contactMessageModel.body.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */