/**
* @typedef companiesFilterModel
* @property {string} role
* @property {number} limit
* @property {number} skip
* @property {string} sort_by.required
* @property {string} sort_direction.required
*/

/**
* This function comment is parsed by doctrine
* @route GET /api/exportCompaniesToCSV/{role}
* @group DAT DATA
* @param {string} role.path.required
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
*/

/**
* This function comment is parsed by doctrine
* @route POST /api/companies
* @group DAT DATA
* @param {companiesFilterModel.model} body.body
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/