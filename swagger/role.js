/**
 * @typedef roleModel
 * @property {string} name.required
 * @property {boolean} is_public.required
 */

/**
 * @route POST /api/role
 * @group Role
 * @param {roleModel.model} roleModel.body.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route PUT /api/role/{id}
 * @group Role
 * @param {string} id.path.required
 * @param {roleModel.model} roleModel.body.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route PUT /api/role/{id}/make_as_public/{status}
 * @group Role
 * @param {string} id.path.required
 * @param {boolean} status.path.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route DELETE /api/role/{id}
 * @group Role
 * @param {string} id.path.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route GET /api/roles
 * @group Role
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */

/**
 * @typedef rolePaginationModel
 * @property {number} skip.required --i.e 1
 * @property {number} limit.required --i.e 10
 * @property {string} sort_by.required --i.e name
 * @property {string} sort_direction.required --i.e asc
 */

/**
 * @route GET /api/get_roles_for_admin
 * @group Role
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */