/**
 * @typedef SiteModuleModel
 * @property {string} name.required
 * @property {string} frontend_key.required
 * @property {array<string>} roles.required
 */

/**
 * @typedef addPrivateFieldModel
 * @property {string} _id.required
 * @property {boolean} is_private.required
 */

/**
 * @route GET /api/get_site_module
 * @group SiteModule and SubscriptionPackages
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */


/**
 * @typedef paginationModel
 * @property {number} skip.required --i.e 1
 * @property {number} limit.required --i.e 10
 * @property {string} sort_by.required --i.e name
 * @property {string} sort_direction.required --i.e asc
 */

/**
 * @route POST /api/get_site_module_for_admin
 * @group SiteModule and SubscriptionPackages
 * @param {paginationModel.model} paginationModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */

/**
 * @route POST /api/add_site_module
 * @group SiteModule and SubscriptionPackages
 * @param {SiteModuleModel.model} SiteModuleModel.body.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route PUT /api/edit_site_module/{id}
 * @group SiteModule and SubscriptionPackages
 * @param {string} id.path.required
 * @param {SiteModuleModel.model} SiteModuleModel.body.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route DELETE /api/delete_site_module/{id}
 * @group SiteModule and SubscriptionPackages
 * @param {string} id.path.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @typedef SubscriptionPackageModel
 * @property {number} price.required
 * @property {string} price_id
 * @property {string} plan_id
 * @property {boolean} is_free.required --i.e false
 * @property {string} subscription_name
 * @property {string} role.required
 * @property {array<string>} module_access.required
 * @property {boolean} with_carrier --i.e: false
 */

/**
 * @route POST /api/add_subscription_package
 * @group SiteModule and SubscriptionPackages
 * @param {SubscriptionPackageModel.model} SubscriptionPackageModel.body.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route PUT /api/edit_subscription_package/{id}
 * @group SiteModule and SubscriptionPackages
 * @param {string} id.path.required
 * @param {SubscriptionPackageModel.model} SubscriptionPackageModel.body.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @typedef SiteModuleSubscriptionPackageModel
 * @property {string} site_module_id.required
 */

/**
 * @route PUT /api/add_module_in_subscription_package/{id}
 * @group SiteModule and SubscriptionPackages
 * @param {string} id.path.required
 * @param {SiteModuleSubscriptionPackageModel.model} SiteModuleSubscriptionPackageModel.body.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route PUT /api/remove_module_in_subscription_package/{id}
 * @group SiteModule and SubscriptionPackages
 * @param {string} id.path.required
 * @param {SiteModuleSubscriptionPackageModel.model} SiteModuleSubscriptionPackageModel.body.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route DELETE /api/delete_subscription_package/{id}
 * @group SiteModule and SubscriptionPackages
 * @param {string} id.path.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route PUT /api/activate_subscription_package/{id}
 * @group SiteModule and SubscriptionPackages
 * @param {string} id.path.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route PUT /api/deactivate_subscription_package/{id}
 * @group SiteModule and SubscriptionPackages
 * @param {string} id.path.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route POST /api/get_subscription_packages_for_Admin
 * @group SiteModule and SubscriptionPackages
 * @param {paginationModel.model} paginationModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route GET /api/get_subscription_packages/{role}
 * @group SiteModule and SubscriptionPackages
 * @param {string} role.path.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */

/**
 * @route GET /api/getModulesWithRoles/{role}
 * @group SiteModule and SubscriptionPackages
 * @param {string} role.path.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */


/**
 * @route PUT /api/addPrivateFieldSubscriptionPackage
 * @group SiteModule and SubscriptionPackages
 * @param {addPrivateFieldModel.model} addPrivateFieldModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */
