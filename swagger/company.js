/**
* @typedef companyFilterModel
* @property {string} company_name
*/

/**
* @typedef companyPostModel
* @property {string} company_name
* @property {string} address
* @property {string} city
* @property {string} state
* @property {string} email
* @property {string} phone
* @property {string} mc_number
*/

/**
* This function comment is parsed by doctrine
* @route POST /api/company
* @group Company
* @param {companyPostModel.model} companyPostModel.body
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
*/

/**
* This function comment is parsed by doctrine
* @route PUT /api/company/{id}
* @group Company
* @param {string} id.path.required
* @param {companyPostModel.model} body.body
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
*/

/**
* This function comment is parsed by doctrine
* @route GET /api/company/{id}
* @group Company
* @param {string} id.path.required
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
*/

/**
* This function comment is parsed by doctrine
* @route POST /api/findcompanies
* @group Company
* @param {companyFilterModel.model} companyFilterModel.body
* @returns {object} 200 - An array of user info
* @returns {Error}  default - Unexpected error
* @security JWT
*/

 /**
 * @typedef companyUploadDocumentFilterModel
 * @property {number} limit
 * @property {number} skip 
 * @property {string} sort_by
 * @property {string} sort_direction
 * @property {string} startDate
 * @property {string} endDate
 * @property {string} document_name
 */

/**
 * @route POST /api/get_company_uploads
 * @group Company
 * @param {companyUploadDocumentFilterModel.model} companyUploadDocumentFilterModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */


/**
 * @route POST /api/get_recipt_uploads
 * @group Company
 * @param {companyUploadDocumentFilterModel.model} companyUploadDocumentFilterModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route POST /api/upload_company_document
 * @group Company
 * @param {file} file.formData.required
 * @param {string} document_type.formData.required
 * @param {string} expire_at.formData
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route DELETE /api/upload_company_document/{id}
 * @group Company
 * @param {string} id.path.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */