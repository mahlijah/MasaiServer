/**
 * @route GET /api/exportUserActivity?user_id={user_id}
 * @group User Activity
 * @param {string} user_id.path.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */