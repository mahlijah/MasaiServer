/**
 * @route GET /api/smsTypes
 * @group SMS Template
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @typedef SearchObjectModel
 * @property {string} _id  
 */

/**


/**
 * @typedef smsTemplateFilterModel
 * @property {number} skip.required 
 * @property {number} limit.required 
 * @property {string} sort_by.required 
 * @property {string} sort_direction.required
 */

/**
 * @typedef dispatcherSmsFilterModel
 * @property {SearchObjectModel.model} data.required
 * @property {boolean} smsCheckedIn 
 * @property {boolean} emailCheckedIn 
*/

/**
 * @typedef smsTypesModel
 * @property {string} sms_type.required 
 */

/**
 * @route POST /api/getSmsTemplateList
 * @group SMS Template 
 * @param {smsTemplateFilterModel.model} smsTemplateFilterModel.body.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @typedef smsTemplateModel
 * @property {string} body.required 
 * @property {string} type 
 */

/**
 * @route POST /api/addSmsTemplate
 * @group SMS Template 
 * @param {smsTemplateModel.model} smsTemplateModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route PUT /api/editSmsTemplate/{id}
 * @group SMS Template
 * @param {string} id.path.required
 * @param {smsTemplateModel.model} smsTemplateModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */



/**
 * @route PUT /api/smsTypes/{id}
 * @group SMS Template
 * @param {string} id.path.required
 * @param {smsTypesModel.model} smsTypesModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */


/**
 * @route POST /api/smsTypes
 * @group SMS Template
 * @param {smsTypesModel.model} smsTypesModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */


/**
 * @route GET /api/smsTypes
 * @group SMS Template
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */



/**
 * @route GET /api/smsTypes/{id}
 * @group SMS Template
 * @param {string} id.path.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route DELETE /api/smsTypes/{id}
 * @group SMS Template
 * @param {string} id.path.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT 
 */

/**
 * @route POST /api/dispatcherloadsSmsAlert
 * @group SMS Template 
 * @param {dispatcherSmsFilterModel.model} dispatcherSmsFilterModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */