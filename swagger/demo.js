
/**
 * @typedef DemoVideoModel
 * @property {string} page_name.required 
 * @property {string} video_popup_title.required
 * @property {string} video_title.required
 * @property {string} video_link.required
 * @property {string} video_description.required
 * @property {array.<string>} video_instructions.required
 */

/**
 * @typedef findDemoVideoModel
 * @property {number} limit.required
 * @property {number} skip.required 
 */

/**
 * @route POST /api/addDemoVideoRecord
 * @group Demo
 * @param {DemoVideoModel.model} body.body 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */


 /**
 * @route POST /api/getAllDemoVideoRecords
 * @group Demo
 * @param {findDemoVideoModel.model} findDemoVideoModel.body.required
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */



 /**
 * @route GET /api/getOneDemoVideoRecords/{page_name}
 * @group Demo
 * @param {string} page_name.path.required
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */


 /**
 * @route PUT /api/updateDemoVideoRecord/{_id}
 * @group Demo
 * @param {string} _id.path.required
 * @param {DemoVideoModel.model} findDemoVideoModel.body.required
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */


 