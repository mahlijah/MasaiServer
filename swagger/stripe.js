/**
 * @route GET /api/stripe/plans
 * @group Stripe
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @typedef planModel
 * @property {string} name.required - Silver,Gold,Platinum -eg: Silver
 * @property {number} price.required - amount should be multiple of 100 example 69.99 * 100  --i.e. 6999
 */

/**
 * @route POST /api/stripe/plan
 * @group Stripe
 * @param {planModel.model} planModel.body.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route GET /api/stripe/coupons
 * @group Stripe
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @typedef couponModel
 * @property {string} type.required - percent or amount - eg: percent
 * @property {number} value.required - value should be multiple of 100 example 69.99 * 100 - eg: 6999
 */

/**
 * @route POST /api/stripe/coupon
 * @group Stripe
 * @param {couponModel.model} couponModel.body.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route DELETE /api/stripe/coupon/{id}
 * @group Stripe
 * @param {string} id.path.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route GET /api/stripe/promotions
 * @group Stripe
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @typedef promotionModel
 * @property {string} coupon.required - coupon id - eg: 25_percent_off
 * @property {string} code.required - eg: WELCOME
 */

/**
 * @route POST /api/stripe/promotion
 * @group Stripe
 * @param {promotionModel.model} promotionModel.body.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */