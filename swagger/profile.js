/**
 * @typedef editProfileModel
 * @property {string} first_name 
 * @property {string} last_name 
 * @property {string} phone
 * @property {string} password
 * @property {string} role
 */

/**
 * @typedef updateProfileModel
 * @property {string} first_name.required 
 * @property {string} last_name.required 
 * @property {string} company.required 
 * @property {string} address.required 
 * @property {string} phone 
 * @property {string} avatar   
 * @property {string} profile_image   
 * @property {string} password   
 * @property {string} mc_Number
 * @property {string} sign_file
 * @property {file} file 
 */

/**
 * @typedef updateSubscriptionModel
 * @property {string} stripeToken.required 
 * @property {string} email.required 
 */

/**
 * @typedef initiatePaymentModel
 * @property {string} email.required 
 */

/**
 * @typedef UserdocsModel
 * @property {string} dosc 
 */

/**
 * @typedef cancelSubscriptionModel
 * @property {string} reason 
 */


/**
 * @typedef zipDocumentModel
 * @property {array.<string>} documentIds
 * @property {string} startDate
 * @property {string} endDate   
 */
/**
 * @typedef searchDocumentModel
 * @property {string} startDate.required 
 * @property {string} endDate.required
 * @property {string} document_type.required 
 */


/**
 * @route GET /api/profile
 * @group Profile
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route GET /api/getAuthuser
 * @group Profile
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route GET /api/profile/{userID}
 * @group Profile
 * @param {string} userID.path.required - userID
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */


/**
 * @route PUT /api/update_account
 * @group Profile
 * @param {editProfileModel.model} editProfileModel.body.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route POST /api/update_subscription
 * @group Profile
 * @param {updateProfileModel.model} updateProfileModel.body.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route POST /api/upload_profile_pic
 * @group Profile
 * @param {file} file.formData.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */


/**
 * @route POST /api/upload_document
 * @group Profile
 * @param {file} file.formData.required
 * @param {string} document_type.formData.required
 * @param {string} expire_at.formData
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route DELETE /api/upload_document/{id}
 * @group Profile
 * @param {string} id.path.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route GET /api/download_user_document/{id}
 * @group Profile
 * @param {string} id.path.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route POST /api/cancel_subscription
 * @group Profile
 * @param {cancelSubscriptionModel.model} cancelSubscriptionModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route POST /api/initiatePayment
 * @group Profile
 * @param {initiatePaymentModel.model} initiatePaymentModel.body.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route POST /api/Userdocs
 * @group Profile
 * @param {UserdocsModel.model} UserdocsModel.body.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */


 /**
 * @typedef profileDocumentFilterModel
 * @property {number} limit
 * @property {number} skip 
 * @property {string} sort_by.required
 * @property {string} sort_direction.required
 */

/**
 * @route POST /api/get_profile_uploads
 * @group Profile
 * @param {profileDocumentFilterModel.model} profileDocumentFilterModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

 /**
 * @typedef updatePaymentMethodModel
 * @property {string} old_pm_id
 * @property {string} new_pm_id 
 */

/**
 * @route POST /api/updatePaymentMethod
 * @group Profile
 * @param {updatePaymentMethodModel.model} updatePaymentMethodModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */


/**
 * @route POST /api/zip_Document
 * @group Profile
 * @param {zipDocumentModel.model} zipDocumentModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */


/**
 * @route POST /api/filter_upload_document
 * @group Profile
 * @param {searchDocumentModel.model} searchDocumentModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */