/**
 * @typedef findTrucksModel
 * @property {array.<string>} pickup_states.required 
 * @property {array.<string>} delivery_states.required 
 * @property {array.<string>} equipment_types.required 
 * @property {string} start_date.required 
 * @property {string} end_date.required 
 * @property {string} limit.required
 * @property {string} skip.required
 * @property {string} sort_by.required
 * @property {string} sort_direction.required
 */ 

/**
 * @typedef GetTruckModelByUserId
 * @property {string} userId.required
 * @property {number} limit
 * @property {number} skip
 * @property {string} sort_by  
 * @property {string} sort_direction  
 * @property {boolean} reset
*/

 /**
 * @typedef stateCreateModel
 * @property {string} name.required 
 * @property {string} abbreviation.required 
 */ 

/**
 * @typedef PostTrucksModel
 * @property {string} address_type_city.required
 * @property {string} destination_bycity_address.required 
 * @property {array.<string>} destination_bycity_lat_long.required
 * @property {string} destination_bycity_range.required
 * @property {string} destination_bycity_state.required  
 * @property {array.<string>} equipment_types.required
 * @property {string} length.required 
 * @property {string} origin_bycity_address.required 
 * @property {string} origin_bycity_city.required
 * @property {array.<string>} origin_bycity_lat_long.required
 * @property {string} origin_bycity_state.required
 * @property {string} rate_per_mile.required
 * @property {string} ready_date.required
 * @property {string} ready_end.required 
 * @property {string} ready_start.required
 * @property {string} weight.required
 */ 


/**
 * @typedef CloseTrucksModel
 * @property {boolean} is_opened.required
 */ 




/**
 * @route GET /api/state/{state}
 * @group Trucks & States
 * @param {string} state.path.required - state
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
/**
 * @route GET /api/state/state/list/type/{type}
 * @group Trucks & States
 * @param {string} type.path - state
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */

 /**
 * @route POST /api/findTrucks
 * @group Trucks & States
 * @param {findTrucksModel.model} findTrucksModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */

 /**
 * @route POST /api/watchListTrucks
 * @group Trucks & States
 * @param {findTrucksModel.model} findTrucksModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

 /**
 * @route POST /api/state/create
 * @group Trucks & States
 * @param {stateCreateModel.model} stateCreateModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */

 /**
 * @route POST /api/add_truck
 * @group Trucks & States
 * @param {PostTrucksModel.model} PostTrucksModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

 /**
 * @route PUT /api/edit_truck/{id}
 * @group Trucks & States
 * @param {string} id.path.required - id
 * @param {PostTrucksModel.model} PostTrucksModel.body.required
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */


/**
 * @route PUT /api/refresh_age/{id}
 * @group Trucks & States
 * @param {string} id.path.required - id
 * @param {PostTrucksModel.model} PostTrucksModel.body.required
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */


/**
 * @route PUT /api/close_truck/{id}
 * @group Trucks & States
 * @param {string} id.path.required - id
 * @param {CloseTrucksModel.model} PostTrucksModel.body.required
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */


/**
 * @route GET /api/detail_truck/{id}
 * @group Trucks & States
 * @param {string} id.path.required - id
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @typedef PaginationModel
 * @property {number} limit.required
 * @property {number} skip.required 
 * @property {string} sort_by.required 
 * @property {string} sort_direction.required 
 */

/**
 * @route POST /api/my_truck
 * @group Trucks & States
 * @param {PaginationModel.model} PaginationModel.body.required
 * @returns {object} 200 - An object of truck info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route GET /api/addToTruckWatchList/{id}
 * @group Trucks & States
 * @param {string} id.path.required - id
 * @returns {object} 200 - An object of addWatchList info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */


/**
 * @route GET /api/removeToTruckWatchList/{id}
 * @group Trucks & States
 * @param {string} id.path.required - id
 * @returns {object} 200 - An object of addWatchList info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */



/**
 * @route POST /api/upload_trucks
 * @group Trucks & States
 * @param {file} file.formData.required
 * @returns {object} 200 - An object of addWatchList info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
 

/**
 * @route POST /api/get_upload_trucks
 * @group Trucks & States
 * @param {PaginationModel.model} PaginationModel.body.required
 * @returns {object} 200 - An object of addWatchList info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route GET /api/download_trucks_file/{id}
 * @group Trucks & States
 * @param {string} id.path.required - id
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */



 /**
 * @route POST /api/getTruckByUserId
 * @group  TruckByUserId
 * @param {GetTruckModelByUserId.model} GetTruckModelByUserId.body.required
 * @returns {array} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
