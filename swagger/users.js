/**
 * @typedef userMobileModel
 * @property {string} email.required 
 * @property {string} password.required 
 * @property {string} device_id.required  
 */


/**
 * @typedef userFilterModel
 * @property {string} status
 * @property {string} roles
 * @property {string} first_name 
 * @property {string} last_name 
 * @property {string} username
 * @property {string} email 
 * @property {string} company  
 * @property {string} mc_Number
 * @property {string} phone 
 * @property {string} businessType
 * @property {string} subscription_type
 * @property {number} limit
 * @property {number} skip 
 * @property {string} sort_by.required
 * @property {string} sort_direction.required
 */
/**
 * @typedef UserPutModel
 * @property {string} first_name.required 
 * @property {string} last_name.required 
 * @property {string} company.required 
 * @property {string} address.required 
 * @property {string} phone.required 
 * @property {string} avatar   
 * @property {string} profile_image   
 * @property {string} password   
 * @property {string} mc_Number
 * @property {string} sign_file
 * @property {file} file 
 */

/**
 * @typedef userModel
 * @property {string} _id.required
 * @property {string} subscription_type
 * @property {string} subscription_expire_on
 * @property {string} status
 * @property {string} role
 * @property {string} subscription
 * @property {string} stripe_id
 * @property {string} first_name.required 
 * @property {string} last_name.required 
 * @property {string} email.required 
 * @property {string} username 
 * @property {string} company.required 
 * @property {string} address.required 
 * @property {string} phone.required 
 * @property {string} avatar.required 
 */
/**
 * @typedef temporaryUserModel
 * @property {string} user_id.required
 * @property {string} notes.required
*/


/**
 * @typedef userCancelSubscriptionModel
 * @property {string} reason.required 
 */

// /**
//  * This function comment is parsed by doctrine
//  * @route GET /api/users
//  * @group Users
//  * @returns {array.<userModel>} 200 - An array of user info
//  * @returns {Error}  default - Unexpected error
//  * @security JWT
//  */


/**
 * @route POST /api/getUsers
 * @group Users
 * @param {userFilterModel.model} body.body 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route POST /api/getAllInternalUsers
 * @group Users
 * @param {userFilterModel.model} body.body 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route POST /api/getInCompleteUsers
 * @group Users
 * @param {userFilterModel.model} body.body 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route POST /api/getPostEveryWhereUsers
 * @group Users
 * @param {userFilterModel.model} body.body 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route GET /api/user/{id}
 * @group Users
 * @param {string} id.path.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
/**
 * @route PUT /api/user/{id}
 * @group Users
 * @param {string} id.path.required 
 * @param {UserPutModel.model} UserPutModel.body.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
/**
 * @route PUT /api/user/{id}/activate
 * @group Users
 * @param {string} id.path.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
/**
 * @route GET /api/exportUserToCSV
 * @group Users
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
/**
 * @route GET /api/exportToCSVOnTab/{tabStatus}
 * @group Users
 * @param {string} tabStatus.path.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
/**
 * @route PUT /api/user/{id}/deactivate
 * @group Users
 * @param {string} id.path.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
/**
 * @route PUT /api/user/{id}/cancel_subscription
 * @group Users
 * @param {string} id.path.required 
 * @param {userCancelSubscriptionModel.model} userCancelSubscriptionModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
/**
 * @route PUT /api/user/{id}/regenerate_password
 * @group Users
 * @param {string} id.path.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
/**
 * @route PUT /api/user/{id}/set_as_internal
 * @group Users
 * @param {string} id.path.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route PUT /api/user/{id}/unset_as_internal
 * @group Users
 * @param {string} id.path.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */


/**
 * @route GET /api/update_payment_status
 * @group Scripts
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route POST /api/addNotes
 * @group TemporaryUsers
 * @param {temporaryUserModel.model} body.body 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route GET /api/getIncompleteUserById/{_id}
 * @group TemporaryUsers
 * @param {string} _id.path.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */



 /**
 * @route POST /api/mobile_signup_step1
 * @group Authentication
 * @param {userMobileModel.model} body.body 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

 
/**
 * @route GET /api/usercsv/{status}
 * @group Scripts
 * @param {string} status.path.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route GET /api/referalcsv
 * @group Users
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */