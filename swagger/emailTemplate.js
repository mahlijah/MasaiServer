/**
 * @route GET /api/emailTypes
 * @group Email Template
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @typedef emailTemplateFilterModel
 * @property {number} skip.required 
 * @property {number} limit.required 
 * @property {string} sort_by.required 
 * @property {string} sort_direction.required 
 */


/**
 * @typedef emailTypesModel
 * @property {string} email_type.required 
 */

/**
 * @route POST /api/getEmailTemplateList
 * @group Email Template 
 * @param {emailTemplateFilterModel.model} emailTemplateFilterModel.body.required 
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @typedef emailTemplateModel
 * @property {string} subject.required 
 * @property {string} body.required 
 * @property {string} type 
 */

/**
 * @route POST /api/addEmailTemplate
 * @group Email Template 
 * @param {emailTemplateModel.model} emailTemplateModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route PUT /api/editEmailTemplate/{id}
 * @group Email Template
 * @param {string} id.path.required
 * @param {emailTemplateModel.model} emailTemplateModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */



/**
 * @route PUT /api/emailTypes/{id}
 * @group Email Template
 * @param {string} id.path.required
 * @param {emailTypesModel.model} emailTypesModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */


/**
 * @route POST /api/emailTypes
 * @group Email Template
 * @param {emailTypesModel.model} emailTypesModel.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */


/**
 * @route GET /api/emailTypes
 * @group Email Template
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */



/**
 * @route GET /api/emailTypes/{id}
 * @group Email Template
 * @param {string} id.path.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

/**
 * @route DELETE /api/emailTypes/{id}
 * @group Email Template
 * @param {string} id.path.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */

